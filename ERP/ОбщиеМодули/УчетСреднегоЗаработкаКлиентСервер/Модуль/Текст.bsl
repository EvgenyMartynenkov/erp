﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема «Учет среднего заработка».
// 
// Процедуры и функции, предназначенные для использования как на сервере, 
// так и на клиенте
////////////////////////////////////////////////////////////////////////////////

#Область СлужебныйПрограммныйИнтерфейс

// Функция выполняет расчет показателя среднедневной заработок 
//  по правилам расчета общего среднего заработка.
//
// Параметры:
//	см. УчетСреднегоЗаработкаКлиентСервер.ДанныеРасчетаСреднегоЗаработка
//
// Возвращаемое значение:
//  Средний дневной (или часовой) заработок.
//
Функция РассчитатьСреднийЗаработокОбщий(Начисления, Время, Индексации, НачалоПериода, ОкончаниеПериода, ПоЧасам, ОписаниеРасчетаДляОтпусков = Неопределено) Экспорт
				
	ДанныеДляРасчета = ДанныеДляРасчетаСреднегоЗаработка(Начисления, Время, Индексации, НачалоПериода, ОкончаниеПериода, ПоЧасам, ОписаниеРасчетаДляОтпусков); 
		
	ВсегоЗаработок 	= ИтогиПоПолю(ДанныеДляРасчета["Заработок"], "Учтено");	
	ВсегоВремя 		= ИтогиПоПолю(ДанныеДляРасчета["ОтработанноеВремя"], "Учтено");	
	
	Возврат СреднийЗаработок(ВсегоЗаработок, ВсегоВремя); 
	
КонецФункции

// Функция собирает данные для расчет показателя среднего заработка 
//  по правилам расчета общего среднего заработка.
//
// Параметры:
//	ДанныеОНачислениях
//	ДанныеОВремени
//	ДанныеОбИндексации
//  ДатаНачалаПериода - дата - начало расчетного периода, за который рассчитывается средний заработок.
//  ДатаОкончанияПериода - дата - окончание расчетного периода, за который рассчитывается средний заработок.
//  ЭтоЧасовойСреднийЗаработок - булево - флажок расчета среднего часового заработка.
//  ОписаниеРасчетаДляОтпусков - структура - необязательный, если задан содержит поля:
//		* СпособРасчетаОтпуска - ПеречислениеСсылка.СпособыРасчетаНачислений
//      * ДатаНачалаСобытия - дата.
//  ПоСтатьямФинансирования - булево - надо ли собирать данные в разрезе статей финансирования.
//
// Возвращаемое значение:
//  Структура, содержащая два массива:
//		Заработок - поля элемента массива описаны в
//		            УчетСреднегоЗаработкаКлиентСервер.СтруктураЗаработкаДляРасчетаСреднегоЗаработка.
//		ОтработанноеВремя - поля элемента массива описаны в
//		                    УчетСреднегоЗаработкаКлиентСервер.СтруктураВремениДляРасчетаСреднегоЗаработка.
//
Функция ДанныеДляРасчетаСреднегоЗаработка(Начисления, Время, Индексации, НачалоПериода, ОкончаниеПериода, ПоЧасам, ОписаниеРасчетаДляОтпусков = Неопределено, ПоСтатьямФинансирования = Ложь) Экспорт
	
	Если ОписаниеРасчетаДляОтпусков = Неопределено Тогда
		ОписаниеРасчетаДляОтпусков = ОписаниеРасчетаДляОтпусков();
	КонецЕсли;
	
	ДатаНачалаСобытия 		= ОписаниеРасчетаДляОтпусков.ДатаНачалаСобытия;
	СпособРасчетаОтпуска 	= ОписаниеРасчетаДляОтпусков.СпособРасчетаОтпуска;
	
	Заработок 			= Новый Массив;
	ОтработанноеВремя 	= Новый Массив;
			
	КоэффициентыИндексацииПоПериодам = КоэффициентыИндексацииПоПериодам(Индексации);
			
	Для Каждого ПервичнаяСтрокаВремени Из Время Цикл
		
		// Используем данные только за рассчитываемый период.
		Если ПервичнаяСтрокаВремени.Период < НачалоПериода 
			Или ПервичнаяСтрокаВремени.Период > ОкончаниеПериода Тогда
			Продолжить;
		КонецЕсли;
		
		Отбор = ОтборЭлементаВремени(ПервичнаяСтрокаВремени.Период);
		
		СтрокаОтработанноеВремя 							= ЭлементВремениПоОтбору(ОтработанноеВремя, Отбор);
		
		СтрокаОтработанноеВремя.КалендарныхДней 			= ПервичнаяСтрокаВремени.ОтработаноДнейКалендарных;
		СтрокаОтработанноеВремя.НормаКалендарныхДней 		= ЗарплатаКадрыКлиентСервер.КоличествоДнейМесяца(ПервичнаяСтрокаВремени.Период);
		
		СтрокаОтработанноеВремя.ОтработаноДней				= ПервичнаяСтрокаВремени.ОтработаноДней;
		СтрокаОтработанноеВремя.ОтработаноЧасов 			= ПервичнаяСтрокаВремени.ОтработаноЧасов;
		
		Если ПоЧасам Тогда
			СтрокаОтработанноеВремя.ОтработаноПятидневка 	= ПервичнаяСтрокаВремени.ОтработаноЧасовПятидневка;
			СтрокаОтработанноеВремя.НормаПятидневка 		= ПервичнаяСтрокаВремени.НормаЧасовПроизводственныйКалендарь;		
		Иначе 	
			СтрокаОтработанноеВремя.ОтработаноПятидневка 	= ПервичнаяСтрокаВремени.ОтработаноДнейПятидневка;
			СтрокаОтработанноеВремя.НормаПятидневка 		= ПервичнаяСтрокаВремени.НормаДнейПроизводственныйКалендарь;
		КонецЕсли;
		
		СтрокаОтработанноеВремя.ОтработаноДнейШестидневка 	= ПервичнаяСтрокаВремени.ОтработаноДнейШестидневка;
		 
		Если СпособРасчетаОтпуска = Неопределено Тогда
		
			СтрокаОтработанноеВремя.Учтено 	= ?(ПоЧасам, СтрокаОтработанноеВремя.ОтработаноЧасов, СтрокаОтработанноеВремя.ОтработаноДней);
			
		ИначеЕсли СпособРасчетаОтпуска = ПредопределенноеЗначение("Перечисление.СпособыРасчетаНачислений.ОплатаОтпускаПоКалендарнымДням") Тогда
			
			СреднемесячноеКоличествоДней 		= УчетСреднегоЗаработкаКлиентСервер.КоличествоДнейВМесяцеДляОплатыОтпусков(ДатаНачалаСобытия);
			КоличествоКалендарныхДнейВМесяце 	= СтрокаОтработанноеВремя.НормаКалендарныхДней;
			ДоляОтработанногоВремениЗаМесяц 	= СтрокаОтработанноеВремя.КалендарныхДней / КоличествоКалендарныхДнейВМесяце;
			
			СтрокаОтработанноеВремя.Учтено 		= Окр(СреднемесячноеКоличествоДней * ДоляОтработанногоВремениЗаМесяц, 2);
			
		ИначеЕсли СпособРасчетаОтпуска = ПредопределенноеЗначение("Перечисление.СпособыРасчетаНачислений.ОплатаОтпускаПоШестидневке") Тогда
			
			СтрокаОтработанноеВремя.Учтено = СтрокаОтработанноеВремя.ОтработаноДнейШестидневка;
			
		КонецЕсли;	
		
	КонецЦикла;	
		
	КоличествоМесяцевПериодаРасчета = УчетСреднегоЗаработкаКлиентСервер.КоличествоМесяцевПериодаРасчетаСреднего(НачалоПериода, ОкончаниеПериода);
	
	ДоляВремени = ДоляОтработанногоВремени(ОтработанноеВремя);
	
	ОтражатьДолюВремени = Ложь;
	
	Для Каждого СтрокаНачислений Из Начисления Цикл
		
		// Используем данные только за рассчитываемый период.
		Если СтрокаНачислений.Период < НачалоПериода 
			Или СтрокаНачислений.Период > ОкончаниеПериода Тогда
			Продолжить;
		КонецЕсли;
		
		СтатьяФинансирования = ?(ПоСтатьямФинансирования, СтрокаНачислений.СтатьяФинансирования, Неопределено);
		
		Если Не ЭтоПремияПроцентом(СтрокаНачислений.СоставнаяЧасть) 
			И Не ЭтоПремияФиксированнойСуммой(СтрокаНачислений.СоставнаяЧасть) Тогда
			// не является премией
			КоэффициентИндексации = 1;
			Если СтрокаНачислений.Индексируется 
				И КоэффициентыИндексацииПоПериодам[СтрокаНачислений.Период] <> Неопределено Тогда
				КоэффициентИндексации = КоэффициентыИндексацииПоПериодам[СтрокаНачислений.Период];
			КонецЕсли;
			
			Отбор = ОтборЭлементаЗаработка(СтрокаНачислений.Период, СтрокаНачислений.СоставнаяЧасть, КоэффициентИндексации, Неопределено, Неопределено, СтатьяФинансирования);
						
			СтрокаЗаработка 		= ЭлементЗаработкаПоОтбору(Заработок, Отбор);
			СтрокаЗаработка.Сумма 	= СтрокаЗаработка.Сумма + СтрокаНачислений.Сумма;
			СтрокаЗаработка.Учтено 	= Окр(СтрокаЗаработка.Сумма * СтрокаЗаработка.КоэффициентИндексации, 2);
			Продолжить;
		КонецЕсли;
		
		// является премией
		КоэффициентИндексации = 1;
		Если СтрокаНачислений.Индексируется Тогда
			КоэффициентИндексации = КоэффициентыИндексацииПоПериодам[СтрокаНачислений.Период];
			Если КоэффициентИндексации = Неопределено Тогда
				КоэффициентИндексации = 1;
			Иначе
				Если КоэффициентИндексации < 1 Тогда
					КоэффициентИндексации = 1;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
		
		КоличествоМесяцев = СтрокаНачислений.КоличествоМесяцев;
		Если ЭтоПремияГодовая(СтрокаНачислений.СоставнаяЧасть) Тогда
			КоличествоМесяцев = 12;
		Иначе	
			Если Не ЗначениеЗаполнено(СтрокаНачислений.КоличествоМесяцев) Тогда
				КоличествоМесяцев = 1;
			КонецЕсли;
		КонецЕсли;	
		
		КоэффициентПремии = 1;
		Если КоличествоМесяцев > КоличествоМесяцевПериодаРасчета Тогда
			КоэффициентПремии = КоличествоМесяцевПериодаРасчета / КоличествоМесяцев;
		КонецЕсли;	
		
		ДоляВремениПремии = 1;
		Если ЭтоПремияФиксированнойСуммой(СтрокаНачислений.СоставнаяЧасть) 
			Или (ЭтоПремияПроцентом(СтрокаНачислений.СоставнаяЧасть)
				И ПремияНачисленаЗаВремяНеВходящееВРасчетныйПериод(СтрокаНачислений, НачалоПериода, ОкончаниеПериода)) Тогда
			// Если премия фиксированной суммой или начислена за время не входящее в расчетный период, рассчитываем коэффициент
			// нормирования.
			ДоляВремениПремии = ДоляВремени;
		КонецЕсли;
		
		Отбор = ОтборЭлементаЗаработка(СтрокаНачислений.Период, СтрокаНачислений.СоставнаяЧасть, КоэффициентИндексации, КоличествоМесяцев, ДоляВремениПремии, СтатьяФинансирования);
	
		СтрокаПремии 				= ЭлементЗаработкаПоОтбору(Заработок, Отбор);
		СтрокаПремии.Сумма 			= СтрокаПремии.Сумма 			+ СтрокаНачислений.Сумма;
		СтрокаПремии.МесячнаяЧасть 	= СтрокаПремии.МесячнаяЧасть 	+ Окр(СтрокаНачислений.Сумма / КоличествоМесяцев, 2);
		СтрокаПремии.Учтено 		= СтрокаПремии.Учтено 			+ Окр(СтрокаНачислений.Сумма * КоэффициентПремии * ДоляВремениПремии * КоэффициентИндексации, 2);
		
	КонецЦикла;	
		
	Возврат Новый Структура("Заработок,ОтработанноеВремя", Заработок, ОтработанноеВремя);
	
КонецФункции	

// Конструктор параметра ОписаниеРасчетаДляОтпусков для метода ДанныеРасчетаСреднегоЗаработка.
//
Функция ОписаниеРасчетаДляОтпусков() Экспорт
	
	ОписаниеРасчетаДляОтпусков = Новый Структура;
	ОписаниеРасчетаДляОтпусков.Вставить("СпособРасчетаОтпуска", Неопределено);
	ОписаниеРасчетаДляОтпусков.Вставить("ДатаНачалаСобытия", '00010101');
	
	Возврат ОписаниеРасчетаДляОтпусков;
	
КонецФункции 

Функция ЭтоПремияПроцентом(СоставнаяЧастьЗаработка) Экспорт
	Возврат СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияПроцентом") 
			Или СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияГодоваяПроцентом");
КонецФункции 

Функция ЭтоПремияФиксированнойСуммой(СоставнаяЧастьЗаработка) Экспорт
	Возврат СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияФиксированнойСуммой") 
			Или СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияГодоваяФиксированнойСуммой");
КонецФункции 

Функция ЭтоПремияГодовая(СоставнаяЧастьЗаработка) Экспорт
	Возврат СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияГодоваяПроцентом") 
			Или СоставнаяЧастьЗаработка = ПредопределенноеЗначение("Перечисление.УчетНачисленийВСреднемЗаработкеОбщий.ПремияГодоваяФиксированнойСуммой");
КонецФункции 

Функция ЭтоПремия(СоставнаяЧастьЗаработка) Экспорт
	Возврат ЭтоПремияПроцентом(СоставнаяЧастьЗаработка) 
			Или ЭтоПремияФиксированнойСуммой(СоставнаяЧастьЗаработка);	
КонецФункции

// Возвращает коэффициент отработанного времени используемый для нормирования премий при расчете среднего заработка.
//
Функция ДоляОтработанногоВремени(ОтработанноеВремя) Экспорт
	
	ДоляОтработанногоВремени = 1;
	
	Норма = 0;
	Отработано = 0;
	
	Норма 		= ИтогиПоПолю(ОтработанноеВремя, "НормаПятидневка");
	Отработано 	= ИтогиПоПолю(ОтработанноеВремя, "ОтработаноПятидневка");
	
	Если Норма > 0 Тогда	
		ДоляОтработанногоВремени = Отработано/Норма;
	КонецЕсли;
	
	Возврат Окр(ДоляОтработанногоВремени, 8);
	
КонецФункции

// Возвращает рассчитанный показатель среднего заработка.
//
Функция СреднийЗаработок(Заработок, Время) Экспорт
	 Возврат ?(Время = 0, 0, Окр(Заработок / Время, 2));
КонецФункции

// Возвращает сумму значений в поле [ИмяПоля] всех элементов коллекции.
//
Функция ИтогиПоПолю(Коллекция, ИмяПоля) Экспорт
	
	ИтогиПоПолю = 0;
	
	Для каждого Элемент Из Коллекция Цикл
		ИтогиПоПолю = ИтогиПоПолю + Элемент[ИмяПоля]; 
	КонецЦикла;
	
	Возврат ИтогиПоПолю;
	
КонецФункции 

// Функция запрашивает период расчета среднего заработка для указанного начисления (или удержания).
//
Функция ПериодРасчетаОбщегоСреднегоЗаработка(ДатаНачалаСобытия, ВидРасчета = Неопределено) Экспорт
	
	ПериодРасчета = Новый СтандартныйПериод;
	
	ДлительностьПериода = 12;
	
	Если ЗначениеЗаполнено(ВидРасчета) Тогда	
		#Если Клиент Тогда
		ИнфоОВидеРасчета = ЗарплатаКадрыРасширенныйКлиентПовтИсп.ПолучитьИнформациюОВидеРасчета(ВидРасчета);
		#Иначе
		ИнфоОВидеРасчета = ЗарплатаКадрыРасширенныйПовтИсп.ПолучитьИнформациюОВидеРасчета(ВидРасчета);
		#КонецЕсли
		Если ИнфоОВидеРасчета.ПорядокОпределенияРасчетногоПериодаСреднегоЗаработка = ПредопределенноеЗначение("Перечисление.ПорядокОпределенияРасчетногоПериодаСреднегоЗаработка.ПоКолдоговору") 
			И ИнфоОВидеРасчета.КоличествоМесяцевРасчетаСреднегоЗаработка > 0 Тогда
			ДлительностьПериода = ИнфоОВидеРасчета.КоличествоМесяцевРасчетаСреднегоЗаработка;
		КонецЕсли;
	КонецЕсли;	
	
	ПериодРасчета.ДатаНачала	= НачалоМесяца(ДобавитьМесяц(ДатаНачалаСобытия, -ДлительностьПериода));
	ПериодРасчета.ДатаОкончания	= КонецМесяца(ДобавитьМесяц(ДатаНачалаСобытия, -1));
	
	Возврат ПериодРасчета;
	
КонецФункции

// Функция определяет порядок расчета общего среднего заработка, 
// актуальный на указанную дату.
//
Функция ПорядокРасчетаОбщегоСреднегоЗаработка(Период) Экспорт
	
	ПрименениеПорядкаРасчета = Новый Массив;
	ПрименениеПорядкаРасчета.Добавить(
		СтруктураПримененияПорядкаРасчета(
			ПредопределенноеЗначение("Перечисление.ПорядокРасчетаСреднегоЗаработкаОбщий.Постановление2010"), 
			Дата(2010, 1, 1)));
	
	// значение по умолчанию		
	ПорядокРасчета = ПредопределенноеЗначение("Перечисление.ПорядокРасчетаСреднегоЗаработкаОбщий.Постановление2010");
	
	ПодобратьПорядокРасчетаПоДатеПрименения(ПорядокРасчета, Период, ПрименениеПорядкаРасчета);
	
	Возврат ПорядокРасчета;
	
КонецФункции

Процедура ПодобратьПорядокРасчетаПоДатеПрименения(ПорядокРасчета, Период, ПравилаПримененияПорядкаРасчета) Экспорт
	
	// Подбирается порядок расчета, 
	// дата начала применения которого "ближе" всего к заданному периоду.
	
	Для Каждого ПрименениеПорядкаРасчета Из ПравилаПримененияПорядкаРасчета Цикл
		Если Период >= ПрименениеПорядкаРасчета.НачалоПрименения Тогда
			ПорядокРасчета = ПрименениеПорядкаРасчета.ПорядокРасчета;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Функция СтруктураПримененияПорядкаРасчета(ПорядокРасчета, НачалоПрименения) Экспорт
	
	СтруктураПрименения = Новый Структура;
	СтруктураПрименения.Вставить("ПорядокРасчета", ПорядокРасчета);
	СтруктураПрименения.Вставить("НачалоПрименения", НачалоПрименения);
	
	Возврат СтруктураПрименения;
	
КонецФункции

// Функция составляет коллекцию показателей среднего заработка.
//
// Возвращаемое значение - структура, 
//		где ключ - служебный идентификатор показателя, значение ссылка на показатель.
//
Функция ПоказателиРасчетаСреднегоЗаработка() Экспорт
	
	Показатели = Новый Структура;
	
	Показатели.Вставить("СреднийЗаработокОбщий", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты.СреднийЗаработокОбщий"));
	Показатели.Вставить("СреднедневнойЗаработок", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты.СреднедневнойЗаработок"));
	СреднечасовойЗаработок = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты.СреднечасовойЗаработок");
	Если СреднечасовойЗаработок <> Неопределено Тогда
		Показатели.Вставить("СреднечасовойЗаработок", СреднечасовойЗаработок);
	КонецЕсли;
	
	Показатели.Вставить("СреднийЗаработокФСС", ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты.СреднийЗаработокФСС"));
	
	Возврат Показатели;
	
КонецФункции

// Функция составляет коллекцию показателей общего среднего заработка.
//
// Возвращаемое значение - массив.
//
Функция ПоказателиОбщегоСреднегоЗаработка() Экспорт
	
	ИменаПоказателей = Новый Массив;
	ИменаПоказателей.Добавить("СреднийЗаработокОбщий");
	ИменаПоказателей.Добавить("СреднедневнойЗаработок");
	ИменаПоказателей.Добавить("СреднечасовойЗаработок");
	
	МассивПоказателей = Новый Массив;
	
	Для Каждого ИмяПоказателя Из ИменаПоказателей Цикл
		ПоказательСсылка = ОбщегоНазначенияКлиентСервер.ПредопределенныйЭлемент("Справочник.ПоказателиРасчетаЗарплаты." + ИмяПоказателя);
		Если ПоказательСсылка <> Неопределено Тогда
			МассивПоказателей.Добавить(ПоказательСсылка);
		КонецЕсли;
	КонецЦикла;
	
	Возврат МассивПоказателей;
	
КонецФункции

// Определяет представление команды, предназначенной для расшифровки показателя.
//
Функция ПредставлениеКомандыРасшифровки() Экспорт
	Возврат НСтр("ru = 'Подробнее'");
КонецФункции

Функция КоличествоДнейВМесяцеДляОплатыОтпусков(ДатаНачалаСобытия) Экспорт 
	
	Возврат ?(Не ЗначениеЗаполнено(ДатаНачалаСобытия), 29.4, ?(ДатаНачалаСобытия < ДатаИзмененияКоличестваДнейДляОтпускных(), 29.4, 29.3));	

КонецФункции

// Функция выполняет упаковку данных общего среднего заработка формы документа для передачи в форму редактирования.
//
Функция ПараметрыРедактированияОбщегоСреднегоЗаработкаПоДокументу() Экспорт
	
	ПараметрыРедактирования = Новый Структура(
	"ДокументСсылка,
	|Сотрудник,
	|Организация,
	|ДатаНачалаСобытия,
	|Начисление,
	|НачалоПериодаРасчета,
	|ОкончаниеПериодаРасчета,
	|ФиксПериодРасчета,
	|ПорядокРасчета,
	|СпособыРасчета,
	|ПереноситьДанныеВДругойУчетСреднегоЗаработка,
	|ЭтоСреднеЧасовойЗаработок,
	|ДокументВладелецДанныеАдрес,
	|ТолькоПросмотр,
	|ДанныеОНачислениях,
	|ДанныеОВремени,
	|ДанныеОбИндексации");
	
	ПараметрыРедактирования.ФиксПериодРасчета = Ложь;
	ПараметрыРедактирования.ПорядокРасчета = ПредопределенноеЗначение("Перечисление.ПорядокРасчетаСреднегоЗаработкаОбщий.Постановление2010");
	ПараметрыРедактирования.ПереноситьДанныеВДругойУчетСреднегоЗаработка = Истина;
	ПараметрыРедактирования.ЭтоСреднеЧасовойЗаработок = Ложь;
	ПараметрыРедактирования.ТолькоПросмотр = Ложь;
	
	Возврат ПараметрыРедактирования;
	
КонецФункции

#Область МетодыОбслуживанияФормДокументовДляРасчетаСреднегоЗаработка

Функция ТекстПредупрежденияДокументНеРассчитан() Экспорт
	ТекстПредупреждения = НСтр("ru = 'Документ не рассчитан'");
	Возврат Новый ФорматированнаяСтрока(ТекстПредупреждения);
КонецФункции

#КонецОбласти

// Функция возвращает количество месяцев между двумя датами.
//
Функция КоличествоМесяцевПериодаРасчетаСреднего(ДатаНачалаПериода, ДатаОкончанияПериода) Экспорт
	
	Возврат (Год(ДатаОкончанияПериода) - Год(ДатаНачалаПериода)) * 12 + Месяц(ДатаОкончанияПериода) - Месяц(ДатаНачалаПериода) + 1;	
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ДатыИзмененийЗаконодательства

Функция ДатаИзмененияКоличестваДнейДляОтпускных()

	Возврат '20140402'

КонецФункции

#КонецОбласти

#Область ФормированиеДанныхРасчетаСреднегоЗаработка

Функция СтруктураЗаработкаДляРасчетаСреднегоЗаработка()
	
	ТипДата = Новый ОписаниеТипов("Дата");
	ПустаяДата = ТипДата.ПривестиЗначение();
	
	ТипЧисло = Новый ОписаниеТипов("Число");
	ПустоеЧисло = ТипЧисло.ПривестиЗначение();  
	
	ТипСоставнаяЧастьЗаработка = Новый ОписаниеТипов("ПеречислениеСсылка.УчетНачисленийВСреднемЗаработкеОбщий");
	ПустаяСоставнаяЧастьЗаработка = ТипСоставнаяЧастьЗаработка.ПривестиЗначение();
	
	Заработок = Новый Структура;
	Заработок.Вставить("Месяц", 					ПустаяДата);
	Заработок.Вставить("СоставнаяЧасть", 			ПустаяСоставнаяЧастьЗаработка);
	Заработок.Вставить("Сумма", 					ПустоеЧисло);
	Заработок.Вставить("КоэффициентИндексации", 	ПустоеЧисло);
	Заработок.Вставить("КоличествоМесяцев", 		ПустоеЧисло);
	Заработок.Вставить("МесячнаяЧасть", 			ПустоеЧисло);
	Заработок.Вставить("КоэффициентНормирования", 	ПустоеЧисло);
	Заработок.Вставить("Учтено", 					ПустоеЧисло);
	Заработок.Вставить("СтатьяФинансирования", ПустаяСтатьяФинансирования());
	
	Возврат Заработок;
	
КонецФункции

Функция СтруктураВремениДляРасчетаСреднегоЗаработка()
	
	ТипДата = Новый ОписаниеТипов("Дата");
	ПустаяДата = ТипДата.ПривестиЗначение();
	
	ТипЧисло = Новый ОписаниеТипов("Число");
	ПустоеЧисло = ТипЧисло.ПривестиЗначение();  
	
	ТипСоставнаяЧастьЗаработка = Новый ОписаниеТипов("ПеречислениеСсылка.УчетНачисленийВСреднемЗаработкеОбщий");
	ПустаяСоставнаяЧастьЗаработка = ТипСоставнаяЧастьЗаработка.ПривестиЗначение();
	
	ОтработанноеВремя = Новый Структура;
	ОтработанноеВремя.Вставить("Месяц", 					ПустаяДата);
	
	ОтработанноеВремя.Вставить("КалендарныхДней", 			ПустоеЧисло);
	ОтработанноеВремя.Вставить("НормаКалендарныхДней", 		ПустоеЧисло);
	
	ОтработанноеВремя.Вставить("ОтработаноДней",			ПустоеЧисло);
	ОтработанноеВремя.Вставить("ОтработаноЧасов", 			ПустоеЧисло);
	
	ОтработанноеВремя.Вставить("ОтработаноПятидневка", 		ПустоеЧисло);
	ОтработанноеВремя.Вставить("НормаПятидневка", 			ПустоеЧисло);
	
	ОтработанноеВремя.Вставить("ОтработаноДнейШестидневка",	ПустоеЧисло);
	
	ОтработанноеВремя.Вставить("Учтено", 					ПустоеЧисло);
	
	Возврат ОтработанноеВремя;
	
КонецФункции

Функция ОписаниеТипаСтатьяФинансирования()
	Возврат Новый ОписаниеТипов("СправочникСсылка.СтатьиФинансированияЗарплата");
КонецФункции

Функция ПустаяСтатьяФинансирования()
	ОписаниеТипаСтатьяФинансирования = ОписаниеТипаСтатьяФинансирования();
	Возврат ОписаниеТипаСтатьяФинансирования.ПривестиЗначение();
КонецФункции

Функция КоэффициентыИндексацииПоПериодам(ДанныеОбИндексации)
	
	КоэффициентыИндексацииПоПериодам = Новый Соответствие;
	
	Для Каждого Индексация Из ДанныеОбИндексации Цикл
		КоэффициентыИндексацииПоПериодам.Вставить(Индексация.Период, Индексация.КоэффициентИндексации);
	КонецЦикла;
	
	Возврат КоэффициентыИндексацииПоПериодам;
	
КонецФункции

Функция ОтборЭлементаЗаработка(Месяц, СоставнаяЧасть, КоэффициентИндексации = Неопределено, КоличествоМесяцев = Неопределено, КоэффициентНормирования = Неопределено, СтатьяФинансирования = Неопределено)
	
	ОтборЭлементаЗаработка = Новый Структура;
	ОтборЭлементаЗаработка.Вставить("Месяц", Месяц);
	ОтборЭлементаЗаработка.Вставить("СоставнаяЧасть", СоставнаяЧасть);
	ОтборЭлементаЗаработка.Вставить("КоэффициентИндексации", КоэффициентИндексации);
	ОтборЭлементаЗаработка.Вставить("КоличествоМесяцев", КоличествоМесяцев);
	ОтборЭлементаЗаработка.Вставить("КоэффициентНормирования", КоэффициентНормирования);
	ОтборЭлементаЗаработка.Вставить("СтатьяФинансирования", СтатьяФинансирования);
	
	Возврат ОтборЭлементаЗаработка;
	
КонецФункции 

Функция ОтборЭлементаВремени(Месяц)
	
	ОтборЭлементаВремени = Новый Структура;
	ОтборЭлементаВремени.Вставить("Месяц", Месяц);
	
	Возврат ОтборЭлементаВремени;
	
КонецФункции 

Функция ЭлементЗаработкаПоОтбору(Коллекция, Отбор)
	
	ПодходящийЭлемент = ЭлементКоллекцииПоОтбору(Коллекция, Отбор);
	
	Если ПодходящийЭлемент = Неопределено Тогда
		ПодходящийЭлемент = СтруктураЗаработкаДляРасчетаСреднегоЗаработка();
		ЗаполнитьЭлементКоллекцииПоОтбору(ПодходящийЭлемент, Отбор);
		
		Коллекция.Добавить(ПодходящийЭлемент);
	КонецЕсли;
	
	Возврат ПодходящийЭлемент;
	
КонецФункции

Функция ЭлементВремениПоОтбору(Коллекция, Отбор)
	
	ПодходящийЭлемент = ЭлементКоллекцииПоОтбору(Коллекция, Отбор);
	
	Если ПодходящийЭлемент = Неопределено Тогда
		ПодходящийЭлемент = СтруктураВремениДляРасчетаСреднегоЗаработка();
		ЗаполнитьЭлементКоллекцииПоОтбору(ПодходящийЭлемент, Отбор);
		
		Коллекция.Добавить(ПодходящийЭлемент);
	КонецЕсли;
	
	Возврат ПодходящийЭлемент;
	
КонецФункции

Процедура ЗаполнитьЭлементКоллекцииПоОтбору(Элемент, Отбор) Экспорт
	Для каждого ЭлементОтбора Из Отбор Цикл
		Если ЭлементОтбора.Значение <> Неопределено Тогда
			Элемент[ЭлементОтбора.Ключ] = ЭлементОтбора.Значение; 
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Функция ЭлементКоллекцииПоОтбору(Коллекция, Отбор) Экспорт
	
	ПодходящийЭлемент = Неопределено;
	
	Для каждого Элемент Из Коллекция Цикл
		ЭтоПодходящийЭлемент = Истина;
		
		Для каждого ЭлементОтбора Из Отбор Цикл
			Если ЭлементОтбора.Значение <> Неопределено
				И Элемент[ЭлементОтбора.Ключ] <> ЭлементОтбора.Значение  Тогда
				ЭтоПодходящийЭлемент = Ложь;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		
		Если ЭтоПодходящийЭлемент Тогда
			ПодходящийЭлемент = Элемент;
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ПодходящийЭлемент;
	
КонецФункции

Функция ПремияНачисленаЗаВремяНеВходящееВРасчетныйПериод(СтрокаНачислений, ДатаНачалаПериода, ДатаОкончанияПериода)
	
	ПремияНачисленаЗаВремяНеВходящееВРасчетныйПериод = Ложь;
	
	Если ЗначениеЗаполнено(СтрокаНачислений.ДатаНачалаБазовогоПериода) Тогда
		Если СтрокаНачислений.ДатаНачалаБазовогоПериода < ДатаНачалаПериода 
			Или СтрокаНачислений.ДатаНачалаБазовогоПериода > ДатаОкончанияПериода Тогда
			ПремияНачисленаЗаВремяНеВходящееВРасчетныйПериод = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Возврат ПремияНачисленаЗаВремяНеВходящееВРасчетныйПериод
	
КонецФункции 

#КонецОбласти

#Область ПроцедурыИФункцииФормыКалькулятораСреднегоЗаработка

// Функция заполняет соответствие месяца и его номера по порядку от 1 до 12.
//
Функция НомераМесяцевПериодаРасчетаСреднего(НачалоПериода, ОкончаниеПериода, КоличествоМесяцев = 12) Экспорт
	
	НомераМесяцев = Новый Соответствие;
	
	ТекущийМесяц	= НачалоМесяца(ОкончаниеПериода);
	НомерМесяца		= КоличествоМесяцев;
	Пока ТекущийМесяц >= НачалоМесяца(НачалоПериода)
		И НомерМесяца >= 1 Цикл
		НомераМесяцев.Вставить(ТекущийМесяц, НомерМесяца);	
		ТекущийМесяц	= ДобавитьМесяц(ТекущийМесяц, -1);
		НомерМесяца		= НомерМесяца - 1;
	КонецЦикла;
	
	Возврат НомераМесяцев;
	
КонецФункции	

Функция СтрокаСреднегоЗаработка(СреднийЗаработок, Идентификатор) Экспорт
	
	НайденныеСтроки = СреднийЗаработок.НайтиСтроки(Новый Структура("Идентификатор", Идентификатор));
	
	Возврат ?(НайденныеСтроки.Количество() = 0, Неопределено, НайденныеСтроки[0]);
	
КонецФункции

Функция ПолучитьДанныеОВремениДляДокументаРасчетаСреднего(Сотрудник, ПорядокРасчета, СреднийЗаработок, СоответствиеИдентификаторовПолямДанныхВремени, ПоляСтруктурыДанныхОВремени, НомераМесяцев) Экспорт
	
	Если ПорядокРасчета = ПредопределенноеЗначение("Перечисление.ПорядокРасчетаСреднегоЗаработкаФСС.Постановление2011") Тогда
		Возврат Неопределено;	
	КонецЕсли;	
	
	ДанныеОВремениДляСреднего = Новый Массив;
	Для Каждого КлючИЗначениеМесяца Из НомераМесяцев Цикл 
		Месяц = КлючИЗначениеМесяца.Ключ; 
		НомерМесяца = КлючИЗначениеМесяца.Значение; 
		СтруктураДанныхОВремени = Новый Структура(ПоляСтруктурыДанныхОВремени);
		СтруктураДанныхОВремени.ДобавлятьСтроку = Ложь;
		СтруктураДанныхОВремени.Сотрудник = Сотрудник;
		Если СтруктураДанныхОВремени.Свойство("ПорядокРасчета") Тогда
			СтруктураДанныхОВремени.ПорядокРасчета = ПорядокРасчета;
		КонецЕсли;	
		СтруктураДанныхОВремени.Период = Месяц;
		Для Каждого КлючЗначение Из СоответствиеИдентификаторовПолямДанныхВремени Цикл 
			СтруктураПоиска = Новый Структура("Идентификатор", КлючЗначение.Значение);
			СтрокиДанных = СреднийЗаработок.НайтиСтроки(СтруктураПоиска);
			Если СтрокиДанных.Количество() > 0 Тогда 
				СтрокаДанныхОВремени = СтрокиДанных[0];
				
				СтруктураДанныхОВремени[КлючЗначение.Значение] = СтрокаДанныхОВремени["Сумма" + НомерМесяца];
				СтруктураДанныхОВремени["Корректировка"] = СтрокаДанныхОВремени["Корректировка" + НомерМесяца];
				
				Если СтруктураДанныхОВремени[КлючЗначение.Значение] <> 0 Или СтрокаДанныхОВремени["ПереноситьНулевуюСумму" + НомерМесяца] Тогда 
					СтруктураДанныхОВремени.ДобавлятьСтроку = Истина;
				КонецЕсли;	
			Иначе
				СтруктураДанныхОВремени[КлючЗначение.Значение] = 0;
			КонецЕсли;	
		КонецЦикла;
		ДанныеОВремениДляСреднего.Добавить(СтруктураДанныхОВремени);	
	КонецЦикла;	
	
	Возврат ДанныеОВремениДляСреднего;
	
КонецФункции

// Функция конструирует структуру, описывающую данные о начислениях 
// для расчета среднего заработка за один месяц расчетного периода.
// Поля структуры заполнены пустыми значениями соответствующих типов.
//
Функция ОписаниеСтрокиДанныхОНачисленияхОбщегоСреднегоЗаработка() Экспорт
	
	ПоляОписания = Новый Соответствие;
	ПоляОписания.Вставить("Сотрудник", Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
	ПоляОписания.Вставить("ПорядокРасчета", Новый ОписаниеТипов("ПеречислениеСсылка.ПорядокРасчетаСреднегоЗаработкаОбщий"));
	ПоляОписания.Вставить("СоставнаяЧасть", Новый ОписаниеТипов("ПеречислениеСсылка.УчетНачисленийВСреднемЗаработкеОбщий"));
	ПоляОписания.Вставить("СтатьяФинансирования", Новый ОписаниеТипов("СправочникСсылка.СтатьиФинансированияЗарплата"));
	ПоляОписания.Вставить("Период", Новый ОписаниеТипов("Дата"));
	ПоляОписания.Вставить("Индексируется", Новый ОписаниеТипов("Булево"));
	ПоляОписания.Вставить("Сумма", Новый ОписаниеТипов("Число"));
	ПоляОписания.Вставить("Корректировка", Новый ОписаниеТипов("Булево"));
	ПоляОписания.Вставить("Год", Новый ОписаниеТипов("Число"));
	ПоляОписания.Вставить("ДатаНачалаБазовогоПериода", Новый ОписаниеТипов("Дата"));
	ПоляОписания.Вставить("КоличествоМесяцев", Новый ОписаниеТипов("Число"));
	
	Описание = Новый Структура;
	Для Каждого КлючИЗначение Из ПоляОписания Цикл
		Описание.Вставить(КлючИЗначение.Ключ, КлючИЗначение.Значение.ПривестиЗначение());
	КонецЦикла;
	
	Возврат Описание;
	
КонецФункции

// Функция конструирует структуру, описывающую данные о времени 
// для расчета среднего заработка за один месяц расчетного периода.
// Поля структуры заполнены пустыми значениями соответствующих типов.
//
Функция ОписаниеСтрокиДанныхОВремениСреднегоЗаработкаФСС() Экспорт
	
	ПоляОписания = Новый Соответствие;
	ПоляОписания.Вставить("ФизическоеЛицо", Новый ОписаниеТипов("СправочникСсылка.ФизическиеЛица"));
	ПоляОписания.Вставить("Период", Новый ОписаниеТипов("Дата"));
	ПоляОписания.Вставить("ОтработаноДнейКалендарных", Новый ОписаниеТипов("Число"));
	ПоляОписания.Вставить("ДнейБолезниУходаЗаДетьми", Новый ОписаниеТипов("Число"));
	ПоляОписания.Вставить("Корректировка", Новый ОписаниеТипов("Булево"));
	
	Описание = Новый Структура;
	Для Каждого КлючИЗначение Из ПоляОписания Цикл
		Описание.Вставить(КлючИЗначение.Ключ, КлючИЗначение.Значение.ПривестиЗначение());
	КонецЦикла;
	
	Возврат Описание;
	
КонецФункции

#КонецОбласти

#КонецОбласти
