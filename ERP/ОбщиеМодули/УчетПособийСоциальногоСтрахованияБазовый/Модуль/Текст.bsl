﻿#Область СлужебныйПрограммныйИнтерфейс

// Заполняет табличные части документа "ИсходящаяСправкаОЗаработкеДляРасчетаПособий".
//
// Параметры:
//  Объект -  ДокументОбъект.ИсходящаяСправкаОЗаработкеДляРасчетаПособий
//  ПараметрыЗаполнения - см. ПараметрыЗаполненияСправкиОЗаработкеИДняхОтсутствия.
//  
// Возвращаемое значение:
//	Истина, если данные в объекте были обновлены.
//
Функция ЗаполнитьСправкуДаннымиОЗаработкеИДняхОтсутствия(Объект, ПараметрыЗаполнения) Экспорт
	
	Если ПараметрыЗаполнения.Обновление Тогда
		
		МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
		СоздатьВТДанныеОЗаработкеДляЗаполнения(МенеджерВременныхТаблиц, ПараметрыЗаполнения);
		Модифицирован = ФиксацияВторичныхДанныхВДокументах.ОбновитьВторичныеДанные(МенеджерВременныхТаблиц, Объект, "ДанныеОЗаработке", "ВТДанныеОЗаработкеДляЗаполнения");
		
	Иначе
		
		Объект.ДанныеОЗаработке.Загрузить(ДанныеОЗаработкеДляЗаполнения(ПараметрыЗаполнения));
		Модифицирован = Истина;
		
	КонецЕсли;
	
	Возврат Модифицирован; 
	
КонецФункции

// Формирует параметры для создания временных таблиц используемых для заполнения справки о заработке для расчета
// пособий.
//
// Параметры:
//  Объект - ДокументОбъект.СправкаОЗаработкеДляРасчетаПособий
//
// Возвращаемое значение:
//    Структура:
//		ГодНачало
//		ГодОкончание
//		Сотрудник
//		Организация
//      ПоВсемОП - данные по Организации или по ГоловнойОрганизации.
//      Обновление - учитывать ли зафиксированные в документе реквизиты.
//      РасчетныеГоды - отбор заполняемых лет, входящих в период между ГодНачало и ГодОкончание.
//      ОграничиватьРазмерЗаработка - применять ли ограничение базой страховых взносов.
//
Функция ПараметрыЗаполненияСправкиОЗаработкеИДняхОтсутствия(Объект = Неопределено) Экспорт
	ПараметрыЗаполненияСправки = Новый Структура("ГодНачала, ГодОкончания, Сотрудник, Организация, ПоВсемОП, Обновление, РасчетныеГоды, ОграничиватьРазмерЗаработка");
	ПараметрыЗаполненияСправки.ПоВсемОП = Ложь;
	ПараметрыЗаполненияСправки.Обновление = Ложь;
	ПараметрыЗаполненияСправки.РасчетныеГоды = Неопределено;
	ПараметрыЗаполненияСправки.ОграничиватьРазмерЗаработка = Истина;
	Возврат ПараметрыЗаполненияСправки
КонецФункции

// Возвращает дату передачи выплаты пособий в ФСС.
//
// Параметры:
//  Организация - СправочникСсылка.Организации
// 
// Возвращаемое значение:
//	Дата
//
Функция ДатаПередачиФССВыплатыПособий(Организация) Экспорт
	
	Возврат '30000101'	
	
КонецФункции

// Возвращает таблицу с данными о заработке сотрудника по годам.
//
// Параметры:
//  ПараметрыЗаполнения - Структура, состав см. в
//                        УчетПособийСоциальногоСтрахования.ПараметрыЗаполненияСправкиОЗаработкеИДняхОтсутствия.
//  
// Возвращаемое значение:
//  Таблица значений с колонками:
//		РасчетныйГод	
//		Заработок	
//			
Функция ДанныеОЗаработкеДляЗаполнения(ПараметрыЗаполнения) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТДанныеОЗаработкеДляЗаполнения(Запрос.МенеджерВременныхТаблиц, ПараметрыЗаполнения);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ДанныеОЗаработкеДляЗаполнения.РасчетныйГод,
	|	ДанныеОЗаработкеДляЗаполнения.Заработок
	|ИЗ
	|	ВТДанныеОЗаработкеДляЗаполнения КАК ДанныеОЗаработкеДляЗаполнения";
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

// Возвращает массив ссылок из ПВР Начисления, соответствующих облагаемым взносами компенсациям, возмещаемым из бюджета ФСС 
// (в частности, оплата 4-х дополнительных выходных дней для ухода за детьми инвалидами).
//
// Параметры:
//	нет
// 
// Возвращаемое значение:
//	Массив
// 
Функция НачисленияОблагаемыхВзносамиПособий() Экспорт

	Возврат Новый Массив()
	
КонецФункции

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

Процедура СоздатьВТДанныеОЗаработкеДляЗаполнения(МенеджерВременныхТаблиц, ПараметрыЗаполнения)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ФизическоеЛицо", ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ПараметрыЗаполнения.Сотрудник, "ФизическоеЛицо"));
	Запрос.УстановитьПараметр("ГоловнаяОрганизация", ЗарплатаКадрыПовтИсп.ГоловнаяОрганизация(ПараметрыЗаполнения.Организация));
	Запрос.УстановитьПараметр("ОграничиватьРазмерЗаработка", ПараметрыЗаполнения.ОграничиватьРазмерЗаработка);
	
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	&ФизическоеЛицо КАК ФизическоеЛицо,
		|	&ГоловнаяОрганизация КАК ГоловнаяОрганизация
		|ПОМЕСТИТЬ ВТФизЛицаОрганизаций";
		
	Запрос.Выполнить();
	
	УчетСтраховыхВзносов.СформироватьВТРасширенныеСведенияОДоходахИВзносах(Дата(ПараметрыЗаполнения.ГодНачала, 1, 1), КонецГода(Дата(ПараметрыЗаполнения.ГодОкончания, 1, 1)), ПараметрыЗаполнения.Организация, Запрос.МенеджерВременныхТаблиц, Истина, , , , Истина);
	
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	ГОД(СведенияОДоходах.Период) КАК РасчетныйГод,
		|	СУММА(СведенияОДоходах.БазаФСС - СведенияОДоходах.СуммаПревысившаяПределФСС) КАК Заработок
		|ПОМЕСТИТЬ ВТДанныеОЗаработкеБезОграничения
		|ИЗ
		|	ВТРасширенныеСведенияОДоходах КАК СведенияОДоходах
		|ГДЕ
		|	&ОтборПоОрганизации
		|
		|СГРУППИРОВАТЬ ПО
		|	ГОД(СведенияОДоходах.Период)
		|
		|ИМЕЮЩИЕ
		|	СУММА(СведенияОДоходах.БазаФСС - СведенияОДоходах.СуммаПревысившаяПределФСС) <> 0
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ДанныеОЗаработке.РасчетныйГод,
		|	СУММА(ВЫБОР
		|			КОГДА ДанныеОЗаработке.Заработок >= ПредельнаяВеличинаБазыСтраховыхВзносов.РазмерФСС
		|					И &ОграничиватьРазмерЗаработка
		|				ТОГДА ПредельнаяВеличинаБазыСтраховыхВзносов.РазмерФСС
		|			ИНАЧЕ ДанныеОЗаработке.Заработок
		|		КОНЕЦ) КАК Заработок
		|ПОМЕСТИТЬ ВТДанныеОЗаработкеДляЗаполнения
		|ИЗ
		|	(ВЫБРАТЬ
		|		МАКСИМУМ(ПредельнаяВеличинаБазыСтраховыхВзносов.Период) КАК Период,
		|		ДанныеОЗаработке.РасчетныйГод КАК РасчетныйГод,
		|		ДанныеОЗаработке.Заработок КАК Заработок
		|	ИЗ
		|		ВТДанныеОЗаработкеБезОграничения КАК ДанныеОЗаработке
		|			ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПредельнаяВеличинаБазыСтраховыхВзносов КАК ПредельнаяВеличинаБазыСтраховыхВзносов
		|			ПО (ДанныеОЗаработке.РасчетныйГод >= ГОД(ПредельнаяВеличинаБазыСтраховыхВзносов.Период))
		|	
		|	СГРУППИРОВАТЬ ПО
		|		ДанныеОЗаработке.РасчетныйГод,
		|		ДанныеОЗаработке.Заработок) КАК ДанныеОЗаработке
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПредельнаяВеличинаБазыСтраховыхВзносов КАК ПредельнаяВеличинаБазыСтраховыхВзносов
		|		ПО ДанныеОЗаработке.Период = ПредельнаяВеличинаБазыСтраховыхВзносов.Период
		|ГДЕ
		|	ДанныеОЗаработке.РасчетныйГод В(&РасчетныеГоды)
		|
		|СГРУППИРОВАТЬ ПО
		|	ДанныеОЗаработке.РасчетныйГод";
	
	Если ПараметрыЗаполнения.ПоВсемОП Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса,"&ОтборПоОрганизации","Истина");
	Иначе		
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса,"&ОтборПоОрганизации","СведенияОДоходах.Организация = &Организация");
		Запрос.УстановитьПараметр("Организация", ПараметрыЗаполнения.Организация);
	КонецЕсли;
	
	Если ПараметрыЗаполнения.РасчетныеГоды = Неопределено Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "ДанныеОЗаработке.РасчетныйГод В(&РасчетныеГоды)", "ИСТИНА");	
	Иначе
		Запрос.УстановитьПараметр("РасчетныеГоды", ПараметрыЗаполнения.РасчетныеГоды);
	КонецЕсли;
	
	Запрос.Текст = ТекстЗапроса; 
	
	Запрос.Выполнить();
	
КонецПроцедуры

#КонецОбласти

