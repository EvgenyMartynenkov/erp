﻿//////////////////////////////////////////////////////////////////////////////////////
//
//  Переопределение поведения подсистемы персонифицированного учёта внутри библиотеки.
// 
//////////////////////////////////////////////////////////////////////////////////////

Процедура СоздатьВТЗаписиСтажаПоДаннымУчета(МенеджерВременныхТаблиц, Организация, ОтчетныйПериод, ПараметрыПолученияДанных, ПараметрыОтбора) Экспорт
	ПерсонифицированныйУчетРасширенный.СоздатьВТЗаписиСтажаПоДаннымУчета(МенеджерВременныхТаблиц, Организация, ОтчетныйПериод, ПараметрыПолученияДанных, ПараметрыОтбора);
КонецПроцедуры	

Процедура СоздатьВТЗарегистрированныеКорректировкиСтажа(МенеджерВременныхТаблиц, Организация, ОтчетныйПериод) Экспорт
	ПерсонифицированныйУчетРасширенный.СоздатьВТЗарегистрированныеКорректировкиСтажа(МенеджерВременныхТаблиц, Организация, ОтчетныйПериод);	
КонецПроцедуры	

Функция КвартальнаяОтчетностьПерераспределятьВзносыАвтоматически() Экспорт 
	Возврат ПерсонифицированныйУчетРасширенный.КвартальнаяОтчетностьПерераспределятьВзносыАвтоматически()	
КонецФункции

Процедура КвартальнаяОтчетностьПФРДополнитьКомандыФормы(Форма) Экспорт
	ПерсонифицированныйУчетРасширенный.КвартальнаяОтчетностьПФРДополнитьКомандыФормы(Форма);
КонецПроцедуры

Процедура КвартальнаяОтчетностьПФРОбновитьДанныеФормы(Форма) Экспорт
	ПерсонифицированныйУчетРасширенный.КвартальнаяОтчетностьПФРОбновитьДанныеФормы(Форма);	
КонецПроцедуры	