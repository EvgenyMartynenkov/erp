﻿////////////////////////////////////////////////////////////////////////////////
// КадровыйУчетКлиент: методы кадрового учета, работающие на стороне клиента.
//  
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Открывает формы выбора физических лиц, работающих в организации.
//
// Параметры:
//		ЭлементФормы						- УправляемаяФорма или элемент управляемой формы, являющийся владельцем формы подбора.
//		Организация							- СправочникСсылка.Организации
//		МножественныйВыбор					- Булево
//		ФиксированныеНастройки 				- НастройкаКомпоновкиДанных (параметр, расширения динамического списка).
//		АдресСпискаПодобранныхСотрудников 	- Строка
//		ОтбиратьПоГоловнойОрганизации		- Булево, если передать Истина в список войдут сотрудники головной организации.
//
Процедура ВыбратьФизическихЛицОрганизации(ЭлементФормы, Организация, МножественныйВыбор = Истина, ФиксированныеНастройки = Неопределено, АдресСпискаПодобранныхСотрудников = "", ОтбиратьПоГоловнойОрганизации = Ложь) Экспорт
	
	СтруктураОтбора = Новый Структура;
	Если ОтбиратьПоГоловнойОрганизации Тогда
		СтруктураОтбора.Вставить("ГоловнаяОрганизация", Организация);
	Иначе
		СтруктураОтбора.Вставить("Организация", Организация);
	КонецЕсли;
	
	ПараметрыОткрытияФормы = Новый Структура;
	ПараметрыОткрытияФормы.Вставить("МножественныйВыбор", МножественныйВыбор);
	ПараметрыОткрытияФормы.Вставить("ЗакрыватьПриВыборе", НЕ МножественныйВыбор);
	ПараметрыОткрытияФормы.Вставить("Отбор", СтруктураОтбора);
	
	Если ФиксированныеНастройки <> Неопределено Тогда
		ПараметрыОткрытияФормы.Вставить("ФиксированныеНастройки", ФиксированныеНастройки);
	КонецЕсли; 
	Если НЕ ПустаяСтрока(АдресСпискаПодобранныхСотрудников) Тогда
		ПараметрыОткрытияФормы.Вставить("АдресСпискаПодобранныхСотрудников", АдресСпискаПодобранныхСотрудников);
	КонецЕсли; 
	
	ОткрытьФорму("Справочник.ФизическиеЛица.Форма.ФормаВыбораСотрудников", ПараметрыОткрытияФормы, ЭлементФормы);	
	
КонецПроцедуры

// Открывает формы выбора физических лиц, работающих в организации, позволяет выбрать несколько значений.
// В параметре АдресСпискаПодобранныхСотрудников передается адрес массива ссылок уже подобранных физических лиц.
// Подобранные физические лица выделяются жирным шрифтом.
//
// Параметры:
//		ЭлементФормы
//		Организация
//		АдресСпискаПодобранныхСотрудников.
//		ОтбиратьПоГоловнойОрганизации		- Булево, если передать Истина в список войдут сотрудники головной организации.
//
Процедура ПодобратьФизическихЛицОрганизации(ЭлементФормы, Организация, АдресСпискаПодобранныхСотрудников, ОтбиратьПоГоловнойОрганизации = Ложь) Экспорт
	
	ВыбратьФизическихЛицОрганизации(ЭлементФормы, Организация, Истина, , АдресСпискаПодобранныхСотрудников, ОтбиратьПоГоловнойОрганизации);
	
КонецПроцедуры

// Открывает форму подбора значений из классификатора.
//
// Параметры:
//			ИмяСправочника - Строка, имя справочника как оно задано в конфигураторе.
//  		ПолныйПутьКМакету - Строка - формат полного пути:
//								"Документ.<ИмяДокумента>.<ИмяМакета>"
//								"Обработка.<ИмяОбработки>.<ИмяМакета>"
//								"ОбщийМакет.<ИмяМакета>".
//			Форма
//			СоответствиеПолей - Соответствие, задает соответствие имен реквизитов справочника\
//							именам полей классификатора.
//
Процедура ОткрытьФормуПодбораИзКлассификатора(ИмяСправочника, ПолныйПутьКМакету, Заголовок, Форма, СоответствиеПолей = Неопределено, ОповещениеЗавершения = Неопределено) Экспорт
	
	СтруктураПараметров = Новый Структура("ИмяСправочника,ПолныйПутьКМакету,Заголовок",
		ИмяСправочника,
		ПолныйПутьКМакету,
		Заголовок);
		
	Если СоответствиеПолей <> Неопределено Тогда
		СтруктураПараметров.Вставить("СоответствиеПолей", СоответствиеПолей);
	КонецЕсли;
	
	ДополнительныеПараметры = Новый Структура("Форма, ОповещениеЗавершения", Форма, ОповещениеЗавершения);
	Оповещение = Новый ОписаниеОповещения("ОткрытьФормуПодбораИзКлассификатораЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	ОткрытьФорму("ОбщаяФорма.ЗаполнениеСправочниковИзКлассификаторов", СтруктураПараметров, Форма, , , , Оповещение, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

// Открывает форму выбора справочника Сотрудники с отбором только работающих
// сотрудников на указанную дату и в указанных, организации и подразделении.
//
// Параметры:
//		ВладелецФормыВыбора					- Форма или элемент формы, для которого открывается форма выбора.
//		Организация							- СправочникСсылка.Организации, если не указана, в отбор попадают все сотрудники компании.
//		Подразделение						- СправочникСсылка.ПодразделенияОрганизации, если не указано - в отбор попадают все сотрудники.
//		ДатаПримененияОтбора				- Дата, на которую необходимо выбрать работающих сотрудников.
//      МножественныйВыбор					- Булево, если ложь - форма откроется для выбора одного сотрудника.
//		АдресСпискаПодобранныхСотрудников	- Строка, адрес массива уже подобранных сотрудников во временном хранилище.
//		СтруктураОтбора						- Структура, аналогичная структуре отбора в параметрах формы списка.
//
Процедура ВыбратьСотрудниковРаботающихНаДату(ВладелецФормыВыбора, Организация = Неопределено, Подразделение = Неопределено, ДатаПримененияОтбора = '00010101', МножественныйВыбор = Истина, АдресСпискаПодобранныхСотрудников = "", СтруктураОтбора = Неопределено) Экспорт
	
	Если СтруктураОтбора = Неопределено Тогда
		СтруктураОтбора = Новый Структура;
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(ДатаПримененияОтбора) Тогда
		СтруктураОтбора.Вставить("ДатаПримененияОтбора", ДатаПримененияОтбора);
	КонецЕсли; 
	
	ОткрытьФормуВыбораСотрудников(ВладелецФормыВыбора, Организация, Подразделение, МножественныйВыбор, СтруктураОтбора, АдресСпискаПодобранныхСотрудников);	
	
КонецПроцедуры

// Открывает форму выбора справочника Сотрудники с отбором только работающих
// сотрудников в периоде.
//
// Параметры:
//		ВладелецФормыВыбора 				- Форма или элемент формы, для которого открывается форма выбора.
//		Организация 						- СправочникСсылка.Организации, если не указана, в отбор попадают все сотрудники компании.
//		Подразделение 						- СправочникСсылка.ПодразделенияОрганизации, если не указано - в отбор попадают все сотрудники.
//		НачалоПериодаПримененияОтбора 		- Дата, начало периода для отбора сотрудников.
//		ОкончаниеПериодаПримененияОтбора 	- Дата, окончание периода для отбора сотрудников.
//      МножественныйВыбор 					- Булево, если ложь - форма откроется для выбора одного сотрудника.
//		АдресСпискаПодобранныхСотрудников	- Строка, адрес массива уже подобранных сотрудников во временном хранилище.
//		СтруктураОтбора						- Структура, аналогичная структуре отбора в параметрах формы списка.
//
Процедура ВыбратьСотрудниковРаботающихВПериоде(ВладелецФормыВыбора, Организация = Неопределено, Подразделение = Неопределено, НачалоПериодаПримененияОтбора = '00010101', ОкончаниеПериодаПримененияОтбора = '00010101', МножественныйВыбор = Истина, АдресСпискаПодобранныхСотрудников = "", СтруктураОтбора = Неопределено) Экспорт
	
	Если СтруктураОтбора = Неопределено Тогда
		СтруктураОтбора = Новый Структура;
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(НачалоПериодаПримененияОтбора) Тогда
		СтруктураОтбора.Вставить("НачалоПериодаПримененияОтбора", НачалоПериодаПримененияОтбора);
		СтруктураОтбора.Вставить("ОкончаниеПериодаПримененияОтбора", ОкончаниеПериодаПримененияОтбора);
	КонецЕсли; 
	
	ОткрытьФормуВыбораСотрудников(ВладелецФормыВыбора, Организация, Подразделение, МножественныйВыбор, СтруктураОтбора, АдресСпискаПодобранныхСотрудников);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ОткрытьФормуВыбораСотрудников(ВладелецФормыВыбора, Организация, Подразделение, МножественныйВыбор, СтруктураОтбора, АдресСпискаПодобранныхСотрудников)
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("РежимВыбора", Истина);
	ПараметрыФормы.Вставить("МножественныйВыбор", МножественныйВыбор);
	ПараметрыФормы.Вставить("ЗакрыватьПриВыборе", НЕ МножественныйВыбор);
		
	Если ЗначениеЗаполнено(Организация) Тогда
		СтруктураОтбора.Вставить("ТекущаяОрганизация", Организация);
	КонецЕсли; 
		
	Если ЗначениеЗаполнено(Подразделение) Тогда
		СтруктураОтбора.Вставить("ТекущееПодразделение", Подразделение);
	КонецЕсли; 
	
	Если СтруктураОтбора.Количество() > 0 Тогда
		ПараметрыФормы.Вставить("Отбор", СтруктураОтбора);
	КонецЕсли;
	
	Если МножественныйВыбор И НЕ ПустаяСтрока(АдресСпискаПодобранныхСотрудников) Тогда
		ПараметрыФормы.Вставить("АдресСпискаПодобранныхСотрудников", АдресСпискаПодобранныхСотрудников);
	КонецЕсли; 
	
	ОткрытьФорму("Справочник.Сотрудники.ФормаВыбора", ПараметрыФормы, ВладелецФормыВыбора);	
	
КонецПроцедуры

// Продолжение процедуры ОткрытьФормуПодбораИзКлассификатора.
//
Процедура ОткрытьФормуПодбораИзКлассификатораЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	ДополнительныеПараметры.Форма.Элементы.Список.Обновить();
	
	Если ДополнительныеПараметры.ОповещениеЗавершения <> Неопределено Тогда
		ВыполнитьОбработкуОповещения(ДополнительныеПараметры.ОповещениеЗавершения);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОповеститьОбИзмененииРабочегоМеста(Форма) Экспорт
	
	ПараметрыОповещения = Новый Структура;
	ПараметрыОповещения.Вставить("ФизическоеЛицо", Форма.Объект.ФизическоеЛицо);
	ПараметрыОповещения.Вставить("Сотрудник", Форма.Объект.Сотрудник);
	
	Оповестить("ИзменениеДанныхМестаРаботы", ПараметрыОповещения, Форма);
	
КонецПроцедуры

Процедура ИсключитьПовторениеЗаписейТекущихТарифныхСтавокСотрудников() Экспорт
	
	ЗарплатаКадрыВызовСервера.ИсключитьПовторениеЗаписейТекущихДанныыхСотрудников("ТекущаяТарифнаяСтавкаСотрудников");
	
КонецПроцедуры

Процедура ИсключитьПовторениеЗаписейТекущихКадровыхДанныхСотрудников() Экспорт
	
	ЗарплатаКадрыВызовСервера.ИсключитьПовторениеЗаписейТекущихДанныыхСотрудников("ТекущиеКадровыеДанныеСотрудников");
	
КонецПроцедуры

#КонецОбласти
