﻿
#Область ПрограммныйИнтерфейс

// Метод для вызова из регламентного задания отражения в регл. учете
Процедура ОтразитьВсеРегламент() Экспорт
	
	ОбщегоНазначения.ПриНачалеВыполненияРегламентногоЗадания();
	
	ПартионныйУчетСервер.СформироватьДвиженияПоУчетуНДС(КонецДня(ТекущаяДата()));
	ОтразитьВсе(КонецДня(ТекущаяДата()));
	
КонецПроцедуры

// Формирует движения по регистру Хозрасчетный для всех документов, помеченных
// к отражению в регламентированном учете.
Процедура ОтразитьВсе(ПериодРасчета, Организация = Неопределено, КоличествоОтраженных = 0, КоличествоОшибок = 0) Экспорт
	ВременныеТаблицы = ВременныеТаблицы();
	
	КОтражению = КОтражению(ПериодРасчета, Организация);
	Пока КОтражению.Следующий() Цикл
		РезультатОтражения = ОтразитьДокумент(КОтражению, Ложь, ВременныеТаблицы);
		
		Отражено = (Перечисления.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете = РезультатОтражения);
		КоличествоОтраженных = КоличествоОтраженных + ?(Отражено, 1, 0);
		КоличествоОшибок = КоличествоОшибок + ?(Отражено, 0, 1);
	КонецЦикла;
	
	ВременныеТаблицы.Закрыть();
КонецПроцедуры

// Формирует движения по регистру Хозрасчетный для указанного документа.
// РеквизитыДокумента - структура {Ссылка, Дата, Организация}
Функция ОтразитьДокумент(РеквизитыДокумента, ВыполнитьПересчеты = Ложь, МенеджерВременныхТаблиц = Неопределено) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если ВыполнитьПересчеты Тогда
		ВыполнитьОффлайновыеРасчеты(КонецМесяца(РеквизитыДокумента.Дата), РеквизитыДокумента.Ссылка);
	КонецЕсли;
	Если МенеджерВременныхТаблиц = Неопределено Тогда
		ВременныеТаблицы = ВременныеТаблицы();
	Иначе
		ВременныеТаблицы = МенеджерВременныхТаблиц;
	КонецЕсли;
	
	МетаданныеСсылки = РеквизитыДокумента.Ссылка.Метаданные();
	// Выборка контекстных данных (данных из документов)
	ЭтоОбъектРасчетов = РеглУчетВыборкиСервер.ЭтоОбъектРасчетов(МетаданныеСсылки.Имя);
	ЗапросДанных = РеглУчетВыборкиСервер.ЗапросДанных(МетаданныеСсылки.Имя, ЭтоОбъектРасчетов);
	ЗапросДанных.МенеджерВременныхТаблиц = ВременныеТаблицы;
	// Выборка счетов учета прочих операций
	ЗапросПрочихСчетов = РеглУчетВыборкиСервер.ЗапросПрочихСчетов();
	ЗапросПрочихСчетов.МенеджерВременныхТаблиц = ВременныеТаблицы;
	// Выборка создаваемых движений
	ЗапросСопоставлений = РеглУчетВыборкиСервер.ЗапросСопоставлений();
	ЗапросСопоставлений.МенеджерВременныхТаблиц = ВременныеТаблицы;
	// Проверяем полученные и дополненные данные
	ЗапросПроверки = РеглУчетВыборкиСервер.ЗапросПроверки();
	ЗапросПроверки.МенеджерВременныхТаблиц = ВременныеТаблицы;
	
	// Формируем структуру параметров по-умолчанию
	СтруктураПараметров = Новый Структура;
	СтруктураПараметров.Вставить("Ссылка", РеквизитыДокумента.Ссылка);
	СтруктураПараметров.Вставить("Дата", РеквизитыДокумента.Дата);
	СтруктураПараметров.Вставить("ГраницаМесяцНачало", Новый Граница(НачалоМесяца(РеквизитыДокумента.Дата), ВидГраницы.Включая));
	СтруктураПараметров.Вставить("ГраницаМесяцОкончание", Новый Граница(КонецМесяца(РеквизитыДокумента.Дата), ВидГраницы.Включая));
	
	МассивВидовСубконто = Новый Массив;
	МассивВидовСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.ОсновныеСредства);
	МассивВидовСубконто.Добавить(ПланыВидовХарактеристик.ВидыСубконтоХозрасчетные.Контрагенты);
	СтруктураПараметров.Вставить("ВидыСубконтоХозрасчетныеОсновныеСредстваКонтрагенты", МассивВидовСубконто);
	
	// Устанавливаем параметры по-умолчанию и {&ИмяФункциональнойОпции}
	ПараметрыДанных = РеглУчетВыборкиСервер.ЗапросДанныхПараметры(МетаданныеСсылки.Имя, ЭтоОбъектРасчетов);
	Для Каждого ПараметрДанных Из ПараметрыДанных Цикл
		ЗначениеПараметра = Неопределено;
		Если Не СтруктураПараметров.Свойство(ПараметрДанных.Имя, ЗначениеПараметра) Тогда
			ЗначениеПараметра = ПолучитьФункциональнуюОпцию(ПараметрДанных.Имя);
		КонецЕсли;
		ЗапросДанных.УстановитьПараметр(ПараметрДанных.Имя, ЗначениеПараметра);
	КонецЦикла;
	ЗапросПрочихСчетов.УстановитьПараметр("Ссылка", РеквизитыДокумента.Ссылка);
	
	ЗапросПрочихСчетов.Выполнить();
	
	Блокировка = Новый БлокировкаДанных;
	ЭлементБлокировки = Блокировка.Добавить("РегистрСведений.ОтражениеДокументовВРеглУчете.НаборЗаписей");
	ЭлементБлокировки.УстановитьЗначение("Регистратор", РеквизитыДокумента.Ссылка);
	НачатьТранзакцию();
	Блокировка.Заблокировать();
	
	ЗапросДанных.Выполнить();
	ЗапросСопоставлений.Выполнить();
	
	РезультатПроверки = ЗапросПроверки.Выполнить();
	Если РезультатПроверки.Пустой() Тогда // Ошибок нет
		ЗапросХозрасчетный = РеглУчетВыборкиСервер.ЗапросХозрасчетный();
		ЗапросХозрасчетный.МенеджерВременныхТаблиц = ВременныеТаблицы;
		
		РезультатЗапроса = ЗапросХозрасчетный.Выполнить().Выгрузить();
		
		ОтразитьБухгалтерскиеПроводки(РеквизитыДокумента.Ссылка, РезультатЗапроса);
		
		Статус = Перечисления.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете;
		Комментарий = "";
		
		ДополнительнаяОбработкаПриОтраженииДокумента(РеквизитыДокумента, РезультатЗапроса);
		
	Иначе // В результате проверки есть строки с описаниями ошибок
		Статус = Перечисления.СтатусыОтраженияДокументовВРеглУчете.НеУказаныСчетаУчета;
		Комментарий = КомментарийОшибокЗаполнения(РезультатПроверки, МетаданныеСсылки);
	КонецЕсли;
	
	НаборЗаписей = РегистрыСведений.ОтражениеДокументовВРеглУчете.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Регистратор.Установить(РеквизитыДокумента.Ссылка);
	ДобавитьСтатусОтражения(НаборЗаписей, РеквизитыДокумента, Статус, Комментарий);
	НаборЗаписей.Записать();
	
	Если ПолучитьФункциональнуюОпцию("ФормироватьПроводкиМеждународногоУчетаПоДаннымРегламентированного") Тогда
		Если Статус = Перечисления.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете Тогда
			СтатусВМеждународномУчете = Перечисления.СтатусыОтраженияВМеждународномУчете.КОтражениюВУчете;
		Иначе
			СтатусВМеждународномУчете = Перечисления.СтатусыОтраженияВМеждународномУчете.ОжидаетсяОтражениеВРеглУчете;
		КонецЕсли;
		НаборЗаписей = РегистрыСведений.ОтражениеДокументовВМеждународномУчете.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.Регистратор.Установить(РеквизитыДокумента.Ссылка);
		ДобавитьСтатусОтражения(НаборЗаписей, РеквизитыДокумента, СтатусВМеждународномУчете);
		НаборЗаписей.Записать();
	КонецЕсли;
	
	Блокировка = Неопределено;
	ЗафиксироватьТранзакцию();
	
	Если МенеджерВременныхТаблиц = Неопределено Тогда
		ВременныеТаблицы.Закрыть();
	Иначе
		ЗапросОчистки = РеглУчетВыборкиСервер.ЗапросОчистки(МетаданныеСсылки.Имя);
		ЗапросОчистки.МенеджерВременныхТаблиц = ВременныеТаблицы;
		ЗапросОчистки.Выполнить();
	КонецЕсли;
	Возврат Статус;
КонецФункции

// Помечает документ-объект к отражению в регл.учете
Процедура ЗарегистрироватьКОтражению(Объект, Отказ, РежимПроведения) Экспорт
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	ДобавлятьСтатус = Истина;
	
	Управленческая = Справочники.Организации.УправленческаяОрганизация;
	Если Объект.Организация = Управленческая Тогда
		ТипОрганизация = Тип("СправочникСсылка.Организации");
		ВозможныеПоляОрганизаций =
			Новый Структура("ОрганизацияПолучатель, КонтрагентДебитор, КонтрагентКредитор, Комиссионер, Контрагент");
		ЗаполнитьЗначенияСвойств(ВозможныеПоляОрганизаций, Объект);
		
		Если ТипЗнч(Объект) = Тип("ДокументОбъект.СчетФактураВыданный") Тогда
			Если ТипЗнч(Объект.ДокументОснование) = Тип("ДокументСсылка.ПередачаТоваровМеждуОрганизациями")
				 ИЛИ ТипЗнч(Объект.ДокументОснование) = Тип("ДокументСсылка.ВозвратТоваровМеждуОрганизациями") Тогда
				ВозможныеПоляОрганизаций.ОрганизацияПолучатель = 
						ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.ДокументОснование, "ОрганизацияПолучатель");
			ИначеЕсли ТипЗнч(Объект.ДокументОснование) = Тип("ДокументСсылка.ОтчетПоКомиссииМеждуОрганизациями") Тогда
				ВозможныеПоляОрганизаций.ОрганизацияПолучатель = 
						ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Объект.ДокументОснование, "Организация");
			КонецЕсли;
		КонецЕсли;

		ДобавлятьСтатус = Ложь;
		Для Каждого Поле Из ВозможныеПоляОрганизаций Цикл
			ДобавлятьСтатус = (ТипЗнч(Поле.Значение) = ТипОрганизация)
				И ЗначениеЗаполнено(Поле.Значение) И Поле.Значение <> Управленческая;
			Если ДобавлятьСтатус Тогда
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если ДобавлятьСтатус Тогда
		ДобавитьСтатусОтражения(
			Объект.Движения.ОтражениеДокументовВРеглУчете, Объект,
			Перечисления.СтатусыОтраженияДокументовВРеглУчете.КОтражениюВРеглУчете);
		Объект.Движения.ОтражениеДокументовВРеглУчете.Записать();
	КонецЕсли;
КонецПроцедуры

// Сбрасывает для коллекции документов, зарегистрированных в ОтражениеДокументовВРеглУчете статус в КОтражениюВРеглУчете
// Если ПолеДокумент неопределено, то Коллекция трактуется как массив.
Процедура ВернутьДокументыКОтражению(Коллекция, ПолеДокумент=Неопределено) Экспорт
	Для Каждого Элемент Из Коллекция Цикл
		Ссылка = ?(Неопределено = ПолеДокумент, Элемент, Элемент[ПолеДокумент]);
		
		Набор = РегистрыСведений.ОтражениеДокументовВРеглУчете.СоздатьНаборЗаписей();
		Набор.Отбор.Регистратор.Установить(Ссылка);
		Набор.Прочитать();
		Для Каждого Запись Из Набор Цикл
			Запись.Статус = Перечисления.СтатусыОтраженияДокументовВРеглУчете.КОтражениюВРеглУчете;
		КонецЦикла;
		Если Набор.Модифицированность() Тогда
			Набор.Записывать = Истина;
			Набор.Записать();
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

// Записывает движения по регистру ПорядокОтраженияПрочихОпераций
Процедура ОтразитьПорядокОтраженияПрочихОпераций(ДополнительныеСвойства, Отказ) Экспорт
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Документ = ДополнительныеСвойства.ДляПроведения.Ссылка;
	
	// Если параметры отражения заданы в документе (списание безналичных ДС),
	// то ТаблицаПорядокОтраженияПрочихОпераций содержит готовые данные для отражения.
	Если ДополнительныеСвойства.Свойство("ПараметрыОтраженияВРеглУчете") Тогда
		
		ПараметрыОтраженияВРеглУчете = ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПорядокОтраженияПрочихОпераций;
		
	Иначе
		
		Запрос = Новый Запрос("
		|ВЫБРАТЬ
		|	Операция.Документ,
		|	Операция.ИдентификаторСтроки,
		|	Операция.Организация,
		|	Операция.Дата
		|ПОМЕСТИТЬ
		|	Операция
		|ИЗ &Таблица КАК Операция;
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	Операция.Документ,
		|	Операция.ИдентификаторСтроки,
		|	Операция.Организация,
		|   Операция.Дата,
		|	Счета.СчетУчета КАК СчетУчета,
		|	Счета.Субконто1 КАК Субконто1,
		|	Счета.Субконто2 КАК Субконто2,
		|	Счета.Субконто3 КАК Субконто3
		|ИЗ
		|	Операция
		|	ПОЛНОЕ СОЕДИНЕНИЕ РегистрСведений.ПорядокОтраженияПрочихОпераций КАК Счета
		|		ПО Счета.Документ = Операция.Документ И Счета.ИдентификаторСтроки = Операция.ИдентификаторСтроки
		|ГДЕ
		|	НЕ Операция.Документ ЕСТЬ NULL
		|");
		Запрос.УстановитьПараметр("Таблица", ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПорядокОтраженияПрочихОпераций);
		Запрос.УстановитьПараметр("Документ", Документ);
		
		ПараметрыОтраженияВРеглУчете = Запрос.Выполнить().Выгрузить();
		
	КонецЕсли;
	
	НаборЗаписей = РегистрыСведений.ПорядокОтраженияПрочихОпераций.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Документ.Установить(Документ);
	НаборЗаписей.Загрузить(ПараметрыОтраженияВРеглУчете);
	НаборЗаписей.Записывать = Истина;
	НаборЗаписей.Записать();
	
КонецПроцедуры

// Проверяет переданные документы на факт отражения в регл. учете
//
// Параметры:
//    МассивДокументов - Массив - Документы
//
// Возвращаемое значение:
//    Массив - Документы, которые не отражены в регл. учете
//
Функция ПроверитьОтражениеДокументовВРеглУчете(МассивДокументов) Экспорт
	
	НеотраженныеДокументы = Новый Массив;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ОтражениеДокументовВРеглУчете.Регистратор КАК Ссылка
	|ИЗ
	|	РегистрСведений.ОтражениеДокументовВРеглУчете КАК ОтражениеДокументовВРеглУчете
	|ГДЕ
	|	ОтражениеДокументовВРеглУчете.Регистратор В (&МассивДокументов)
	|	И НЕ ОтражениеДокументовВРеглУчете.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете)
	|	
	|ОБЪЕДИНИТЬ ВСЕ
	|	
	|ВЫБРАТЬ
	|	ОтражениеДокументовВРеглУчете.Регистратор КАК Ссылка
	|ИЗ
	|	РегистрСведений.ОтражениеДокументовВРеглУчете КАК ОтражениеДокументовВРеглУчете
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ
	|		Документ.АвансовыйОтчет.ЗакупкаЗаНаличныйРасчет КАК ЗакупкиЗаНаличныйРасчет
	|	ПО
	|		ЗакупкиЗаНаличныйРасчет.ДокументПоступления = ОтражениеДокументовВРеглУчете.Регистратор
	|		И ЗакупкиЗаНаличныйРасчет.Ссылка В (&МассивДокументов)
	|ГДЕ
	|	НЕ ОтражениеДокументовВРеглУчете.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете)
	|";
	
	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);
	
	Результат = Запрос.Выполнить();
	Если Не Результат.Пустой() Тогда
		НеотраженныеДокументы = Результат.Выгрузить().ВыгрузитьКолонку("Ссылка");
	КонецЕсли;
	
	Возврат НеотраженныеДокументы;
	
КонецФункции

// Формирует движения по регистру Хозрасчетный для указанных документов
//
// Параметры:
//    МассивДокументов - Массив - Документы
//
// Возвращаемое значение:
//    Массив - Документы, которые не удалось отразить в регл. учете
//
Функция ОтразитьДокументыВРеглУчете(МассивДокументов) Экспорт
	
	НеотраженныеДокументы = Новый Массив;
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	Данные.Период КАК Дата,
	|	Данные.Регистратор КАК Ссылка,
	|	Данные.Организация КАК Организация
	|ИЗ
	|	РегистрСведений.ОтражениеДокументовВРеглУчете КАК Данные
	|ГДЕ
	|	Данные.Регистратор В (&МассивДокументов)
	|УПОРЯДОЧИТЬ ПО
	|	Данные.Период ВОЗР
	|");
	
	Запрос.УстановитьПараметр("МассивДокументов", МассивДокументов);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		РезультатОтражения = ОтразитьДокумент(Выборка, Истина);
		Отражено = (Перечисления.СтатусыОтраженияДокументовВРеглУчете.ОтраженоВРеглУчете = РезультатОтражения);
		Если Не Отражено Тогда
			НеотраженныеДокументы.Добавить(Выборка.Ссылка);
		КонецЕсли;
	КонецЦикла;
	
	Возврат НеотраженныеДокументы;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Выборка документов, которые необходимо отразить (провести) по регл.учету
// Результат: ВыборкаИзРезультатаЗапроса (Дата: ДатаВремя, Ссылка: ДокументСсылка, Организация: СправочникСсылка)
Функция КОтражению(ПериодОтражения, Организация)
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	Данные.Период КАК Дата,
	|	Данные.Регистратор КАК Ссылка,
	|	Данные.Организация КАК Организация
	|ИЗ
	|	РегистрСведений.ОтражениеДокументовВРеглУчете КАК Данные
	|ГДЕ
	|	Данные.Период <= &ПериодОтражения
	|	И Данные.Статус В (
	|		ЗНАЧЕНИЕ(Перечисление.СтатусыОтраженияДокументовВРеглУчете.КОтражениюВРеглУчете),
	|		ЗНАЧЕНИЕ(Перечисление.СтатусыОтраженияДокументовВРеглУчете.НеУказаныСчетаУчета))
	|	И &Организация В (Данные.Организация, НЕОПРЕДЕЛЕНО, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка))
	|УПОРЯДОЧИТЬ ПО
	|	Данные.Период ВОЗР
	|");
	Запрос.УстановитьПараметр("ПериодОтражения", ПериодОтражения);
	Запрос.УстановитьПараметр("Организация", Организация);
	Возврат Запрос.Выполнить().Выбрать();
КонецФункции

// Восстановление партий, расчетов по документам, сумм в валюте регл., расчет себестоимости
Процедура ВыполнитьОффлайновыеРасчеты(ПериодРасчета, Ссылка)
	
	НачатьТранзакцию();
	ПартионныйУчетСервер.ПодготовитьСчетаФактурыКОтражению(ПериодРасчета, Ссылка);
	ЗафиксироватьТранзакцию();
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Расчеты.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам
	|ИЗ
	|	РегистрНакопления.РасчетыСПоставщиками КАК Расчеты
	|ГДЕ
	|	Расчеты.Период МЕЖДУ &НачалоПериода И &ОкончаниеПериода
	|	И Расчеты.Регистратор = &Ссылка
	|	И Расчеты.Активность
	|");
	Запрос.УстановитьПараметр("НачалоПериода", НачалоМесяца(ПериодРасчета));
	Запрос.УстановитьПараметр("ОкончаниеПериода", КонецМесяца(ПериодРасчета));
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	МассивАналитик = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("АналитикаУчетаПоПартнерам");
	МассивДокументовРасчетов = Новый Массив;
	Если МассивАналитик.Количество() > 0 Тогда
		ВзаиморасчетыСервер.ВыполнитьПроведениеДокументовПоРасчетамСПоставщиками(МассивАналитик,,,МассивДокументовРасчетов);
	КонецЕсли;
	
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Расчеты.АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК Расчеты
	|ГДЕ
	|	Расчеты.Период МЕЖДУ &НачалоПериода И &ОкончаниеПериода
	|	И Расчеты.Регистратор = &Ссылка
	|	И Расчеты.Активность
	|");
	Запрос.УстановитьПараметр("НачалоПериода", НачалоМесяца(ПериодРасчета));
	Запрос.УстановитьПараметр("ОкончаниеПериода", КонецМесяца(ПериодРасчета));
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	МассивАналитик = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("АналитикаУчетаПоПартнерам");
	МассивДокументов = Новый Массив;
	Если МассивАналитик.Количество() > 0 Тогда
		ВзаиморасчетыСервер.ВыполнитьПроведениеДокументовПоРасчетамСКлиентами(МассивАналитик,,,МассивДокументов);
	КонецЕсли;
	ОбщегоНазначенияКлиентСервер.ДополнитьМассив(МассивДокументовРасчетов, МассивДокументов, Истина);
	УправленческийУчетПроведениеСервер.СформироватьДвиженияКонтрагентКонтрагент(МассивДокументовРасчетов);
	
КонецПроцедуры

Процедура ОтразитьБухгалтерскиеПроводки(Ссылка, ТаблицаЗагрузки)
	НаборЗаписей = РегистрыБухгалтерии.Хозрасчетный.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.Регистратор.Установить(Ссылка);
	НаборЗаписей.Загрузить(ТаблицаЗагрузки);
	НаборЗаписей.Записывать = Истина;
	НаборЗаписей.Записать();
КонецПроцедуры

Функция КомментарийОшибокЗаполнения(РезультатПроверки, МетаданныеСсылки)
	Комментарий = Нстр("ru = 'Обнаружены ошибки (%ВсегоОшибок%):'");
	ВсегоОшибок = 0;
	
	ОШ = РезультатПроверки.Выбрать();
	Пока ОШ.Следующий() Цикл
		КодОшибки = ОШ.КодОшибки;
		ВсегоОшибок = ВсегоОшибок + 1;
		Ошибка = РеглУчетВыборкиСервер.ШаблонКомментарияОшибок(КодОшибки);
		Ошибка = СтрЗаменить(Ошибка, "%Индекс%", Формат(ВсегоОшибок, "ЧН=; ЧГ="));
		Ошибка = СтрЗаменить(Ошибка, "%ВидСчета%", Строка(ОШ.ВидСчета));
		Ошибка = СтрЗаменить(Ошибка, "%Организация%", Строка(ОШ.Организация));
		Ошибка = СтрЗаменить(Ошибка, "%МестоАналитика%", РеглУчетВыборкиСервер.СтрокаМестоАналитика(ОШ.МестоУчета, ОШ.АналитикаУчета));
		Ошибка = СтрЗаменить(Ошибка, "%Счет%", Строка(ОШ.Счет));
		Комментарий = Комментарий + Символы.ПС + Ошибка;
	КонецЦикла;
	
	Возврат СтрЗаменить(Комментарий, "%ВсегоОшибок%", Формат(ВсегоОшибок, "ЧН=; ЧГ="));
КонецФункции

Функция ВременныеТаблицы()
	ВременныеТаблицы = Новый МенеджерВременныхТаблиц;

	ЗапросПланаСчетов = РеглУчетВыборкиСервер.ЗапросПланаСчетов();
	ЗапросПланаСчетов.МенеджерВременныхТаблиц = ВременныеТаблицы;
	ЗапросПланаСчетов.Выполнить();
	// выборка вариабельных счетов учета
	ЗапросСчетов = РеглУчетВыборкиСервер.ЗапросСчетов();
	ЗапросСчетов.МенеджерВременныхТаблиц = ВременныеТаблицы;
	ЗапросСчетов.Выполнить();
	
	// выборка счетов по умолчанию
	ЗапросСчетов = РеглУчетВыборкиСервер.ЗапросСчетовПоУмолчанию();
	ЗапросСчетов.МенеджерВременныхТаблиц = ВременныеТаблицы;
	ЗапросСчетов.Выполнить();
	
	Возврат ВременныеТаблицы;
КонецФункции

Процедура ДобавитьСтатусОтражения(НаборЗаписей, Реквизиты, Статус, Комментарий = "")
	НаборЗаписей.Записывать = Истина;
	Запись = НаборЗаписей.Добавить();

	Если (ТипЗнч(Реквизиты) = Тип("ДокументОбъект.СписаниеБезналичныхДенежныхСредств")
		Или ТипЗнч(Реквизиты) = Тип("ДокументОбъект.ПоступлениеБезналичныхДенежныхСредств"))
		И Реквизиты.ПроведеноБанком Тогда
		Запись.Период = КонецДня(Реквизиты.ДатаПроведенияБанком);
	Иначе
		Запись.Период = Реквизиты.Дата;
	КонецЕсли;
	
	Запись.Организация = Реквизиты.Организация;
	Запись.Статус = Статус;
	Запись.Комментарий = Комментарий;
КонецПроцедуры

Процедура ДополнительнаяОбработкаПриОтраженииДокумента(РеквизитыДокумента, РезультатЗапроса)
	
	Если ТипЗнч(РеквизитыДокумента.Ссылка) = Тип("ДокументСсылка.СписаниеБезналичныхДенежныхСредств")
		И Константы.ИспользоватьНачислениеЗарплаты.Получить() Тогда
		ДенежныеСредстваСервер.ЗарегистрироватьПеречислениеНалоговИВзносов(РеквизитыДокумента.Ссылка, РезультатЗапроса);
	КонецЕсли;
	
	Если ТипЗнч(РеквизитыДокумента.Ссылка) = Тип("ДокументСсылка.ПринятиеКУчетуОС") Тогда
		Документы.ПринятиеКУчетуОС.ЗаполнитьПервоначальныеСуммыПриОтражении(РеквизитыДокумента, РезультатЗапроса);
	КонецЕсли;
	
	Если ТипЗнч(РеквизитыДокумента.Ссылка) = Тип("ДокументСсылка.МодернизацияОС") Тогда
		Документы.МодернизацияОС.ЗаполнитьПараметрыНачисленияАмортизацииПриОтражении(РеквизитыДокумента, РезультатЗапроса);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
