﻿//{УП}

Функция Существует(Организация, Период, ВыводитьСообщениеОбОтсутствииУчетнойПолитики = Ложь, ДокументСсылка = Неопределено) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.Существует(Организация, Период, ВыводитьСообщениеОбОтсутствииУчетнойПолитики, ДокументСсылка);

КонецФункции

Функция ПлательщикНалогаНаПрибыль(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПлательщикНалогаНаПрибыль(Организация, Период);

КонецФункции 

Функция ПоддержкаПБУ18(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПоддержкаПБУ18(Организация, Период);

КонецФункции 

Функция МетодНачисленияАмортизацииНУ(Организация, Период) Экспорт
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	ЕСТЬNULL(Т.УчетнаяПолитика.МетодНачисленияАмортизацииНУ, ЗНАЧЕНИЕ(Перечисление.МетодыНачисленияАмортизации.Линейный)) КАК МетодНачисленияАмортизацииНУ
		|ИЗ
		|	РегистрСведений.УчетнаяПолитикаОрганизаций.СрезПоследних(&Период, Организация = &Организация) КАК Т");
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Период", Новый Граница(Период, ВидГраницы.Включая));
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка.МетодНачисленияАмортизацииНУ;
		
	КонецЕсли;
	
	Возврат Перечисления.МетодыНачисленияАмортизации.Линейный;
	
КонецФункции

//{/УП}

// Параметры учетной политики по НДС

Функция ПлательщикНДС(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПлательщикНДС(Организация, Период);

КонецФункции 

Функция ПрименяетсяОсвобождениеОтУплатыНДС(Организация, Период) Экспорт

	Результат = УчетнаяПолитикаПереопределяемый.ПрименяетсяОсвобождениеОтУплатыНДС(Организация, Период);

	Возврат Результат;	

КонецФункции

//{УП}

Функция Учитывать5ПроцентныйПорог(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.Учитывать5ПроцентныйПорог(Организация, Период);

КонецФункции 

// Параметры учетной политики по ЕНВД
Функция ПлательщикЕНВД(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПлательщикЕНВД(Организация, Период);

КонецФункции 

// Параметры учетной политики по УСН

Функция ПрименяетсяУСНДоходыМинусРасходы(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПрименяетсяУСНДоходыМинусРасходы(Организация, Период);
	
КонецФункции 

Функция ПрименяетсяУСНДоходы(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПрименяетсяУСНДоходы(Организация, Период);

КонецФункции 

//{/УП}

Функция ПрименяетсяУСН(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ПрименяетсяУСН(Организация, Период);

КонецФункции 

//{УП}

Функция СтавкаНалогаУСН(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.СтавкаНалогаУСН(Организация, Период);

КонецФункции

Функция ОбъектНалогообложенияУСН(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ОбъектНалогообложенияУСН(Организация, Период);

КонецФункции 

// Прочие параметры учетной политики

Функция СистемаНалогообложения(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.СистемаНалогообложения(Организация, Период);

КонецФункции 

// Параметры учетной политики по резервам по сомнительным долгам

Функция ФормироватьРезервыПоСомнительнымДолгамБУ(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ФормироватьРезервыПоСомнительнымДолгамБУ(Организация, Период);

КонецФункции 

Функция ФормироватьРезервыПоСомнительнымДолгамНУ(Организация, Период) Экспорт

	Возврат УчетнаяПолитикаПереопределяемый.ФормироватьРезервыПоСомнительнымДолгамНУ(Организация, Период);

КонецФункции 

//{/УП}