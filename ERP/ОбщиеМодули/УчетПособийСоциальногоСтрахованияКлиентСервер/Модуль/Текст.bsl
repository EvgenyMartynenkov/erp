﻿
#Область СлужебныеПроцедурыИФункции

// Возвращает дату вступления в силу статьи 1 Федерального закона от 25.02.2011 № 21-ФЗ.
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   дата
//
Функция ДатаОтдельногоРасчетаПособийПоМатеринству() Экспорт 

	Возврат '20130101'

КонецФункции 

// Возвращает дату вступления в силу Федерального Закона от 08.12.2010 № 343-ФЗ.
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   дата
//
Функция ДатаНачалаРеформыСоцСтрахования() Экспорт 
	
	Возврат '20110101'
	
КонецФункции 

// Возвращает дату вступления в силу Федерального закона от 05.04.2013 № 36-ФЗ "О внесении изменений в статью 9.
// Федерального закона "Об обязательном социальном страховании от несчастных случаев на производстве и профессиональных
// заболеваний" и статью 1 Федерального закона "Об обязательном социальном страховании на случай временной
// нетрудоспособности и в связи с материнством".
//
// Параметры:
//  нет
//
// Возвращаемое значение:
//   дата
//
Функция ДатаВводаОграниченийДляПособийПоНС_ПЗ() Экспорт 

	Возврат '20130408'

КонецФункции 

// Функция возвращает длину суток в секундах.
//
Функция ДлинаСуток() Экспорт
	Возврат 24 * 60 * 60;
КонецФункции

// Функция возвращает количество дней за счет работодателя в зависимости от даты начала нетрудоспособности.
//
Функция КоличествоДнейЗаСчетРаботодателя(ДатаНачалаСобытия) Экспорт
	Возврат ?(ДатаНачалаСобытия > ДатаНачалаРеформыСоцСтрахования(), 3, 2);
КонецФункции

Функция ДатаНачалаНеполногоПериодаРасчетаСреднегоЗаработка() Экспорт
	Возврат '20140301';
КонецФункции

#КонецОбласти
