﻿
&НаКлиенте
Процедура Очистить(Команда)
	
	ПолеВводаВид = "";
	ПолеВводаНомер = "";
	ПолеВводаСерия = "";
	
	СформироватьПредставление();
	
КонецПроцедуры

&НаКлиенте
Процедура СформироватьПредставление()

	Серия = ПолеВводаСерия;
	Номер = РегламентированнаяОтчетностьКлиентСервер.ДополнитьСтроку(СокрП(ПолеВводаНомер), 5, "0");
	Вид = РегламентированнаяОтчетностьКлиентСервер.ДополнитьСтроку(ПолеВводаВид, 2, " ", 1);

	Если НЕ ЗначениеЗаполнено(Серия) И НЕ ЗначениеЗаполнено(Вид) И НЕ ЗначениеЗаполнено(ПолеВводаНомер) Тогда
		ПолеВводаПредставление = "";
	Иначе

		Представление = ПолеВводаСерия + "" + Номер + "" + Вид;
		ПолеВводаПредставление = Представление;

	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПолеВводаСерияПриИзменении(Элемент)
	
	Элемент = РегламентированнаяОтчетностьКлиентСервер.ДополнитьСтроку(СтрЗаменить(Элемент, ";", ""), 3, " ");
	СформироватьПредставление();
	
КонецПроцедуры

&НаКлиенте
Процедура ПолеВводаНомерПриИзменении(Элемент)
	
	СформироватьПредставление();

КонецПроцедуры

&НаКлиенте
Процедура ПолеВводаВидПриИзменении(Элемент)
	
	СформироватьПредставление();

КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	НачальноеЗначениеВыбора = Параметры.НачальноеЗначениеВыбора;
	Если ЗначениеЗаполнено(НачальноеЗначениеВыбора) Тогда
		Если СтрДлина(НачальноеЗначениеВыбора) = 10 Тогда
			НачЗначение = СокрЛП(НачальноеЗначениеВыбора);
			
			Серия = Сред(НачЗначение, 1, 3);
			Номер = Сред(НачЗначение, 4, 5);
			Вид = Сред(НачЗначение, 9, 2);
			
			ПолеВводаСерия = Серия;
			ПолеВводаНомер = Номер;
			ПолеВводаВид = Вид;
			ПолеВводаПредставление = ПолеВводаСерия + "" + Номер + "" + Вид;
		Иначе
			ПолеВводаПредставление = НачальноеЗначениеВыбора;
			Сообщение = Новый СообщениеПользователю;
			Сообщение.Текст = НСтр("'ru=Необходимо ввести серию, номер и вид лицензии в соответствии с правилами ввода реквизитов лицензии (10 символов и без разделителей "","").'");
			Сообщение.Сообщить();
		КонецЕсли;
	КонецЕсли;

	БылаНажатаКнопкаОК = Ложь;

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность И Не БылаНажатаКнопкаЗакрыть Тогда
		
		Отказ = Истина;
		
		ТекстВопроса = НСтр("ru = 'Данные были изменены. Отказаться от изменений?'");
		
		ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса,  РежимДиалогаВопрос.ОКОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Если РезультатВопроса = КодВозвратаДиалога.ОК Тогда
		Модифицированность = Ложь;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Если НЕ БылаНажатаКнопкаЗакрыть Тогда

		Возврат;

	КонецЕсли;

	НачальноеЗначениеВыбора = ПолеВводаПредставление;
	
КонецПроцедуры

&НаКлиенте
Процедура Ввести(Команда)
	
	БылаНажатаКнопкаЗакрыть = Истина;
	
	Закрыть(ПолеВводаПредставление);

КонецПроцедуры