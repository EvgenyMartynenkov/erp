﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	ОткрытьФорму(
		"Отчет.ОценкаРентабельностиПродаж.Форма",
		ПараметрыФормы(ПараметрКоманды),
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры
	
&НаСервере 
Функция ПараметрыФормы(ПараметрКоманды)
	КлючВариантаОтчета = КлючВариантаОтчета();
	
	ПараметрыФормы = Новый Структура(
	"КлючВарианта, КлючНазначенияИспользования, Отбор,  СформироватьПриОткрытии, ВидимостьКомандВариантовОтчетов",
	КлючВариантаОтчета,
	КлючВариантаОтчета + "=" + Строка(ПараметрКоманды),
	Новый Структура("Документ", ПараметрКоманды),
	Истина,
	Ложь);
	
	Возврат ПараметрыФормы;
КонецФункции
	
&НаСервере
Функция КлючВариантаОтчета()
	ИспользоватьРучныеСкидкиВПродажах = ПолучитьФункциональнуюОпцию("ИспользоватьРучныеСкидкиВПродажах");
	ИспользоватьАвтоматическиеСкидкиВПродажах = ПолучитьФункциональнуюОпцию("ИспользоватьАвтоматическиеСкидкиВПродажах");
	
	Если Не ИспользоватьРучныеСкидкиВПродажах
		И Не ИспользоватьАвтоматическиеСкидкиВПродажах Тогда
		КлючВарианта = "ОценкаРентабельностиПродажиБезСкидокКонтекст";
	ИначеЕсли ИспользоватьРучныеСкидкиВПродажах И Не ИспользоватьАвтоматическиеСкидкиВПродажах Тогда
		КлючВарианта = "ОценкаРентабельностиПродажиТолькоРучныеСкидкиКонтекст";
	ИначеЕсли Не ИспользоватьРучныеСкидкиВПродажах И ИспользоватьАвтоматическиеСкидкиВПродажах Тогда
		КлючВарианта = "ОценкаРентабельностиПродажиТолькоАвтоСкидкиКонтекст";
	Иначе
		КлючВарианта = "ОценкаРентабельностиПродажиКонтекст";
	КонецЕсли;
	
	Возврат КлючВарианта;
КонецФункции