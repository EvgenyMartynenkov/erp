﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	//Вставить содержимое обработчика.
	ОткрытьФорму("Отчет.КонтрольПередачиПродукцииДавальцу.Форма",
		Новый Структура("КлючНазначенияИспользования, Отбор, КлючВарианта, СформироватьПриОткрытии, ВидимостьКомандВариантовОтчетов", 
			ПараметрКоманды, 
			Новый Структура("ЗаказДавальца", ПараметрКоманды), 
			"КонтрольПередачиПродукцииДавальцуКонтекст", 
			Истина,
			Ложь),
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно);
	
КонецПроцедуры
