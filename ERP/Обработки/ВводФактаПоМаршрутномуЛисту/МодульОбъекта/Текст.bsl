﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	МассивНепроверяемыхРеквизитов.Добавить("ВыходныеИзделия.Склад");
	
	МассивНепроверяемыхРеквизитов.Добавить("ВыходныеИзделия.КоличествоУпаковок");
	МассивНепроверяемыхРеквизитов.Добавить("МатериалыИУслуги.КоличествоУпаковок");
	МассивНепроверяемыхРеквизитов.Добавить("Трудозатраты.Количество");
	
	МассивНепроверяемыхРеквизитов.Добавить("ВыходныеИзделия.Характеристика");
	МассивНепроверяемыхРеквизитов.Добавить("МатериалыИУслуги.Характеристика");
	
	МассивНепроверяемыхРеквизитов.Добавить("МатериалыИУслуги.ДатаРасхода");
	
	ПроверитьЗаполнениеДокументаВЗависимостиОтСтатуса(МассивНепроверяемыхРеквизитов, Отказ);
	
	ПроверитьЗаполнениеОтклонений("ВыходныеИзделия",  Отказ);
	ПроверитьЗаполнениеОтклонений("ВозвратныеОтходы", Отказ);
	ПроверитьЗаполнениеОтклонений("МатериалыИУслуги", Отказ);
	ПроверитьЗаполнениеОтклонений("Трудозатраты",     Отказ);
	
	ПроверитьНормативыТрудозатрат(Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры
 
#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

Процедура ПроверитьЗаполнениеСклада(Отказ)

	ШаблонСообщения = НСтр("ru = 'Не определено направление выпуска в строке %1 списка ""Продукция"".'");
	
	Для каждого СтрокаТаблицы Из ВыходныеИзделия Цикл
		
		Если ЗначениеЗаполнено(СтрокаТаблицы.Склад) Тогда
			Продолжить;
		КонецЕсли;
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, Формат(СтрокаТаблицы.НомерСтроки, "ЧГ="));
		Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("ВыходныеИзделия", СтрокаТаблицы.НомерСтроки, "Склад");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, Поле, "Объект", Отказ);
		
	КонецЦикла;
	
	ШаблонСообщения = НСтр("ru = 'Не определено направление выпуска в строке %1 списка ""Возвратные отходы"".'");
	
	Для каждого СтрокаТаблицы Из ВозвратныеОтходы Цикл
		
		Если ЗначениеЗаполнено(СтрокаТаблицы.Склад) Тогда
			Продолжить;
		КонецЕсли;
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, Формат(СтрокаТаблицы.НомерСтроки, "ЧГ="));
		Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("ВыходныеИзделия", СтрокаТаблицы.НомерСтроки, "Склад");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, Поле, "Объект", Отказ);
		
	КонецЦикла;
	
КонецПроцедуры
 
Процедура ПроверитьЗаполнениеОтклонений(ИмяТаблицы, Отказ)
	
	Если НРег(ИмяТаблицы) = НРег("ВыходныеИзделия") Тогда
		ИмяРеквизитаКоличество = "КоличествоУпаковокФакт";
		ИмяРеквизита           = "КоличествоУпаковокОтклонение";
		
		ШаблонСообщения = НСтр("ru='Отклонение от норматива не должно приводить к отрицательному выпуску.'");
		ШаблонСообщенияКоличество = НСтр("ru = 'Необходимо заполнить колонку ""Факт"" или ""Отклонение"" в строке %1 списка ""Продукция"".'");
		
	ИначеЕсли НРег(ИмяТаблицы) = НРег("ВозвратныеОтходы") Тогда
		ИмяРеквизитаКоличество = "КоличествоУпаковокФакт";
		ИмяРеквизита           = "КоличествоУпаковокОтклонение";
		
		ШаблонСообщения = НСтр("ru='Отклонение от норматива не должно приводить к отрицательному выпуску.'");
		ШаблонСообщенияКоличество = НСтр("ru = 'Необходимо заполнить колонку ""Факт"" или ""Отклонение"" в строке %1 списка ""Возвратные отходы"".'");
		
	ИначеЕсли НРег(ИмяТаблицы) = НРег("МатериалыИУслуги") Тогда
		ИмяРеквизитаКоличество = "КоличествоУпаковокФакт";
		ИмяРеквизита           = "КоличествоУпаковокОтклонение";
		
		ШаблонСообщения = НСтр("ru='Отклонение от норматива не должно приводить к отрицательному потреблению.'");
		ШаблонСообщенияКоличество = НСтр("ru = 'Необходимо заполнить колонку ""Факт"" или ""Отклонение"" в строке %1 списка ""Материалы и работы"".'");
		
	ИначеЕсли НРег(ИмяТаблицы) = НРег("Трудозатраты") Тогда
		ИмяРеквизитаКоличество = "КоличествоФакт";
		ИмяРеквизита           = "КоличествоОтклонение";
		
		ШаблонСообщения = НСтр("ru='Отклонение от норматива не должно приводить к отрицательным трудозатратам.'");
		ШаблонСообщенияКоличество = НСтр("ru = 'Необходимо заполнить колонку ""Факт"" или ""Отклонение"" в строке %1 списка ""Трудозатраты"".'");
		
	КонецЕсли; 
	
	Для каждого ДанныеСтроки Из ЭтотОбъект[ИмяТаблицы] Цикл
		
		Если ДанныеСтроки.Количество + ДанныеСтроки.КоличествоОтклонение < 0 Тогда
			
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, ДанныеСтроки.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ИмяТаблицы, ДанныеСтроки.НомерСтроки, ИмяРеквизита);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, Поле, "Объект", Отказ);
			
		ИначеЕсли ДанныеСтроки.КоличествоФакт = 0 И ДанныеСтроки.КоличествоОтклонение = 0 Тогда
			
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщенияКоличество, ДанныеСтроки.НомерСтроки);
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти(ИмяТаблицы, ДанныеСтроки.НомерСтроки, ИмяРеквизитаКоличество);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, Поле, "Объект", Отказ);
			
		КонецЕсли;
		
	КонецЦикла; 
	
КонецПроцедуры

Процедура ПроверитьЗаполнениеДокументаВЗависимостиОтСтатуса(МассивНепроверяемыхРеквизитов, Отказ)
	
	ПроверитьЗаполнениеСклада(Отказ);

	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.ИмяТЧ = "ВыходныеИзделия";
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ, ПараметрыПроверки);

	ПараметрыПроверки.ИмяТЧ = "ВозвратныеОтходы";
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ, ПараметрыПроверки);

	ПараметрыПроверки.ИмяТЧ = "МатериалыИУслуги";
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект, МассивНепроверяемыхРеквизитов, Отказ, ПараметрыПроверки);
	
	ПроверитьДатуРасходаМатериалов(Отказ);
	
КонецПроцедуры

Процедура ПроверитьНормативыТрудозатрат(Отказ)

	ШаблонСообщения = НСтр("ru = 'Норматив по виду работ ""%1"" в сумме по всем бигадам должен быть равен %2 (сейчас сумма %3).'");
	
	Для каждого СтрокаТрудозатратыИсходные Из ТрудозатратыИсходные Цикл
		СтруктураПоиска = Новый Структура("ВидРабот", СтрокаТрудозатратыИсходные.ВидРабот);
		СписокСтрок = Трудозатраты.НайтиСтроки(СтруктураПоиска);
		Норматив = 0;
		Для каждого СтрокаТрудозатраты Из СписокСтрок Цикл
			Норматив = Норматив + СтрокаТрудозатраты.Количество;
		КонецЦикла;
		Если Норматив <> СтрокаТрудозатратыИсходные.Количество Тогда
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						ШаблонСообщения, 
						СтрокаТрудозатратыИсходные.ВидРабот,
						Формат(СтрокаТрудозатратыИсходные.Количество, "ЧН=0; ЧГ="),
						Формат(Норматив, "ЧН=0; ЧГ="));
						
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,, "Объект", Отказ);
		КонецЕсли; 
	КонецЦикла; 
	
КонецПроцедуры

Процедура ПроверитьДатуРасходаМатериалов(Отказ)

	Если СтатусВыполненияОперации <> Перечисления.СтатусыВыполненияОпераций.Завершено Тогда
		Возврат;
	КонецЕсли;
	
	ШаблонСообщения = НСтр("ru = 'Материал (работа) фактически израсходован, но не указана дата расхода в строке %1 списка ""Материалы и работы""'");
	
	Для каждого СтрокаТаблицы Из МатериалыИУслуги Цикл
		Если СтрокаТаблицы.ДатаРасхода = '000101010000'
			И СтрокаТаблицы.КоличествоФакт <> 0 Тогда
			
			ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонСообщения, 
																						Формат(СтрокаТаблицы.НомерСтроки, "ЧГ="));
																						
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("МатериалыИУслуги", СтрокаТаблицы.НомерСтроки, "ДатаРасхода");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,, Поле,"Объект", Отказ);
		КонецЕсли; 	
	КонецЦикла; 
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли