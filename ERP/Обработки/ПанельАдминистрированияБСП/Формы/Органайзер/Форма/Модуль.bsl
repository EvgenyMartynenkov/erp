﻿&НаКлиенте
Перем ОбновитьИнтерфейс;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	// Значения реквизитов формы
	СоставНабораКонстантФормы    = ОбщегоНазначенияУТ.ПолучитьСтруктуруНабораКонстант(НаборКонстант);
	ВнешниеРодительскиеКонстанты = ОбщегоНазначенияУТПовтИсп.ПолучитьСтруктуруРодительскихКонстант(СоставНабораКонстантФормы);
	РежимРаботы 				 = ОбщегоНазначенияПовтИсп.РежимРаботыПрограммы();
	
	РежимРаботы.Вставить("СоставНабораКонстантФормы",    Новый ФиксированнаяСтруктура(СоставНабораКонстантФормы));
	РежимРаботы.Вставить("ВнешниеРодительскиеКонстанты", Новый ФиксированнаяСтруктура(ВнешниеРодительскиеКонстанты));
	РежимРаботы.Вставить("БазоваяВерсия", 				 ПолучитьФункциональнуюОпцию("БазоваяВерсия"));
	
	РежимРаботы = Новый ФиксированнаяСтруктура(РежимРаботы);
	
	// Настройки видимости при запуске.
	Элементы.ГруппаПрименитьНастройки.Видимость = РежимРаботы.ЭтоВебКлиент;
	
	// СтандартныеПодсистемы.Взаимодействия
	Если РежимРаботы.МодельСервиса Тогда
		Элементы.ИспользоватьПочтовыйКлиент.Видимость                = Ложь;
		Элементы.ПояснениеИспользоватьПочтовыйКлиент.Видимость       = Ложь;
		Элементы.ОтправлятьПисьмаВФорматеHTML.Видимость              = Ложь;
		Элементы.ПояснениеОтправлятьПисьмаВФорматеHTML.Видимость     = Ложь;
		Элементы.ИспользоватьПрочиеВзаимодействия.Видимость          = Ложь;
		Элементы.ПояснениеИспользоватьПрочиеВзаимодействия.Видимость = Ложь;
		Элементы.ИспользоватьПризнакРассмотрено.Видимость            = Ложь;
		Элементы.ПояснениеИспользоватьПризнакРассмотрено.Видимость   = Ложь;
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Взаимодействия
	
	// Обновление состояния элементов.
	УстановитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	#Если Не ВебКлиент Тогда
	ОбновитьИнтерфейсПрограммы();
	#КонецЕсли
КонецПроцедуры

&НаКлиенте
// Обработчик оповещения формы.
//
// Параметры:
//	ИмяСобытия - Строка - обрабатывается только событие Запись_НаборКонстант, генерируемое панелями администрирования.
//	Параметр   - Структура - содержит имена констант, подчиненных измененной константе, "вызвавшей" оповещение.
//	Источник   - Строка - имя измененной константы, "вызвавшей" оповещение.
//
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия <> "Запись_НаборКонстант" Тогда
		Возврат; // такие событие не обрабатываются
	КонецЕсли;
	
	// Если это изменена константа, расположенная в другой форме и влияющая на значения констант этой формы,
	// то прочитаем значения констант и обновим элементы этой формы.
	Если РежимРаботы.ВнешниеРодительскиеКонстанты.Свойство(Источник)
	 ИЛИ (ТипЗнч(Параметр) = Тип("Структура")
	 		И ОбщегоНазначенияУТКлиентСервер.ПолучитьОбщиеКлючиСтруктур(
	 			Параметр, РежимРаботы.ВнешниеРодительскиеКонстанты).Количество() > 0) Тогда
		
		ЭтаФорма.Прочитать();
		УстановитьДоступность();
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура КакПрименитьНастройкиОбработкаНавигационнойСсылки(Элемент, НавигационнаяСсылка, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ОбновитьИнтерфейс = Истина;
	ПодключитьОбработчикОжидания("ОбновитьИнтерфейсПрограммы", 0.1, Истина);
КонецПроцедуры

// СтандартныеПодсистемы.Взаимодействия
&НаКлиенте
Процедура ИспользоватьПочтовыйКлиентПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПрочиеВзаимодействияПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ОтправлятьПисьмаВФорматеHTMLПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПризнакРассмотреноПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры
// Конец СтандартныеПодсистемы.Взаимодействия

// СтандартныеПодсистемы.ЗаметкиПользователя
&НаКлиенте
Процедура ИспользоватьЗаметкиПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры
// Конец СтандартныеПодсистемы.ЗаметкиПользователя

// СтандартныеПодсистемы.НапоминанияПользователя
&НаКлиенте
Процедура ИспользоватьНапоминанияПользователяПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры
// Конец СтандартныеПодсистемы.НапоминанияПользователя

// СтандартныеПодсистемы.БизнесПроцессыИЗадачи
&НаКлиенте
Процедура ИспользоватьБизнесПроцессыИЗадачиПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПодчиненныеБизнесПроцессыПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИзменятьЗаданияЗаднимЧисломПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьДатуНачалаЗадачПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьДатуИВремяВСрокахЗадачПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры
// Конец СтандартныеПодсистемы.БизнесПроцессыИЗадачи

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.БизнесПроцессыИЗадачи
&НаКлиенте
Процедура ОткрытьРолиИИсполнителиБизнесПроцессов(Команда)
	
	ОткрытьФорму("РегистрСведений.ИсполнителиЗадач.Форма.АдресацияПоОбъекту",,ЭтотОбъект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.БизнесПроцессыИЗадачи

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

////////////////////////////////////////////////////////////////////////////////
// Клиент

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизита(Элемент, ОбновлятьИнтерфейс = Истина)
	
	Результат = ПриИзмененииРеквизитаСервер(Элемент.Имя);
	
	Если ОбновлятьИнтерфейс Тогда
		ОбновитьИнтерфейс = Истина;
		#Если Не ВебКлиент Тогда
		ПодключитьОбработчикОжидания("ОбновитьИнтерфейсПрограммы", 1, Истина);
		#КонецЕсли
	КонецЕсли;
	
	СтандартныеПодсистемыКлиент.ПоказатьРезультатВыполнения(ЭтотОбъект, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьИнтерфейсПрограммы()
	
	Если ОбновитьИнтерфейс = Истина Тогда
		ОбновитьИнтерфейс = Ложь;
		ОбщегоНазначенияКлиент.ОбновитьИнтерфейсПрограммы();
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Вызов сервера

&НаСервере
Функция ПриИзмененииРеквизитаСервер(ИмяЭлемента)
	
	Результат = Новый Структура;
	
	РеквизитПутьКДанным = Элементы[ИмяЭлемента].ПутьКДанным;
	
	СохранитьЗначениеРеквизита(РеквизитПутьКДанным, Результат);
	
	УстановитьДоступность(РеквизитПутьКДанным);
	
	ОбновитьПовторноИспользуемыеЗначения();
	
	Возврат Результат;
	
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// Сервер

&НаСервере
Процедура СохранитьЗначениеРеквизита(РеквизитПутьКДанным, Результат)
	
	// Сохранение значений реквизитов, не связанных с константами напрямую (в отношении один-к-одному).
	Если РеквизитПутьКДанным = "" Тогда
		Возврат;
	КонецЕсли;
	
	// Определение имени константы.
	КонстантаИмя = "";
	Если НРег(Лев(РеквизитПутьКДанным, 14)) = НРег("НаборКонстант.") Тогда
		// Если путь к данным реквизита указан через "НаборКонстант".
		КонстантаИмя = Сред(РеквизитПутьКДанным, 15);
	Иначе
		// Определение имени и запись значения реквизита в соответствующей константе из "НаборКонстант".
		// Используется для тех реквизитов формы, которые связаны с константами напрямую (в отношении один-к-одному).
	КонецЕсли;
	
	// Сохранения значения константы.
	Если КонстантаИмя <> "" Тогда
		КонстантаМенеджер = Константы[КонстантаИмя];
		КонстантаЗначение = НаборКонстант[КонстантаИмя];
		
		Если КонстантаМенеджер.Получить() <> КонстантаЗначение Тогда
			КонстантаМенеджер.Установить(КонстантаЗначение);
		КонецЕсли;
		
		Если (КонстантаИмя = "ИспользоватьПочтовыйКлиент" ИЛИ КонстантаИмя = "ИспользоватьБизнесПроцессыИЗадачи") И КонстантаЗначение = Ложь Тогда
			ЭтотОбъект.Прочитать();
		КонецЕсли;
		
		СтандартныеПодсистемыКлиентСервер.РезультатВыполненияДобавитьОповещениеОткрытыхФорм(Результат, "Запись_НаборКонстант", Новый Структура, КонстантаИмя);
		// СтандартныеПодсистемы.ВариантыОтчетов
		ВариантыОтчетов.ДобавитьОповещениеПриИзмененииЗначенияКонстанты(Результат, КонстантаМенеджер);
		// Конец СтандартныеПодсистемы.ВариантыОтчетов
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступность(РеквизитПутьКДанным = "")
	
	// СтандартныеПодсистемы.Взаимодействия
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьПочтовыйКлиент" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		Элементы.ИспользоватьПрочиеВзаимодействия.Доступность = НаборКонстант.ИспользоватьПочтовыйКлиент;
		Элементы.ИспользоватьПризнакРассмотрено.Доступность   = НаборКонстант.ИспользоватьПочтовыйКлиент;
		Элементы.ОтправлятьПисьмаВФорматеHTML.Доступность     = НаборКонстант.ИспользоватьПочтовыйКлиент;
		
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Взаимодействия
	
	// СтандартныеПодсистемы.БизнесПроцессыИЗадачи
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		ДоступностьБизнесПроцессовИЗадач =
			НЕ НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи
			ИЛИ (НЕ Константы.ИспользоватьСогласованиеЗаказовКлиентов.Получить()
				И НЕ Константы.ИспользоватьСогласованиеСоглашенийСКлиентами.Получить()
				И НЕ Константы.ИспользоватьСогласованиеКоммерческихПредложений.Получить()
				И НЕ Константы.ИспользоватьСогласованиеЗаявокНаВозвратТоваровОтКлиентов.Получить()
				И НЕ Константы.ИспользоватьСогласованиеЦенНоменклатуры.Получить()
				И НЕ Константы.ИспользоватьСогласованиеЗаказовПоставщикам.Получить()
				И НЕ Константы.ИспользоватьИнтеграциюС1СДокументооборот.Получить());
		
		Элементы.ИспользоватьБизнесПроцессыИЗадачи.Доступность 		   = ДоступностьБизнесПроцессовИЗадач;
		Элементы.ГруппаКомментарийИспользоватьБизнесПроцессы.Видимость = НЕ ДоступностьБизнесПроцессовИЗадач;
		
		Если НЕ ДоступностьБизнесПроцессовИЗадач Тогда
			Элементы.КомментарийИспользоватьБизнесПроцессы.Заголовок = ТекстКомментарияИспользоватьБизнесПроцессы();
		КонецЕсли;
		
		Элементы.ОткрытьРолиИИсполнителиБизнесПроцессов.Доступность = НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи;
		Элементы.ИспользоватьПодчиненныеБизнесПроцессы.Доступность  = НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи;
		Элементы.ИзменятьЗаданияЗаднимЧислом.Доступность            = НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи;
		Элементы.ИспользоватьДатуНачалаЗадач.Доступность            = НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи;
		Элементы.ИспользоватьДатуИВремяВСрокахЗадач.Доступность     = НаборКонстант.ИспользоватьБизнесПроцессыИЗадачи;
		
	КонецЕсли;
	// Конец СтандартныеПодсистемы.БизнесПроцессыИЗадачи
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// Прочие

&НаСервере
Функция ТекстКомментарияИспользоватьБизнесПроцессы()
	
	ТекстКомментарияИспользоватьБизнесПроцессы = НСтр("ru = 'Невозможно отключение бизнес-процессов и задач, потому что включено:'");
	
	ДополнениеККомментарию = "";
	
	Если Константы.ИспользоватьСогласованиеЦенНоменклатуры.Получить() Тогда
		
		ДополнениеККомментарию =
			Символы.ПС + Символы.Таб + "- "
			+ НСтр("ru = 'Согласование цен в разделе ""Маркетинг""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьСогласованиеЗаказовКлиентов.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Согласование заказов клиентов в разделе ""Продажи""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьСогласованиеЗаявокНаВозвратТоваровОтКлиентов.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Согласование заявок на возврат в разделе ""Продажи""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьСогласованиеКоммерческихПредложений.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Согласование коммерческих предложений в разделе ""Продажи""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьСогласованиеСоглашенийСКлиентами.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Согласование соглашений с клиентами в разделе ""Продажи""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьСогласованиеЗаказовПоставщикам.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Согласование документов закупок в разделе ""Закупки""'");
		
	КонецЕсли;
	
	Если Константы.ИспользоватьИнтеграциюС1СДокументооборот.Получить() Тогда
		
		ДополнениеККомментарию =
			ДополнениеККомментарию + ?(Не ПустаяСтрока(ДополнениеККомментарию), ";", "") + Символы.ПС + Символы.Таб + "- " 
			+ НСтр("ru = 'Интеграция с 1С:Документооборотом в разделе ""Документооборот""'");
		
	КонецЕсли;
	
	Если Не ПустаяСтрока(ДополнениеККомментарию) Тогда
		ТекстКомментарияИспользоватьБизнесПроцессы = ТекстКомментарияИспользоватьБизнесПроцессы + " " + ДополнениеККомментарию + ".";
	Иначе 
		ТекстКомментарияИспользоватьБизнесПроцессы = "";
	КонецЕсли;
	
	Возврат ТекстКомментарияИспользоватьБизнесПроцессы;
	
КонецФункции

#КонецОбласти
