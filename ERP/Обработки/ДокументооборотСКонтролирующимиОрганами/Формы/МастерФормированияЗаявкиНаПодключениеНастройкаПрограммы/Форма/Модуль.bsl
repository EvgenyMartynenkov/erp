﻿&НаКлиенте
Перем КонтекстЭДОКлиент Экспорт;

&НаКлиенте
Перем ПрограммноеЗакрытие;

&НаКлиенте
Перем РеквизитыДокумента;

&НаКлиенте
Перем Статус;

&НаКлиенте
Перем СпецоператорСвязи;

&НаКлиенте
Перем ИдентификаторДокументооборота;

&НаКлиенте
Перем РеквизитыДокументаДляЗаписи;

&НаКлиенте
Перем Результат;

&НаКлиенте
Перем СписокСтатей;

&НаКлиенте
Перем СсылкаНаДокумент;

&НаКлиенте
Перем ДатаПолученияОтвета;


////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ИдентификаторАбонента 			= Параметры.ИдентификаторАбонента;
	НовыйСтатусДокумента  			= Параметры.НовыйСтатусДокумента;
	ОтпечатокСертификатаИзОтвета	= Параметры.ОтпечатокСертификатаИзОтвета;
	
	ЭтоВторичноеЗаявление = ДокументЗаявление.ТипЗаявления = Перечисления.ТипыЗаявленияАбонентаСпецоператораСвязи.Изменение;
	НастроитьВнешнийВидЭлементовУправления();
	
	НастройкаПользователей();
	
КонецПроцедуры     

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	ПрограммноеЗакрытие = Ложь;
	
	ОписаниеОповещения = Новый ОписаниеОповещения("ПриОткрытииЗавершение", ЭтотОбъект);
	
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытииЗавершение(РезультатПолученияКонтекста, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = РезультатПолученияКонтекста.КонтекстЭДО;
	
	РеквизитыДокументаДляЗаписи =  Новый Структура;

	СсылкаНаДокумент = СсылкаНаДокумент();
	РеквизитыДокумента = КонтекстЭДОКлиент.ПолучитьСтруктуруРеквизитовДокумента(СсылкаНаДокумент);
	Статус 						  = РеквизитыДокумента.Статус;
	ПредыдущийСтатус 		      = Статус;
	СпецоператорСвязи			  = РеквизитыДокумента.СпецоператорСвязи;
	ИдентификаторДокументооборота = РеквизитыДокумента.ИдентификаторДокументооборота;
	
	
	Если НовыйСтатусДокумента = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено") Тогда
		Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОдобрено;
	Иначе 
		Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОтклонено;
		ТекстОшибокПриОтклоненномЗаявлении = ДокументЗаявление.СтатусКомментарий;
		ПрограммноеЗакрытие = Истина;
	КонецЕсли;
	
	УправлениеКнопкамиНавигации();
	ЗаполнитьДанныеСлужбыПоддержки();
	
	СписокСтатей = ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентПереопределяемый.СписокСсылокНаСтатьиПо1СОтчетности();
	УстановитьСсылкиНаСтатьи();
	
	Если ДокументЗаявление <> Неопределено Тогда
    	Оповестить("ЗаполнитьСводнуюИнформациюПоЗаявлениюАбонентаСпецоператораСвязи",ДокументЗаявление.Ссылка);
	КонецЕсли;
	
	Если ЭтоЭлектроннаяПодписьВМоделиСервиса Тогда
		ПодключитьОбработчикОжидания("Подключаемый_ВыполнитьАвтонастройку", 0.2, Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Элементы.КоманднаяПанельДалее.ПодчиненныеЭлементы.Закрыть.Заголовок = "Закрыть" Тогда
		ПрограммноеЗакрытие = Истина;
	КонецЕсли;	
	
	Если НЕ ПрограммноеЗакрытие Тогда
		
		Отказ = Истина;
		ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОписаниеОповещения, НСтр("ru = 'Прервать работу мастера ?'"), РежимДиалогаВопрос.ДаНет, , КодВозвратаДиалога.Да);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(КодВозврата, ДополнительныеПараметры) Экспорт
	
	Если КодВозврата = КодВозвратаДиалога.Да Тогда
		ПрограммноеЗакрытие = Истина;
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Если НовыйСтатусДокумента = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено") Тогда
		
		ПараметрыЗапуска = ПолучитьПараметрыДляЗапускаМетодаБЭД();
		Если ЗначениеЗаполнено(ПараметрыЗапуска.Организация) Тогда
			ПараметрОрганизация = ПараметрыЗапуска.Организация;
			ДополнительныеПараметры = Новый Структура;
			ДополнительныеПараметры.Вставить("КодРегиона", ПараметрыЗапуска.КодРегиона);
			ЭлектронныйДокументооборотСКонтролирующимиОрганамиКлиентПереопределяемый.ОткрытьФормуПодключенияКСервисуЭлектронныхДокументов(ПараметрОрганизация, ДополнительныеПараметры);
		КонецЕсли;	
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура КоманднаяПанельМастерНазад(Команда)
	ПоказатьПредыдущуюСтраницу();
КонецПроцедуры

&НаКлиенте
Процедура КоманднаяПанельМастерДалее(Команда)
	
	ОчиститьСообщения();
	
	Если Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг1 Тогда
		
		Результат = Ложь;
		
		Если Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОдобрено или 
			Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОшибкаПодключенияКСерверу Тогда
			
			ДатаПолученияОтвета 			= ДатаПолученияОтвета(); // серверный вызов для соблюдения стандарта работы в разных часовых поясах
			ЭтоВторичноеЗаявление 			= (ДокументЗаявление.ТипЗаявления = ПредопределенноеЗначение("Перечисление.ТипыЗаявленияАбонентаСпецоператораСвязи.Изменение"));
			ПараметрыОбработатьОбновление 	= КонтекстЭДОКлиент.ПараметрыОбработатьОбновление();
			ПараметрыОбработатьОбновление.ЭтоЭлектроннаяПодписьВМоделиСервиса 	= ДокументЗаявление.ЭлектроннаяПодписьВМоделиСервиса;
			ОписаниеОповещения 				= Новый ОписаниеОповещения("КоманднаяПанельМастерДалееШаг1Завершение", ЭтотОбъект, ПараметрыОбработатьОбновление);
			
			Если ПараметрыОбработатьОбновление.ЭтоЭлектроннаяПодписьВМоделиСервиса Тогда
				
				ПараметрыОбработатьОбновление.ИдентификаторДокументооборота 	= 
					?(ЗначениеЗаполнено(
						ДокументЗаявление.ИдентификаторКлючевогоКонтейнера), 
						ДокументЗаявление.ИдентификаторКлючевогоКонтейнера, 
						ДокументЗаявление.ИдентификаторДокументооборота);
						
				ПараметрыОбработатьОбновление.ТелефонМобильныйДляАвторизации 	= ДокументЗаявление.ТелефонМобильныйДляАвторизации;
				КриптосервисВМоделиСервисаКлиент.ИнициализацияСАвторизацией(ПараметрыОбработатьОбновление, , ОписаниеОповещения);
				
			Иначе
				
				ВыполнитьОбработкуОповещения(ОписаниеОповещения, Истина);
				
			КонецЕсли;
		
		Иначе 
			// Если статус заявления - "Отклонено", тогда создаем новое заявление
			ПараметрыФормы = Новый Структура("ЗначениеКопирования",СсылкаНаДокумент);
			Форма = ПолучитьФорму("Документ.ЗаявлениеАбонентаСпецоператораСвязи.ФормаОбъекта",ПараметрыФормы,,Истина);
			Форма.Открыть();
			
			ПрограммноеЗакрытие = Истина;
			Закрыть();
			
		КонецЕсли;
		
	ИначеЕсли Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг2  Тогда
		
		// Записываем выбранных пользователей в регистр сведений
		ЗаписатьПользователейУчетныхЗаписейДокументооборота(КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
		
		Если Результат Тогда
			СтатусКомментарий = "Создана учетная запись.";
			Статус = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено");
			
			РеквизитыДокументаДляЗаписи.Вставить("Статус",Статус);
			РеквизитыДокументаДляЗаписи.Вставить("ДатаПолученияОтвета", ДатаПолученияОтвета);
			РеквизитыДокументаДляЗаписи.Вставить("УчетнаяЗапись", КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
			РеквизитыДокументаДляЗаписи.Вставить("Обработана", Истина);
			
			КонтекстЭДОКлиент.УстановитьУчетнуюЗаписьОрганизации(РеквизитыДокумента.Организация, КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
			ОповеститьОбИзменении(КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
			Оповестить("ОбновитьУчетнуюЗапись",КонтекстЭДОКлиент.НоваяУчетнаяЗапись,);
		Иначе
			СтатусКомментарий = "Заявление одобрено, но не удалось создать учетную запись.";
		КонецЕсли;
		
		РеквизитыДокументаДляЗаписи.Вставить("СтатусКомментарий",СтатусКомментарий);
		
		// Настройка списка пользователей
		КонтекстЭДОКлиент.ОбновитьДокумент(СсылкаНаДокумент,РеквизитыДокументаДляЗаписи);
		ОповеститьОбИзменении(ДокументЗаявление.Ссылка);
		
		Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг3;
		
		КоманднаяПанельМастерДалееУстановитьВидимостьДоступность(ДокументЗаявление);
		
	Иначе
		
		КоманднаяПанельМастерДалееУстановитьВидимостьДоступность(ДокументЗаявление);
	
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КоманднаяПанельМастерДалееШаг1Завершение(РезультатИнициализацииКриптосервиса, ПараметрыОбработатьОбновление) Экспорт
	
	ТекстОшибокДляМастераПодключенияК1СОтчетности = "";
	
	Если РезультатИнициализацииКриптосервиса <> Истина Тогда
		Возврат;
	КонецЕсли;	
		
	Если ЭтоВторичноеЗаявление Тогда
		РеквизитыУчетнойЗаписи = ЭлектроннаяПодписьВМоделиСервисаВызовСервера.РеквизитыУчетнойЗаписи(ДокументЗаявление.УчетнаяЗапись);
		ПараметрыОбработатьОбновление.ИдентификаторДокументооборота = РеквизитыУчетнойЗаписи.ИдентификаторДокументооборота;
		ПараметрыОбработатьОбновление.ТелефонМобильныйДляАвторизации = РеквизитыУчетнойЗаписи.ТелефонМобильныйДляАвторизации;
	Иначе
		ПараметрыОбработатьОбновление.ИдентификаторДокументооборота = 
			?(ЗначениеЗаполнено(
				ДокументЗаявление.ИдентификаторКлючевогоКонтейнера), 
				ДокументЗаявление.ИдентификаторКлючевогоКонтейнера, 
				ДокументЗаявление.ИдентификаторДокументооборота);
				
		ПараметрыОбработатьОбновление.ТелефонМобильныйДляАвторизации = ДокументЗаявление.ТелефонМобильныйДляАвторизации;
	КонецЕсли;
	ПараметрыОбработатьОбновление.ИдентификаторАбонента 				= ИдентификаторАбонента;
	ПараметрыОбработатьОбновление.СпецОператорСвязи 					= СпецоператорСвязи;
	ПараметрыОбработатьОбновление.ПутьКонтейнерЗакрытогоКлюча 			= РеквизитыДокумента.ПутьКонтейнерЗакрытогоКлюча;
	ПараметрыОбработатьОбновление.Организация 							= РеквизитыДокумента.Организация;
	ПараметрыОбработатьОбновление.ЭтоВторичноеЗаявление 				= ЭтоВторичноеЗаявление;
	ПараметрыОбработатьОбновление.ОтпечатокСертификатаИзОтвета 			= ОтпечатокСертификатаИзОтвета;
	ПараметрыОбработатьОбновление.ВызовИзМастераПодключенияК1СОтчетности = Истина;
	
	ДополнительныеПараметры = Новый Структура("ТекстОшибокДляМастераПодключенияК1СОтчетности", ТекстОшибокДляМастераПодключенияК1СОтчетности);
	ОписаниеОповещения = Новый ОписаниеОповещения("КоманднаяПанельМастерДалееОбработатьОбновлениеЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	КонтекстЭДОКлиент.ОбработатьОбновление(ПараметрыОбработатьОбновление, ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КоманднаяПанельМастерДалееОбработатьОбновлениеЗавершение(СтруктураРезультат, ДополнительныеПараметры) Экспорт
	
	Результат = СтруктураРезультат.РезультатОбновления;
	ТекстОшибокДляМастераПодключенияК1СОтчетности = ДополнительныеПараметры.ТекстОшибокДляМастераПодключенияК1СОтчетности;
	
	Если НЕ Результат или ИдентификаторАбонента = "" Тогда
		
		Если Найти(ВРЕГ(ТекстОшибокДляМастераПодключенияК1СОтчетности), НСтр("ru = 'СЕРВЕР'")) Тогда
			Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОшибкаПодключенияКСерверу;	
			ТекстОшибокПриОтклоненномЗаявлении = ТекстОшибокДляМастераПодключенияК1СОтчетности;
		Иначе				
			Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеПрочиеОшибки;	
			ТекстОшибокПриОтклоненномЗаявлении = ТекстОшибокДляМастераПодключенияК1СОтчетности;
		КонецЕсли;	
		
		КоманднаяПанельМастерДалееУстановитьВидимостьДоступность(ДокументЗаявление);
		
	Иначе
		
		Если ДокументЗаявление.ТипЗаявления = ПредопределенноеЗначение("Перечисление.ТипыЗаявленияАбонентаСпецоператораСвязи.Изменение") Тогда
			// Вторичное заявление
			
			Обработана = Ложь;
			Если Результат Тогда
				СтатусКомментарий = НСтр("ru = 'Выполнено '") + ИзмененныеРеквизиты;
				Статус = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено");
				Обработана = Истина;
			Иначе
				СтатусКомментарий = НСтр("ru = 'Заявление одобрено но не удалось создать учетную запись.'");
				Статус = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Отправлено");
			КонецЕсли;
			
			РеквизитыДокументаДляЗаписи.Вставить("ДатаПолученияОтвета", ДатаПолученияОтвета);
			РеквизитыДокументаДляЗаписи.Вставить("Статус",Статус);
			РеквизитыДокументаДляЗаписи.Вставить("СтатусКомментарий",СтатусКомментарий);
			РеквизитыДокументаДляЗаписи.Вставить("Обработана", Обработана);
			
			// Настройка списка пользователей
			КонтекстЭДОКлиент.ОбновитьДокумент(СсылкаНаДокумент,РеквизитыДокументаДляЗаписи);
			ОповеститьОбИзменении(ДокументЗаявление.Ссылка);
			
			// Если заявление вторичное, то пропускаем шаг 2
			Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг3;
			
		ИначеЕсли СписокПользователей.Количество() <= 1 Тогда
			// Первичное заявление
			// Если пользователь 1, то не показываем Шаг 2, а сразу выполняем запись в регистр и переходим к Шагу 3
			Для Каждого Пользователь Из СписокПользователей Цикл
				Пользователь.Пометка = Истина;
			КонецЦикла;
			// Записываем выбранных пользователей в регистр сведений
			ЗаписатьПользователейУчетныхЗаписейДокументооборота(КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
			
			Обработана = Ложь;
			Если Результат Тогда
				СтатусКомментарий = "Создана учетная запись.";
				Статус = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено");
				
				РеквизитыДокументаДляЗаписи.Вставить("УчетнаяЗапись", КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
				
				КонтекстЭДОКлиент.УстановитьУчетнуюЗаписьОрганизации(РеквизитыДокумента.Организация, КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
				ОповеститьОбИзменении(КонтекстЭДОКлиент.НоваяУчетнаяЗапись);
				Оповестить("ОбновитьУчетнуюЗапись", КонтекстЭДОКлиент.НоваяУчетнаяЗапись,);
				Обработана = Истина;
			Иначе
				СтатусКомментарий = "Заявление одобрено, но не удалось создать учетную запись.";
				Статус = ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Отправлено");
			КонецЕсли;
			
			РеквизитыДокументаДляЗаписи.Вставить("ДатаПолученияОтвета", ДатаПолученияОтвета);
			РеквизитыДокументаДляЗаписи.Вставить("Статус",Статус);
			РеквизитыДокументаДляЗаписи.Вставить("СтатусКомментарий",СтатусКомментарий);
			РеквизитыДокументаДляЗаписи.Вставить("Обработана", Обработана);
			
			// Настройка списка пользователей
			КонтекстЭДОКлиент.ОбновитьДокумент(СсылкаНаДокумент,РеквизитыДокументаДляЗаписи);
			ОповеститьОбИзменении(ДокументЗаявление.Ссылка);
			
			Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг3;
			
		Иначе 
			// Если пользователей больше, чем 1, то показываем Шаг 2
			Элементы.ОсновнаяПанель.ТекущаяСтраница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.НастрокаПрограммыШаг2;
		КонецЕсли;
		
		КоманднаяПанельМастерДалееУстановитьВидимостьДоступность(ДокументЗаявление);
		
	КонецЕсли;
	
	
КонецПроцедуры

&НаКлиенте
Процедура КоманднаяПанельМастерДалееУстановитьВидимостьДоступность(ДокументЗаявление)

	Если ДокументЗаявление <> Неопределено Тогда
		Оповестить("ЗаполнитьСводнуюИнформациюПоЗаявлениюАбонентаСпецоператораСвязи", ДокументЗаявление.Ссылка);
	КонецЕсли;	
	
	Элементы.Назад.Видимость = Истина;
	Элементы.Далее.Доступность = Истина;
	
	УправлениеКнопкамиНавигации();

КонецПроцедуры


&НаКлиенте
Процедура ОткрытьСсылкуНаСтатьюПоПодключению(Команда)
	
	Если ЭтоВторичноеЗаявление Тогда
		ОбщегоНазначенияКлиент.ПерейтиПоСсылке("http://its.1c.ru/db/metod81#content:5274:1");
	Иначе
		КонтекстЭДОКлиент.ОткрытьИнструкциюПоПодключениюК1СОтчетности();
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьВидеоПоПодключению(Команда)
	КонтекстЭДОКлиент.ОткрытьВидеоИнструкциюПоПодключениюК1СОтчетности();
КонецПроцедуры


////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаКлиенте
Процедура ПоказатьПредыдущуюСтраницу()
	
	ТекущаяСтраница = Элементы.ОсновнаяПанель.ТекущаяСтраница;
	
	Если Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.Индекс(ТекущаяСтраница) > 0 Тогда
		
		Страница = Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.Получить(Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.Индекс(ТекущаяСтраница) -1);
		Элементы.ОсновнаяПанель.ТекущаяСтраница = Страница;
		
	КонецЕсли;
	УправлениеКнопкамиНавигации();
	
КонецПроцедуры

&НаКлиенте
Процедура УправлениеКнопкамиНавигации()
	
	ТекущаяСтраница 		= Элементы.ОсновнаяПанель.ТекущаяСтраница;
	ИндексТекущейСтраницы 	= Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.Индекс(ТекущаяСтраница);
	ВсегоСтраниц 			= Элементы.ОсновнаяПанель.ПодчиненныеЭлементы.Количество();
		
	КнопкаДалее	  = Элементы.КоманднаяПанельДалее.ПодчиненныеЭлементы.Далее;
	КнопкаНазад   = Элементы.КоманднаяПанельНазад.ПодчиненныеЭлементы.Назад;
	КнопкаЗакрыть = Элементы.КоманднаяПанельДалее.ПодчиненныеЭлементы.Закрыть;
	
	КнопкаДалее.Заголовок = "Далее>";
	КнопкаДалее.Видимость = Истина;
	КнопкаНазад.Заголовок = "<Назад";
	КнопкаНазад.Видимость = Истина;
	
	Если ИндексТекущейСтраницы = 0 Тогда
		
    	//первая закладка		
		Если Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОдобрено Тогда
			КнопкаЗакрыть.Заголовок 		= "Отмена";			
			КнопкаНазад.Видимость 			= Ложь;
		ИначеЕсли Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеОшибкаПодключенияКСерверу Тогда
			КнопкаДалее.Заголовок 			= "Обновить статус заявления";
			КнопкаЗакрыть.Заголовок 		= "Закрыть";	
			КнопкаНазад.Видимость 			= Ложь;
			КнопкаДалее.КнопкаПоУмолчанию 	= Истина;

		ИначеЕсли Элементы.ГруппаСостоянийЗаявление.ТекущаяСтраница = Элементы.ГруппаСостоянийЗаявление.ПодчиненныеЭлементы.ГруппаСостояниеПрочиеОшибки Тогда
			КнопкаДалее.Видимость 			= Ложь;
	    	КнопкаНазад.Видимость 			= Ложь;
			КнопкаЗакрыть.Заголовок 		= "Закрыть";
			КнопкаЗакрыть.КнопкаПоУмолчанию = Истина;			
		Иначе	
			КнопкаДалее.Заголовок 			= "Подготовить новое заявление";
			КнопкаЗакрыть.Заголовок			= "Закрыть";
			КнопкаНазад.Видимость 			= Ложь;
			КнопкаДалее.КнопкаПоУмолчанию 	= Истина;
		КонецЕсли;
		
	ИначеЕсли ИндексТекущейСтраницы = 1 Тогда 	
		//вторая закладка
		КнопкаЗакрыть.Заголовок 			= "Отмена";
		КнопкаДалее.Видимость 				= Истина;
    	КнопкаНазад.Видимость 				= Ложь;
		КнопкаДалее.КнопкаПоУмолчанию 		= Истина;		
    ИначеЕсли ИндексТекущейСтраницы = ВсегоСтраниц - 1 Тогда
		//последняя закладка
		КнопкаДалее.Видимость 				= Ложь;
    	КнопкаНазад.Видимость 				= Ложь;
		КнопкаЗакрыть.Заголовок 			= "Закрыть";
		КнопкаЗакрыть.КнопкаПоУмолчанию 	= Истина;		
		
		ПрограммноеЗакрытие 				= Истина;
	Иначе	
		//все остальные закладки
		КнопкаЗакрыть.Заголовок 			= "Отмена";
		КнопкаНазад.Видимость 				= Истина;
    	КнопкаДалее.Видимость 				= Истина;
		КнопкаДалее.КнопкаПоУмолчанию 		= Истина;	
    КонецЕсли;
		
КонецПроцедуры

&НаКлиенте
Процедура ЗакрытьМастер()
	
	Закрыть();
	
КонецПроцедуры

&НаСервере
Процедура НастройкаПользователей()
	
	СписокПользователей.Очистить();
	
	Запрос = Новый Запрос("ВЫБРАТЬ РАЗРЕШЕННЫЕ
	                      |	ВЫБОР
	                      |		КОГДА Настройки.УчетнаяЗапись ЕСТЬ NULL 
	                      |			ТОГДА Ложь
	                      |		ИНАЧЕ Истина
	                      |	КОНЕЦ КАК Пометка,
	                      |	Пользователи.Ссылка КАК Пользователь
	                      |ИЗ
	                      |	Справочник.Пользователи КАК Пользователи
	                      |		ЛЕВОЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	                      |			ПользователиУчетныхЗаписейДокументооборота.УчетнаяЗапись КАК УчетнаяЗапись,
	                      |			ПользователиУчетныхЗаписейДокументооборота.Пользователь КАК Пользователь
	                      |		ИЗ
	                      |			РегистрСведений.ПользователиУчетныхЗаписейДокументооборота КАК ПользователиУчетныхЗаписейДокументооборота
	                      |		ГДЕ
	                      |			ПользователиУчетныхЗаписейДокументооборота.УчетнаяЗапись = &УчетнаяЗапись) КАК Настройки
	                      |		ПО Пользователи.Ссылка = Настройки.Пользователь
	                      |ГДЕ
	                      |	НЕ Пользователи.ПометкаУдаления
	                      |	И НЕ Пользователи.Недействителен
	                      |	И НЕ Пользователи.Служебный
	                      |	И Пользователи.ИдентификаторПользователяИБ <> &ПустойИдентификаторПользователяИБ
	                      |
	                      |УПОРЯДОЧИТЬ ПО
	                      |	Пользователи.Наименование");
	Запрос.УстановитьПараметр("УчетнаяЗапись", Справочники.УчетныеЗаписиДокументооборота.ПустаяСсылка());
	Запрос.УстановитьПараметр("ПустойИдентификаторПользователяИБ", Новый УникальныйИдентификатор("00000000-0000-0000-0000-000000000000"));
	Выборка = Запрос.Выполнить().Выбрать();
	ТекущийПользователь = Пользователи.ТекущийПользователь();
	Пока Выборка.Следующий() Цикл 
		Если Выборка.Пользователь = ТекущийПользователь Тогда 
			Картинка = БиблиотекаКартинок.Пользователь;
		Иначе 
			Картинка = Неопределено;
		КонецЕсли;
		СписокПользователей.Добавить(Выборка.Пользователь, Выборка.Пользователь.Наименование, Выборка.Пометка, Картинка);
	КонецЦикла;

	СтрокаПользователь = СписокПользователей.НайтиПоЗначению(ТекущийПользователь);
	Если СтрокаПользователь <> Неопределено Тогда 
		СтрокаПользователь.Пометка = Истина;
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ЗаписатьПользователейУчетныхЗаписейДокументооборота(СсылкаУчетнаяЗапись)
	
	НаборЗаписей = РегистрыСведений.ПользователиУчетныхЗаписейДокументооборота.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.УчетнаяЗапись.Установить(СсылкаУчетнаяЗапись.Ссылка);
	ФлагОтметки = Ложь;
	
	Для Каждого СтрокаСписка Из СписокПользователей Цикл
		Если СтрокаСписка.Пометка Тогда
			НоваяСтрока = НаборЗаписей.Добавить();
			НоваяСтрока.УчетнаяЗапись = СсылкаУчетнаяЗапись.Ссылка;
			НоваяСтрока.Пользователь = СтрокаСписка.Значение;
			ФлагОтметки = Истина;
		КонецЕсли;
	КонецЦикла;
	
	Попытка
		НаборЗаписей.Записать();
	Исключение
		РегламентированнаяОтчетностьКлиентСервер.СообщитьОбОшибке(ОписаниеОшибки(), ,
		"Не удалось обновить список пользователей по учетной записи налогоплательщика """ + СокрЛП(СсылкаУчетнаяЗапись.Ссылка) + """.");
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаОткрытьСсылку(Команда)
	
	НомерСсылки = 0;
	Попытка
		НомерСсылки = Число(СтрЗаменить(Команда.Имя,"КомандаОткрытьСсылку",""));
	Исключение
	КонецПопытки; 
	
	Если НомерСсылки <> 0 Тогда 
		Ссылка = СписокСтатей.Получить(НомерСсылки - 1).Значение;
		ОбщегоНазначенияКлиент.ПерейтиПоСсылке(Ссылка);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСсылкиНаСтатьи()
	
	Если СписокСтатей.Количество() >= 1 тогда
	
		Счетчик = 1;
		Для каждого ЗаписьСтатьи Из СписокСтатей Цикл
			
			Если Счетчик > 4 тогда
				Прервать;
			КонецЕсли;	
			
			Элементы["КомандаОткрытьСсылку" + Строка(Счетчик)].Заголовок = ЗаписьСтатьи.Представление;
			
			Счетчик = Счетчик + 1;
		КонецЦикла; 
	Иначе
		
		Для Счетчик = 1 по 4 Цикл
			Элементы["КомандаОткрытьСсылку" + Строка(Счетчик)].Видимость = Ложь;
		КонецЦикла; 
		
		Элементы.ПодсказкаСсылкиНаСтатьи.Видимость = Ложь;
		
	КонецЕсли;	
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьДанныеСлужбыПоддержки()

	// Контактные данные ЗАО "Калуга Астрал"
	ТелефонСлужбыПоддержки = "8-800-700-86-68";
	АдресЭлектроннойПочтыСлужбыПоддержки = "1c@astralnalog.ru";

КонецПроцедуры

&НаСервере
Функция СсылкаНаДокумент()

	Возврат РеквизитФормыВЗначение("ДокументЗаявление").Ссылка;	

КонецФункции 


////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаКлиенте
Процедура Подключаемый_ВыполнитьАвтонастройку()
	
	КоманднаяПанельМастерДалее(Неопределено);
	
КонецПроцедуры

&НаСервере
Функция ДатаПолученияОтвета()
	Возврат ТекущаяДатаСеанса();
КонецФункции
	
&НаСервере
Процедура ДобавитьРеквизитКСтроке(НазваниеРеквизита, РеквизитИзменился, ИзмененныеРеквизиты)
	
	Если РеквизитИзменился Тогда
		Если ИзмененныеРеквизиты = "" Тогда
			ИзмененныеРеквизиты = НазваниеРеквизита;
		Иначе
			ИзмененныеРеквизиты = ИзмененныеРеквизиты + "%1" + НазваниеРеквизита;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НастроитьВнешнийВидЭлементовУправления()
	
	КонтекстЭДОСервер = ДокументооборотСКОВызовСервера.ПолучитьОбработкуЭДО();
	
	// Настраиваем разный внешний вид для вторичных и первичных заявлений
	Если ЭтоВторичноеЗаявление Тогда
		
		// Определяем, что было изменено
		СписокИзмененныхРеквизитов = ДокументЗаявление.ИзменившиесяРеквизитыВторичногоЗаявления;
		
		ПродлитьСертификат = КонтекстЭДОСервер.ЭтотПараметрИзменился(СписокИзмененныхРеквизитов, 
			Перечисления.ПараметрыПодключенияК1СОтчетности.ПродлениеАбонентскогоСертификата);
			
		ИзменитьВладельцаСертификата = КонтекстЭДОСервер.ЭтотПараметрИзменился(СписокИзмененныхРеквизитов, 
			Перечисления.ПараметрыПодключенияК1СОтчетности.ВладелецЭЦП);
			
		ПродлитьЛицензиюНа1СОтчетность = КонтекстЭДОСервер.ЭтотПараметрИзменился(СписокИзмененныхРеквизитов, 
			Перечисления.ПараметрыПодключенияК1СОтчетности.ПродлениеЛицензии);
			
		ИзменитьМобильныйТелефон = КонтекстЭДОСервер.ЭтотПараметрИзменился(СписокИзмененныхРеквизитов, 
			Перечисления.ПараметрыПодключенияК1СОтчетности.ТелефонМобильный);
			
		ПереиздатьСертификат = ?(КонтекстЭДОСервер.ЭтотПараметрИзменился(СписокИзмененныхРеквизитов, 
			Перечисления.ПараметрыПодключенияК1СОтчетности.ПереизданиеСертификата), 2, 0);
			
		ИзменитьСоставКонтролирующихОрганов = 
			КонтекстЭДОСервер.БылИзмененСоставКонтролирующихОрганов(СписокИзмененныхРеквизитов);
			
		ИзменитьРеквизитыПодключенияК1СОтчетности = 
			КонтекстЭДОСервер.БылиИзменененыРеквизитыПодключенияК1СОтчетности(СписокИзмененныхРеквизитов);
			
		// Представление изменившихся настроек подключения 
		ИзмененныеРеквизиты = "";
		ДобавитьРеквизитКСтроке(НСтр("ru = 'изменение состава контролирующих органов'"), ИзменитьСоставКонтролирующихОрганов, ИзмененныеРеквизиты);
		ДобавитьРеквизитКСтроке(НСтр("ru = 'изменение сотрудника-владельца сертификата'"), ИзменитьВладельцаСертификата, ИзмененныеРеквизиты);
		ДобавитьРеквизитКСтроке(НСтр("ru = 'переиздание сертификата'"), ПереиздатьСертификат, ИзмененныеРеквизиты);
		ДобавитьРеквизитКСтроке(НСтр("ru = 'продление лицензии на 1С-Отчетность'"), ПродлитьЛицензиюНа1СОтчетность, ИзмененныеРеквизиты);
		ДобавитьРеквизитКСтроке(НСтр("ru = 'изменение номера мобильного телефона для SMS-уведомления'"), ИзменитьМобильныйТелефон, ИзмененныеРеквизиты);
		ДобавитьРеквизитКСтроке(НСтр("ru = 'изменение реквизитов подключения к 1С-Отчетности'"), ИзменитьРеквизитыПодключенияК1СОтчетности, ИзмененныеРеквизиты);
		КонтекстЭДОСервер.ЗаменитьЗапятуюНаИВСтроке(ИзмененныеРеквизиты);
			
		// Состав контролирующих органов
		ИзменитьСоставКонтролирующихОрганов = КонтекстЭДОСервер.БылИзмененСоставКонтролирующихОрганов(СписокИзмененныхРеквизитов);
		
		Заголовок =  НСтр("ru = 'Изменение настроек подключения к 1С-Отчетности'");
		
		// Текст заголовка при одобрении 
		ТекстЗаголовкаПриОдобрении = НСтр("ru = 'Ваше заявление%1одобрено'");
		ТекстЗаголовкаПриОдобрении = СтрЗаменить(ТекстЗаголовкаПриОдобрении, "%1", 
			?(ПустаяСтрока(ИзмененныеРеквизиты), " ", НСтр("ru = ' на '") + ИзмененныеРеквизиты + " "));
		Элементы.ПодсказкаПоРезультатуОдобрено.Заголовок = ТекстЗаголовкаПриОдобрении;
		
		// Текст заголовка при отклонении
		ТекстЗаголовкаПриОтклонении = НСтр("ru = 'Ваше заявление%1отклонено'");
		ТекстЗаголовкаПриОтклонении = СтрЗаменить(ТекстЗаголовкаПриОтклонении, "%1", 
			?(ПустаяСтрока(ИзмененныеРеквизиты), " ", НСтр("ru = ' на '") + ИзмененныеРеквизиты + " "));
		Элементы.ПодсказкаПоРезультатуОтклонено.Заголовок = ТекстЗаголовкаПриОтклонении;
		
		ТекстСообщения = "";
		Если ПереиздатьСертификат Тогда
			ТекстСообщения = НСтр("ru = 'Далее произойдет установка сертификатов и автоматическая настройка электронного документооборота.'");
		Иначе
			ТекстСообщения = НСтр("ru = 'Далее произойдет автоматическая настройка электронного документооборота.'");
		КонецЕсли;
		Элементы.ПодсказкаПоРезультатам1.Заголовок = ТекстСообщения;
		
		Элементы.ГруппаЗакладокЭтап1.ТекущаяСтраница = Элементы.Этап1ВторичноеЗаявление;
		Элементы.ГруппаЗакладокЭтап3.ТекущаяСтраница = Элементы.Этап3ВторичноеЗаявление;
		
		// Третий шаг
		Элементы.ПодсказкаСсылкиНаСтатьи.Видимость = Ложь;
		Элементы.КомандаОткрытьСсылку1.Видимость = Ложь;
		Элементы.КомандаОткрытьСсылку2.Видимость = Ложь;
		Элементы.КомандаОткрытьСсылку3.Видимость = Ложь;
		Элементы.КомандаОткрытьСсылку4.Видимость = Ложь;
		
		Элементы.ПодсказкаКонечныйРезультат.Заголовок = НСтр("ru = 'Поздравляем!
			|Новые настройки подключения к 1С-Отчетности успешно применены'");
			
		// Гиперссылка
		Элементы.ОткрытьСсылкуНаСтатьюПоПодключению.Заголовок = НСтр("ru = 'Инструкция по изменению настроек подключения к 1С-Отчетности'");
			
	Иначе
			
		Заголовок =  НСтр("ru = 'Помощник подключения к 1С-Отчетность'");
			
		// Первый шаг 
		Элементы.ПодсказкаПоРезультатуОдобрено.Заголовок 	= СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = '" + Элементы.ПодсказкаПоРезультатуОдобрено.Заголовок + "'" ),
			ДокументЗаявление.Номер,Формат(ДокументЗаявление.Дата,"ДФ=dd.MM.yyyy"),ДокументЗаявление.Организация);
		Элементы.ПодсказкаПоРезультатуОтклонено.Заголовок 	= СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = '" + Элементы.ПодсказкаПоРезультатуОтклонено.Заголовок + "'"),
			ДокументЗаявление.Номер,Формат(ДокументЗаявление.Дата,"ДФ=dd.MM.yyyy"),ДокументЗаявление.Организация);
		Элементы.ГруппаЗакладокЭтап1.ТекущаяСтраница = Элементы.Этап1ПервичноеЗаявление;
			
		// Третий шаг
		Элементы.ПодсказкаКонечныйРезультат.Заголовок 		= СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = '" + Элементы.ПодсказкаКонечныйРезультат.Заголовок + "'"),
			ДокументЗаявление.Организация);
		Элементы.ПодсказкаПоРезультатамШаг2.Заголовок 		= СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = '" + Элементы.ПодсказкаПоРезультатамШаг2.Заголовок + "'"),
			ДокументЗаявление.Организация);
		Элементы.ГруппаЗакладокЭтап3.ТекущаяСтраница 		= Элементы.Этап3ПервичноеЗаявление;
		
	КонецЕсли;
	
	Элементы.ОткрытьВидеоПоПодключению.Видимость = Ложь;
	
КонецПроцедуры

&НаСервере
Функция ПолучитьПараметрыДляЗапускаМетодаБЭД()
	
	Результат = Новый Структура("Организация, КодРегиона", Справочники.Организации.ПустаяСсылка(), "");
		
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
		|	ЗаявлениеАбонентаСпецоператораСвязи.Организация,
		|	ЗаявлениеАбонентаСпецоператораСвязи.АдресЮридический
		|ИЗ
		|	Документ.ЗаявлениеАбонентаСпецоператораСвязи КАК ЗаявлениеАбонентаСпецоператораСвязи
		|ГДЕ
		|	ЗаявлениеАбонентаСпецоператораСвязи.УчетнаяЗапись <> ЗНАЧЕНИЕ(Справочник.УчетныеЗаписиДокументооборота.ПустаяСсылка)
		|	И ЗаявлениеАбонентаСпецоператораСвязи.Ссылка = &ЗаявлениеСсылка";
	
	Запрос.УстановитьПараметр("ЗаявлениеСсылка", ДокументЗаявление.Ссылка);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Если Выборка.Следующий() Тогда
		
		Результат.Вставить("Организация", Выборка.Организация);
		АдресЮридический = Выборка.АдресЮридический;
		Если ЗначениеЗаполнено(АдресЮридический) Тогда
			
			КодРегиона = РегламентированнаяОтчетностьКлиентСервер.РазложитьАдрес(АдресЮридический).Регион;
			
			Если ЗначениеЗаполнено(КодРегиона) Тогда
				
				Результат.Вставить("КодРегиона", КодРегиона);
				
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции