﻿&НаКлиенте
Перем КонтекстЭДОКлиент;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ЗаполнитьДеревоНовое();
	
	Заголовок = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Обмен с контролирующими органами %1'"), Формат(ДатаПоследнегоОбновления, "ДФ='dd.MM.yyyy H:mm'"));
		
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Изменение признака прочтенности" Тогда
		СтрокиДерева = Новое.ПолучитьЭлементы();
		Для Каждого СтрокаДерева Из СтрокиДерева Цикл
			Если СтрокаДерева.Группа <> ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.ПолученныеСообщения") Тогда
				Прервать;				
			КонецЕсли;
			
			Если ТипЗнч(Параметр) = Тип("Структура") И Параметр.Свойство("Ссылка")
				И СтрокаДерева.Ссылка = Параметр.Ссылка Тогда
				СтрокаДерева.НеПрочитано = Ложь;
				СписокСсылок = Новый Массив;
				СписокСсылок.Добавить(Параметр.Ссылка);
				ПометитьНовыеСобытияПросмотреннымиНаСервере(СписокСсылок);
				ОбновитьКоличествоНовых(ЭтаФорма);
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии()

	ПодключитьОбработчикОжидания("ИнициализацияКонтекстЭДО", 0.1, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура ИнициализацияКонтекстЭДО() Экспорт

	ОписаниеОповещения = Новый ОписаниеОповещения("ИнициализацияКонтекстЭДОЗавершение", ЭтотОбъект);
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры	

&НаКлиенте
Процедура ИнициализацияКонтекстЭДОЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	ТекстОшибкиИнициализацииКонтекстаЭДО = Результат.ТекстОшибки;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	ПометитьНовыеСобытияПросмотренными();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыНовое

&НаКлиенте
Процедура ТаблицаНовоеВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Если Поле.Имя = "ТаблицаНовоеОбъектНаименованиеСсылка" Тогда
		Если Элемент.ТекущиеДанные.ЕстьОтвет Тогда // есть ответ
			Если КонтекстЭДОКлиент <> Неопределено Тогда
				Ссылка = Элемент.ТекущиеДанные.Ссылка;
				Если ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗапросНаИнформационноеОбслуживаниеНалогоплательщика") Тогда
					КонтекстЭДОКлиент.ПоказатьОтветНаЗапросИОН(Ссылка);
				ИначеЕсли ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗапросНаИнформационноеОбслуживаниеСтрахователя") Тогда
					КонтекстЭДОКлиент.ПоказатьОтветНаЗапросИОС(Ссылка);
				ИначеЕсли ТипЗнч(Ссылка) = Тип("ДокументСсылка.ЗапросНаВыпискуИзЕГРЮЛ_ЕГРИП") Тогда
					КонтекстЭДОКлиент.ПоказатьОтветНаЗапросВыпискиЕГРЮЛ_ЕГРИП(Ссылка);
				КонецЕсли;				
			КонецЕсли;
		Иначе
			РедактироватьОбъект(Элемент);
		КонецЕсли;
	ИначеЕсли Поле.Имя = "ТаблицаНовоеСтатус" Тогда
		ПоказатьФормуСтатусовОтправкиИзСписка(Элемент);
	Иначе
		РедактироватьОбъект(Элемент);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура РедактироватьОбъект(Элемент)
	
	ТекущиеДанные = Элемент.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		
		Ссылка = ТекущиеДанные.Ссылка;
		Если ЗначениеЗаполнено(Ссылка) Тогда
			ПоказатьЗначение(,Ссылка);		
		КонецЕсли;
		
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ПоказатьФормуСтатусовОтправкиИзСписка(Элемент)
	
	Если КонтекстЭДОКлиент = Неопределено Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибкиИнициализацииКонтекстаЭДО);
	КонецЕсли;
	
	КонтекстЭДОКлиент.ПоказатьФормуСтатусовОтправкиИзСписка(Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура ПометитьНовыеСобытияПросмотренными()

	СписокСсылок = Новый Массив;
	Для Каждого СтрокаСобытие Из Новое.ПолучитьЭлементы() Цикл
		Если ЗначениеЗаполнено(СтрокаСобытие.Ссылка) Тогда
			Если СтрокаСобытие.Группа = ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.ПолученныеСообщения")
				И Не СтрокаСобытие.НеПрочитано
				ИЛИ СтрокаСобытие.Группа <> ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.НезавершенныеОтправки") Тогда
				СписокСсылок.Добавить(СтрокаСобытие.Ссылка);
				СтрокаСобытие.НеПрочитано = Ложь;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	ПометитьНовыеСобытияПросмотреннымиНаСервере(СписокСсылок);
	ОбновитьКоличествоНовых(ЭтаФорма);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ПометитьНовыеСобытияПросмотреннымиНаСервере(СписокСсылок)
	
	ЭлектронныйДокументооборотСКонтролирующимиОрганами.ПометитьНовыеСобытияПросмотренными(СписокСсылок);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьКоличествоНовых(Форма)
	
	Форма.КоличествоНовых = 0;
	
	КоличествоПолученныеСообщения = 0;
	КоличествоОбработанныеЗапросы = 0;
	КоличествоЗавершенныеОтправки = 0;
	
	СтрокаБлокПолученныеСообщения = Неопределено;
	СтрокаБлокОбработанныеЗапросы = Неопределено;
	СтрокаБлокЗавершенныеОтправки = Неопределено;
	
	СтрокиДерева = Форма.Новое.ПолучитьЭлементы();
	Для Каждого СтрокаДерева Из СтрокиДерева Цикл
		Если СтрокаДерева.Группа = ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.ПолученныеСообщения") Тогда
			Если СтрокаДерева.ЭтоЗаголовокБлока Тогда
				СтрокаБлокПолученныеСообщения = СтрокаДерева;
			ИначеЕсли СтрокаДерева.НеПрочитано Тогда
				КоличествоПолученныеСообщения = КоличествоПолученныеСообщения + 1;
			КонецЕсли;
				
		ИначеЕсли СтрокаДерева.Группа = ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.ОбработанныеЗапросы") Тогда 
			Если СтрокаДерева.ЭтоЗаголовокБлока Тогда
				СтрокаБлокОбработанныеЗапросы = СтрокаДерева;
			ИначеЕсли СтрокаДерева.НеПрочитано Тогда
				КоличествоОбработанныеЗапросы = КоличествоОбработанныеЗапросы + 1;
			КонецЕсли;
			
		ИначеЕсли СтрокаДерева.Группа = ПредопределенноеЗначение("Перечисление.ГруппыНовыхСобытийДокументооборотаСКонтролирующимиОрганами.ЗавершенныеОтправки") Тогда 
			Если СтрокаДерева.ЭтоЗаголовокБлока Тогда
				СтрокаБлокЗавершенныеОтправки = СтрокаДерева;
			ИначеЕсли СтрокаДерева.НеПрочитано Тогда
				КоличествоЗавершенныеОтправки = КоличествоЗавершенныеОтправки + 1;
			КонецЕсли;
			
		КонецЕсли;
	КонецЦикла;
	
	Форма.КоличествоНовых = КоличествоПолученныеСообщения + КоличествоОбработанныеЗапросы + КоличествоЗавершенныеОтправки;
	
	Если КоличествоПолученныеСообщения = 0 Тогда
		СтрокаБлокПолученныеСообщения.ЗаголовокБлока = НСтр("ru = 'Полученные сообщения'");	
	Иначе
		СтрокаБлокПолученныеСообщения.ЗаголовокБлока = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Полученные сообщения (%1)'"), КоличествоПолученныеСообщения);
	КонецЕсли;
	
	Если КоличествоОбработанныеЗапросы = 0 Тогда
		СтрокаБлокОбработанныеЗапросы.ЗаголовокБлока = НСтр("ru = 'Обработанные запросы'");	
	Иначе
		СтрокаБлокОбработанныеЗапросы.ЗаголовокБлока = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Обработанные запросы (%1)'"), КоличествоОбработанныеЗапросы);
	КонецЕсли;

	Если КоличествоЗавершенныеОтправки = 0 Тогда
		СтрокаБлокЗавершенныеОтправки.ЗаголовокБлока = НСтр("ru = 'Завершенные отправки'");	
	Иначе
		СтрокаБлокЗавершенныеОтправки.ЗаголовокБлока = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
		НСтр("ru = 'Завершенные отправки (%1)'"), КоличествоЗавершенныеОтправки);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДеревоНовое()
	
	ИнициаторСеанса = Перечисления.ИнициаторыСеансовСвязиСКонтролирующимиОрганами.Автообмен;
	
	ДеревоНовое = РеквизитФормыВЗначение("Новое");
	ЭлектронныйДокументооборотСКонтролирующимиОрганами.ЗаполнитьДеревоНовое(ДеревоНовое, ИнициаторСеанса);
	ЗначениеВРеквизитФормы(ДеревоНовое, "Новое");
		
	ДатаПоследнегоОбновления = ЭлектронныйДокументооборотСКонтролирующимиОрганами.ДатаПоследнегоОбновленияСобытий(ИнициаторСеанса);
	
	Если Не ЗначениеЗаполнено(ДатаПоследнегоОбновления) Тогда
		ДатаПоследнегоОбновления = ТекущаяУниверсальнаяДата();
	КонецЕсли;
	
	ДатаПоследнегоОбновления = МестноеВремя(ДатаПоследнегоОбновления);
	
	ОбновитьКоличествоНовых(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти