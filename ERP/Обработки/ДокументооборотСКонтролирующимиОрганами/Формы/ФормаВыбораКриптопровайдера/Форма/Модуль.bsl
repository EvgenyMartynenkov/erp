﻿
&НаКлиенте
Процедура ОК(Команда)
	Закрыть(ВыборКриптопровайдера);
КонецПроцедуры

&НаКлиенте
Процедура Отмена(Команда)
	Закрыть(ВыборКриптопровайдераПоУмолчанию);
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ВызовИзВторичногоМастера = Параметры.ВызовИзВторичногоМастера;
	
	Если ВызовИзВторичногоМастера Тогда
		Элементы.ТекстПредупреждения.Видимость = Ложь;
		ЭтаФорма.Заголовок = Параметры.ЗаголовокФормы;
	КонецЕсли;
	
	ВыборКриптопровайдераПоУмолчанию = Параметры.ВыборКриптопровайдераПоУмолчанию;
	Если ВыборКриптопровайдераПоУмолчанию <> 0 Тогда
		ВыборКриптопровайдера = ВыборКриптопровайдераПоУмолчанию;
	КонецЕсли;
	
КонецПроцедуры
