﻿
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьСтатьиБюджетов(Команда)
	
	ОткрытьФорму("Справочник.СтатьиБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьАналитикиСтатейБюджетов(Команда)
	
	ОткрытьФорму("ПланВидовХарактеристик.АналитикиСтатейБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСценарии(Команда)
	
	ОткрытьФорму("Справочник.Сценарии.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьВидыБюджетов(Команда)
	
	ОткрытьФорму("Справочник.ВидыБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьПоказателиБюджетов(Команда)
	
	ОткрытьФорму("Справочник.ПоказателиБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСвязиПоказателейБюджетов(Команда)
	
	ОткрытьФорму("РегистрСведений.СвязиПоказателейБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьНефинансовыеПоказатели(Команда)
	
	ОткрытьФорму("Справочник.НефинансовыеПоказателиБюджетов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьШаблоныВводаНефинансовыхПоказателей(Команда)
	
	ОткрытьФорму("Справочник.ШаблоныВводаНефинансовыхПоказателей.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьШагиБюджетногоПроцесса(Команда)
	
	ОткрытьФорму("Справочник.ШагиБюджетныхПроцессов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти
