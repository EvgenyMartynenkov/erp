﻿&НаКлиенте
Перем ОбновитьИнтерфейс;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	// Значения реквизитов формы
	СоставНабораКонстантФормы    = ОбщегоНазначенияУТ.ПолучитьСтруктуруНабораКонстант(НаборКонстант);
	ВнешниеРодительскиеКонстанты = ОбщегоНазначенияУТПовтИсп.ПолучитьСтруктуруРодительскихКонстант(СоставНабораКонстантФормы);
	РежимРаботы 				 = ОбщегоНазначенияПовтИсп.РежимРаботыПрограммы();
	
	ВнешниеРодительскиеКонстанты.Вставить("ИспользоватьПередачиТоваровМеждуОрганизациями");
	ВнешниеРодительскиеКонстанты.Вставить("ИспользоватьЗаказыНаВнутреннееПотребление");
	ВнешниеРодительскиеКонстанты.Вставить("ИспользоватьЗаказыНаСборку");
	
	РежимРаботы.Вставить("СоставНабораКонстантФормы",    Новый ФиксированнаяСтруктура(СоставНабораКонстантФормы));
	РежимРаботы.Вставить("ВнешниеРодительскиеКонстанты", Новый ФиксированнаяСтруктура(ВнешниеРодительскиеКонстанты));
	
	РежимРаботы = Новый ФиксированнаяСтруктура(РежимРаботы);
	
	// Обновление состояния элементов
	УстановитьДоступность();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	ОбновитьИнтерфейсПрограммы();
КонецПроцедуры

&НаКлиенте
// Обработчик оповещения формы.
//
// Параметры:
//	ИмяСобытия - Строка - обрабатывается только событие Запись_НаборКонстант, генерируемое панелями администрирования.
//	Параметр   - Структура - содержит имена констант, подчиненных измененной константе, "вызвавшей" оповещение.
//	Источник   - Строка - имя измененной константы, "вызвавшей" оповещение.
//
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия <> "Запись_НаборКонстант" Тогда
		Возврат; // такие событие не обрабатываются
	КонецЕсли;
	
	// Если это изменена константа, расположенная в другой форме и влияющая на значения констант этой формы,
	// то прочитаем значения констант и обновим элементы этой формы.
	Если РежимРаботы.ВнешниеРодительскиеКонстанты.Свойство(Источник)
	 ИЛИ (ТипЗнч(Параметр) = Тип("Структура")
	 		И ОбщегоНазначенияУТКлиентСервер.ПолучитьОбщиеКлючиСтруктур(
	 			Параметр, РежимРаботы.ВнешниеРодительскиеКонстанты).Количество() > 0) Тогда
		
		ЭтаФорма.Прочитать();
		УстановитьДоступность();
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ПрочееОприходованиеТоваровПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьОрдерныеСкладыПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьУправлениеДоставкойПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьАдресаДоставкиТолькоВДиалогеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент, Ложь);
КонецПроцедуры

&НаКлиенте
Процедура ШаблонЭтикеткиДляДоставкиПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент, Ложь);
КонецПроцедуры

&НаКлиенте
Процедура ШаблонЭтикеткиДляДоставкиНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Отбор = Новый Структура(
		"Назначение",
		ПредопределенноеЗначение("Перечисление.НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаДляДоставки"));
	
	ШаблонЭтикетки = Неопределено;

	
	ОткрытьФорму("Справочник.ШаблоныЭтикетокИЦенников.ФормаВыбора", Новый Структура("Отбор", Отбор),,,,, Новый ОписаниеОповещения("ШаблонЭтикеткиДляДоставкиНачалоВыбораЗавершение", ЭтотОбъект, Новый Структура("Элемент", Элемент)), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ШаблонЭтикеткиДляДоставкиНачалоВыбораЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    Элемент = ДополнительныеПараметры.Элемент;
    
    
    ШаблонЭтикетки =
    Результат;
    Если ШаблонЭтикетки = Неопределено Тогда
        Возврат;
    КонецЕсли;
    
    НаборКонстант.ШаблонЭтикеткиДляДоставки = ШаблонЭтикетки;
    
    Подключаемый_ПриИзмененииРеквизита(Элемент, Ложь);

КонецПроцедуры

&НаКлиенте
Процедура ШаблонЭтикеткиУпаковочногоЛистаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Отбор = Новый Структура(
		"Назначение",
		ПредопределенноеЗначение("Перечисление.НазначенияШаблоновЭтикетокИЦенников.ЭтикеткаУпаковочныхЛистов"));
	
	ШаблонЭтикетки = Неопределено;

	
	ОткрытьФорму("Справочник.ШаблоныЭтикетокИЦенников.ФормаВыбора", Новый Структура("Отбор", Отбор),,,,, Новый ОписаниеОповещения("ШаблонЭтикеткиУпаковочногоЛистаНачалоВыбораЗавершение", ЭтотОбъект, Новый Структура("Элемент", Элемент)), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

&НаКлиенте
Процедура ШаблонЭтикеткиУпаковочногоЛистаНачалоВыбораЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    Элемент = ДополнительныеПараметры.Элемент;
    
    
    ШаблонЭтикетки =
    Результат;
    Если ШаблонЭтикетки = Неопределено Тогда
        Возврат;
    КонецЕсли;
    
    НаборКонстант.ШаблонЭтикеткиУпаковочногоЛиста = ШаблонЭтикетки;
    
    Подключаемый_ПриИзмененииРеквизита(Элемент, Ложь);

КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьНесколькоСкладовПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПеремещениеТоваровПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьЗаказыНаПеремещениеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьПеремещениеПоНесколькимЗаказамПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыПеремещенийТоваровПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыЗаказовНаПеремещениеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыЗаказовНаВнутреннееПотреблениеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыВнутреннихПотребленийТоваровПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыЗаказовНаСборкуПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьСтатусыСборокТоваровПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ПеремещатьТоварыДругихОрганизацийПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьВнутреннееПотреблениеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьЗаказыНаВнутреннееПотреблениеПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказамПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура СборкаРазборкаПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьЗаказыНаСборкуПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьУпаковочныеЛистыПриИзменении(Элемент)
	Подключаемый_ПриИзмененииРеквизита(Элемент);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Клиент

&НаКлиенте
Процедура Подключаемый_ПриИзмененииРеквизита(Элемент, ОбновлятьИнтерфейс = Истина)
	
	Результат = ПриИзмененииРеквизитаСервер(Элемент.Имя);
	
	Если ОбновлятьИнтерфейс Тогда
		#Если НЕ ВебКлиент Тогда
		ПодключитьОбработчикОжидания("ОбновитьИнтерфейсПрограммы", 1, Истина);
		ОбновитьИнтерфейс = Истина;
		#КонецЕсли
	КонецЕсли;
	
	СтандартныеПодсистемыКлиент.ПоказатьРезультатВыполнения(ЭтаФорма, Результат);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьИнтерфейсПрограммы()
	
	#Если НЕ ВебКлиент Тогда
	Если ОбновитьИнтерфейс = Истина Тогда
		ОбновитьИнтерфейс = Ложь;
		ОбновитьИнтерфейс();
	КонецЕсли;
	#КонецЕсли
	
КонецПроцедуры

#КонецОбласти

#Область ВызовСервера

&НаСервере
Функция ПриИзмененииРеквизитаСервер(ИмяЭлемента)
	
	Результат = Новый Структура;
	
	РеквизитПутьКДанным = Элементы[ИмяЭлемента].ПутьКДанным;
	
	СохранитьЗначениеРеквизита(РеквизитПутьКДанным, Результат);
	
	УстановитьДоступность(РеквизитПутьКДанным);
	
	ОбновитьПовторноИспользуемыеЗначения();
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти

#Область Сервер

&НаСервере
Процедура СохранитьЗначениеРеквизита(РеквизитПутьКДанным, Результат)
	
	// Сохранение значений реквизитов, не связанных с константами напрямую (в отношении один-к-одному).
	Если РеквизитПутьКДанным = "" Тогда
		Возврат;
	КонецЕсли;
	
	// Определение имени константы.
	КонстантаИмя = "";
	Если НРег(Лев(РеквизитПутьКДанным, 14)) = НРег("НаборКонстант.") Тогда
		// Если путь к данным реквизита указан через "НаборКонстант".
		КонстантаИмя = Сред(РеквизитПутьКДанным, 15);
	Иначе
		// Определение имени и запись значения реквизита в соответствующей константе из "НаборКонстант".
		// Используется для тех реквизитов формы, которые связаны с константами напрямую (в отношении один-к-одному).
	КонецЕсли;
	
	// Сохранения значения константы.
	Если КонстантаИмя <> "" Тогда
		КонстантаМенеджер = Константы[КонстантаИмя];
		КонстантаЗначение = НаборКонстант[КонстантаИмя];
		
		Если КонстантаМенеджер.Получить() <> КонстантаЗначение Тогда
			КонстантаМенеджер.Установить(КонстантаЗначение);
		КонецЕсли;
		
		Если ОбщегоНазначенияУТПовтИсп.ЕстьПодчиненныеКонстанты(КонстантаИмя, КонстантаЗначение) Тогда
			ЭтаФорма.Прочитать();
		КонецЕсли;
		
		СтандартныеПодсистемыКлиентСервер.РезультатВыполненияДобавитьОповещениеОткрытыхФорм(Результат,
			"Запись_НаборКонстант", ОбщегоНазначенияУТПовтИсп.ПолучитьСтруктуруПодчиненныхКонстант(КонстантаИмя), КонстантаИмя);
		// СтандартныеПодсистемы.ВариантыОтчетов
		ВариантыОтчетов.ДобавитьОповещениеПриИзмененииЗначенияКонстанты(Результат, КонстантаМенеджер);
		// Конец СтандартныеПодсистемы.ВариантыОтчетов
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура УстановитьДоступность(РеквизитПутьКДанным = "")
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьОрдерныеСклады" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьОрдерныеСклады;
		
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьОрдерныеСклады, ЗначениеКонстанты);
		Элементы.ИспользоватьУпаковочныеЛисты.Доступность = ЗначениеКонстанты;
		
		ИспользоватьСерииНоменклатуры = Константы.ИспользоватьСерииНоменклатуры.Получить();
		Элементы.ГруппаКомментарийСерииНаОрдерныхСкладах.Видимость = Не ЗначениеКонстанты И ИспользоватьСерииНоменклатуры;
		
	КонецЕсли;
		
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьУправлениеДоставкой" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьУправлениеДоставкой;
		
		Элементы.РедактироватьАдресаДоставкиТолькоВДиалоге.Доступность		= ЗначениеКонстанты;
		Элементы.ШаблонЭтикеткиДляДоставки.Доступность 						= ЗначениеКонстанты;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьВнутреннееПотребление" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьВнутреннееПотребление;
		
		Элементы.ИспользоватьЗаказыНаВнутреннееПотребление.Доступность 			  = ЗначениеКонстанты;
		Элементы.ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам.Доступность = ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыВнутреннихПотребленийТоваров.Доступность 	  = ЗначениеКонстанты;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьСборкуРазборку" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьСборкуРазборку;
		
		Элементы.ИспользоватьЗаказыНаСборку.Доступность 	  = ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыСборокТоваров.Доступность = ЗначениеКонстанты;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьСтатусыСборокТоваров" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		ЗначениеКонстанты = Константы.ИспользоватьСтатусыСборокТоваров.Получить();
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьСтатусыСборокТоваров, ЗначениеКонстанты);
		
	КонецЕсли;

	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьСтатусыВнутреннихПотребленийТоваров" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		ЗначениеКонстанты = Константы.ИспользоватьСтатусыВнутреннихПотребленийТоваров.Получить();
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьСтатусыВнутреннихПотребленийТоваров, ЗначениеКонстанты);
		
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьСтатусыЗаказовНаСборку" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		ЗначениеКонстанты = Константы.ИспользоватьСтатусыЗаказовНаСборку.Получить();
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьСтатусыЗаказовНаСборку, ЗначениеКонстанты);
		
	КонецЕсли;

	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьСтатусыЗаказовНаВнутреннееПотребление" ИЛИ РеквизитПутьКДанным = "" Тогда
		
		ЗначениеКонстанты = Константы.ИспользоватьСтатусыЗаказовНаВнутреннееПотребление.Получить();
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьСтатусыЗаказовНаВнутреннееПотребление, ЗначениеКонстанты);
		
	КонецЕсли;
		
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьЗаказыНаСборку" ИЛИ РеквизитПутьКДанным = "" Тогда
		Элементы.ИспользоватьСтатусыЗаказовНаСборку.Доступность = НаборКонстант.ИспользоватьЗаказыНаСборку;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьЗаказыНаВнутреннееПотребление" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьЗаказыНаВнутреннееПотребление;
		
		Элементы.ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам.Доступность = ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыЗаказовНаВнутреннееПотребление.Доступность    = ЗначениеКонстанты;
	КонецЕсли;
		
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьНесколькоСкладов" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьНесколькоСкладов;
		
		Если ЗначениеКонстанты Тогда
			Элементы.ГруппаСтраницыСклады.ТекущаяСтраница = Элементы.ГруппаПояснениеНесколькоСкладов;
		Иначе
			Элементы.ГруппаСтраницыСклады.ТекущаяСтраница = Элементы.ГруппаПояснениеОдинСклад;
		КонецЕсли;
		
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьНесколькоСкладов, ЗначениеКонстанты);
		
		Элементы.ИспользоватьПеремещениеТоваров.Доступность 			= ЗначениеКонстанты;
		Элементы.ИспользоватьЗаказыНаПеремещение.Доступность 	   		= НаборКонстант.ИспользоватьПеремещениеТоваров;
		Элементы.ИспользоватьСтатусыПеремещенийТоваров.Доступность 		= НаборКонстант.ИспользоватьПеремещениеТоваров;
		Элементы.ИспользоватьПеремещениеПоНесколькимЗаказам.Доступность = НаборКонстант.ИспользоватьЗаказыНаПеремещение;
		Элементы.ИспользоватьСтатусыЗаказовНаПеремещение.Доступность    = НаборКонстант.ИспользоватьЗаказыНаПеремещение;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьЗаказыНаВнутреннееПотребление" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьЗаказыНаВнутреннееПотребление;
		
		Элементы.ИспользоватьВнутреннееПотреблениеПоНесколькимЗаказам.Доступность = ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыЗаказовНаВнутреннееПотребление.Доступность    = ЗначениеКонстанты;
	КонецЕсли;
		
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьПеремещениеТоваров" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты 		   = НаборКонстант.ИспользоватьПеремещениеТоваров;
		ПередачиМеждуОрганизациями = Константы.ИспользоватьПередачиТоваровМеждуОрганизациями.Получить();
		
		Элементы.ИспользоватьЗаказыНаПеремещение.Доступность 	   		= ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыПеремещенийТоваров.Доступность 		= ЗначениеКонстанты;
		Элементы.ИспользоватьПеремещениеПоНесколькимЗаказам.Доступность = НаборКонстант.ИспользоватьЗаказыНаПеремещение;
		Элементы.ИспользоватьСтатусыЗаказовНаПеремещение.Доступность    = НаборКонстант.ИспользоватьЗаказыНаПеремещение;
		
		Элементы.ПеремещатьТоварыДругихОрганизаций.Доступность 				  = ЗначениеКонстанты И ПередачиМеждуОрганизациями;
		Элементы.ГруппаКомментарийПеремещатьТоварыДругихОрганизаций.Видимость =	НЕ ПередачиМеждуОрганизациями;
	КонецЕсли;
		
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьЗаказыНаПеремещение" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьЗаказыНаПеремещение;
		
		Элементы.ИспользоватьПеремещениеПоНесколькимЗаказам.Доступность = ЗначениеКонстанты;
		Элементы.ИспользоватьСтатусыЗаказовНаПеремещение.Доступность    = ЗначениеКонстанты;
	КонецЕсли;
	
	Если РеквизитПутьКДанным = "НаборКонстант.ИспользоватьУпаковочныеЛисты" ИЛИ РеквизитПутьКДанным = "" Тогда
		ЗначениеКонстанты = НаборКонстант.ИспользоватьУпаковочныеЛисты;
		
		ОбщегоНазначенияУТКлиентСервер.ОтображениеПредупрежденияПриРедактировании(
			Элементы.ИспользоватьУпаковочныеЛисты, ЗначениеКонстанты);
		Элементы.ШаблонЭтикеткиУпаковочногоЛиста.Доступность = ЗначениеКонстанты;
	КонецЕсли;
	
	ОбменДаннымиУТУП.УстановитьДоступностьНастроекУзлаИнформационнойБазы(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
