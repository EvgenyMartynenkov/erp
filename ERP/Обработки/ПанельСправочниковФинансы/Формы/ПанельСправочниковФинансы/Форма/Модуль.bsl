﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	УправлениеЭлементами();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "Запись_НаборКонстант" Тогда
		// Изменены настройки программы в панелях администрирования
		УправлениеЭлементами();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

#Область ОбщиеСправочники

&НаКлиенте
Процедура ОткрытьСправочникСтатьиДвиженияДенежныхСредств(Команда)
	
	ОткрытьФорму("Справочник.СтатьиДвиженияДенежныхСредств.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникЭквайринговыеТерминалы(Команда)
	
	ОткрытьФорму("Справочник.ЭквайринговыеТерминалы.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникСпособыРаспределенияПоНаправлениямДеятельности(Команда)
	
	ОткрытьФорму("Справочник.СпособыРаспределенияПоНаправлениямДеятельности.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникНастройкаРаспределенияПоНаправлениямДеятельности(Команда)
	
	ОткрытьФорму("РегистрСведений.НастройкаРаспределенияПоНаправлениямДеятельности.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникНаправленияДеятельности(Команда)
	
	ОткрытьФорму("Справочник.НаправленияДеятельности.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникПрочиеРасходы(Команда)
	
	ОткрытьФорму("Справочник.ПрочиеРасходы.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникСтатьиДоходов(Команда)
	
	ОткрытьФорму("ПланВидовХарактеристик.СтатьиДоходов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникСтатьиРасходов(Команда)
	
	ОткрытьФорму("ПланВидовХарактеристик.СтатьиРасходов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСправочникСтатьиАктивовПассивов(Команда)
	
	ОткрытьФорму("ПланВидовХарактеристик.СтатьиАктивовПассивов.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьГруппыАналитическогоУчетаНоменклатуры(Команда)
	
	ОткрытьФорму("Справочник.ГруппыАналитическогоУчетаНоменклатуры.ФормаСписка", , ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭлементами()
	
	//Функциональные опции
	ИспользоватьОплатуПлатежнымиКартами		= ПолучитьФункциональнуюОпцию("ИспользоватьОплатуПлатежнымиКартами");
	ФормироватьФинансовыйРезультат			= ПолучитьФункциональнуюОпцию("ФормироватьФинансовыйРезультат");
	ИспользоватьУчетПрочихДоходовРасходов	= ПолучитьФункциональнуюОпцию("ИспользоватьУчетПрочихДоходовРасходов");
	ИспользоватьГруппыАналитическогоУчета	= ПолучитьФункциональнуюОпцию("ИспользоватьГруппыАналитическогоУчетаНоменклатуры");
	
	//Право чтения
	ПравоДоступаЭквайринговыеТерминалы		= ПравоДоступа("Просмотр", Метаданные.Справочники.ЭквайринговыеТерминалы);
	ПравоДоступаНаправленияДеятельности		= ПравоДоступа("Просмотр", Метаданные.Справочники.НаправленияДеятельности);
	ПравоДоступаПрочиеРасходы				= ПравоДоступа("Просмотр", Метаданные.Справочники.ПрочиеРасходы);
	ПравоДоступаСтатьиДоходов				= ПравоДоступа("Просмотр", Метаданные.ПланыВидовХарактеристик.СтатьиДоходов);
	ПравоДоступаСтатьиРасходов				= ПравоДоступа("Просмотр", Метаданные.ПланыВидовХарактеристик.СтатьиРасходов);
	ПравоДоступаСтатьиПрочихАктивовПассивов	= ПравоДоступа("Просмотр", Метаданные.ПланыВидовХарактеристик.СтатьиАктивовПассивов);
	ПравоДоступаГруппыАналитическогоУчета   = ПравоДоступа("Просмотр", Метаданные.Справочники.ГруппыАналитическогоУчетаНоменклатуры);
	ПравоДоступаСтатьиДвиженияДенежныхСредств = ПравоДоступа("Просмотр", Метаданные.Справочники.СтатьиДвиженияДенежныхСредств);
	ПравоДоступаСпособыРаспределенияПоНаправлениямДеятельности = ПравоДоступа("Просмотр", Метаданные.Справочники.СпособыРаспределенияПоНаправлениямДеятельности);
	ПравоДоступаНастройкаРаспределенияПоНаправлениямДеятельности = ПравоДоступа("Просмотр", Метаданные.РегистрыСведений.НастройкаРаспределенияПоНаправлениямДеятельности);
	
	//ДенежныеСредства
	Элементы.ГруппаНастроекСтатьиДвиженияДенежныхСредств.Видимость = ПравоДоступаСтатьиДвиженияДенежныхСредств;
	Элементы.ГруппаНастроекЭквайринговыеТерминалы.Видимость = ИспользоватьОплатуПлатежнымиКартами
		И ПравоДоступаЭквайринговыеТерминалы;
	
	//ФинансовыйРезультат
	Элементы.ГруппаНастроекСпособыРаспределенияПоНаправлениямДеятельности.Видимость = ФормироватьФинансовыйРезультат 
		И ПравоДоступаСпособыРаспределенияПоНаправлениямДеятельности;
	Элементы.ГруппаНастроекНастройкаРаспределенияПоНаправлениямДеятельности.Видимость = ФормироватьФинансовыйРезультат
		И ПравоДоступаНастройкаРаспределенияПоНаправлениямДеятельности;
	
	//НастройкаАналитики
	Элементы.ГруппаНастроекНаправленияДеятельности.Видимость = ФормироватьФинансовыйРезультат
		И ПравоДоступаНаправленияДеятельности;
	Элементы.ГруппаНастроекПрочиеРасходы.Видимость = ИспользоватьУчетПрочихДоходовРасходов 
		И ПравоДоступаПрочиеРасходы;
	Элементы.ГруппаНастроекСтатьиДоходов.Видимость = ИспользоватьУчетПрочихДоходовРасходов
		И ПравоДоступаСтатьиДоходов;
	Элементы.ГруппаНастроекСтатьиРасходов.Видимость = ИспользоватьУчетПрочихДоходовРасходов
		И ПравоДоступаСтатьиРасходов;
	Элементы.ГруппаНастроекСтатьиАктивовПассивов.Видимость = ПравоДоступаСтатьиПрочихАктивовПассивов;
	Элементы.ОткрытьГруппыАналитическогоУчетаНоменклатуры.Видимость = ИспользоватьГруппыАналитическогоУчета
		И ПравоДоступаГруппыАналитическогоУчета;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти