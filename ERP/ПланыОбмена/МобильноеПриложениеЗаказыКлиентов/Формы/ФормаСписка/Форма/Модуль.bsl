﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("НеОтображатьЭтотУзел") И Параметры.НеОтображатьЭтотУзел Тогда
		
		ЭлементОтбора = Список.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Ссылка");
		ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
		ЭлементОтбора.Использование = Истина;
		ЭлементОтбора.ПравоеЗначение = ПланыОбмена.МобильноеПриложениеЗаказыКлиентов.ЭтотУзел();
		ЭлементОтбора.РежимОтображения = РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Недоступный;
		
	КонецЕсли;
	Заголовок = Нстр("ru = 'Настройки синхронизации для пользователей мобильного приложения ""1С:Заказы""'");
КонецПроцедуры

#КонецОбласти


