﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПриЗаписи(Отказ)
	ОрганизационнаяСтруктураСобытия.ОбновитьСтруктуруПредприятия(Отказ);
КонецПроцедуры

#КонецОбласти

#КонецЕсли