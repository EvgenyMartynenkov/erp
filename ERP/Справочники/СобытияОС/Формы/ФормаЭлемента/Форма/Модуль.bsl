﻿
////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ОткрытьФормуРедактированияКомментария(Элемент.ТекстРедактирования, Объект.Комментарий, Модифицированность);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	// Ограничение списка выбора поля "Вид события ОС"
	Если Параметры.Свойство("ВидСобытияОСДоступныеЗначения") Тогда
		Элементы.ВидСобытияОС.РежимВыбораИзСписка = Истина;
		Элементы.ВидСобытияОС.СписокВыбора.ЗагрузитьЗначения(Параметры.ВидСобытияОСДоступныеЗначения);
	КонецЕсли;
	
КонецПроцедуры
