﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриСозданииЧтенииНаСервере();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	ПриСозданииЧтенииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Проверки = Новый Структура;
	Проверки.Вставить("СтатьяРасходов", "АналитикаРасходов");
	
	Если РежимУчетаВзносов = 1 Тогда
		Проверки.Вставить("СтатьяРасходовВзносов", "АналитикаРасходовВзносов");
	ИначеЕсли РежимУчетаВзносов = 2 Тогда
		Проверки.Вставить("СтатьяРасходовПФР", "АналитикаРасходовПФР");
		Проверки.Вставить("СтатьяРасходовФСС", "АналитикаРасходовФСС");
		Проверки.Вставить("СтатьяРасходовФОМС", "АналитикаРасходовФОМС");
	КонецЕсли;
	
	Если Не Объект.ОсобыйСоответствуетОсновному Тогда
		Проверки.Вставить("СтатьяРасходовЕНВД", "АналитикаРасходовЕНВД");
	КонецЕсли;
	
	Если РежимУчетаВзносовЕНВД = 1 Тогда
		Проверки.Вставить("СтатьяРасходовВзносовЕНВД", "АналитикаРасходовВзносовЕНВД");
	ИначеЕсли РежимУчетаВзносовЕНВД = 2 Тогда
		Проверки.Вставить("СтатьяРасходовЕНВДПФР", "АналитикаРасходовЕНВДПФР");
		Проверки.Вставить("СтатьяРасходовЕНВДФСС", "АналитикаРасходовЕНВДФСС");
		Проверки.Вставить("СтатьяРасходовЕНВДФОМС", "АналитикаРасходовЕНВДФОМС");
	КонецЕсли;
	
	Для Каждого Проверка Из Проверки Цикл
		
		Если Проверка.Ключ = "СтатьяРасходовВзносов" Тогда
			
			Если Не ЗначениеЗаполнено(СтатьяРасходовВзносов) Тогда
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					НСтр("ru = 'Статья расходов для взносов не заполнена.'"),
					,
					"СтатьяРасходовВзносов",
					,
					Отказ);
			КонецЕсли;
			
			Если АналитикаРасходовВзносовОбязательна И Не ЗначениеЗаполнено(АналитикаРасходовВзносов) Тогда
				
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					НСтр("ru = 'Аналитика расходов не заполнена.'"),
					,
					"АналитикаРасходовВзносов",
					,
					Отказ);
			КонецЕсли;
			
			Продолжить;
		КонецЕсли;
		
		Если Проверка.Ключ = "СтатьяРасходовВзносовЕНВД" Тогда
			
			Если Не ЗначениеЗаполнено(СтатьяРасходовВзносовЕНВД) Тогда
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					НСтр("ru = 'Статья расходов для взносов не заполнена.'"),
					,
					"СтатьяРасходовВзносовЕНВД",
					,
					Отказ);
			КонецЕсли;
			
			Если АналитикаРасходовВзносовЕНВДОбязательна И Не ЗначениеЗаполнено(АналитикаРасходовВзносовЕНВД) Тогда
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					НСтр("ru = 'Аналитика расходов не заполнена.'"),
					,
					"АналитикаРасходовВзносовЕНВД",
					,
					Отказ);
				
			КонецЕсли;
			Продолжить;
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Объект[Проверка.Ключ]) Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				НСтр("ru = 'Статья расходов не заполнена.'"),
				,
				Проверка.Ключ,
				"Объект",
				Отказ);
		КонецЕсли;
	
		Если Не ЭтаФорма[Проверка.Значение + "Автоматически"] Тогда
			ПланыВидовХарактеристик.СтатьиРасходов.ПроверитьЗаполнениеАналитик(
				Объект,
				"" + Проверка.Ключ + ", " + Проверка.Значение,
				МассивНепроверяемыхРеквизитов,
				Отказ);
		КонецЕсли;
		
	КонецЦикла;
	
	МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовВзносов");
	МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовВзносовЕНВД");
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(
		ПроверяемыеРеквизиты,
		МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Если РежимУчетаВзносов = 0 Тогда
		Статья = Объект.СтатьяРасходов;
		Аналитика = Объект.АналитикаРасходов;
	ИначеЕсли РежимУчетаВзносов = 1 Тогда
		Статья = СтатьяРасходовВзносов;
		Аналитика = АналитикаРасходовВзносов;
	КонецЕсли;
	
	Если РежимУчетаВзносов <> 2 Тогда
		ТекущийОбъект.СтатьяРасходовПФР = Статья;
		ТекущийОбъект.АналитикаРасходовПФР = Аналитика;
		ТекущийОбъект.СтатьяРасходовФСС = Статья;
		ТекущийОбъект.АналитикаРасходовФСС = Аналитика;
		ТекущийОбъект.СтатьяРасходовФОМС = Статья;
		ТекущийОбъект.АналитикаРасходовФОМС = Аналитика;
	КонецЕсли;
	
	Если РежимУчетаВзносовЕНВД = 0 Тогда
		Статья = Объект.СтатьяРасходовЕНВД;
		Аналитика = Объект.АналитикаРасходовЕНВД;
	ИначеЕсли РежимУчетаВзносовЕНВД = 1 Тогда
		Статья = СтатьяРасходовВзносовЕНВД;
		Аналитика = АналитикаРасходовВзносовЕНВД;
	КонецЕсли;
	
	Если РежимУчетаВзносовЕНВД <> 2 Тогда
		ТекущийОбъект.СтатьяРасходовЕНВДПФР = Статья;
		ТекущийОбъект.АналитикаРасходовЕНВДПФР = Аналитика;
		ТекущийОбъект.СтатьяРасходовЕНВДФСС = Статья;
		ТекущийОбъект.АналитикаРасходовЕНВДФСС = Аналитика;
		ТекущийОбъект.СтатьяРасходовЕНВДФОМС = Статья;
		ТекущийОбъект.АналитикаРасходовЕНВДФОМС = Аналитика;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовУправленияФормы

&НаКлиенте
Процедура СтатьяРасходовПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходов) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходов, Объект.АналитикаРасходов, "АналитикаРасходов");
	Иначе
		Объект.АналитикаРасходов = Неопределено;
		АналитикаРасходовОбязательна = Ложь;
		АналитикаРасходовАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходов", АналитикаРасходовАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбщаяСтатьяВзносовПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(СтатьяРасходовВзносов) Тогда
		СтатьяРасходовПриИзмененииНаСервере(СтатьяРасходовВзносов, АналитикаРасходовВзносов, "АналитикаРасходовВзносов");
	Иначе
		Объект.АналитикаРасходовВзносов = Неопределено;
		АналитикаРасходовВзносовОбязательна = Ложь;
		АналитикаРасходовВзносовАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовВзносов", АналитикаРасходовВзносовАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовПФРПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовПФР) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовПФР, Объект.АналитикаРасходовПФР, "АналитикаРасходовПФР");
	Иначе
		Объект.АналитикаРасходовПФР = Неопределено;
		АналитикаРасходовПФРОбязательна = Ложь;
		АналитикаРасходовПФРАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовПФР", АналитикаРасходовПФРАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовФССПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовФСС) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовФСС, Объект.АналитикаРасходовФСС, "АналитикаРасходовФСС");
	Иначе
		Объект.АналитикаРасходовФСС = Неопределено;
		АналитикаРасходовФССОбязательна = Ложь;
		АналитикаРасходовФССАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовФСС", АналитикаРасходовФССАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовФОМСПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовФОМС) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовФОМС, Объект.АналитикаРасходовФОМС, "АналитикаРасходовФОМС");
	Иначе
		Объект.АналитикаРасходовФОМС = Неопределено;
		АналитикаРасходовФОМСОбязательна = Ложь;
		АналитикаРасходовФОМСАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовФОМС", АналитикаРасходовФОМСАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовЕНВДПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВД) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовЕНВД, Объект.АналитикаРасходовЕНВД, "АналитикаРасходовЕНВД");
	Иначе
		Объект.АналитикаРасходовЕНВД = Неопределено;
		АналитикаРасходовЕНВДОбязательна = Ложь;
		АналитикаРасходовЕНВДАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовЕНВД", АналитикаРасходовЕНВДАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбщаяСтатьяВзносовЕНВДПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(СтатьяРасходовВзносовЕНВД) Тогда
		СтатьяРасходовПриИзмененииНаСервере(СтатьяРасходовВзносовЕНВД, АналитикаРасходовВзносовЕНВД, "АналитикаРасходовВзносовЕНВД");
	Иначе
		Объект.АналитикаРасходовВзносовЕНВД = Неопределено;
		АналитикаРасходовВзносовЕНВДОбязательна = Ложь;
		АналитикаРасходовВзносовЕНВДАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовВзносовЕНВД", АналитикаРасходовВзносовЕНВДАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовЕНВДПФРПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВДПФР) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовЕНВДПФР, Объект.АналитикаРасходовЕНВДПФР, "АналитикаРасходовЕНВДПФР");
	Иначе
		Объект.АналитикаРасходовЕНВДПФР = Неопределено;
		АналитикаРасходовЕНВДПФРОбязательна = Ложь;
		АналитикаРасходовЕНВДПФРАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовЕНВДПФР", АналитикаРасходовЕНВДПФРАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовЕНВДФССПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВДФСС) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовЕНВДФСС, Объект.АналитикаРасходовЕНВДФСС, "АналитикаРасходовЕНВДФСС");
	Иначе
		Объект.АналитикаРасходовЕНВДФСС = Неопределено;
		АналитикаРасходовЕНВДФССОбязательна = Ложь;
		АналитикаРасходовЕНВДФССАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовЕНВДФСС", АналитикаРасходовЕНВДФССАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура СтатьяРасходовЕНВДФОМСПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходовЕНВДФОМС) Тогда
		СтатьяРасходовПриИзмененииНаСервере(Объект.СтатьяРасходовЕНВДФОМС, Объект.АналитикаРасходовЕНВДФОМС, "АналитикаРасходовЕНВДФОМС");
	Иначе
		Объект.АналитикаРасходовЕНВДФОМС = Неопределено;
		АналитикаРасходовЕНВДФОМСОбязательна = Ложь;
		АналитикаРасходовЕНВДФОМСАвтоматически = Ложь;
		ЗаполнитьПодсказкиВвода("АналитикаРасходовЕНВДФОМС", АналитикаРасходовЕНВДФОМСАвтоматически);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура РежимАналитикиВзносовПриИзменении(Элемент)
	
	НастроитьЭлементыФормы("НеЕНВД");
	
КонецПроцедуры

&НаКлиенте
Процедура РежимАналитикиВзносовЕНВДПриИзменении(Элемент)
	
	НастроитьЭлементыФормы("ЕНВД");
	
КонецПроцедуры

&НаКлиенте
Процедура ОсобыйСоответствуетОсновномуПриИзменении(Элемент)
	
	ОсобыйСоответствуетОсновномуПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходов = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовВзносовЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		АналитикаРасходовВзносов = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовПФРНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовПФРЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовПФРОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовПФР = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовПФРАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовПФРОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФССНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовФССЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФССОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовФСС = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФССАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФССОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФОМСНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовФОМСЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФОМСОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовФОМС = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФОМСАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовФОМСОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЕНВДЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовЕНВД = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовЕНВДНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовВзносовЕНВДЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовЕНВДОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		АналитикаРасходовВзносовЕНВД = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовЕНВДАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовВзносовЕНВДОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДПФРНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЕНВДПФРЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДПФРОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовЕНВДПФР = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДПФРАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДПФРОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФССНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЕНВДФССЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФССОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовЕНВДФСС = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФССАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФССОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФОМСНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	Если АналитикаРасходовЕНВДФОМСЗаказРеализация Тогда
		СтандартнаяОбработка = Ложь;
		ОткрытьФорму("ОбщаяФорма.ВыборАналитикиРасходов", , Элемент);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФОМСОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ТипЗнч(ВыбранноеЗначение) = Тип("Структура") Тогда
		Объект.АналитикаРасходовФОМС = ВыбранноеЗначение.АналитикаРасходов;
		СтандартнаяОбработка = Ложь;
		Модифицированность = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФОМСАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура АналитикаРасходовЕНВДФОМСОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Если ЗначениеЗаполнено(Текст) Тогда
		СтандартнаяОбработка = Ложь;
		АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОФормление()
	
	УсловноеОформление.Элементы.Очистить();
	
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(УсловноеОформление,
		"СтатьяРасходов, АналитикаРасходов, СтатьяРасходовВзносов, АналитикаРасходовВзносов, СтатьяРасходовПФР, АналитикаРасходовПФР,
		|СтатьяРасходовФСС, АналитикаРасходовФСС, СтатьяРасходовФОМС, АналитикаРасходовФОМС");
		
	ПланыВидовХарактеристик.СтатьиРасходов.УстановитьУсловноеОформлениеАналитик(УсловноеОформление,
		"СтатьяРасходовЕНВД, АналитикаРасходовЕНВД, СтатьяРасходовВзносовЕНВД, АналитикаРасходовВзносовЕНВД, СтатьяРасходовЕНВДПФР, АналитикаРасходовЕНВДПФР,
		|СтатьяРасходовЕНВДФСС, АналитикаРасходовЕНВДФСС, СтатьяРасходовЕНВДФОМС, АналитикаРасходовЕНВДФОМС");
	
КонецПроцедуры

&НаСервере
Процедура НастроитьЭлементыФормы(ПризнакОчисткиСтатей = Неопределено)
	
	ОчиститьСтатьиРасходов(ПризнакОчисткиСтатей);
	
	Элементы.ГруппаВзносыОбщаяАналитика.Видимость = РежимУчетаВзносов = 1;
	Элементы.ГруппаВзносыВФонды.Видимость = РежимУчетаВзносов = 2;
	
	Если Объект.ОсобыйСоответствуетОсновному Тогда
		Элементы.ОсобыйСоответствуетОсновномуСтраницы.ТекущаяСтраница = Элементы.ГруппаПустаяАналитикаЕНВД;
		Элементы.ГруппаНастройкиСтатейЕНВД.Доступность = Ложь;
	Иначе
		Элементы.ОсобыйСоответствуетОсновномуСтраницы.ТекущаяСтраница = Элементы.ГруппаОсновнаяАналитикаЕНВД;
		Элементы.ГруппаНастройкиСтатейЕНВД.Доступность = Истина;
	КонецЕсли;
	
	Элементы.ГруппаВзносыОбщаяАналитикаЕНВД.Видимость = РежимУчетаВзносовЕНВД = 1;
	Элементы.ГруппаВзносыВФондыЕНВД.Видимость = РежимУчетаВзносовЕНВД = 2;
	
	СписокСтатей = СписокПараметровСтатейАналитик(ЭтаФорма);
	
	Для Каждого Элемент Из СписокСтатей Цикл
		ЗаполнитьПодсказкиВвода(Элемент.Представление, ЭтаФорма[Элемент.Представление + "Автоматически"]);
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ОчиститьСтатьиРасходов(ПризнакОчисткиСтатей = Неопределено)
	
	Если ПризнакОчисткиСтатей = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если ПризнакОчисткиСтатей = "ЕНВД" Тогда
		
		СтатьяРасходовВзносовЕНВД = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		АналитикаРасходовВзносовЕНВД = Неопределено;
		АналитикаРасходовВзносовЕНВДОбязательна = Ложь;
		АналитикаРасходовВзносовЕНВДАвтоматически = Ложь;
		
		Объект.СтатьяРасходовЕНВДПФР = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовЕНВДПФР = Неопределено;
		АналитикаРасходовЕНВДПФРОбязательна = Ложь;
		АналитикаРасходовЕНВДПФРАвтоматически = Ложь;
		
		Объект.СтатьяРасходовЕНВДФСС = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовЕНВДФСС = Неопределено;
		АналитикаРасходовЕНВДФССОбязательна = Ложь;
		АналитикаРасходовЕНВДФССАвтоматически = Ложь;
		
		Объект.СтатьяРасходовЕНВДФОМС = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовЕНВДФОМС = Неопределено;
		АналитикаРасходовЕНВДФОМСОбязательна = Ложь;
		АналитикаРасходовЕНВДФОМСАвтоматически = Ложь;
		
	Иначе
		
		СтатьяРасходовВзносов = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		АналитикаРасходовВзносов = Неопределено;
		АналитикаРасходовВзносовОбязательна = Ложь;
		АналитикаРасходовВзносовАвтоматически = Ложь;
		
		Объект.СтатьяРасходовПФР = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовПФР = Неопределено;
		АналитикаРасходовПФРОбязательна = Ложь;
		АналитикаРасходовПФРАвтоматически = Ложь;
		
		Объект.СтатьяРасходовФСС = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовФСС = Неопределено;
		АналитикаРасходовФССОбязательна = Ложь;
		АналитикаРасходовФССАвтоматически = Ложь;
		
		Объект.СтатьяРасходовФОМС = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
		Объект.АналитикаРасходовФОМС = Неопределено;
		АналитикаРасходовФОМСОбязательна = Ложь;
		АналитикаРасходовФОМСАвтоматически = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПодсказкиВвода(ИмяЭлементаФормы, ОтображатьПодсказку)
	
	Если ОтображатьПодсказку Тогда
		Элементы[ИмяЭлементаФормы].ПодсказкаВвода = НСтр("ru = '<подбирается автоматически>'");
	Иначе
		Элементы[ИмяЭлементаФормы].ПодсказкаВвода = "";
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьРеквизитыФормы()
	
	ПорядокУчетаВзносовСовпадает = Ложь;
	
	Если Объект.СтатьяРасходовПФР = Объект.СтатьяРасходовФСС И
		Объект.СтатьяРасходовПФР = Объект.СтатьяРасходовФОМС И
		Объект.АналитикаРасходовПФР = Объект.АналитикаРасходовФСС И
		Объект.АналитикаРасходовПФР = Объект.АналитикаРасходовФОМС Тогда
		
		СтатьяРасходовВзносов = Объект.СтатьяРасходовПФР;
		АналитикаРасходовВзносов = Объект.АналитикаРасходовПФР;
		
		ПорядокУчетаВзносовСовпадает = Истина;
		
	КонецЕсли;
	
	Если Объект.СтатьяРасходов = СтатьяРасходовВзносов И
		Объект.АналитикаРасходов = АналитикаРасходовВзносов Тогда
		РежимУчетаВзносов = 0;
	ИначеЕсли ПорядокУчетаВзносовСовпадает Тогда
		РежимУчетаВзносов = 1;
	Иначе
		РежимУчетаВзносов = 2;
	КонецЕсли;
	
	ПорядокУчетаВзносовСовпадает = Ложь;
	
	Если Объект.СтатьяРасходовЕНВДПФР = Объект.СтатьяРасходовЕНВДФСС И
		Объект.СтатьяРасходовЕНВДПФР = Объект.СтатьяРасходовЕНВДФОМС И
		Объект.АналитикаРасходовЕНВДПФР = Объект.АналитикаРасходовЕНВДФСС И
		Объект.АналитикаРасходовЕНВДПФР = Объект.АналитикаРасходовЕНВДФОМС Тогда
		
		СтатьяРасходовВзносовЕНВД = Объект.СтатьяРасходовЕНВДПФР;
		АналитикаРасходовВзносовЕНВД = Объект.АналитикаРасходовЕНВДПФР;
		
		ПорядокУчетаВзносовСовпадает = Истина;
		
	КонецЕсли;
	
	Если Объект.ОсобыйСоответствуетОсновному Тогда
		РежимУчетаВзносовЕНВД = -1;
	ИначеЕсли Объект.СтатьяРасходовЕНВД = СтатьяРасходовВзносовЕНВД И
		Объект.АналитикаРасходовЕНВД = АналитикаРасходовВзносовЕНВД Тогда
		РежимУчетаВзносовЕНВД = 0;
	ИначеЕсли ПорядокУчетаВзносовСовпадает Тогда
		РежимУчетаВзносовЕНВД = 1;
	Иначе
		РежимУчетаВзносовЕНВД = 2;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииЧтенииНаСервере()
	
	ЗаполнитьРеквизитыФормы();
	
	УстановитьПризнакиКонтроляАналитикиРасходов();
	
	НастроитьЭлементыФормы();
	
КонецПроцедуры

&НаСервере
Процедура УстановитьПризнакиКонтроляАналитикиРасходов(СтатьяРасходов = Неопределено, ИмяЭлемента = "", АвтоподборВидаВзноса = Ложь)
	
	ОписаниеТиповПодразделение = Новый ОписаниеТипов("СправочникСсылка.СтруктураПредприятия");
	ОписаниеТиповВидыВзносов = Новый ОписаниеТипов("ПеречислениеСсылка.ВидыОбязательногоСтрахованияСотрудников");
	
	Если СтатьяРасходов <> Неопределено Тогда
		СписокСтатей = Новый СписокЗначений;
		СписокСтатей.Добавить(СтатьяРасходов,                 ИмяЭлемента,                   АвтоподборВидаВзноса);
	Иначе
		СписокСтатей = СписокПараметровСтатейАналитик(ЭтаФорма);
	КонецЕсли;
	
	Реквизиты = ОбщегоНазначения.ЗначенияРеквизитовОбъектов(СписокСтатей.ВыгрузитьЗначения(), "КонтролироватьЗаполнениеАналитики, АналитикаРасходовЗаказРеализация");
	
	Для Каждого Элемент Из СписокСтатей Цикл
		
		Если Элемент.Пометка Тогда
			Автоматически = Элемент.Значение.ТипЗначения = ОписаниеТиповПодразделение Или
				Элемент.Значение.ТипЗначения = ОписаниеТиповВидыВзносов;
		Иначе
			Автоматически = Элемент.Значение.ТипЗначения = ОписаниеТиповПодразделение;
		КонецЕсли;
		
		Если Реквизиты[Элемент.Значение] <> Неопределено Тогда
			Обязательна = Реквизиты[Элемент.Значение].КонтролироватьЗаполнениеАналитики И Не Автоматически;
			ЗаказРеализация = Реквизиты[Элемент.Значение].АналитикаРасходовЗаказРеализация;
		Иначе
			Обязательна = Ложь;
			ЗаказРеализация = Ложь;
		КонецЕсли;
		
		ЭтаФорма[Элемент.Представление + "Обязательна"] = Обязательна;
		ЭтаФорма[Элемент.Представление + "Автоматически"] = Автоматически;
		ЭтаФорма[Элемент.Представление + "ЗаказРеализация"] = ЗаказРеализация;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ОсобыйСоответствуетОсновномуПриИзмененииНаСервере()
	
	Если Объект.ОсобыйСоответствуетОсновному Тогда
		РежимУчетаВзносовЕНВД = -1;
	Иначе
		РежимУчетаВзносовЕНВД = 0;
	КонецЕсли;
	
	Объект.СтатьяРасходовЕНВД = ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
	Объект.АналитикаРасходовЕНВД = Неопределено;
	АналитикаРасходовЕНВДОбязательна = Ложь;
	АналитикаРасходовЕНВДАвтоматически = Ложь;

	НастроитьЭлементыФормы("ЕНВД");
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СписокПараметровСтатейАналитик(Форма)
	
	СписокСтатей = Новый СписокЗначений;
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходов,          "АналитикаРасходов");
	СписокСтатей.Добавить(Форма.СтатьяРасходовВзносов,          "АналитикаРасходовВзносов",     Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовПФР,       "АналитикаРасходовПФР",         Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовФСС,       "АналитикаРасходовФСС",         Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовФОМС,      "АналитикаРасходовФОМС",        Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовЕНВД,      "АналитикаРасходовЕНВД");
	СписокСтатей.Добавить(Форма.СтатьяРасходовВзносовЕНВД,      "АналитикаРасходовВзносовЕНВД", Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовЕНВДПФР,   "АналитикаРасходовЕНВДПФР",     Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовЕНВДФСС,   "АналитикаРасходовЕНВДФСС",     Истина);
	СписокСтатей.Добавить(Форма.Объект.СтатьяРасходовЕНВДФОМС,  "АналитикаРасходовЕНВДФОМС",    Истина);
	
	Возврат СписокСтатей;
	
КонецФункции

&НаСервере
Процедура СтатьяРасходовПриИзмененииНаСервере(Статья, Аналитика, ИмяЭлемента)
	
	СтрокаТаблицы = Новый Структура("СтатьяРасходов, АналитикаРасходов, АналитикаРасходовОбязательна",
		Статья, Аналитика, ЭтаФорма[ИмяЭлемента + "Обязательна"]);
	
	ДоходыИРасходыСервер.СтатьяРасходовПриИзменении(
		Объект,
		СтрокаТаблицы.СтатьяРасходов,
		СтрокаТаблицы.АналитикаРасходов);
		
	Аналитика = СтрокаТаблицы.АналитикаРасходов;
	
	Если ИмяЭлемента = "АналитикаРасходов" Или ИмяЭлемента = "АналитикаРасходовЕНВД" Тогда
		АвтоподборВидаВзноса = Ложь;
	Иначе
		АвтоподборВидаВзноса = Истина;
	КонецЕсли;
	
	УстановитьПризнакиКонтроляАналитикиРасходов(Статья, ИмяЭлемента, АвтоподборВидаВзноса);
	ЗаполнитьПодсказкиВвода(ИмяЭлемента, ЭтаФорма[ИмяЭлемента + "Автоматически"]);
	
КонецПроцедуры

&НаСервереБезКонтекста
Процедура АналитикаРасходовПолучениеДанныхВыбора(ДанныеВыбора, Текст)
	
	ДанныеВыбора = Новый СписокЗначений;
	ПродажиСервер.ЗаполнитьДанныеВыбораАналитикиРасходов(ДанныеВыбора, Текст);
	
КонецПроцедуры

#КонецОбласти