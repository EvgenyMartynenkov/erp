﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ЭтоГруппа Тогда
		Массив = ПоказателиШаблона.ВыгрузитьКолонку("Показатель");
		ПредставлениеПоказателей = СтроковыеФункцииКлиентСервер.ПолучитьСтрокуИзМассиваПодстрок(Массив, "; ");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли