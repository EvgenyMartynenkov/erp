﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ЕдиницаРазмеров = Константы.ЕдиницаИзмеренияЛинейныхРазмеров.Получить();
	ЕдиницаИзмеренияОбъема = Константы.ЕдиницаИзмеренияОбъема.Получить();

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	Элементы.Высота.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Глубина.Доступность 			= НЕ Объект.Безразмерная;
	Элементы.Ширина.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Объем.Доступность  			= НЕ Объект.Безразмерная;

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УстановитьДоступность();
КонецПроцедуры

&НаКлиенте
Процедура  ПослеЗаписи(ПараметрыЗаписи)

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВысотаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура ГлубинаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура ШиринаПриИзменении(Элемент)
	ПересчитатьОбъем();
КонецПроцедуры

&НаКлиенте
Процедура БезразмернаяПриИзменении(Элемент)
	УстановитьДоступность();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаКлиенте
Процедура УстановитьДоступность()
	Элементы.Высота.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Глубина.Доступность 			= НЕ Объект.Безразмерная;
	Элементы.Ширина.Доступность  			= НЕ Объект.Безразмерная;
	Элементы.Объем.Доступность  			= НЕ Объект.Безразмерная;
КонецПроцедуры

&НаСервере
Процедура ПересчитатьОбъем()
	
	КоэффициентПересчетаВЕдиницыОбъема = Константы.КоэффициентПересчетаВЕдиницыОбъема.Получить();
	
	Если НЕ ЗначениеЗаполнено(КоэффициентПересчетаВЕдиницыОбъема) Тогда
		
		ТекстСообщения = НСтр("ru='В настройках программы не установлен коэффициент перерасчета из единиц измерения линейных размеров в единицы измерения объема'");
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		
	Иначе
		
		Объект.Объем = Объект.Высота * Объект.Глубина * Объект.Ширина * КоэффициентПересчетаВЕдиницыОбъема;
	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
