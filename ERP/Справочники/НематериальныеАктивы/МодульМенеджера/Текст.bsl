﻿
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Контекст")Тогда
		
		ИндексЗапятой = Найти(Параметры.Контекст, ",");
		Контекст = Параметры.Контекст;
		Если ИндексЗапятой <> 0 Тогда
			Контекст = Сред(Контекст, 1, ИндексЗапятой-1);
		КонецЕсли;
		Если Контекст = "БУ" И ТребуетсяОтборПоДаннымУчетаБУ(Параметры) Тогда
			Параметры.Отбор.Вставить("Ссылка", ЭлементыПоОтборуБУ(Параметры));
		ИначеЕсли Контекст = "МФУ" И ТребуетсяОтборПоДаннымУчетаМФУ(Параметры) Тогда
			Параметры.Отбор.Вставить("Ссылка", ЭлементыПоОтборуМФУ(Параметры));
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ТребуетсяОтборПоДаннымУчетаБУ(Параметры)
	
	ПоляВПараметрах = Новый Структура("ТекущийРегистратор, ДатаСведений");
	Для Каждого Поле Из ПоляВПараметрах Цикл
		Если Параметры.Свойство(Поле.Ключ) Тогда
			Возврат Истина;
		КонецЕсли;
	КонецЦикла;
	
	Если Параметры.Свойство("Отбор") Тогда
		ПоляВОтборе = Новый Структура("БУОрганизация, БУСостояние");
		Для Каждого Поле Из ПоляВПараметрах Цикл
			Если Параметры.Отбор.Свойство(Поле.Ключ) Тогда
				Возврат Истина;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции

Функция ЭлементыПоОтборуБУ(Параметры)
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	Активы.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.НематериальныеАктивы КАК Активы
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПервоначальныеСведенияНМАБухгалтерскийУчет.СрезПоследних(&ДатаСведений, Регистратор <> &ТекущийРегистратор) КАК ПервоначальныеСведенияНМАБухгалтерскийУчетСрезПоследних
		|		ПО Активы.Ссылка = ПервоначальныеСведенияНМАБухгалтерскийУчетСрезПоследних.НематериальныйАктив
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.СостоянияНМАОрганизаций.СрезПоследних(&ДатаСведений, Регистратор <> &ТекущийРегистратор) КАК СостоянияНМАОрганизацийСрезПоследних
		|		ПО Активы.Ссылка = СостоянияНМАОрганизацийСрезПоследних.НематериальныйАктив
		|ГДЕ
		|	ИСТИНА
		|	И (НЕ &ТребуетсяБУОрганизация
		|			ИЛИ ЕСТЬNULL(ПервоначальныеСведенияНМАБухгалтерскийУчетСрезПоследних.Организация, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)) В (&БУОрганизация))
		|	И (НЕ &ТребуетсяБУСостояние
		|			ИЛИ ЕСТЬNULL(СостоянияНМАОрганизацийСрезПоследних.Состояние, ЗНАЧЕНИЕ(Перечисление.ВидыСостоянийНМА.НеПринятКУчету)) В (&БУСостояние))"
	);
	
	ПоляОтбора = Новый Структура("БУОрганизация, БУСостояние, ТекущийРегистратор, ДатаСведений");
	ПоляОтбора.ДатаСведений = ТекущаяДатаСеанса();
	ЗаполнитьЗначенияСвойств(ПоляОтбора, Параметры);
	Если Параметры.Свойство("Отбор") Тогда
		ЗаполнитьЗначенияСвойств(ПоляОтбора, Параметры.Отбор);
	КонецЕсли;
	
	Для Каждого КлючИЗначение Из ПоляОтбора Цикл
		Запрос.УстановитьПараметр("Требуется"+КлючИЗначение.Ключ, ЗначениеЗаполнено(КлючИЗначение.Значение));
		Запрос.УстановитьПараметр(КлючИЗначение.Ключ, КлючИЗначение.Значение);
	КонецЦикла;
	
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат Справочники.ОбъектыЭксплуатации.ПустаяСсылка();
	КонецЕсли;
	
	Возврат Результат.Выгрузить().ВыгрузитьКолонку("Ссылка");
	
КонецФункции

Функция ТребуетсяОтборПоДаннымУчетаМФУ(Параметры)
	
	ПоляВПараметрах = Новый Структура("ТекущийРегистратор, ДатаСведений");
	Для Каждого Поле Из ПоляВПараметрах Цикл
		Если Параметры.Свойство(Поле.Ключ) Тогда
			Возврат Истина;
		КонецЕсли;
	КонецЦикла;
	
	Если Параметры.Свойство("Отбор") Тогда
		ПоляВОтборе = Новый Структура("МФУСостояние, МФУОрганизация, МФУПодразделение");
		Для Каждого Поле Из ПоляВПараметрах Цикл
			Если Параметры.Отбор.Свойство(Поле.Ключ) Тогда
				Возврат Истина;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Возврат Ложь;
	
КонецФункции

Функция ЭлементыПоОтборуМФУ(Параметры)
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	Активы.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.НематериальныеАктивы КАК Активы
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НематериальныеАктивыМеждународныйУчет.СрезПоследних(&ДатаСведений, Регистратор <> &ТекущийРегистратор) КАК ДанныеУчета
		|		ПО Активы.Ссылка = ДанныеУчета.НематериальныйАктив
		|ГДЕ
		|	ИСТИНА
		|	И (НЕ &ТребуетсяМФУОрганизация
		|			ИЛИ ЕСТЬNULL(ДанныеУчета.Организация, ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)) В (&МФУОрганизация))
		|	И (НЕ &ТребуетсяМФУПодразделение
		|			ИЛИ ЕСТЬNULL(ДанныеУчета.Подразделение, ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка)) В (&МФУПодразделение))
		|	И (НЕ &ТребуетсяМФУСостояние
		|			ИЛИ ЕСТЬNULL(ДанныеУчета.Состояние, ЗНАЧЕНИЕ(Перечисление.ВидыСостоянийНМА.НеПринятКУчету)) В (&МФУСостояние))"
	);
	
	ПоляОтбора = Новый Структура("МФУОрганизация, МФУПодразделение, МФУСостояние, ТекущийРегистратор, ДатаСведений");
	ПоляОтбора.ДатаСведений = ТекущаяДатаСеанса();
	ЗаполнитьЗначенияСвойств(ПоляОтбора, Параметры);
	Если Параметры.Свойство("Отбор") Тогда
		ЗаполнитьЗначенияСвойств(ПоляОтбора, Параметры.Отбор);
	КонецЕсли;
	
	Для Каждого КлючИЗначение Из ПоляОтбора Цикл
		Запрос.УстановитьПараметр("Требуется"+КлючИЗначение.Ключ, ЗначениеЗаполнено(КлючИЗначение.Значение));
		Запрос.УстановитьПараметр(КлючИЗначение.Ключ, КлючИЗначение.Значение);
	КонецЦикла;
	
	Результат = Запрос.Выполнить();
	Если Результат.Пустой() Тогда
		Возврат Справочники.ОбъектыЭксплуатации.ПустаяСсылка();
	КонецЕсли;
	
	Возврат Результат.Выгрузить().ВыгрузитьКолонку("Ссылка");
	
КонецФункции

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли

#КонецОбласти

