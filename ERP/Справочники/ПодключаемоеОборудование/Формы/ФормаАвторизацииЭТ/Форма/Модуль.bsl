﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;

	СписокТиповКарт = Неопределено;

	СуммаВременная = 0;
	Если Параметры.Свойство("Сумма", СуммаВременная)
	   И СуммаВременная > 0 Тогда
		Сумма = СуммаВременная;
	Иначе
		Сумма = 0;
	КонецЕсли;

	времПределСуммы = 0;
	Если Параметры.Свойство("ПределСуммы", времПределСуммы)
	   И времПределСуммы > 0 Тогда
		Элементы.Сумма.МаксимальноеЗначение = времПределСуммы;
	Иначе
		Элементы.Сумма.МаксимальноеЗначение = Неопределено;
	КонецЕсли;
	
	Если Параметры.Свойство("ЗапретРедактированияСуммы") Тогда
		Элементы.Сумма.ТолькоПросмотр = Параметры.ЗапретРедактированияСуммы;
	КонецЕсли;
	
	Если Параметры.Свойство("СписокТиповКарт", СписокТиповКарт)
	   И ТипЗнч(СписокТиповКарт) = Тип("СписокЗначений")
	   И СписокТиповКарт. Количество() > 0 Тогда
		Для Каждого СтрокаСписка Из СписокТиповКарт Цикл
			Элементы.ТипКарты.СписокВыбора.Добавить(СтрокаСписка.Значение, СтрокаСписка.Представление);
		КонецЦикла;
		Элементы.ТипКарты.Видимость = Истина;
	КонецЕсли;
	
	Элементы.НомерКарты.Видимость            = Ложь;
	Элементы.НомерКарты.ТолькоПросмотр       = Истина;
	Элементы.НомерКарты.РедактированиеТекста = Ложь;

	Если Параметры.Свойство("ПоказыватьНомерКарты", ПоказыватьНомерКарты) Тогда
		Если ПоказыватьНомерКарты Тогда
			Элементы.НомерКарты.Видимость            = Истина;
			Элементы.НомерКарты.ТолькоПросмотр       = Ложь;
			Элементы.НомерКарты.РедактированиеТекста = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Если Элементы.Сумма.ТолькоПросмотр Тогда
		Элементы.Сумма.АктивизироватьПоУмолчанию = Ложь;
		Если Элементы.ТипКарты.Видимость И Элементы.ТипКарты.СписокВыбора.Количество() > 1 Тогда
			Элементы.ТипКарты.АктивизироватьПоУмолчанию = Истина;
		ИначеЕсли Элементы.НомерКарты.Видимость Тогда
			Элементы.НомерКарты.АктивизироватьПоУмолчанию = Истина;
		Иначе
			Элементы.ВыполнитьОперацию.АктивизироватьПоУмолчанию = Истина;
		КонецЕсли;
	КонецЕсли;
	
	Если Параметры.Свойство("УказатьДополнительныеДанные") Тогда
		УказатьДополнительныеДанные = Истина;
	КонецЕсли;
	
	ТипыПодключенногоОборудования = МенеджерОборудованияСерверПовтИсп.ТипыПодключенногоОборудования();
	
	ДоступноСчитываниеНаСМК = ПоказыватьНомерКарты И (ТипыПодключенногоОборудования <> Неопределено)
		И (ТипыПодключенногоОборудования.Найти(Перечисления.ТипыПодключаемогоОборудования.СчитывательМагнитныхКарт) <> Неопределено);
		
	Элементы.ГруппаРучнойВводДанных.Видимость = УказатьДополнительныеДанные;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если ДоступноСчитываниеНаСМК Тогда
		// Проверка и подключение считывателей МК.
		ПоддерживаемыеТипыПО = Новый Массив();
		ПоддерживаемыеТипыПО.Добавить("СчитывательМагнитныхКарт");
		Если МенеджерОборудованияКлиент.ПодключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыПО) Тогда
			Элементы.НадписьДоступноСчитываниеНаСМК.Видимость = Истина;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Если ДоступноСчитываниеНаСМК Тогда
		// Отключение считывателей МК
		ПоддерживаемыеТипыПО = Новый Массив();
		ПоддерживаемыеТипыПО.Добавить("СчитывательМагнитныхКарт");
		МенеджерОборудованияКлиент.ОтключитьОборудованиеПоТипу(УникальныйИдентификатор, ПоддерживаемыеТипыПО);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)

	Если Источник = "ПодключаемоеОборудование"
	   И ВводДоступен() Тогда
		Если ИмяСобытия = "TracksData" Тогда
			Если Параметр[1] = Неопределено Тогда
				ПолученКодКарты(Параметр[0], Параметр[0]);
			Иначе
				ПолученКодКарты(Параметр[0], Параметр[1][1]);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ВнешнееСобытие(Источник, Событие, Данные)
	
	Если ВводДоступен() Тогда
		
		ОписаниеСобытия = Новый Структура();
		ОписаниеОшибки  = "";
		ОписаниеСобытия.Вставить("Источник", Источник);
		ОписаниеСобытия.Вставить("Событие",  Событие);
		ОписаниеСобытия.Вставить("Данные",   Данные);
		
		Результат = МенеджерОборудованияКлиент.ПолучитьСобытиеОтУстройства(ОписаниеСобытия, ОписаниеОшибки);
		Если Результат = Неопределено Тогда 
			ТекстСообщения = НСтр("ru = 'При обработке внешнего события от устройства произошла ошибка:'")
								+ Символы.ПС + ОписаниеОшибки;
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения);
		Иначе
			ОбработкаОповещения(Результат.ИмяСобытия, Результат.Параметр, Результат.Источник);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура УказатьДополнительныеДанныеПриИзменении(Элемент)

	Элементы.ГруппаРучнойВводДанных.Видимость = УказатьДополнительныеДанные;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВыполнитьОперацию(Команда)
	
	Ошибки = "";
	
	ОчиститьСообщения();
	
	Если Сумма = 0 Тогда
		Ошибки = НСтр("ru='Платеж не может быть осуществлен на нулевую сумму.'");
	КонецЕсли;
	
	Если ПоказыватьНомерКарты И Не ЗначениеЗаполнено(НомерКарты) Тогда
		Ошибки = Ошибки + ?(ПустаяСтрока(Ошибки),"",Символы.ПС) + НСтр("ru='Платеж не может быть осуществлен без номера карты.'");
	КонецЕсли;
	
	Если ПустаяСтрока(Ошибки) Тогда
		Если Не УказатьДополнительныеДанные Тогда
			СсылочныйНомер = "";
			НомерЧека      = "";
			КодАвторизации = "";
		КонецЕсли;
		
		СтруктураВозврата = Новый Структура("Сумма, ДанныеКарты, СсылочныйНомер, НомерЧека, ТипКарты, НомерКарты, КодАвторизации",
											Сумма, ДанныеКарты, СсылочныйНомер, НомерЧека, ТипКарты, НомерКарты, КодАвторизации);
		ОчиститьСообщения();
		Закрыть(СтруктураВозврата);
		
	Иначе
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(Ошибки,,"НомерКарты");
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Функция ПолученКодКарты(КодКарты, ДанныеДорожек)

	Если ТипЗнч(ДанныеДорожек) = Тип("Массив")
	   И ДанныеДорожек.Количество() > 1
	   И ДанныеДорожек[1] <> Неопределено
	   И Не ПустаяСтрока(ДанныеДорожек[1]) Тогда
		ДанныеКарты = ДанныеДорожек[1];

		ПозицияРазделителя = Найти(ДанныеКарты, "=");
		Если ПозицияРазделителя > 16 Тогда
			НомерКарты = Лев(ДанныеКарты, ПозицияРазделителя - 1);
			Элементы.НомерКарты.Видимость = Истина;
		Иначе
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru='Указана неверная карта или произошла ошибка при считывании карты.
			|Повторите считывание или считайте другую карту'"));
		КонецЕсли;
	Иначе
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru='Указана неверная карта или произошла ошибка при считывании карты.
		|Повторите считывание или считайте другую карту'"));
	КонецЕсли;
	
	ОбновитьОтображениеДанных();
	
	Возврат Истина;
	
КонецФункции

&НаКлиенте
Процедура СуммаОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, СтандартнаяОбработка)

	Если Элементы.Сумма.МаксимальноеЗначение <> Неопределено
	   И Элементы.Сумма.МаксимальноеЗначение < Число(Текст) Тогда
	   
		СтандартнаяОбработка = Ложь;
		СтруктураЗначения = Новый Структура;
		СтруктураЗначения.Вставить("Предупреждение", НСтр("ru = 'Сумма оплаты по карте превышает необходимую безналичную оплату.
		|Значение будет изменено на максимально возможное.'"));
		СтруктураЗначения.Вставить("Значение", Формат(Элементы.Сумма.МаксимальноеЗначение, "ЧЦ=15; ЧДЦ=2; ЧН=0; ЧГ=0; ЧО=1"));
		
		СписокЗначений = Новый СписокЗначений;
		СписокЗначений.Добавить(СтруктураЗначения);
		ДанныеВыбора = СписокЗначений;
		
	КонецЕсли;

КонецПроцедуры

#КонецОбласти
