﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныйПрограммныйИнтерфейс

// Метод предназначен для установки признака ОбособленноеПодразделение.
//
// Параметры:
//	Подразделение - СправочникСсылка.ПодразделенияОрганизаций,
//	ЯвляетсяОбособленнымПодразделением - булево, 
//	ИсторияРегистрацийВНалоговомОргане - таблица значений, необязательный.
//		Позволяет установить значение регистрации в налоговом органе для обособленного подразделения.
//		Содержит обязательные колонки Период и РегистрацияВНалоговомОргане.
//
Процедура УстановитьПризнакОбособленногоПодразделения(Подразделение, ЯвляетсяОбособленнымПодразделением, ИсторияРегистрацийВНалоговомОргане = Неопределено) Экспорт
	
	// Записываем историю регистраций в налоговом органе.
	Если ЯвляетсяОбособленнымПодразделением = Истина И ИсторияРегистрацийВНалоговомОргане <> Неопределено Тогда
		НаборЗаписей = РегистрыСведений.ИсторияРегистрацийВНалоговомОргане.СоздатьНаборЗаписей();
		Для Каждого СтрокаКоллекции Из ИсторияРегистрацийВНалоговомОргане Цикл
			СтрокаНабора = НаборЗаписей.Добавить();
			ЗаполнитьЗначенияСвойств(СтрокаНабора, СтрокаКоллекции);
			СтрокаНабора.СтруктурнаяЕдиница = Подразделение;
		КонецЦикла;
		НаборЗаписей.Отбор.СтруктурнаяЕдиница.Установить(Подразделение);
		НаборЗаписей.Записать();
	КонецЕсли;
	
	ПодразделениеОбъект = Подразделение.ПолучитьОбъект();
	ПодразделениеОбъект.ОбособленноеПодразделение = ЯвляетсяОбособленнымПодразделением;
	ПодразделениеОбъект.ДополнительныеСвойства.Вставить("УстановитьПризнакОбособленногоПодразделения", Истина);
	Если ЯвляетсяОбособленнымПодразделением = Истина И ИсторияРегистрацийВНалоговомОргане <> Неопределено Тогда
		ПодразделениеОбъект.РегистрацияВНалоговомОргане = ЗарплатаКадры.РегистрацияВНалоговомОргане(Подразделение);
	КонецЕсли;
	ПодразделениеОбъект.Записать();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	Перем ТолькоДействующиеПодразделения;
	
	Если Параметры.Свойство("ТолькоДействующиеПодразделения", ТолькоДействующиеПодразделения) И
			Параметры.ТолькоДействующиеПодразделения Тогда
		Параметры.Отбор.Вставить("Сформировано", Истина);
		Параметры.Отбор.Вставить("Расформировано", Ложь);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Возвращает головную структурную единицу и массив подчиненных подразделений, для которых требуется тиражирование
// значения из головной структурной единицы.
//
// Параметры:
//		СтруктурнаяЕдиница - Организация или подразделение для которых требуется получить подчиненные подразделения до
//		                     первого обособленного.
//
// Возвращаемое значение:
//		Структура
//			ГоловнаяСтруктурнаяЕдиница - Организация или подразделение - Вышестоящая структурная единица, из которой будет
//			                             тиражироваться значение.
//			ПодчиненныеСтруктурныеЕдиницы - Массив - Подчиненные подразделения, для которых будет установлено значение из
//			                                         вышестоящей структурной единицы.
//
Функция ПодчиненныеСтруктурныеЕдиницы(СтруктурнаяЕдиница) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("СтруктурнаяЕдиница", СтруктурнаяЕдиница);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ПодразделенияОрганизаций.Ссылка,
	|	ВЫБОР
	|		КОГДА ПодразделенияОрганизаций.Родитель = ЗНАЧЕНИЕ(Справочник.ПодразделенияОрганизаций.ПустаяСсылка)
	|			ТОГДА ПодразделенияОрганизаций.Владелец
	|		ИНАЧЕ ПодразделенияОрганизаций.Родитель
	|	КОНЕЦ КАК Родитель
	|ИЗ
	|	Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	|ГДЕ
	|	ПодразделенияОрганизаций.Ссылка В ИЕРАРХИИ(&СтруктурнаяЕдиница)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	Организации.Ссылка,
	|	ВЫБОР
	|		КОГДА Организации.ГоловнаяОрганизация = Организации.Ссылка
	|			ТОГДА Организации.Ссылка
	|		ИНАЧЕ Организации.ГоловнаяОрганизация
	|	КОНЕЦ
	|ИЗ
	|	Справочник.Организации КАК Организации
	|ГДЕ
	|	Организации.Ссылка = &СтруктурнаяЕдиница";
	
	Если ТипЗнч(СтруктурнаяЕдиница) = Тип("СправочникСсылка.ПодразделенияОрганизаций") Тогда
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "Организации.Ссылка = &СтруктурнаяЕдиница", "(ИСТИНА)");
	Иначе
		Запрос.Текст = СтрЗаменить(Запрос.Текст, "ПодразделенияОрганизаций.Ссылка В ИЕРАРХИИ(&СтруктурнаяЕдиница)", "ПодразделенияОрганизаций.Владелец = &СтруктурнаяЕдиница");
	КонецЕсли;
	
	ТаблицаПодчиненности = Новый ТаблицаЗначений;
	ТаблицаПодчиненности.Колонки.Добавить("Элемент", Новый ОписаниеТипов("СправочникСсылка.ПодразделенияОрганизаций, СправочникСсылка.Организации"));
	ТаблицаПодчиненности.Колонки.Добавить("Родитель", Новый ОписаниеТипов("СправочникСсылка.ПодразделенияОрганизаций, СправочникСсылка.Организации"));
	ТаблицаПодчиненности.Колонки.Добавить("Уровень", Новый ОписаниеТипов("Число"));
	
	СоответствиеПодчиненности = Новый Соответствие;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		СоответствиеПодчиненности.Вставить(Выборка.Ссылка, Выборка.Родитель);
	КонецЦикла;
	
	Выборка.Сбросить();
	Пока Выборка.Следующий() Цикл
		Родитель = СоответствиеПодчиненности[Выборка.Ссылка];
		Уровень = 1;
		Пока Истина Цикл
			НоваяСтрока = ТаблицаПодчиненности.Добавить();
			НоваяСтрока.Элемент = Выборка.Ссылка;
			НоваяСтрока.Родитель = Родитель;
			НоваяСтрока.Уровень = Уровень;
			Если Выборка.Ссылка = Родитель Тогда
				НоваяСтрока.Уровень = 0;
				Прервать;
			КонецЕсли;
			ПредыдущийРодитель = Родитель;
			Родитель = СоответствиеПодчиненности[Родитель];
			Уровень = Уровень + 1;
			Если ПредыдущийРодитель = Родитель Тогда
				Прервать;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	
	Запрос.УстановитьПараметр("ТаблицаПодчиненности", ТаблицаПодчиненности);
	Запрос.УстановитьПараметр("СтруктурнаяЕдиница", СтруктурнаяЕдиница);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ТаблицаПодчиненности.Элемент КАК ПодчиненнаяСтруктурнаяЕдиница,
	|	ТаблицаПодчиненности.Родитель КАК СтруктурнаяЕдиница,
	|	ТаблицаПодчиненности.Уровень
	|ПОМЕСТИТЬ ВТСтруктурныеЕдиницы
	|ИЗ
	|	&ТаблицаПодчиненности КАК ТаблицаПодчиненности
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ПодчиненнаяСтруктурнаяЕдиница
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница,
	|	СтруктурныеЕдиницы.Уровень КАК Уровень
	|ПОМЕСТИТЬ ВТПодчиненныеПодразделенияИОрганизации
	|ИЗ
	|	ВТСтруктурныеЕдиницы КАК СтруктурныеЕдиницы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ПодразделенияОрганизаций КАК ПодразделенияОрганизаций
	|		ПО (ПодразделенияОрганизаций.ОбособленноеПодразделение)
	|			И СтруктурныеЕдиницы.СтруктурнаяЕдиница = ПодразделенияОрганизаций.Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница,
	|	СтруктурныеЕдиницы.Уровень
	|ИЗ
	|	ВТСтруктурныеЕдиницы КАК СтруктурныеЕдиницы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Организации КАК Организации
	|		ПО СтруктурныеЕдиницы.СтруктурнаяЕдиница = Организации.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница,
	|	ЕСТЬNULL(Подразделения.ОбособленноеПодразделение, Организации.ОбособленноеПодразделение) КАК ОбособленноеПодразделение,
	|	МИНИМУМ(ПодчиненныеПодразделенияИОрганизации.Уровень) КАК Уровень,
	|	ВЫБОР
	|		КОГДА ЕСТЬNULL(Подразделения.ОбособленноеПодразделение, Организации.ОбособленноеПодразделение)
	|				ИЛИ ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница ССЫЛКА Справочник.Организации
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ПодразделениеОбособленноеИлиОрганизация
	|ПОМЕСТИТЬ ВТПодчиненныеСтруктурныеЕдиницы
	|ИЗ
	|	ВТПодчиненныеПодразделенияИОрганизации КАК ПодчиненныеПодразделенияИОрганизации
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПодразделенияОрганизаций КАК Подразделения
	|		ПО ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница = Подразделения.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Организации КАК Организации
	|		ПО ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница = Организации.Ссылка
	|ГДЕ
	|	(Подразделения.Ссылка ЕСТЬ НЕ NULL 
	|			ИЛИ Организации.Ссылка ЕСТЬ НЕ NULL )
	|
	|СГРУППИРОВАТЬ ПО
	|	ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница,
	|	ЕСТЬNULL(Подразделения.ОбособленноеПодразделение, Организации.ОбособленноеПодразделение),
	|	Подразделения.ОбособленноеПодразделение,
	|	Организации.ОбособленноеПодразделение
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	ПодчиненныеПодразделенияИОрганизации.ПодчиненнаяСтруктурнаяЕдиница
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СтруктурныеЕдиницы.СтруктурнаяЕдиница
	|ПОМЕСТИТЬ ВТГоловнаяСтруктурнаяЕдиница
	|ИЗ
	|	ВТПодчиненныеСтруктурныеЕдиницы КАК ПодчиненныеСтруктурныеЕдиницы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТСтруктурныеЕдиницы КАК СтруктурныеЕдиницы
	|		ПО (ПодчиненныеСтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница = &СтруктурнаяЕдиница)
	|			И (ПодчиненныеСтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница = СтруктурныеЕдиницы.СтруктурнаяЕдиница
	|					И ПодчиненныеСтруктурныеЕдиницы.ПодразделениеОбособленноеИлиОрганизация
	|				ИЛИ НЕ ПодчиненныеСтруктурныеЕдиницы.ОбособленноеПодразделение
	|					И ПодчиненныеСтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница = СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница
	|					И ПодчиненныеСтруктурныеЕдиницы.Уровень = СтруктурныеЕдиницы.Уровень)
	|
	|СГРУППИРОВАТЬ ПО
	|	СтруктурныеЕдиницы.СтруктурнаяЕдиница
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница КАК СтруктурнаяЕдиница
	|ИЗ
	|	ВТПодчиненныеСтруктурныеЕдиницы КАК ПодчиненныеСтруктурныеЕдиницы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТСтруктурныеЕдиницы КАК СтруктурныеЕдиницы
	|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТГоловнаяСтруктурнаяЕдиница КАК ГоловнаяСтруктурнаяЕдиница
	|			ПО СтруктурныеЕдиницы.СтруктурнаяЕдиница = ГоловнаяСтруктурнаяЕдиница.СтруктурнаяЕдиница
	|		ПО ПодчиненныеСтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница = СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница
	|			И ПодчиненныеСтруктурныеЕдиницы.Уровень = СтруктурныеЕдиницы.Уровень
	|			И (НЕ ПодчиненныеСтруктурныеЕдиницы.ОбособленноеПодразделение)
	|			И (СтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница ССЫЛКА Справочник.ПодразделенияОрганизаций)
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТСтруктурныеЕдиницы КАК СтруктурныеЕдиницы2
	|		ПО ПодчиненныеСтруктурныеЕдиницы.ПодчиненнаяСтруктурнаяЕдиница = СтруктурныеЕдиницы2.ПодчиненнаяСтруктурнаяЕдиница
	|			И (СтруктурныеЕдиницы2.СтруктурнаяЕдиница = &СтруктурнаяЕдиница)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ГоловнаяСтруктурнаяЕдиница.СтруктурнаяЕдиница
	|ИЗ
	|	ВТГоловнаяСтруктурнаяЕдиница КАК ГоловнаяСтруктурнаяЕдиница";
	
	ПодчиненныеСтруктурныеЕдиницы = Новый Структура();
	ПодчиненныеСтруктурныеЕдиницы.Вставить("ГоловнаяСтруктурнаяЕдиница", Справочники.Организации.ПустаяСсылка());
	ПодчиненныеСтруктурныеЕдиницы.Вставить("ПодчиненныеСтруктурныеЕдиницы", Новый Массив);
	
	РезультатЗапросов = Запрос.ВыполнитьПакет();
	Выборка = РезультатЗапросов[РезультатЗапросов.Количество() - 1].Выбрать();
	Если Выборка.Следующий() Тогда
		Подчиненные = РезультатЗапросов[РезультатЗапросов.Количество() - 2].Выгрузить();
		ПодчиненныеСтруктурныеЕдиницы.Вставить("ГоловнаяСтруктурнаяЕдиница", Выборка.СтруктурнаяЕдиница);
		ПодчиненныеСтруктурныеЕдиницы.Вставить("ПодчиненныеСтруктурныеЕдиницы", Подчиненные.ВыгрузитьКолонку("СтруктурнаяЕдиница"));
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат ПодчиненныеСтруктурныеЕдиницы;
	
КонецФункции

#КонецОбласти

#КонецЕсли