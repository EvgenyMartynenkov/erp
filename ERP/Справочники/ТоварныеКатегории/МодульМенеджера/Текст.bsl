﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УТ 11.1.6.5
// Установка значения константы использования товарных категорий и форматов магазинов, 
// если ранее была установлена константа "Использовать ассортимент"
//
Процедура ЗаполнитьНовыеКонстантыАссортимента() Экспорт
	
	ИспользоватьАссортимент = Константы.ИспользоватьАссортимент.Получить();
	
	Если ИспользоватьАссортимент Тогда
	
		Константы.ИспользоватьТоварныеКатегории.Установить(Истина);
		Константы.ИспользоватьФорматыМагазинов.Установить(Истина);
	
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли