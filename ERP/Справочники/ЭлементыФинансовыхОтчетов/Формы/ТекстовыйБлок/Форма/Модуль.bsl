﻿
#Область ОбработчикиСобытийФормы

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(АдресЭлементаВХранилище) Тогда
		СтандартнаяОбработка = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Справочники.ЭлементыФинансовыхОтчетов.ФормаПриСозданииНаСервере(ЭтаФорма);
	Заголовок = Параметры.ВидЭлемента;

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Справочники.ЭлементыФинансовыхОтчетов.ФормаПередЗаписьюНаСервере(ЭтаФорма, ТекущийОбъект, Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура КоментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ОткрытьФормуРедактированияКомментария(Элемент.ТекстРедактирования, Объект.Комментарий, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура ТекстовыйБлокПриИзменении(Элемент)
	
	Объект.НаименованиеДляПечати = СокрЛП(СтрПолучитьСтроку(Текст, 1));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗавершитьРедактирование(Команда)
	
	ФинансоваяОтчетностьКлиент.ЗавершитьРедактированиеЭлементаОтчета(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиенте
Процедура ДобавитьШаблонВидОтчета(Команда)
	 
	ТекстДляВставки = "["+НСтр("ru='Вид отчета'")+"]";
	ВставитьТекстВЗаголовок(ТекстДляВставки);
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьШаблонПериод(Команда)
	
	ТекстДляВставки = "["+НСтр("ru='Период отчетности'")+"]";
	ВставитьТекстВЗаголовок(ТекстДляВставки);
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьШаблонКонецПериода(Команда)
	
	ТекстДляВставки = "["+НСтр("ru='Конечная дата периода отчета'")+"]";
	ВставитьТекстВЗаголовок(ТекстДляВставки);
	
КонецПроцедуры

&НаКлиенте
Процедура ВставитьТекстВЗаголовок(ТекстДляВставки, Сдвиг = 0)
	
	СтрокаНач = 0;
	СтрокаКон = 0;
	КолонкаНач = 0;
	КолонкаКон = 0;
	
	Элементы.ТекстовыйБлок.ПолучитьГраницыВыделения(СтрокаНач, КолонкаНач, СтрокаКон, КолонкаКон);
	
	Если (КолонкаКон = КолонкаНач) И (КолонкаКон + СтрДлина(ТекстДляВставки)) > Элементы.ТекстовыйБлок.Ширина / 8 Тогда
		Элементы.ТекстовыйБлок.ВыделенныйТекст = "";
	КонецЕсли;
	
	Элементы.ТекстовыйБлок.ВыделенныйТекст = ТекстДляВставки;
	
	Если Не Сдвиг = 0 Тогда
		Элементы.ТекстовыйБлок.ПолучитьГраницыВыделения(СтрокаНач, КолонкаНач, СтрокаКон, КолонкаКон);
		Элементы.ТекстовыйБлок.УстановитьГраницыВыделения(СтрокаНач, КолонкаНач - Сдвиг, СтрокаКон, КолонкаКон - Сдвиг);
	КонецЕсли;
	ТекущийЭлемент = Элементы.ТекстовыйБлок;
	
КонецПроцедуры

#КонецОбласти

