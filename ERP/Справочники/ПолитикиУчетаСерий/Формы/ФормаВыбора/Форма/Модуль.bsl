﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ИспользоватьСерии = Неопределено;
	
	Если Не Параметры.Свойство("ИспользоватьСерии", ИспользоватьСерии) Тогда
		Возврат;
	КонецЕсли;
	
	Если Не ИспользоватьСерии Тогда
		Возврат;
	КонецЕсли;
	
	МассивДопустимыхТиповПолитик = НоменклатураКлиентСервер.ПолучитьМассивДопустимыхТиповПолитикУчетаСерий(Параметры);

	Параметры.Отбор.Вставить("ТипПолитики", Новый ФиксированныйМассив(МассивДопустимыхТиповПолитик));
	
	Склад = ?(ЗначениеЗаполнено(Параметры.Склад), Параметры.Склад, Справочники.Склады.СкладПоУмолчанию());
	Если ЗначениеЗаполнено(Склад)
		И ТипЗнч(Склад) = Тип("СправочникСсылка.Склады") Тогда
		
		Параметры.Отбор.Вставить("ДляСклада", Истина);
		Если Не ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад,"ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач") Тогда
			Параметры.Отбор.Вставить("УказыватьПриОтраженииИзлишков", Ложь);
			Параметры.Отбор.Вставить("УказыватьПриОтраженииНедостач", Ложь);
		КонецЕсли;
	ИначеЕсли ТипЗнч(Склад) = Тип("СправочникСсылка.СтруктураПредприятия") Тогда
		Параметры.Отбор.Вставить("ДляПроизводства", Истина);
	КонецЕсли;

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

#КонецОбласти
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

