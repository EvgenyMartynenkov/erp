﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

///////////////////////////////////////////////////////////////////////////////
// Обновление информационной базы

Процедура УстановитьРеквизитДопУпорядочиванияСтатейПрочихРасходов() Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = "ВЫБРАТЬ
	|	ПрочиеРасходы.Ссылка
	|ИЗ
	|	Справочник.ПрочиеРасходы КАК ПрочиеРасходы
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПрочиеРасходы.Наименование";
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл 
		Объект = Выборка.Ссылка.ПолучитьОбъект();
		ЗаблокироватьДанныеДляРедактирования(Объект.Ссылка);
		НастройкаПорядкаЭлементов.ЗаполнитьЗначениеРеквизитаУпорядочивания(Объект, Ложь);
		ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект);
	КонецЦикла;
КонецПроцедуры
	
#КонецЕсли
