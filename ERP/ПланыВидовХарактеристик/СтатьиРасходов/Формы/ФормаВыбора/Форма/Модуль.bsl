﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	УстановитьПараметрыДинамическогоСписка();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ХозяйственнаяОперация = Неопределено;
	
	Если Параметры.Свойство("Отбор") Тогда
		
		Если Параметры.Отбор.Свойство("ХозяйственнаяОперация") Тогда
			
			Если ЗначениеЗаполнено(Параметры.Отбор.ХозяйственнаяОперация) Тогда
				ХозяйственнаяОперация = Параметры.Отбор.ХозяйственнаяОперация;
			КонецЕсли;
			Параметры.Отбор.Удалить("ХозяйственнаяОперация");
			
		КонецЕсли;
		
	КонецЕсли;
	
	Список.Параметры.УстановитьЗначениеПараметра("ХозяйственнаяОперация", ХозяйственнаяОперация);
	Список.Параметры.УстановитьЗначениеПараметра("БезОграниченияИспользования", Не ЗначениеЗаполнено(ХозяйственнаяОперация));
	
	ФормироватьФинансовыйРезультат = ПолучитьФункциональнуюОпцию("ФормироватьФинансовыйРезультат");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПереместитьЭлементВверх(Команда)
	
	НастройкаПорядкаЭлементовКлиент.ПереместитьЭлементВверхВыполнить(Список, Элементы.Список);
	
КонецПроцедуры

&НаКлиенте
Процедура ПереместитьЭлементВниз(Команда)
	
	НастройкаПорядкаЭлементовКлиент.ПереместитьЭлементВнизВыполнить(Список, Элементы.Список);
	
КонецПроцедуры

#КонецОбласти


#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ВариантРаспределенияРасходов.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Список.ВариантРаспределенияРасходов");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Перечисления.ВариантыРаспределенияРасходов.НаНаправленияДеятельности;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ФормироватьФинансовыйРезультат");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;

	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'На финансовый результат'"));

КонецПроцедуры

&НаСервере
Процедура УстановитьПараметрыДинамическогоСписка()
	
	Если ПолучитьФункциональнуюОпцию("БазоваяВерсия") Тогда
		
		СписокСтатей = Новый СписокЗначений;
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.КурсовыеРазницы);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.НачисленныйНДСПриВыкупеМногооборотнойТары);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.ПогрешностьРасчетаСебестоимости);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.ПоступлениеОС);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.ПрибыльУбытокПрошлыхЛет);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.РазницыСтоимостиВозвратаИФактическойСтоимостиТоваров);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.СебестоимостьПродаж);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.ФормированиеРезервовПоСомнительнымДолгам);
		СписокСтатей.Добавить(ПланыВидовХарактеристик.СтатьиРасходов.НДСНалоговогоАгента);
		
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
			Список,
			"Ссылка",
			СписокСтатей,
			ВидСравненияКомпоновкиДанных.НеВСписке);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти
