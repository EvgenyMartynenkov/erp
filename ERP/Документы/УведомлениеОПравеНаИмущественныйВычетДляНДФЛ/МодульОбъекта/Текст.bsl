﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "Сотрудник");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.Сотрудники") Тогда
		ЗарплатаКадры.ЗаполнитьПоОснованиюСотрудником(ЭтотОбъект, ДанныеЗаполнения);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	ДанныеДляПроведения = ПолучитьДанныеДляПроведения();
	
	Если ДанныеДляПроведения.Количество() = 0 Тогда
		Возврат
	КонецЕсли;
	
	УчетНДФЛ.СформироватьПравоНаИмущественныеВычеты(Движения, Отказ, Организация, ПрименятьВычетыС, ДанныеДляПроведения);
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если РасходыНаСтроительствоПриобретение + ПроцентыПоКредитам + ПроцентыПриПерекредитовании = 0 Тогда
		ТекстСообщения = НСтр("ru = 'Не заполнены размер вычетов.'");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, , "Объект.РасходыНаСтроительствоПриобретение");
		Отказ = Истина;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ПолучитьДанныеДляПроведения()
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Сотрудник КАК ФизическоеЛицо,
	|	ЗНАЧЕНИЕ(Справочник.ВидыВычетовНДФЛ.Код311) КАК КодВычета,
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.НалоговыйПериод КАК Год,
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.РасходыНаСтроительствоПриобретение КАК Сумма
	|ИЗ
	|	Документ.УведомлениеОПравеНаИмущественныйВычетДляНДФЛ КАК УведомлениеОПравеНаИмущественныйВычетДляНДФЛ
	|ГДЕ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Ссылка = &Ссылка
	|	И УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.РасходыНаСтроительствоПриобретение <> 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Сотрудник,
	|	ЗНАЧЕНИЕ(Справочник.ВидыВычетовНДФЛ.Код312),
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.НалоговыйПериод,
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.ПроцентыПоКредитам
	|ИЗ
	|	Документ.УведомлениеОПравеНаИмущественныйВычетДляНДФЛ КАК УведомлениеОПравеНаИмущественныйВычетДляНДФЛ
	|ГДЕ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Ссылка = &Ссылка
	|	И УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.ПроцентыПоКредитам <> 0
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Сотрудник,
	|	ЗНАЧЕНИЕ(Справочник.ВидыВычетовНДФЛ.Код318),
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.НалоговыйПериод,
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.ПроцентыПриПерекредитовании
	|ИЗ
	|	Документ.УведомлениеОПравеНаИмущественныйВычетДляНДФЛ КАК УведомлениеОПравеНаИмущественныйВычетДляНДФЛ
	|ГДЕ
	|	УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.Ссылка = &Ссылка
	|	И УведомлениеОПравеНаИмущественныйВычетДляНДФЛ.ПроцентыПриПерекредитовании <> 0";
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
						
	Возврат Запрос.Выполнить().Выгрузить();

КонецФункции

#КонецОбласти

#КонецЕсли