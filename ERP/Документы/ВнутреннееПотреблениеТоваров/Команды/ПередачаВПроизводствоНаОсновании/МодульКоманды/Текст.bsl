﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	//{УП}
	Основание = Новый Структура;
	Основание.Вставить("ДокументОснование", ПараметрКоманды);
	Основание.Вставить("ХозяйственнаяОперация", ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ПередачаВПроизводство"));
	
	ПараметрыФормы = Новый Структура("Основание", Основание);
	
	ОткрытьФорму(
		"Документ.ВнутреннееПотреблениеТоваров.ФормаОбъекта",
		ПараметрыФормы,
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно,
		ПараметрыВыполненияКоманды.НавигационнаяСсылка);
	
	//{/УП}
	
	Возврат;
	
КонецПроцедуры
