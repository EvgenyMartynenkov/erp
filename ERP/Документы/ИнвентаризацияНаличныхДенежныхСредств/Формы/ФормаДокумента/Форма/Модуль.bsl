﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ИспользоватьНесколькоКасс = ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоКасс");
	
	ЗаполнитьСписокВыбораКассовыхКниг();
	
	// Обработчик механизма "Свойства"
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтаФорма, Объект, "ГруппаДополнительныеРеквизиты");
	
	// Обработчик механизма "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом
	
	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// СтандартныеПодсистемы.Свойства 
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтаФорма, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ЗаполнитьАналитикиДоходовРасходов(ЭтаФорма);
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтаФорма, Отказ, ПроверяемыеРеквизиты);
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Для каждого СтрокаТЧ Из Объект.Кассы Цикл
		Если ТипЗнч(СтрокаТЧ.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиДоходов") Тогда
			СтрокаТЧ.АналитикаДоходов = СтрокаТЧ.АналитикаДоходовРасходов;
			СтрокаТЧ.АналитикаРасходов = Неопределено;
		ИначеЕсли ТипЗнч(СтрокаТЧ.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиРасходов") Тогда
			СтрокаТЧ.АналитикаДоходов = Неопределено;
			СтрокаТЧ.АналитикаРасходов = СтрокаТЧ.АналитикаДоходовРасходов;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтаФорма, ТекущийОбъект);
	
	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ЗаполнитьАналитикиДоходовРасходов(ЭтаФорма);
	
	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	Объект.Кассы.Очистить();
	Объект.ПоследнийНомерПКО = "";
	Объект.ПоследнийНомерРКО = "";
	
	ОрганизацияПриИзмененииСервер();
	
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииСервер()
	
	ОтветственныеЛицаСервер.ПриИзмененииСвязанныхРеквизитовДокумента(Объект);
	
	ЗаполнитьСписокВыбораКассовыхКниг();
	
КонецПроцедуры

&НаКлиенте
Процедура КассоваяКнигаПриИзменении(Элемент)
	
	Объект.Кассы.Очистить();
	Объект.ПоследнийНомерПКО = "";
	Объект.ПоследнийНомерРКО = "";
	
КонецПроцедуры

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ОткрытьФормуРедактированияКомментария(Элемент.ТекстРедактирования, 
		Объект.Комментарий, 
		Модифицированность);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТабличнойЧасти

&НаКлиенте
Процедура КассыПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	СтрокаТаблицы = Элементы.Кассы.ТекущиеДанные;
	
	Если НоваяСтрока И Не Копирование Тогда
		Если Не ИспользоватьНесколькоКасс Тогда
			Если Не ЗначениеЗаполнено(Касса) Тогда
				ПолучитьКассуПоУмолчанию();
			КонецЕсли;
			СтрокаТаблицы.Касса = Касса;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КассыКассаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Кассы.ТекущиеДанные;
	ТекущаяСтрока.СуммаПоУчету = ОстатокВКассеПоУчету(ТекущаяСтрока.Касса);
	
КонецПроцедуры

&НаКлиенте
Процедура КассыСуммаПоУчетуПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Кассы.ТекущиеДанные;
	ТекущаяСтрока.СуммаРасхождения = ТекущаяСтрока.СуммаПоФакту - ТекущаяСтрока.СуммаПоУчету;
	
КонецПроцедуры

&НаКлиенте
Процедура КассыСуммаФактПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Кассы.ТекущиеДанные;
	ТекущаяСтрока.СуммаРасхождения = ТекущаяСтрока.СуммаПоФакту - ТекущаяСтрока.СуммаПоУчету;
	
КонецПроцедуры

&НаКлиенте
Процедура КассыСтатьяДоходовРасходовПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Кассы.ТекущиеДанные;

	Если ЗначениеЗаполнено(ТекущаяСтрока.СтатьяДоходовРасходов) Тогда
		КассыСтатьяДоходовРасходовПриИзмененииСервер();
	Иначе
		ТекущаяСтрока.АналитикаДоходовРасходов = Неопределено;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура КассыСтатьяДоходовРасходовПриИзмененииСервер()
	
	ТекущаяСтрока = Объект.Кассы.НайтиПоИдентификатору(Элементы.Кассы.ТекущаяСтрока);
	
	Если ТипЗнч(ТекущаяСтрока.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиДоходов") Тогда

		ДоходыИРасходыСервер.СтатьяДоходовПриИзменении(
			Объект,
			ТекущаяСтрока.СтатьяДоходовРасходов,
			ТекущаяСтрока.Подразделение,
			ТекущаяСтрока.АналитикаДоходовРасходов);
			
	ИначеЕсли ТипЗнч(ТекущаяСтрока.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиРасходов") Тогда
		
		ДоходыИРасходыСервер.СтатьяРасходовПриИзменении(
			Объект,
			ТекущаяСтрока.СтатьяДоходовРасходов,
			ТекущаяСтрока.АналитикаДоходовРасходов);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КассыСтатьяДоходовРасходовНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ТекущаяСтрока = Элементы.Кассы.ТекущиеДанные;
	
	МассивТипов = Новый Массив;
	Если ТекущаяСтрока.СуммаРасхождения > 0 Тогда
		МассивТипов.Добавить(Тип("ПланВидовХарактеристикСсылка.СтатьиДоходов"));
		Элемент.ОграничениеТипа = Новый ОписаниеТипов(МассивТипов, , );
	ИначеЕсли ТекущаяСтрока.СуммаРасхождения < 0 Тогда
		МассивТипов.Добавить(Тип("ПланВидовХарактеристикСсылка.СтатьиРасходов"));
		Элемент.ОграничениеТипа = Новый ОписаниеТипов(МассивТипов, , );
	Иначе
		СтандартнаяОбработка = Ложь;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаполнитьКассы(Команда)
	
	СтруктураРеквизитов = Новый Структура;
	СтруктураРеквизитов.Вставить("Организация");
	
	Если ОбщегоНазначенияУТКлиент.ВозможноЗаполнениеТабличнойЧасти(ЭтаФорма, Объект.Кассы, СтруктураРеквизитов, Ложь) Тогда
		ЗаполнитьКассыСервер();
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.Свойства
&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств()
	
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтаФорма, Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

 &НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()
	
	УсловноеОформление.Элементы.Очистить();
	
	// Подсказка ввода суммы по факту
	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.КассыСуммаПоФакту.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Кассы.СуммаПоФакту");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = 0;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЦветТекстаОтмененнойСтрокиДокумента);
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = '<для внесения результата>'"));
	
	// Поле расхождения - недостача
	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ИзлишекНедостача.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Кассы.СуммаРасхождения");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Меньше;
	ОтборЭлемента.ПравоеЗначение = 0;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Недостача'"));
	
	// Поле расхождения - излишек
	
	Элемент = УсловноеОформление.Элементы.Добавить();
	
	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ИзлишекНедостача.Имя);
	
	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.Кассы.СуммаРасхождения");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Больше;
	ОтборЭлемента.ПравоеЗначение = 0;
	
	Элемент.Оформление.УстановитьЗначениеПараметра("Текст", НСтр("ru = 'Излишек'"));
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСписокВыбораКассовыхКниг()
	
	Справочники.КассовыеКниги.КассовыеКнигиОрганизации(Объект.Организация, Элементы.КассоваяКнига.СписокВыбора);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьКассыСервер()
	
	Объект.Кассы.Очистить();
	Объект.ПоследнийНомерПКО = "";
	Объект.ПоследнийНомерРКО = "";
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	Кассы.Ссылка КАК Касса,
	|	Кассы.ВалютаДенежныхСредств КАК Валюта,
	|	ЕСТЬNULL(ДенежныеСредстваНаличные.СуммаОстаток, 0) КАК СуммаПоУчету,
	|	ВЫБОР КОГДА ЕСТЬNULL(ДенежныеСредстваНаличные.СуммаОстаток, 0) > 0 ТОГДА
	|		-ЕСТЬNULL(ДенежныеСредстваНаличные.СуммаОстаток, 0)
	|	ИНАЧЕ
	|		0
	|	КОНЕЦ КАК СуммаРасхождения
	|	
	|ИЗ
	|	Справочник.Кассы КАК Кассы
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрНакопления.ДенежныеСредстваНаличные.Остатки(&ДатаИнвентаризации, Организация = &Организация) КАК ДенежныеСредстваНаличные
	|	ПО
	|		ДенежныеСредстваНаличные.Касса = Кассы.Ссылка
	|		И ДенежныеСредстваНаличные.Организация = &Организация
	|	
	|ГДЕ
	|	Кассы.Владелец = &Организация
	|	И Кассы.КассоваяКнига = &КассоваяКнига
	|	И НЕ Кассы.ПометкаУдаления
	|	
	|";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("Организация", Объект.Организация);
	Запрос.УстановитьПараметр("КассоваяКнига", Объект.КассоваяКнига);
	Запрос.УстановитьПараметр("ДатаИнвентаризации", Новый Граница(КонецДня(Объект.Дата)));
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(НСтр("ru = 'Отсутствуют кассы для заполнения.'"));
		Возврат;
	Иначе
		ТаблицаКасс = РезультатЗапроса.Выгрузить();
		ИнвентаризируемыеКассы = ТаблицаКасс.ВыгрузитьКолонку("Касса");
		Объект.Кассы.Загрузить(ТаблицаКасс);
	КонецЕсли;
	
	// Максимальные номера ордеров
	ТекстЗапроса = "
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПриходныйКассовыйОрдер.Номер КАК НомерПКО
	|ИЗ
	|	Документ.ПриходныйКассовыйОрдер КАК ПриходныйКассовыйОрдер
	|ГДЕ
	|	ПриходныйКассовыйОрдер.Проведен
	|	И ПриходныйКассовыйОрдер.Касса В (&Кассы)
	|	И ПриходныйКассовыйОрдер.Дата <= &ДатаИнвентаризации
	|УПОРЯДОЧИТЬ ПО
	|	ПриходныйКассовыйОрдер.Дата Убыв
	|;
	|/////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	РасходныйКассовыйОрдер.Номер КАК НомерРКО
	|ИЗ
	|	Документ.РасходныйКассовыйОрдер КАК РасходныйКассовыйОрдер
	|ГДЕ
	|	РасходныйКассовыйОрдер.Проведен
	|	И РасходныйКассовыйОрдер.Касса В (&Кассы)
	|	И РасходныйКассовыйОрдер.Дата <= &ДатаИнвентаризации
	|УПОРЯДОЧИТЬ ПО
	|	РасходныйКассовыйОрдер.Дата Убыв
	|";
	
	Запрос = Новый Запрос(ТекстЗапроса);
	Запрос.УстановитьПараметр("Кассы", ИнвентаризируемыеКассы);
	Запрос.УстановитьПараметр("ДатаИнвентаризации", КонецДня(Объект.Дата));
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	Если Не РезультатЗапроса[0].Пустой() Тогда
		Выборка = РезультатЗапроса[0].Выбрать();
		Выборка.Следующий();
		Объект.ПоследнийНомерПКО = Выборка.НомерПКО;
	КонецЕсли;
	Если Не РезультатЗапроса[1].Пустой() Тогда
		Выборка = РезультатЗапроса[1].Выбрать();
		Выборка.Следующий();
		Объект.ПоследнийНомерРКО = Выборка.НомерРКО;
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ОстатокВКассеПоУчету(КассаСсылка)
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДенежныеСредстваНаличные.СуммаОстаток КАК СуммаПоУчету
	|ИЗ
	|	РегистрНакопления.ДенежныеСредстваНаличные.Остатки(,Касса = &Касса) КАК ДенежныеСредстваНаличные
	|");
	
	Запрос.УстановитьПараметр("Касса", КассаСсылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Выборка.Следующий();
	
	Возврат Выборка.СуммаПоУчету;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаполнитьАналитикиДоходовРасходов(Форма)
	
	Кассы = Форма.Объект.Кассы;
	
	Для каждого СтрокаТЧ Из Кассы Цикл
		Если ТипЗнч(СтрокаТЧ.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиДоходов") Тогда
			СтрокаТЧ.АналитикаДоходовРасходов = СтрокаТЧ.АналитикаДоходов;
		ИначеЕсли ТипЗнч(СтрокаТЧ.СтатьяДоходовРасходов) = Тип("ПланВидовХарактеристикСсылка.СтатьиРасходов") Тогда
			СтрокаТЧ.АналитикаДоходовРасходов = СтрокаТЧ.АналитикаРасходов;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ПолучитьКассуПоУмолчанию()
	
	Касса = ЗначениеНастроекПовтИсп.ПолучитьКассуОрганизацииПоУмолчанию();
	
КонецПроцедуры

#КонецОбласти
