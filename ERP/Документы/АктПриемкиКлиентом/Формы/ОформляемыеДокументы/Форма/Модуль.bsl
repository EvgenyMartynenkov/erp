﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	Если НЕ Параметры.Свойство("АктПриемкиКлиента") ИЛИ НЕ ЗначениеЗаполнено(Параметры.АктПриемкиКлиента) Тогда
		Возврат;
	КонецЕсли;
	
	АктПриемки = Параметры.АктПриемкиКлиента;
	ИспользоватьЗаявкиНаВозврат = ПолучитьФункциональнуюОпцию("ИспользоватьЗаявкиНаВозвратТоваровОтКлиентов");
	Заголовок = НСтр("ru = 'Оформление документов'") + " " + НСтр("ru = 'по'") + " " + Строка(АктПриемки);

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	СформироватьОтчет();
	
	ТабличныйДокумент.Показать();
	УправлениеДоступностью();
	
КонецПроцедуры

#КонецОбласти

#Область КомандыФормы

&НаКлиенте
Процедура Обновить(Команда)
	
	СформироватьОтчет();
	ТабличныйДокумент.Показать();
	УправлениеДоступностью();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура СформироватьОтчет()

	ЕстьОшибки = Ложь;
		
	ТабличныйДокумент.Очистить();
	Макет = Документы.АктПриемкиКлиентом.ПолучитьМакет("ОформлениеДокументов");
	ОбластьПустаяСтрока = Макет.ПолучитьОбласть("ПустаяСтрока");
	
	Запрос = Новый Запрос;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	АктПриемкиКлиентом.Ссылка,
	|	АктПриемкиКлиентом.Статус,
	|	АктПриемкиКлиентом.СпособОтраженияРасхождений,
	|	ВЫБОР
	|		КОГДА АктПриемкиКлиентом.Проведен
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК ЕстьОшибкиПроведение,
	|	ВЫБОР
	|		КОГДА АктПриемкиКлиентом.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыАктаПриемкиКлиента.НеСогласовано)
	|			ТОГДА ИСТИНА
	|		ИНАЧЕ ЛОЖЬ
	|	КОНЕЦ КАК ЕстьОшибкиСтатус,
	|	АктПриемкиКлиентом.ХозяйственнаяОперация
	|ИЗ
	|	Документ.АктПриемкиКлиентом КАК АктПриемкиКлиентом
	|ГДЕ
	|	АктПриемкиКлиентом.Ссылка = &АктПриемкиКлиентом";
	
	Запрос.УстановитьПараметр("АктПриемкиКлиентом", АктПриемки);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	Если РезультатЗапроса[0].Пустой() Тогда
		ВывестиОшибку(НСтр("ru = 'Данные документа недоступны'"), Макет);
		Возврат;
	КонецЕсли;
	
	ВыборкаШапка = РезультатЗапроса[0].Выбрать();
	ВыборкаШапка.Следующий();
	
	Если ВыборкаШапка.ЕстьОшибкиПроведение Тогда
		ВывестиОшибку(НСтр("ru = 'Документ не проведен. Оформление документов по результатам приемки невозможно.'"), Макет);
		Возврат;
	КонецЕсли;
	
	Если ВыборкаШапка.ЕстьОшибкиСтатус Тогда
		ВывестиОшибку(НСтр("ru = 'Документ находится в статусе ""Не согласовано"" . Оформление документов по результатам приемки невозможно.'"), Макет);
		Возврат;
	КонецЕсли;
	
	СпособОтраженияРасхождений = ВыборкаШапка.СпособОтраженияРасхождений;
	ХозяйственнаяОперацияАкта  = ВыборкаШапка.ХозяйственнаяОперация;
	
	Если Не ЗначениеЗаполнено(СпособОтраженияРасхождений) Тогда 
		
		ВывестиОшибку(НСтр("ru = 'В акте приемки не задан способ отражения расхождений'"), Макет);
		Возврат;
		
	КонецЕсли;
	
	Запрос.Текст = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	АктПриемкиКлиентомТовары.Действие,
	|	АктПриемкиКлиентомТовары.Реализация КАК Реализация
	|ПОМЕСТИТЬ РеализацииДействия
	|ИЗ
	|	Документ.АктПриемкиКлиентом.Товары КАК АктПриемкиКлиентомТовары
	|ГДЕ
	|	АктПриемкиКлиентомТовары.Ссылка = &АктПриемкиКлиентом
	|	И АктПриемкиКлиентомТовары.КоличествоУпаковокПоДокументу - АктПриемкиКлиентомТовары.Количество <> 0
	|	И АктПриемкиКлиентомТовары.Действие <> ЗНАЧЕНИЕ(Перечисление.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ПерепоставленноеДарится)
	|	И АктПриемкиКлиентомТовары.Действие <> ЗНАЧЕНИЕ(Перечисление.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.НедостачаНеПризнана)
	|	И АктПриемкиКлиентомТовары.Действие <> ЗНАЧЕНИЕ(Перечисление.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ПустаяСсылка)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	РеализацииДействия.Действие,
	|	РеализацииДействия.Реализация КАК Реализация
	|ИЗ
	|	РеализацииДействия КАК РеализацииДействия
	|ИТОГИ ПО
	|	Реализация
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	КорректировкаРеализации.ДокументОснование КАК Реализация,
	|	КорректировкаРеализации.Ссылка КАК Корректировка,
	|	КорректировкаРеализации.ХозяйственнаяОперация
	|ИЗ
	|	Документ.КорректировкаРеализации КАК КорректировкаРеализации
	|ГДЕ
	|	КорректировкаРеализации.АктПриемкиКлиентомОснование = &АктПриемкиКлиентом
	|	И КорректировкаРеализации.Проведен
	|	И ВЫБОР
	|			КОГДА &СпособОтраженияРасхождений = ЗНАЧЕНИЕ(Перечисление.СпособыОтраженияРасхожденийАктПриемкиКлиента.ИсправлениеПервичныхДокументов)
	|				ТОГДА КорректировкаРеализации.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ИсправлениеОшибок)
	|			ИНАЧЕ КорректировкаРеализации.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.РеализацияПерепоставленногоТовара)
	|					ИЛИ КорректировкаРеализации.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратНедопоставленногоТовара)
	|		КОНЕЦ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ЗаявкаНаВозвратТоваровОтКлиентаВозвращаемыеТовары.ДокументРеализации КАК Реализация,
	|	ЗаявкаНаВозвратТоваровОтКлиентаВозвращаемыеТовары.Ссылка КАК ДокументВозврата
	|ИЗ
	|	Документ.ЗаявкаНаВозвратТоваровОтКлиента.ВозвращаемыеТовары КАК ЗаявкаНаВозвратТоваровОтКлиентаВозвращаемыеТовары
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РеализацииДействия КАК РеализацииДействия
	|		ПО ЗаявкаНаВозвратТоваровОтКлиентаВозвращаемыеТовары.ДокументРеализации = РеализацииДействия.Реализация
	|ГДЕ
	|	&ИспользоватьЗаявкиНаВозврат
	|	И ЗаявкаНаВозвратТоваровОтКлиентаВозвращаемыеТовары.Ссылка.Проведен
	|
	|ОБЪЕДИНИТЬ
	|
	|ВЫБРАТЬ
	|	ВозвратТоваровОтКлиентаТовары.ДокументРеализации,
	|	ВозвратТоваровОтКлиентаТовары.Ссылка
	|ИЗ
	|	Документ.ВозвратТоваровОтКлиента.Товары КАК ВозвратТоваровОтКлиентаТовары
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РеализацииДействия КАК РеализацииДействия
	|		ПО ВозвратТоваровОтКлиентаТовары.ДокументРеализации = РеализацииДействия.Реализация
	|ГДЕ
	|	НЕ &ИспользоватьЗаявкиНаВозврат
	|	И ВозвратТоваровОтКлиентаТовары.Ссылка.Проведен
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	АктПриемкиКлиентомТовары.ЗаказКлиента КАК ЗаказКлиента,
	|	АктПриемкиКлиентомТовары.Реализация
	|ИЗ
	|	Документ.АктПриемкиКлиентом.Товары КАК АктПриемкиКлиентомТовары
	|ГДЕ
	|	АктПриемкиКлиентомТовары.КоличествоУпаковокПоДокументу <> АктПриемкиКлиентомТовары.КоличествоУпаковок
	|	И АктПриемкиКлиентомТовары.Действие = ЗНАЧЕНИЕ(Перечисление.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ДопоставкаНеТребуется)
	|	И АктПриемкиКлиентомТовары.Ссылка = &АктПриемкиКлиентом
	|	И АктПриемкиКлиентомТовары.ЗаказКлиента <> НЕОПРЕДЕЛЕНО
	|
	|УПОРЯДОЧИТЬ ПО
	|	ЗаказКлиента УБЫВ
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	РеализацияТоваровУслуг.Ссылка КАК ОформленнаяРеализация,
	|	РеализацияТоваровУслуг.ДопоставкаПоРеализации КАК РеализацияАкта
	|ИЗ
	|	РеализацииДействия КАК РеализацииДействия
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.РеализацияТоваровУслуг КАК РеализацияТоваровУслуг
	|		ПО (РеализацияТоваровУслуг.Проведен)
	|			И РеализацииДействия.Реализация = РеализацияТоваровУслуг.ДопоставкаПоРеализации";
	
	Запрос.УстановитьПараметр("СпособОтраженияРасхождений", СпособОтраженияРасхождений);
	Запрос.УстановитьПараметр("ИспользоватьЗаявкиНаВозврат", ИспользоватьЗаявкиНаВозврат);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	Если РезультатЗапроса[1].Пустой() Тогда
		ВывестиОшибку(НСтр("ru = 'По результатам приемки не выявлено расхождений, требующих оформления документов'"), Макет);
		Возврат;
	КонецЕсли;
	
	РеализацииДействия.Очистить();
	
	ВыборкаРеализации = РезультатЗапроса[1].Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	Пока ВыборкаРеализации.Следующий() Цикл
		
		НоваяСтрока = РеализацииДействия.Добавить();
		НоваяСтрока.Реализация = ВыборкаРеализации.Реализация;
		
		ВыборкаДействия = ВыборкаРеализации.Выбрать();
		Пока ВыборкаДействия.Следующий() Цикл
			
			НоваяСтрока.ВариантыДействий.Добавить(ВыборкаДействия.Действие);
			
			Если ВыборкаДействия.Действие = Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ВозвратПерепоставленного
				ИЛИ ВыборкаДействия.Действие = Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ПокупкаПерепоставленного Тогда
				
				НоваяСтрока.ЕстьОтражениеИзлишков = Истина;
				
			КонецЕсли;
			
			Если ВыборкаДействия.Действие = Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ДопоставкаНеТребуется
				ИЛИ ВыборкаДействия.Действие = Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ТребуетсяДопоставка Тогда
				
				НоваяСтрока.ЕстьОтражениеНедостач = Истина;
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЦикла;
	
	РеализацииКорректировки.Загрузить(РезультатЗапроса[2].Выгрузить());
	РеализацииВозвраты.Загрузить(РезультатЗапроса[3].Выгрузить());
	РеализацииЗаказы.Загрузить(РезультатЗапроса[4].Выгрузить());
	РеализацииРеализации.Загрузить(РезультатЗапроса[5].Выгрузить());
	МассивОформленныхДокументов = Новый Массив;
	
	ВыводитьГруппуРеализации = (РеализацииДействия.Количество() > 1 
	                           И ХозяйственнаяОперацияАкта <> Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию);
	
	Для Каждого СтрокаРеализацииДействия ИЗ РеализацииДействия Цикл
		
		
		Если ВыводитьГруппуРеализации Тогда
			
			ВывестиРеализациюАкта(СтрокаРеализацииДействия.Реализация, Макет);
			
		КонецЕсли;
		
		Если СпособОтраженияРасхождений = Перечисления.СпособыОтраженияРасхожденийАктПриемкиКлиента.ИсправлениеПервичныхДокументов Тогда
			
			Если ХозяйственнаяОперацияАкта = Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию Тогда
				ВывестиИсправлениеРеализации(СтрокаРеализацииДействия.Реализация, Макет);
			Иначе
				ВывестиДокументКорректировки(СтрокаРеализацииДействия, Перечисления.ХозяйственныеОперации.ИсправлениеОшибок, Макет);
			КонецЕсли;
		
			Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ВозвратПерепоставленного) <> Неопределено Тогда
				
				ВывестиДокументВозврата(СтрокаРеализацииДействия, Макет);
				
			КонецЕсли;
			
			Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ДопоставкаНеТребуется) <> Неопределено Тогда
				
				ВывестиДокументЗаказ(СтрокаРеализацииДействия, Макет);
				
			КонецЕсли;
			
			Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ТребуетсяДопоставка) <> Неопределено Тогда
				
				ВывестиДокументРеализация(СтрокаРеализацииДействия, Макет);
				
			КонецЕсли;
			
			ТабличныйДокумент.ЗакончитьГруппуСтрок();
			
		ИначеЕсли СпособОтраженияРасхождений = Перечисления.СпособыОтраженияРасхожденийАктПриемкиКлиента.ОформлениеНовыхПервичныхДокументов Тогда
			
			Если СтрокаРеализацииДействия.ЕстьОтражениеИзлишков Тогда
				
				ВывестиДокументКорректировки(СтрокаРеализацииДействия, Перечисления.ХозяйственныеОперации.РеализацияПерепоставленногоТовара, Макет);
				
				Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ВозвратПерепоставленного) <> Неопределено Тогда
					
					ВывестиДокументВозврата(СтрокаРеализацииДействия, Макет);
					
				КонецЕсли;
				
				ТабличныйДокумент.ЗакончитьГруппуСтрок();
			
			КонецЕсли;
			
			Если СтрокаРеализацииДействия.ЕстьОтражениеНедостач Тогда
				
				ВывестиДокументКорректировки(СтрокаРеализацииДействия, Перечисления.ХозяйственныеОперации.ВозвратНедопоставленногоТовара, Макет);
				
				Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ДопоставкаНеТребуется) <> Неопределено Тогда
					
					ВывестиДокументЗаказ(СтрокаРеализацииДействия, Макет);
					
				КонецЕсли;
				
				Если СтрокаРеализацииДействия.ВариантыДействий.НайтиПоЗначению(Перечисления.ВариантыДействийПоРасхождениямВАктеПриемкеКлиента.ТребуетсяДопоставка) <> Неопределено Тогда
					
					ВывестиДокументРеализация(СтрокаРеализацииДействия, Макет);
					
				КонецЕсли;
				
				ТабличныйДокумент.ЗакончитьГруппуСтрок();
				
			КонецЕсли;
			
		КонецЕсли;
		
		Если ВыводитьГруппуРеализации Тогда
			
			ТабличныйДокумент.ЗакончитьГруппуСтрок();
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура ВывестиДокументКорректировки(СтрокаРеализацииДействия, ХозяйственнаяОперация, Макет)
	
	МассивОформленныхДокументов = Новый Массив;

	ОформленныеДокументы = РеализацииКорректировки.НайтиСтроки(Новый Структура("Реализация, ХозяйственнаяОперация",
	                                                                           СтрокаРеализацииДействия.Реализация,
	                                                                           ХозяйственнаяОперация));
	
	Если ОформленныеДокументы.Количество() = 0 Тогда
		
		СтруктураКОформлению = Новый Структура("ОформляемыйДокумент, Реализация, ХозяйственнаяОперация",
		Документы.КорректировкаРеализации.ПустаяСсылка(),
		СтрокаРеализацииДействия.Реализация,
		ХозяйственнаяОперация);
		
		МассивОформленныхДокументов.Добавить(СтруктураКОформлению);
		
	Иначе
		
		Для Каждого ОформленныйДокумент Из ОформленныеДокументы Цикл
			
			МассивОформленныхДокументов.Добавить(ОформленныйДокумент.Корректировка);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ВывестиДокументКОформлению("КорректировкаРеализации", 1, Макет, МассивОформленныхДокументов, ХозяйственнаяОперация);
	
	ТабличныйДокумент.НачатьГруппуСтрок("Корректировка");
	
КонецПроцедуры

&НаСервере
Процедура ВывестиИсправлениеРеализации(ДокументРеализации, Макет)
	
	МассивОформленныхДокументов = Новый Массив;
	МассивОформленныхДокументов.Добавить(ДокументРеализации);
	
	ВывестиДокументКОформлению("ИсправлениеРеализации", 1, Макет, МассивОформленныхДокументов, , Ложь);
	
	ТабличныйДокумент.НачатьГруппуСтрок("Корректировка");
	
КонецПроцедуры

&НаСервере
Процедура ВывестиДокументВозврата(СтрокаРеализацииДействия, Макет)
	
	МассивОформленныхДокументов = Новый Массив;

	ОформленныеДокументы = РеализацииВозвраты.НайтиСтроки(Новый Структура("Реализация", СтрокаРеализацииДействия.Реализация));
	
	Если ОформленныеДокументы.Количество() = 0 Тогда
		
		СтруктураКОформлению = Новый Структура("ОформляемыйДокумент, Реализация",
		?(ИспользоватьЗаявкиНаВозврат, Документы.ЗаявкаНаВозвратТоваровОтКлиента.ПустаяСсылка(), Документы.ВозвратТоваровОтКлиента.ПустаяСсылка()),
		СтрокаРеализацииДействия.Реализация);
		
		МассивОформленныхДокументов.Добавить(СтруктураКОформлению);
		
	Иначе
		
		Для Каждого ОформленныйДокумент Из ОформленныеДокументы Цикл
			
			МассивОформленныхДокументов.Добавить(ОформленныйДокумент.ДокументВозврата);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ВывестиДокументКОформлению("ДокументВозврата", 2, Макет, МассивОформленныхДокументов);
	
КонецПроцедуры

&НаСервере
Процедура ВывестиДокументРеализация(СтрокаРеализацииДействия, Макет)
	
	МассивОформленныхДокументов = Новый Массив;

	ОформленныеДокументы = РеализацииРеализации.НайтиСтроки(Новый Структура("РеализацияАкта", СтрокаРеализацииДействия.Реализация));
	
	Если ОформленныеДокументы.Количество() = 0 Тогда
		
		СтруктураКОформлению = Новый Структура("ОформляемыйДокумент, Реализация",
		Документы.РеализацияТоваровУслуг.ПустаяСсылка(),
		СтрокаРеализацииДействия.Реализация);
		
		МассивОформленныхДокументов.Добавить(СтруктураКОформлению);
		
	Иначе
		
		Для Каждого ОформленныйДокумент Из ОформленныеДокументы Цикл
			
			МассивОформленныхДокументов.Добавить(ОформленныйДокумент.ОформленнаяРеализация);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ВывестиДокументКОформлению("РеализацияТоваровИУслуг", 2, Макет, МассивОформленныхДокументов);
	
КонецПроцедуры

&НаСервере
Процедура ВывестиДокументЗаказ(СтрокаРеализацииДействия, Макет)
	
	МассивОформленныхДокументов = Новый Массив;
	
	ОформленныеДокументы = РеализацииЗаказы.НайтиСтроки(Новый Структура("Реализация", СтрокаРеализацииДействия.Реализация));
	
	Если ОформленныеДокументы.Количество() = 0 Тогда
		
		Возврат;
		
	Иначе
		
		Для Каждого ОформленныйДокумент Из ОформленныеДокументы Цикл
			
			МассивОформленныхДокументов.Добавить(ОформленныйДокумент.ЗаказКлиента);
			
		КонецЦикла;
		
	КонецЕсли;
	
	ВывестиДокументКОформлению("ЗаказКлиента", 2, Макет, МассивОформленныхДокументов);
	
КонецПроцедуры

&НаСервере
Процедура ВывестиРеализациюАкта(Реализация, Макет)

	ОбластьПустаяСтрока = Макет.ПолучитьОбласть("ПустаяСтрока");
	ОбластьРеализация   = Макет.ПолучитьОбласть("Реализация");
	
	ТабличныйДокумент.Вывести(ОбластьПустаяСтрока);
	ОбластьРеализация.Параметры.Реализация = Реализация;
	ТабличныйДокумент.Вывести(ОбластьРеализация);
	
	ТабличныйДокумент.НачатьГруппуСтрок("Реализация");
	
КонецПроцедуры

&НаСервере
Функция ТекстШапки(ТипДокумента, ХозяйственнаяОперация = Неопределено)
	
	Если ТипДокумента = "КорректировкаРеализации" Тогда
		Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ИсправлениеОшибок Тогда
			Возврат НСтр("ru = 'Документ ""Корректировка реализации"" по выявленным расхождениям'");
		ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.РеализацияПерепоставленногоТовара Тогда
			Возврат НСтр("ru = 'Документ ""Корректировка реализации"" по реализации перепоставленных товаров'");
		ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратНедопоставленногоТовара Тогда
			Возврат НСтр("ru = 'Документ ""Корректировка реализации"" для отражения возврата недопоставленного товара'");
		КонецЕсли;
	ИначеЕсли ТипДокумента = "ИсправлениеРеализации" Тогда
		Возврат НСтр("ru = 'Отразить расхождения в документе «Реализация товаров и услуг»'");
	ИначеЕсли ТипДокумента = "ДокументВозврата" Тогда
		Если ИспользоватьЗаявкиНаВозврат Тогда
			Возврат НСтр("ru = 'Документ ""Заявка на возврат товаров от клиента"" по перепоставленным товарам к возврату'");
		Иначе
			Возврат НСтр("ru = 'Документ ""Возврат товаров от клиента"" по перепоставленным товарам к возврату'");
		КонецЕсли;
	ИначеЕсли ТипДокумента = "ЗаказКлиента" Тогда
		Возврат НСтр("ru = 'Отменить строки документов ""Заказ клиента"" по недопоставленным товарам'");
	ИначеЕсли ТипДокумента = "РеализацияТоваровИУслуг" Тогда
		Возврат НСтр("ru = 'Документ ""Реализация товаров и услуг"" для реализации недопоставленного товара'");
	КонецЕсли;
	
КонецФункции

&НаСервере
Функция ТекстЛегенды(ТипДокумента, ХозяйственнаяОперация = Неопределено)
	
	Если ТипДокумента = "КорректировкаРеализации" Тогда
		Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ИсправлениеОшибок Тогда
			Возврат НСтр("ru = 'Необходимо оформить документ ""Корректировка реализации"" с операцией ""Исправление ошибок"" для отражения излишков и недостач. Документ является основанием для выставления исправленных первичных документов и исправительного счета-фактуры.'");
		ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.РеализацияПерепоставленногоТовара Тогда
			Возврат НСтр("ru = 'Необходимо оформить документ ""Корректировка реализации"" с операцией ""Реализация перепоставленного товара"" для отражения в учете факта продажи излишне доставленных товаров. Документ является основанием для выставления первичных документов и счета-фактуры. Оформляется на строки акта приемки с вариантами отработки: ""Возврат перепоставленного"" и ""Покупка перепоставленного"".'");
		ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратНедопоставленногоТовара Тогда
			Возврат НСтр("ru = 'Необходимо зарегистрировать документ ""Корректировка реализации"" с операцией ""Возврат недопоставленного товара"". Выполняется сторнирование в учете факта продажи товаров, которые были отражены при реализации, но не были доставлены. Документ регистрируется на основании первичных документов и счета-фактуры, полученных от клиента. Оформляется на строки акта приемки с вариантами отработки: ""Требуется допоставка"" и ""Допоставка не требуется"".'");
		КонецЕсли;
	ИначеЕсли ТипДокумента = "ИсправлениеРеализации" Тогда
			Возврат НСтр("ru = 'Необходимо скорректировать документ ""Реализация товаров и услуг"", отразив в нем количество по факту. Следует выполнить для строк акта приемки с вариантами отработки: ""Возврат перепоставленного"", ""Покупка перепоставленного"", ""Требуется допоставка"" и ""Допоставка не требуется"".'");
	ИначеЕсли ТипДокумента = "ДокументВозврата" Тогда
		Если ИспользоватьЗаявкиНаВозврат Тогда
			Возврат НСтр("ru = 'Необходимо оформить документ ""Заявка на возврат товаров от клиента"" по излишкам, отмеченным к возврату клиентом. Оформляется на строки акта приемки с вариантом отработки ""Возврат перепоставленного"".'");
		Иначе
			Возврат НСтр("ru = 'Необходимо зарегистрировать документ ""Возврат товаров от клиента"" по излишкам, отмеченным к возврату клиентом. Оформляется на строки акта приемки с вариантом отработки ""Возврат перепоставленного"".'");
		КонецЕсли;
	ИначеЕсли ТипДокумента = "ЗаказКлиента" Тогда
		Возврат НСтр("ru = 'Необходимо скорректировать документы ""Заказ клиента"", отменив строки с товарами, которые были отражены при реализации, но не были доставлены. Следует выполнить для строк акта приемки с вариантом отработки ""Допоставка не требуется"".'");
	ИначеЕсли ТипДокумента = "РеализацияТоваровИУслуг" Тогда
		Возврат НСтр("ru = 'Необходимо оформить документ ""Реализация товаров и услуг"" по недостачам, отмеченным к допоставке клиенту. Документ является распоряжением на отгрузку товаров клиенту. Оформляется на строки акта приемки с вариантом отработки ""Требуется допоставка"".'");
	КонецЕсли;
	
КонецФункции

&НаСервере
Процедура ВывестиОшибку(ТекстСообщения, Макет)
	
	ОбластьОшибка = Макет.ПолучитьОбласть("СтрокаОшибка");
	ОбластьОшибка.Параметры.ТекстОшибки = ТекстСообщения;
	ТабличныйДокумент.Вывести(ОбластьОшибка);
	
	ЕстьОшибка = Истина;
	
КонецПроцедуры

&НаСервере
Процедура ВывестиДокументКОформлению(ВидДокумента, Уровень, Макет,
	                                 МассивОформленныхДокументов,
	                                 ХозяйственнаяОперация = Неопределено,
	                                 ВыводитьШапкуОфомленныеДокументы = Истина)
	
	ОбластьПустаяСтрока       = Макет.ПолучитьОбласть("ПустаяСтрока");
	ОбластьТонкаяПустаяСтрока = Макет.ПолучитьОбласть("ТонкаяПустаяСтрока");
	ОбластьШапкаДокумента     = Макет.ПолучитьОбласть("ШапкаУровень" + Строка(Уровень));
	ОбластьЛегендаДокумента   = Макет.ПолучитьОбласть("ЛегендаУровень" + Строка(Уровень));
	ОбластьСтрокаДокумента    = Макет.ПолучитьОбласть("СтрокаУровень" + Строка(Уровень));
	
	ЖирныйШрифтTahoma9  = Новый Шрифт("Tahoma", 9, Истина,Ложь,Истина);
	ОбычныйШрифтTahoma9 = Новый Шрифт("Tahoma", 9, Ложь, Ложь, Истина);
	ЖирныйШрифтTahoma8  = Новый Шрифт("Tahoma", 8,Ложь, Истина,Ложь);
	
	ТабличныйДокумент.Вывести(ОбластьПустаяСтрока);
	ОбластьШапкаДокумента.Параметры.Текст = ТекстШапки(ВидДокумента, ХозяйственнаяОперация);
	ТабличныйДокумент.Вывести(ОбластьШапкаДокумента);
	
	Если ВыводитьЛегенду Тогда
		ТабличныйДокумент.Вывести(ОбластьТонкаяПустаяСтрока);
		ОбластьЛегендаДокумента.Параметры.Текст = ТекстЛегенды(ВидДокумента, ХозяйственнаяОперация);
		ТабличныйДокумент.Вывести(ОбластьЛегендаДокумента);
		ТабличныйДокумент.Вывести(ОбластьТонкаяПустаяСтрока);
	КонецЕсли;
	
	//строки
	
	ПервыйЭлемент = Истина;
	
	Для Каждого ЭлементМассива Из МассивОформленныхДокументов Цикл
		
		Если ТипЗнч(ЭлементМассива) = Тип("Структура") Тогда
			
			ОбластьСтрокаДокумента.Параметры.Текст = НСтр("ru = 'Оформить'");
			ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].Шрифт = ЖирныйШрифтTahoma9;
			ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].ЦветТекста = ЦветаСтиля.ЦветГиперссылки;
			
		Иначе
			
			Если ПервыйЭлемент И ВыводитьШапкуОфомленныеДокументы Тогда
				ОбластьСтрокаДокумента.Параметры.Текст = Строка(НСтр("ru = 'Оформленные документы:'"));
				ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].ЦветТекста = Новый Цвет(0, 0, 0);
				ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].Шрифт = ЖирныйШрифтTahoma8;
				ТабличныйДокумент.Вывести(ОбластьСтрокаДокумента);
				ТабличныйДокумент.Вывести(ОбластьТонкаяПустаяСтрока);
			КонецЕсли;
			
			ОбластьСтрокаДокумента.Параметры.Текст = Строка(ЭлементМассива);
			ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].ЦветТекста = Новый Цвет(0, 0, 0);
			ОбластьСтрокаДокумента.Области["СтрокаУровень" + Строка(Уровень)].Шрифт = ОбычныйШрифтTahoma9;
			
			ПервыйЭлемент = Ложь;
			
		КонецЕсли;
		
		ОбластьСтрокаДокумента.Параметры.ОформленныйДокумент = ЭлементМассива;
		ТабличныйДокумент.Вывести(ОбластьСтрокаДокумента);
		
	КонецЦикла;
	
	МассивОформленныхДокументов.Очистить();

КонецПроцедуры

&НаКлиенте
Процедура УправлениеДоступностью()

	Элементы.ТабличныйДокумент.ТолькоПросмотр = ЕстьОшибка;

КонецПроцедуры 

&НаКлиенте
Процедура ТабличныйДокументВыбор(Элемент, Область, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если Не ЗначениеЗаполнено(Область.Расшифровка) Тогда
		Возврат;
	КонецЕсли;
	
	Реализация = ПредопределенноеЗначение("Документ.РеализацияТоваровУслуг.ПустаяСсылка");
	Операция = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ИсправлениеОшибок");
	
	Если ТипЗнч(Область.Расшифровка) <> Тип("Структура") Тогда
		ПоказатьЗначение(, Область.Расшифровка);
	Иначе
		
		ПараметрыФормы = Новый Структура;
		
		Если ТипЗнч(Область.Расшифровка.ОформляемыйДокумент) = Тип("ДокументСсылка.КорректировкаРеализации") Тогда
			
			СамообслуживаниеКлиент.ОткрытьФормуКорректировкиРеализацииНаОснованииАктаПриемкиСоСтороныПартнера(
			      АктПриемки,
			      Область.Расшифровка.Реализация,
			      Область.Расшифровка.ХозяйственнаяОперация);
			
		ИначеЕсли ТипЗнч(Область.Расшифровка.ОформляемыйДокумент) = Тип("ДокументСсылка.ЗаявкаНаВозвратТоваровОтКлиента") Тогда
			
			ПараметрыФормы.Вставить("АктПриемки", АктПриемки);
			ПараметрыФормы.Вставить("РеализацияТоваровУслуг", Область.Расшифровка.Реализация);
			ОткрытьФорму("Документ.ЗаявкаНаВозвратТоваровОтКлиента.Форма.ФормаДокумента", Новый Структура("Основание", ПараметрыФормы), ЭтаФорма);
			
		ИначеЕсли ТипЗнч(Область.Расшифровка.ОформляемыйДокумент) = Тип("ДокументСсылка.ВозвратТоваровОтКлиента") Тогда
			
			ПараметрыФормы.Вставить("АктПриемки", АктПриемки);
			ПараметрыФормы.Вставить("РеализацияТоваровУслуг", Область.Расшифровка.Реализация);
			ОткрытьФорму("Документ.ВозвратТоваровОтКлиента.Форма.ФормаДокумента", Новый Структура("Основание", ПараметрыФормы), ЭтаФорма);
			
		ИначеЕсли ТипЗнч(Область.Расшифровка.ОформляемыйДокумент) = Тип("ДокументСсылка.РеализацияТоваровУслуг") Тогда
			
			ПараметрыФормы.Вставить("АктПриемки", АктПриемки);
			ПараметрыФормы.Вставить("РеализацияТоваровУслуг", Область.Расшифровка.Реализация);
			ОткрытьФорму("Документ.РеализацияТоваровУслуг.Форма.ФормаДокумента", Новый Структура("Основание", ПараметрыФормы), ЭтаФорма);
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


