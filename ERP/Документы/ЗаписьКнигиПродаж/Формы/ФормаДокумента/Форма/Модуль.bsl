﻿&НаКлиенте
Перем КэшированныеЗначения;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы "Дополнительные отчеты и обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтаФорма);
	
	// Обработчик механизма "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтаФорма);
	
	// Выполним действия, которые выполняются при чтении на сервере
	Если НЕ ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ПриЧтенииСозданииНаСервере();
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтаФорма, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

	// ИнтеграцияС1СДокументооборотом
	ИнтеграцияС1СДокументооборот.ПриСозданииНаСервере(ЭтаФорма);
	// Конец ИнтеграцияС1СДокументооборотом

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);
	
	Если ТекущийВариантИнтерфейсаКлиентскогоПриложения() = ВариантИнтерфейсаКлиентскогоПриложения.Версия8_2 Тогда
		Элементы.ГруппаИтого.ЦветФона = Новый Цвет();
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ИсточникВыбора.ИмяФормы = "Документ.СчетФактураВыданный.Форма.ФормаДокумента"
	 ИЛИ ИсточникВыбора.ИмяФормы = "Документ.СчетФактураВыданный.Форма.ФормаСписка" Тогда
	 	
	 	НастроитьОтображениеРеквизитовСчетаФактуры(ЭтаФорма, ВыбранноеЗначение);
		
	КонецЕсли;
	
	Если Окно <> Неопределено Тогда
		Окно.Активизировать();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Ценности,
		Новый Структура("ЗаполнитьПризнакВедетсяУчетПоГТД",
			Новый Структура("Номенклатура", "ВедетсяУчетПоГТД")));
	
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначения.ПолучитьКартинкуКомментария(Объект.Комментарий);

	МодификацияКонфигурацииПереопределяемый.ПослеЗаписиНаСервере(ЭтаФорма, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	// Обработчик механизма "ДатыЗапретаИзменения"
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);
	
	ПриЧтенииСозданииНаСервере();
	
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначения.ПолучитьКартинкуКомментария(Объект.Комментарий);

	МодификацияКонфигурацииПереопределяемый.ПриЧтенииНаСервере(ЭтаФорма, ТекущийОбъект);

КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)

	МодификацияКонфигурацииКлиентПереопределяемый.ПослеЗаписи(ЭтаФорма, ПараметрыЗаписи);

КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)

	МодификацияКонфигурацииПереопределяемый.ПередЗаписьюНаСервере(ЭтаФорма, Отказ, ТекущийОбъект, ПараметрыЗаписи);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ЗаписьДополнительногоЛистаПриИзменении(Элемент)
	
	УстановитьДоступностьЭлементовДопЛистов(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзменении(Элемент)
	
	НеобходимоПересчитатьВалюту(Новый ОписаниеОповещения("ВалютаПриИзмененииЗавершение", ЭтотОбъект));
	
КонецПроцедуры

&НаКлиенте
Процедура ВалютаПриИзмененииЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    Если Результат Тогда
        
        ПриИзмененииВалютыСервер(Объект.Валюта);
        ЦенообразованиеКлиент.ОповеститьОбОкончанииПересчетаСуммВВалюту(ВалютаДокумента, Объект.Валюта);
        
    КонецЕсли;
    
    ВалютаДокумента = Объект.Валюта;

КонецПроцедуры

&НаКлиенте
Процедура ДокументРасчетовПриИзменении(Элемент)
	
	УстановитьВидимостьЭлементовПоОперации(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ТекстСчетФактураНажатие(Элемент, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ПродажиКлиент.ВвестиСчетФактуру(ЭтаФорма, Объект.Организация, Истина);
	
КонецПроцедуры

&НаКлиенте
Процедура СписокСчетовФактурНажатие(Элемент)
	
	СтруктураОтбор = Новый Структура("ДокументОснование, Организация, ПометкаУдаления", Объект.Ссылка, Объект.Организация, Ложь);
	
	ОткрытьФорму(
		"Документ.СчетФактураВыданный.ФормаСписка",
		Новый Структура("Отбор", СтруктураОтбор),
		ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура КодВидаОперацииНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ВыбранныйЭлемент = Неопределено;
	
	ПоказатьВыборИзМеню(Новый ОписаниеОповещения("КодВидаОперацииНачалоВыбораЗавершение", ЭтотОбъект), СписокКодовВидовОпераций, Элемент);
	
КонецПроцедуры

&НаКлиенте
Процедура КодВидаОперацииНачалоВыбораЗавершение(ВыбранныйЭлемент, ДополнительныеПараметры) Экспорт
	
	Если ВыбранныйЭлемент <> Неопределено Тогда
		Объект.КодВидаОперации = ВыбранныйЭлемент.Значение;
		ПредставлениеВидаОперации = Сред(ВыбранныйЭлемент.Представление, 4);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЦенности

&НаКлиенте
Процедура ЦенностиПослеУдаления(Элемент)
	
	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенностиПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенностиНоменклатураПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Ценности.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ЗаполнитьСтавкуНДС",  НалогообложениеНДС);
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСумму", "Количество");
	СтруктураДействий.Вставить("ЗаполнитьПризнакВедетсяУчетПоГТД", Новый Структура("Номенклатура", "ВедетсяУчетПоГТД"));

	СтруктураДействий.Вставить("НоменклатураПриИзмененииПереопределяемый", Новый Структура("ИмяФормы, ИмяТабличнойЧасти",
		ЭтаФорма.ИмяФормы, "Ценности"));

	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
	ОбновитьСуммыПодвала(ЭтаФорма);

КонецПроцедуры

&НаКлиенте
Процедура ЦенностиКоличествоПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Ценности.ТекущиеДанные;
	
	СтруктураДействий = Новый Структура;
	ДобавитьВСтруктуруДействияПриИзмененииКоличества(СтруктураДействий, Объект);	
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенностиЦенаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Ценности.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСумму", "Количество");
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);

	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенностиСуммаПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.Ценности.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ЦенностиСтавкаНДСПриИзменении(Элемент)

	ТекущаяСтрока = Элементы.Ценности.ТекущиеДанные;
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	
	СтруктураДействий = Новый Структура;
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	
	ОбработкаТабличнойЧастиКлиент.ОбработатьСтрокуТЧ(ТекущаяСтрока, СтруктураДействий, КэшированныеЗначения);
	
	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыДокументыОплаты

&НаКлиенте
Процедура ДокументыОплатыДокументОплатыПриИзменении(Элемент)
	
	ТекущаяСтрока = Элементы.ДокументыОплаты.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущаяСтрока.ДокументОплаты) Тогда
		ТекущаяСтрока.ДатаОплаты = ОбщегоНазначенияУТВызовСервера.ЗначениеРеквизитаОбъекта(ТекущаяСтрока.ДокументОплаты, "Дата");
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ВвестиИсправлениеСчетаФактуры(Команда)
	
	ПродажиКлиент.ВвестиСчетФактуру(ЭтаФорма, Объект.Организация, Ложь, Истина);
	
КонецПроцедуры

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
	ИнтеграцияС1СДокументооборотКлиент.ВыполнитьПодключаемуюКомандуИнтеграции(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаКлиенте
Процедура Подключаемый_ВыполнитьНазначаемуюКоманду(Команда)
	
	Если НЕ ДополнительныеОтчетыИОбработкиКлиент.ВыполнитьНазначаемуюКомандуНаКлиенте(ЭтаФорма, Команда.Имя) Тогда
		РезультатВыполнения = Неопределено;
		ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(Команда.Имя, РезультатВыполнения);
		ДополнительныеОтчетыИОбработкиКлиент.ПоказатьРезультатВыполненияКоманды(ЭтаФорма, РезультатВыполнения);
	КонецЕсли;
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//
	
	НоменклатураСервер.УстановитьУсловноеОформлениеНомераГТД(ЭтаФорма,
															 "ЦенностиНомерГТД",
															 "Объект.Ценности.ВедетсяУчетПоГТД");
	
	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ЦенностиСторнирующаяЗаписьДопЛиста.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ЗаписьДополнительногоЛиста");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;

	Элемент.Оформление.УстановитьЗначениеПараметра("Видимость", Ложь);

КонецПроцедуры

// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

&НаСервере
Процедура ДополнительныеОтчетыИОбработкиВыполнитьНазначаемуюКомандуНаСервере(ИмяЭлемента, РезультатВыполнения)
	
	ДополнительныеОтчетыИОбработки.ВыполнитьНазначаемуюКомандуНаСервере(ЭтаФорма, ИмяЭлемента, РезультатВыполнения);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки

#Область ПриИзмененииРеквизитов

// Пересчитывает суммы документа в выбранную валюту
//
// Параметры:
// НоваяВалюта - Валюта, в которую необходимо пересчитать суммы
//
&НаСервере
Процедура ПриИзмененииВалютыСервер(НоваяВалюта)
	
	СтараяВалюта                = ВалютаДокумента;
	ДатаДокумента               = ?(ЗначениеЗаполнено(Объект.Дата), Объект.Дата,ТекущаяДата());
	СтруктураКурсовСтаройВалюты = РаботаСКурсамиВалют.ПолучитьКурсВалюты(СтараяВалюта, ДатаДокумента);
	СтруктураКурсовНовойВалюты  = РаботаСКурсамиВалют.ПолучитьКурсВалюты(НоваяВалюта,  ДатаДокумента);
	
	Для Каждого ТекСтрока Из Объект.Ценности Цикл
		
		ТекСтрока.Цена = РаботаСКурсамиВалютКлиентСервер.ПересчитатьИзВалютыВВалюту(
			ТекСтрока.Цена,
			СтараяВалюта,НоваяВалюта,
			СтруктураКурсовСтаройВалюты.Курс,СтруктураКурсовНовойВалюты.Курс,
			СтруктураКурсовСтаройВалюты.Кратность,СтруктураКурсовНовойВалюты.Кратность);
		
		ТекСтрока.Сумма = ТекСтрока.Количество * ТекСтрока.Цена;
			
		ТекСтрока.СуммаНДС = Ценообразование.РассчитатьСуммуНДС(ТекСтрока.Сумма, ТекСтрока.СтавкаНДС, Ложь);
		
	КонецЦикла;
		
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ДобавитьВСтруктуруДействияПриИзмененииКоличества(СтруктураДействий, Объект)
	
	СтруктураПересчетаСуммы = ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ();
	СтруктураДействий.Вставить("ПересчитатьСуммуНДС", СтруктураПересчетаСуммы);
	СтруктураДействий.Вставить("ПересчитатьСумму", "Количество");
	
КонецФункции

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	НалогообложениеНДС = Перечисления.ТипыНалогообложенияНДС.ПродажаОблагаетсяНДС;
	
	ВалютаДокумента = Объект.Валюта;
	УстановитьВидимостьЭлементовПоОперации(ЭтаФорма);
	УстановитьДоступностьЭлементовДопЛистов(ЭтаФорма);
	
	НоменклатураСервер.ЗаполнитьСлужебныеРеквизитыПоНоменклатуреВКоллекции(
		Объект.Ценности,
		Новый Структура("ЗаполнитьПризнакВедетсяУчетПоГТД",
			Новый Структура("Номенклатура", "ВедетсяУчетПоГТД")));
	
	НастроитьОтображениеРеквизитовСчетаФактуры(
		ЭтаФорма,
		ПродажиСервер.ПараметрыПредставленияСчетаФактуры(Объект.Ссылка, Объект.Организация));
		
	СписокКодовВидовОпераций = Документы.СчетФактураВыданный.СписокКодовВидовОпераций();
	ОбновитьПредставлениеВидаОперации(ЭтаФорма);

	ОбновитьСуммыПодвала(ЭтаФорма);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьВидимостьЭлементовПоОперации(Форма)

	ЭтоКорректировкаНачисленногоНДС = ЗначениеЗаполнено(Форма.Объект.ДокументРасчетов);
	
	// Операция "Корректировка начисленного НДС"
	Форма.Элементы.ЦенностиВидЦенности.Видимость                = ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиСобытие.Видимость                    = ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиСторнирующаяЗаписьДопЛиста.Видимость = ЭтоКорректировкаНачисленногоНДС;
	
	// Операция "Начисление НДС"
	Форма.Элементы.ЦенностиНоменклатура.Видимость                 = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиНоменклатураЕдиницаИзмерения.Видимость = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиКоличество.Видимость                   = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиЦена.Видимость                         = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиНомерГТДСтранаПроисхождения.Видимость  = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ЦенностиНомерГТД.Видимость                     = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ГруппаСчетФактура.Видимость                    = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.Грузоотправитель.Доступность                   = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.Грузополучатель.Доступность                    = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.Руководитель.Доступность                       = НЕ ЭтоКорректировкаНачисленногоНДС;
	Форма.Элементы.ГлавныйБухгалтер.Доступность                   = НЕ ЭтоКорректировкаНачисленногоНДС;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьЭлементовДопЛистов(Форма)

	Форма.Элементы.ЦенностиСторнирующаяЗаписьДопЛиста.Доступность            = Форма.Объект.ЗаписьДополнительногоЛиста;
	Форма.Элементы.КорректируемыйПериод.Доступность                          = Форма.Объект.ЗаписьДополнительногоЛиста; 
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьСуммыПодвала(Форма)

	Форма.СуммаВсего = Форма.Объект.Ценности.Итог("Сумма") + Форма.Объект.Ценности.Итог("СуммаНДС");

КонецПроцедуры

&НаКлиенте
Процедура НеобходимоПересчитатьВалюту(Знач Оповещение)

	Если Не ЗначениеЗаполнено(Объект.Валюта) Тогда
		ВыполнитьОбработкуОповещения(Оповещение, Ложь);
		Возврат;
	ИначеЕсли Не ЗначениеЗаполнено(ВалютаДокумента) Тогда
		ВыполнитьОбработкуОповещения(Оповещение, Ложь);
		Возврат;
	ИначеЕсли ВалютаДокумента = Объект.Валюта Тогда
		ВыполнитьОбработкуОповещения(Оповещение, Ложь);
		Возврат;
	ИначеЕсли Объект.Ценности.Итог("Сумма") = 0 Тогда
		ВыполнитьОбработкуОповещения(Оповещение, Ложь);
		Возврат;
	КонецЕсли;
	
	ТекстСообщения = НСтр("ru='Пересчитать суммы в документе в валюту %1 ?'");
	ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстСообщения, Объект.Валюта);
	
	ОтветНаВопрос = Неопределено;

	
	ПоказатьВопрос(Новый ОписаниеОповещения("НеобходимоПересчитатьВалютуЗавершение", ЭтотОбъект, Новый Структура("Оповещение", Оповещение)), ТекстСообщения,РежимДиалогаВопрос.ДаНет);

КонецПроцедуры

&НаКлиенте
Процедура НеобходимоПересчитатьВалютуЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
    
    Оповещение = ДополнительныеПараметры.Оповещение;
    
    
    ОтветНаВопрос = РезультатВопроса;
    
    Если ОтветНаВопрос = КодВозвратаДиалога.Да Тогда
        ВыполнитьОбработкуОповещения(Оповещение, Истина);
        Возврат;
    Иначе
        ВыполнитьОбработкуОповещения(Оповещение, Ложь);
        Возврат;
    КонецЕсли;

КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьСтруктуруПересчетаСуммыНДСВСтрокеТЧ()

	СтруктураЗаполненияЦены = Новый Структура;
	СтруктураЗаполненияЦены.Вставить("ЦенаВключаетНДС", Ложь);
	
	Возврат СтруктураЗаполненияЦены;

КонецФункции
 
&НаКлиентеНаСервереБезКонтекста
Процедура НастроитьОтображениеРеквизитовСчетаФактуры(Форма, ПараметрыПредставления)
	
	Форма.ТекстСчетФактура = ПараметрыПредставления.ТекстСчетФактура;
	Форма.Элементы.ТекстСчетФактура.Гиперссылка = ПараметрыПредставления.ГиперссылкаСчетФактура;
	Форма.Элементы.СписокСчетовФактур.Заголовок = ПараметрыПредставления.ТекстСписок;
	Форма.Элементы.СписокСчетовФактур.Гиперссылка = ЗначениеЗаполнено(ПараметрыПредставления.ТекстСписок);
	Форма.Элементы.ВвестиИсправлениеСчетаФактуры.Видимость = ПараметрыПредставления.РазрешеныИсправления;
	
	Если ЗначениеЗаполнено(ПараметрыПредставления.ТекстСписок) Тогда
		Форма.Элементы.ГруппаСтраницыСчетФактура.ТекущаяСтраница = Форма.Элементы.ГруппаСписок;
	Иначе
		Форма.Элементы.ГруппаСтраницыСчетФактура.ТекущаяСтраница = Форма.Элементы.ГруппаСсылка;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьПредставлениеВидаОперации(Форма)
	
	ТекущийКод = Форма.СписокКодовВидовОпераций.НайтиПоЗначению(Форма.Объект.КодВидаОперации);
	Если ТекущийКод <> Неопределено Тогда
		Форма.ПредставлениеВидаОперации = Сред(ТекущийКод.Представление, 4);
	Иначе
		Форма.ПредставлениеВидаОперации = "";
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтаФорма, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти
