﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Подсистема "Управление доступом".

// Процедура ЗаполнитьНаборыЗначенийДоступа по свойствам объекта заполняет наборы значений доступа
// в таблице с полями:
//    НомерНабора     - Число                                     (необязательно, если набор один),
//    ВидДоступа      - ПланВидовХарактеристикСсылка.ВидыДоступа, (обязательно),
//    ЗначениеДоступа - Неопределено, СправочникСсылка или др.    (обязательно),
//    Чтение          - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Добавление      - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Изменение       - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//    Удаление        - Булево (необязательно, если набор для всех прав) устанавливается для одной строки набора,
//
//  Вызывается из процедуры УправлениеДоступомСлужебный.ЗаписатьНаборыЗначенийДоступа(),
// если объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьНаборыЗначенийДоступа" и
// из таких же процедур объектов, у которых наборы значений доступа зависят от наборов этого
// объекта (в этом случае объект зарегистрирован в "ПодпискаНаСобытие.ЗаписатьЗависимыеНаборыЗначенийДоступа").
//
// Параметры:
//  Таблица      - ТабличнаяЧасть,
//                 РегистрСведенийНаборЗаписей.НаборыЗначенийДоступа,
//                 ТаблицаЗначений, возвращаемая УправлениеДоступом.ТаблицаНаборыЗначенийДоступа().
//
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт
	
	ЗарплатаКадры.ЗаполнитьНаборыПоОрганизацииИФизическимЛицам(ЭтотОбъект, Таблица, "Организация", "ФизическиеЛица.ФизическоеЛицо");
	
КонецПроцедуры

// Подсистема "Управление доступом".

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ОбщегоНазначенияКлиентСервер.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("ГосударственнаяСлужба");
		Модуль.ОбработкаЗаполненияДокументаНачальнаяШтатнаяРасстановка(ЭтотОбъект, ДанныеЗаполнения, СтандартнаяОбработка);
	КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	КадровыйУчет.ПроверитьВозможностьПроведенияПоКадровомуУчету(
		ЭтотОбъект.Месяц, ЭтотОбъект.Сотрудники.ВыгрузитьКолонку("Сотрудник"), ЭтотОбъект.Ссылка, Отказ, Перечисления.ВидыКадровыхСобытий.Прием);
		
	ПроверитьУникальностьЗапрашиванияПоказателя(Отказ);
	
	Если ОбщегоНазначенияКлиентСервер.ПодсистемаСуществует("ЗарплатаКадрыПриложения.ГосударственнаяСлужба") Тогда
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("ГосударственнаяСлужба");
		Модуль.ПроверкаЗаполненияДокументаНачальнаяШтатнаяРасстановка(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	Иначе
		ОбщегоНазначенияКлиентСервер.УдалитьЗначениеИзМассива(ПроверяемыеРеквизиты, "ВидДоговора");
	КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	// Подготовка к регистрации перерасчетов
	ДанныеДляРегистрацииПерерасчетов = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТДанныеДокументов(ДанныеДляРегистрацииПерерасчетов);
	ЕстьПерерасчеты = ПерерасчетЗарплаты.СборДанныхДляРегистрацииПерерасчетов(Ссылка, ДанныеДляРегистрацииПерерасчетов, Организация, , , Истина);
	
	// Проведение документа
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	ДанныеДляПроведения = ДанныеДляПроведения();
	
	ЗарплатаКадрыРасширенный.УстановитьВремяРегистрацииДокумента(Движения, ДанныеДляПроведения.СотрудникиДаты, Ссылка);
	
	КадровыйУчет.СформироватьКадровыеДвижения(ЭтотОбъект, Движения, ДанныеДляПроведения.КадровыеДвижения);
	КадровыйУчетРасширенный.СформироватьИсториюИзмененияГрафиков(Движения, ДанныеДляПроведения.КадровыеДвижения);
		
	СтруктураПлановыхНачислений = Новый Структура;
	СтруктураПлановыхНачислений.Вставить("ДанныеОПлановыхНачислениях", ДанныеДляПроведения.ПлановыеНачисления);
	СтруктураПлановыхНачислений.Вставить("ЗначенияПоказателей", ДанныеДляПроведения.ЗначенияПоказателейНачислений);
	СтруктураПлановыхНачислений.Вставить("ПрименениеДополнительныхПоказателей", ДанныеДляПроведения.ПрименениеДополнительныхПоказателей);
	
	РасчетЗарплаты.СформироватьДвиженияПлановыхНачислений(ЭтотОбъект, Движения, СтруктураПлановыхНачислений);
	
	РасчетЗарплатыРасширенный.СформироватьДвиженияПорядкаПересчетаТарифныхСтавок(Движения, ДанныеДляПроведения.ПорядокПересчетаТарифнойСтавки);
	РасчетЗарплатыРасширенный.СформироватьДвиженияЗначенийСовокупныхТарифныхСтавок(Движения, ДанныеДляПроведения.ДанныеСовокупныхТарифныхСтавок);
	
	РазрядыКатегорииДолжностей.СформироватьДвиженияРазрядовКатегорийСотрудников(Движения, ДанныеДляПроведения.РазрядыКатегорииСотрудников);
	
	РасчетЗарплаты.СформироватьДвиженияПлановыхВыплат(Движения, ДанныеДляПроведения.КадровыеДвижения);
	
	ОстаткиОтпусков.СформироватьДвиженияПоложенныхЕжегодныхОтпусков(Движения, ДанныеДляПроведения.ЕжегодныеОтпуска);
	ОстаткиОтпусков.СформироватьДвиженияНачальныхОстатковОтпусков(Движения, ДанныеДляПроведения.ЕжегодныеОтпуска);
	
	СостоянияСотрудников.ЗарегистрироватьСостоянияСотрудников(Движения, Ссылка, ДанныеДляПроведения.ДанныеСостояний);
	
	УчетСтажаПФР.ЗарегистрироватьПериодыВУчетеСтажаПФР(Движения, ДанныеДляРегистрацииВУчетаСтажаПФР());
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыКорпоративнаяПодсистемы.Грейды") Тогда 
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("Грейды");
		Модуль.СформироватьДвиженияГрейдовСотрудников(Движения, ДанныеДляПроведения.ДанныеГрейдовСотрудников);
	КонецЕсли;
	
	// Регистрация перерасчетов
	Если ЕстьПерерасчеты Тогда
		ПерерасчетЗарплаты.РегистрацияПерерасчетов(Движения, ДанныеДляРегистрацииПерерасчетов, Организация, , Истина);
	КонецЕсли; 
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	// Подготовка к регистрации перерасчетов
	ДанныеДляРегистрацииПерерасчетов = Новый МенеджерВременныхТаблиц;
	
	СоздатьВТДанныеДокументов(ДанныеДляРегистрацииПерерасчетов);
	ЕстьПерерасчеты = ПерерасчетЗарплаты.СборДанныхДляРегистрацииПерерасчетов(Ссылка, ДанныеДляРегистрацииПерерасчетов, Организация);
	
	// Регистрация перерасчетов
	Если ЕстьПерерасчеты Тогда
		ПерерасчетЗарплаты.РегистрацияПерерасчетовПриОтменеПроведения(Ссылка, ДанныеДляРегистрацииПерерасчетов, Организация);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Функция ДанныеДляПроведения()
	
	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК ДатаСобытия,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаСотрудники.ДолжностьПоШтатномуРасписанию КАК Позиция,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Подразделение КАК Подразделение,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Должность КАК Должность,
	|	НачальнаяШтатнаяРасстановкаСотрудники.КоличествоСтавок КАК КоличествоСтавок,
	|	НачальнаяШтатнаяРасстановкаСотрудники.ГрафикРаботы КАК ГрафикРаботы,
	|	НачальнаяШтатнаяРасстановкаСотрудники.СпособРасчетаАванса КАК СпособРасчетаАванса,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Аванс КАК Аванс,
	|	НачальнаяШтатнаяРасстановкаСотрудники.ВидЗанятости КАК ВидЗанятости,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.ВидДоговора КАК ВидДоговора,
	|	ЗНАЧЕНИЕ(Перечисление.ВидыКадровыхСобытий.НачальныеДанные) КАК ВидСобытия,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник.ФизическоеЛицо КАК ФизическоеЛицо
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаНачисления.Ссылка.Месяц КАК ДатаСобытия,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо,
	|	НачальнаяШтатнаяРасстановкаНачисления.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаНачисления.Начисление,
	|	ИСТИНА КАК Используется,
	|	НачальнаяШтатнаяРасстановкаНачисления.Сотрудник.ФизическоеЛицо КАК ФизическоеЛицо,
	|	НачальнаяШтатнаяРасстановкаНачисления.Размер
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Начисления КАК НачальнаяШтатнаяРасстановкаНачисления
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаНачисления.Ссылка = &Ссылка
	|	И НачальнаяШтатнаяРасстановкаНачисления.Начисление ССЫЛКА ПланВидовРасчета.Начисления
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановка.Организация,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Сотрудник КАК Сотрудник,
	|	СправочникСотрудники.ФизическоеЛицо,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Показатель,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Значение
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Показатели КАК НачальнаяШтатнаяРасстановкаПоказатели
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.НачальнаяШтатнаяРасстановка КАК НачальнаяШтатнаяРасстановка
	|		ПО НачальнаяШтатнаяРасстановкаПоказатели.Ссылка = НачальнаяШтатнаяРасстановка.Ссылка
	|			И (НачальнаяШтатнаяРасстановкаПоказатели.Ссылка = &Ссылка)
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Сотрудники КАК СправочникСотрудники
	|		ПО НачальнаяШтатнаяРасстановкаПоказатели.Сотрудник = СправочникСотрудники.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.ДатаОстатка КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.Сотрудник,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.ВидЕжегодногоОтпуска,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.КоличествоДнейВГод,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.ДатаОстатка КАК ДатаОстатка,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.РабочийГодНачало,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.РабочийГодОкончание,
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.КоличествоДней
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.ЕжегодныеОтпуска КАК НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаЕжегодныеОтпуска.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник.ФизическоеЛицо КАК ФизическоеЛицо,
	|	НачальнаяШтатнаяРасстановкаСотрудники.ПорядокРасчетаСтоимостиЕдиницыВремени КАК ПорядокРасчета,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник.ФизическоеЛицо КАК ФизическоеЛицо,
	|	НачальнаяШтатнаяРасстановкаСотрудники.СовокупнаяТарифнаяСтавка КАК Значение,
	|	ВЫБОР
	|		КОГДА НачальнаяШтатнаяРасстановкаСотрудники.СовокупнаяТарифнаяСтавка = 0
	|			ТОГДА ЗНАЧЕНИЕ(Перечисление.ВидыТарифныхСтавок.ПустаяСсылка)
	|		ИНАЧЕ НачальнаяШтатнаяРасстановкаСотрудники.ВидТарифнойСтавки
	|	КОНЕЦ КАК ВидТарифнойСтавки,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаСотрудники.РазрядКатегория КАК РазрядКатегория,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник,
	|	ЗНАЧЕНИЕ(Перечисление.СостоянияСотрудника.Работа) КАК Состояние,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК Начало,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК Окончание
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаСотрудники.Сотрудник КАК Сотрудник
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК НачальнаяШтатнаяРасстановкаСотрудники
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаСотрудники.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	НачальнаяШтатнаяРасстановкаПоказатели.Ссылка.Месяц КАК ДатаСобытия,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Ссылка.Организация КАК Организация,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Сотрудник КАК Сотрудник,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Сотрудник.ФизическоеЛицо КАК ФизическоеЛицо,
	|	НачальнаяШтатнаяРасстановкаПоказатели.Показатель КАК Показатель,
	|	ИСТИНА КАК Применение,
	|	ДАТАВРЕМЯ(1, 1, 1) КАК ДействуетДо
	|ИЗ
	|	Документ.НачальнаяШтатнаяРасстановка.Показатели КАК НачальнаяШтатнаяРасстановкаПоказатели
	|ГДЕ
	|	НачальнаяШтатнаяРасстановкаПоказатели.ИдентификаторСтрокиВидаРасчета = 0
	|	И НачальнаяШтатнаяРасстановкаПоказатели.Ссылка = &Ссылка";
	
	РезультатыЗапроса = Запрос.ВыполнитьПакет();
	
	ДанныеДляПроведения = Новый Структура; 
	
	// Первый набор данных для проведения - таблица для формирования кадровых движений, истории графиков, авансов.
	КадровыеДвижения = РезультатыЗапроса[0].Выгрузить();
	ДанныеДляПроведения.Вставить("КадровыеДвижения", КадровыеДвижения);
	
	// Второй набор данных для проведения - таблица для формирования плановых начислений.
	ПлановыеНачисления = РезультатыЗапроса[1].Выгрузить();
	ДанныеДляПроведения.Вставить("ПлановыеНачисления", ПлановыеНачисления);
	
	// Третий набор данных для проведения - таблица для формирования значений показателей начислений.
	ЗначенияПоказателей = РезультатыЗапроса[2].Выгрузить();
	ДанныеДляПроведения.Вставить("ЗначенияПоказателейНачислений", ЗначенияПоказателей);
	
	// Четвертый набор данных для проведения - таблица для формирования положенных видов ежегодных отпусков.
	ПоложенныеЕжегодныеОтпуска = РезультатыЗапроса[3].Выгрузить();
	ДанныеДляПроведения.Вставить("ЕжегодныеОтпуска", ПоложенныеЕжегодныеОтпуска);
	
	// Пятый набор данных для проведения - таблица для формирования значений порядка пересчета тарифной ставки.
	ПорядокПересчетаТарифнойСтавки = РезультатыЗапроса[4].Выгрузить();
	ДанныеДляПроведения.Вставить("ПорядокПересчетаТарифнойСтавки", ПорядокПересчетаТарифнойСтавки);
	
	// Шестой набор данных для проведения - таблица для формирования значений совокупной тарифной ставки.
	ДанныеСовокупныхТарифныхСтавок = РезультатыЗапроса[5].Выгрузить();
	ДанныеДляПроведения.Вставить("ДанныеСовокупныхТарифныхСтавок", ДанныеСовокупныхТарифныхСтавок);
	
	// Седьмой набор данных для проведения - таблица для формирования значений разряда сотрудника.
	РазрядыКатегорииСотрудников = РезультатыЗапроса[6].Выгрузить();
	ДанныеДляПроведения.Вставить("РазрядыКатегорииСотрудников", РазрядыКатегорииСотрудников);
	
	// Данные состояний
	ДанныеСостояний = РезультатыЗапроса[7].Выгрузить();
	ДанныеДляПроведения.Вставить("ДанныеСостояний", ДанныеСостояний);
	
	// Девятый набор данных для проведения - таблица для формирования времени регистрации документа.
	СотрудникиДаты = РезультатыЗапроса[8].Выгрузить();
	ДанныеДляПроведения.Вставить("СотрудникиДаты", СотрудникиДаты);
	
	// Десятый набор данных для проведения - таблица для формирования применения дополнительных показателей.
	ПрименениеДополнительныхПоказателей = РезультатыЗапроса[9].Выгрузить();
	ДанныеДляПроведения.Вставить("ПрименениеДополнительныхПоказателей", ПрименениеДополнительныхПоказателей);
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ЗарплатаКадрыКорпоративнаяПодсистемы.Грейды") Тогда 
		Модуль = ОбщегоНазначенияКлиентСервер.ОбщийМодуль("Грейды");
		ДанныеГрейдовСотрудников = Модуль.ДанныеДляПроведенияНачальнойШтатнойРасстановкиГрейдыСотрудников(Ссылка);
		ДанныеДляПроведения.Вставить("ДанныеГрейдовСотрудников", ДанныеГрейдовСотрудников);
	КонецЕсли;
	
	Возврат ДанныеДляПроведения;
	
КонецФункции

Процедура ПроверитьУникальностьЗапрашиванияПоказателя(Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("ДатаСобытия", Месяц);
	Запрос.УстановитьПараметр("ТаблицаНачислений", Начисления);
	Запрос.УстановитьПараметр("ТаблицаПоказателей", Показатели);
	Запрос.УстановитьПараметр("ДополнительныеПоказатели", РасчетЗарплатыРасширенный.ПустаяТаблицаДополнительныхПоказателей());
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ТаблицаВидовРасчета.Сотрудник КАК Сотрудник,
	               |	ТаблицаВидовРасчета.Начисление КАК Начисление,
	               |	ЗНАЧЕНИЕ(Перечисление.ДействияСНачислениямиИУдержаниями.Утвердить) КАК Действие,
	               |	ТаблицаВидовРасчета.ИдентификаторСтрокиВидаРасчета КАК ИдентификаторСтрокиВидаРасчета
	               |ПОМЕСТИТЬ ВТВидыРасчета
	               |ИЗ
	               |	&ТаблицаНачислений КАК ТаблицаВидовРасчета
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ТаблицаПоказателей.Показатель КАК Показатель,
	               |	ТаблицаПоказателей.Значение КАК ЗначениеПоказателя,
	               |	ТаблицаПоказателей.ИдентификаторСтрокиВидаРасчета КАК ИдентификаторСтрокиВидаРасчета
	               |ПОМЕСТИТЬ ВТПоказатели
	               |ИЗ
	               |	&ТаблицаПоказателей КАК ТаблицаПоказателей
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	&ДатаСобытия КАК Период,
	               |	ВидыРасчета.Сотрудник КАК Сотрудник,
	               |	ВидыРасчета.Начисление КАК Начисление,
	               |	ВидыРасчета.Действие КАК Действие,
	               |	ЕСТЬNULL(Показатели.Показатель, ЗНАЧЕНИЕ(Справочник.ПоказателиРасчетаЗарплаты.ПустаяСсылка)) КАК Показатель,
	               |	ЕСТЬNULL(Показатели.ЗначениеПоказателя, 0) КАК ЗначениеПоказателя
	               |ПОМЕСТИТЬ ВТПоказателиВидовРасчета
	               |ИЗ
	               |	ВТВидыРасчета КАК ВидыРасчета
	               |		ЛЕВОЕ СОЕДИНЕНИЕ ВТПоказатели КАК Показатели
	               |		ПО ВидыРасчета.ИдентификаторСтрокиВидаРасчета = Показатели.ИдентификаторСтрокиВидаРасчета
	               |;
	               |
	               |////////////////////////////////////////////////////////////////////////////////
	               |ВЫБРАТЬ
	               |	ДополнительныеПоказатели.Период КАК Период,
	               |	ДополнительныеПоказатели.Сотрудник КАК Сотрудник,
	               |	ДополнительныеПоказатели.Показатель КАК Показатель,
	               |	ДополнительныеПоказатели.ЗначениеПоказателя КАК ЗначениеПоказателя,
	               |	ДополнительныеПоказатели.Действие КАК Действие
	               |ПОМЕСТИТЬ ВТДополнительныеПоказатели
	               |ИЗ
	               |	&ДополнительныеПоказатели КАК ДополнительныеПоказатели";
				   
	Запрос.Выполнить();			   
	
	РасчетЗарплатыРасширенный.ПроверитьУникальностьЗапрашиванияПоказателя(Запрос.МенеджерВременныхТаблиц, Ссылка, Отказ);

КонецПроцедуры

Функция ДанныеДляРегистрацииВУчетаСтажаПФР()
	МассивСсылок = Новый Массив;
	МассивСсылок.Добавить(Ссылка);
	
	ДанныеДляРегистрации = Документы.НачальнаяШтатнаяРасстановка.ДанныеДляРегистрацииВУчетаСтажаПФР(МассивСсылок);
	
	Возврат ДанныеДляРегистрации[Ссылка];
	
КонецФункции	

Процедура СоздатьВТДанныеДокументов(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Регистратор", Ссылка);
	
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ТаблицаДокумента.Ссылка.Организация КАК Организация,
		|	ТаблицаДокумента.Сотрудник,
		|	НАЧАЛОПЕРИОДА(ТаблицаДокумента.Ссылка.Месяц, МЕСЯЦ) КАК ПериодДействия,
		|	ТаблицаДокумента.Ссылка КАК ДокументОснование
		|ПОМЕСТИТЬ ВТДанныеДокументов
		|ИЗ
		|	Документ.НачальнаяШтатнаяРасстановка.Сотрудники КАК ТаблицаДокумента
		|ГДЕ
		|	ТаблицаДокумента.Ссылка = &Регистратор";
		
	Запрос.Выполнить();
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
