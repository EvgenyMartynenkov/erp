﻿
#Область ОбработчикиСобытийЭлементовТабличнойЧастиОС

&НаКлиенте
Процедура ОСОсновноеСредствоПриИзменении(Элемент)
	
	ТекущиеДанные = Элементы.ОС.ТекущиеДанные;
	ТекущиеДанные.ИнвентарныйНомер = КодОбъектаЭксплуатации(ТекущиеДанные.ОсновноеСредство);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервереБезКонтекста
Функция КодОбъектаЭксплуатации(Ссылка)
	
	Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка, "Код");
	
КонецФункции

#КонецОбласти