﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") 
		 И ДанныеЗаполнения.Свойство("ПериодРегистрации") Тогда
		Дата = ДанныеЗаполнения.ПериодРегистрации;
	КонецЕсли;
	
	ЗаполнитьРеквизитыПоУмолчанию();
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ВыручкаНДС0 = 0;
	ДокументыЭкспорт.Очистить();
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)

	ВыполнитьПроверкуАктуальностиПартионногоУчета(Отказ);
	
	ПроверитьДублиДокументов(Отказ);
	
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если СписатьНДСКакЦенности Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовНеНДС");
		МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовЕНВД");
	Иначе
		Если ВыручкаЕНВД = 0 Тогда
			МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовЕНВД");
		КонецЕсли;
		Если ВыручкаНеНДС = 0 Тогда
			МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходовНеНДС");
		КонецЕсли;
	КонецЕсли;
	
	ПланыВидовХарактеристик.СтатьиРасходов.ПроверитьЗаполнениеАналитик(
		ЭтотОбъект,
		"СтатьяРасходовНеНДС, АналитикаРасходовНеНДС, СтатьяРасходовЕНВД, АналитикаРасходовЕНВД",
		МассивНепроверяемыхРеквизитов,
		Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	Если СписатьНДСКакЦенности Тогда
		Если СтатьяРасходовНеНДС <> ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка() Тогда
			СтатьяРасходовНеНДС		= ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
			АналитикаРасходовНеНДС	= Неопределено;
		КонецЕсли;
		Если СтатьяРасходовЕНВД <> ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка() Тогда
			СтатьяРасходовЕНВД		= ПланыВидовХарактеристик.СтатьиРасходов.ПустаяСсылка();
			АналитикаРасходовЕНВД	= Неопределено;
		КонецЕсли;
	КонецЕсли;
	
	Если Не БазаРаспределенияУстанавливаетсяВручную Тогда
		
		БазаРаспределения = Документы.РаспределениеНДС.ПолучитьБазуРаспределения(Дата, Организация);
		
		ВыручкаНДС   = БазаРаспределения.ТаблицаВыручка.Итог("ВыручкаНДС");
		ВыручкаНеНДС = БазаРаспределения.ТаблицаВыручка.Итог("ВыручкаНеНДС");
		ВыручкаЕНВД  = БазаРаспределения.ТаблицаВыручка.Итог("ВыручкаЕНВД");
		ВыручкаНДС0  = БазаРаспределения.ТаблицаДокументыЭкспорт.Итог("Сумма");
		
		ДокументыЭкспорт.Загрузить(БазаРаспределения.ТаблицаДокументыЭкспорт);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	// Инициализация дополнительных свойств для проведения документа
	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	
	// Инициализация данных документа
	Документы.РаспределениеНДС.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	
	// Подготовка наборов записей
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	// Движения по НДС
	ДоходыИРасходыСервер.ОтразитьНДСЗаписиКнигиПродаж(ДополнительныеСвойства, Движения, Отказ);
	ДоходыИРасходыСервер.ОтразитьНДСЗаписиКнигиПокупок(ДополнительныеСвойства, Движения, Отказ);
	ДоходыИРасходыСервер.ОтразитьПартииПрочихРасходов(ДополнительныеСвойства, Движения, Отказ);
	
	//{УП}
	// Движения по параметрам амортизации ОС
	ПроведениеСервер.ОтразитьДвижения(
		ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПараметрыАмортизацииОСБухгалтерскийУчет,
		Движения.ПараметрыАмортизацииОСБухгалтерскийУчет,
		Отказ);
	// Движения по первоначальным сведениям НМА
	ПроведениеСервер.ОтразитьДвижения(
		ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПервоначальныеСведенияНМАБухгалтерскийУчет,
		Движения.ПервоначальныеСведенияНМАБухгалтерскийУчет,
		Отказ);
	// Движения по первоначальным сведениям ОС
	ПроведениеСервер.ОтразитьДвижения(
		ДополнительныеСвойства.ТаблицыДляДвижений.ТаблицаПервоначальныеСведенияОСБухгалтерскийУчет,
		Движения.ПервоначальныеСведенияОСБухгалтерскийУчет,
		Отказ);
	// Движения по прочим расходвам в регл учете
	ДоходыИРасходыСервер.ОтразитьПрочиеРасходы(ДополнительныеСвойства, Движения, Отказ);
	//{/УП}
	
	// Запись наборов записей
	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	// Инициализация дополнительных свойств для проведения документа
	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);
	
	// Подготовка наборов записей
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);
	
	// Запись наборов записей
	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

#Область ЗаполнениеДокумента

Процедура ЗаполнитьРеквизитыПоУмолчанию()

	Ответственный	= Пользователи.ТекущийПользователь();
	Организация		= ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	
	ПравилаСписания = Документы.РаспределениеНДС.ЗаполнитьПравилаСписанияНДС(Организация);
	ЗаполнитьЗначенияСвойств(ЭтотОбъект,ПравилаСписания);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ПроверитьДублиДокументов (Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	РаспределениеНДС.Ссылка КАК Ссылка
		|ИЗ
		|	Документ.РаспределениеНДС КАК РаспределениеНДС
		|ГДЕ
		|	РаспределениеНДС.Проведен
		|	И РаспределениеНДС.Организация = &Организация
		|	И РаспределениеНДС.Дата МЕЖДУ &ПериодНачало И &ПериодОкончание
		|	И НЕ РаспределениеНДС.Ссылка = &ТекущийДокумент");
	
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ПериодНачало", НачалоКвартала(Дата));
	Запрос.УстановитьПараметр("ПериодОкончание", КонецКвартала(Дата));
	//{УП}
	Запрос.УстановитьПараметр("ПериодНачало", НачалоМесяца(Дата));
	Запрос.УстановитьПараметр("ПериодОкончание", КонецМесяца(Дата));
	//{/УП}
	Запрос.УстановитьПараметр("ТекущийДокумент", Ссылка);
	
	Результат = Запрос.Выполнить();
	Если Не Результат.Пустой() Тогда
		
		ПредставлениеПериода = ПредставлениеПериода(НачалоКвартала(Дата), КонецКвартала(Дата), "ДЛФ=D");
		
		//{УП}
		Если Месяц(Дата)%3=0 Тогда
			ПредставлениеПериода = ПредставлениеПериода(НачалоМесяца(Дата), КонецМесяца(Дата), "ДЛФ=D");
		КонецЕсли;
		//{/УП}
		
		ШаблонТекста = НСтр("ru = 'В налоговом периоде ""%1"" уже есть проведенный документ ""Распределение НДС"" для организации %2'");
		
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ШаблонТекста, ПредставлениеПериода, Организация);
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки, ЭтотОбъект, "Дата", , Отказ);
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ВыполнитьПроверкуАктуальностиПартионногоУчета(Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	ДатаОбработки = КонецКвартала(Дата);
	//{УП}
	ДатаОбработки = КонецМесяца(Дата);
	//{/УП}
	
	ШаблонТекста = НСтр("ru = 'На месяц %1 нарушена последовательность партионного учета. Для выполнения расчета себестоимости необходимо произвести восстановление'");
	
	Если ПартионныйУчет.НачалоПериодаРасчета(ДатаОбработки) < ДатаОбработки Тогда
		
		ТекстОшибки = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				ШаблонТекста,
				Формат(ДатаОбработки, "ДФ='ММММ гггг'") + " г.");
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				ТекстОшибки,
				ЭтотОбъект,
				"Дата",
				, // ПутьКДанным 
				Отказ);
				
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
