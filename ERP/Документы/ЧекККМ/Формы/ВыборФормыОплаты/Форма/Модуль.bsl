﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	ИспользоватьОплатуНаличными                    = Параметры.Параметр.ИспользоватьОплатуНаличными.Значение;
	Элементы.ИспользоватьОплатуНаличными.Видимость = Параметры.Параметр.ИспользоватьОплатуНаличными.Видимость;
	
	ИспользоватьОплатуПлатежнойКартой                    = Параметры.Параметр.ИспользоватьОплатуПлатежнымиКартами.Значение;
	Элементы.ИспользоватьОплатуПлатежнойКартой.Видимость = Параметры.Параметр.ИспользоватьОплатуПлатежнымиКартами.Видимость;
	
	КартаЛояльности = Параметры.Параметр.ИспользоватьОплатуБонуснымиБаллами.Параметр;
	
	Если ЗначениеЗаполнено(КартаЛояльности) И ПолучитьФункциональнуюОпцию("ИспользоватьБонусныеПрограммыЛояльности") Тогда
		
		ПараметрыКартыЛояльности = БонусныеБаллыСервер.БонуснаяПрограммаКартыЛояльности(КартаЛояльности);
		Если ЗначениеЗаполнено(ПараметрыКартыЛояльности.БонуснаяПрограммаЛояльности) Тогда
			
			ОстаткиБонусныхБаллов = БонусныеБаллыСервер.ОстаткиИДвиженияБонусныхБаллов(ПараметрыКартыЛояльности.БонуснаяПрограммаЛояльности, ПараметрыКартыЛояльности.Партнер);
			
			Остаток = ОстаткиБонусныхБаллов[0].Сумма;
			ОстатокБонусныхБаллов = НСтр("ru = 'Остаток:'") + " " +  СтроковыеФункцииКлиентСервер.ЧислоЦифрамиПредметИсчисленияПрописью(Остаток, НСтр("ru = 'балл, балла, баллов'"));
			
			ИспользоватьОплатуБонуснымиБаллами                   = Параметры.Параметр.ИспользоватьОплатуБонуснымиБаллами.Значение И Остаток > 0;
			Элементы.ГруппаОплатаБонуснымиБаллами.Видимость      = Параметры.Параметр.ИспользоватьОплатуБонуснымиБаллами.Видимость;
			Элементы.ГруппаОплатаБонуснымиБаллами.Доступность    = Остаток > 0;
			Элементы.ДекорацияНачислениеНеПроизводится.Видимость = ПараметрыКартыЛояльности.НеНачислятьБаллыПриОплатеБонусами;
			
		Иначе
			ИспользоватьОплатуБонуснымиБаллами              = Ложь;
			Элементы.ГруппаОплатаБонуснымиБаллами.Видимость = Ложь;
		КонецЕсли;
		
	Иначе
		ИспользоватьОплатуБонуснымиБаллами                   = Ложь;
		Элементы.ГруппаОплатаБонуснымиБаллами.Видимость      = Ложь;
		Элементы.ДекорацияНачислениеНеПроизводится.Видимость = Ложь;
	КонецЕсли;
	
	ИспользоватьОплатуПодарочнымиСертификатами                    = Параметры.Параметр.ИспользоватьОплатуПодарочнымиСертификатами.Значение;
	Элементы.ИспользоватьОплатуПодарочнымиСертификатами.Видимость = Параметры.Параметр.ИспользоватьОплатуПодарочнымиСертификатами.Видимость;

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Выбрать(Команда)
	
	Оплата = Новый Структура;
	Оплата.Вставить("ИспользоватьОплатуНаличными",                ИспользоватьОплатуНаличными);
	Оплата.Вставить("ИспользоватьОплатуБонуснымиБаллами",         ИспользоватьОплатуБонуснымиБаллами);
	Оплата.Вставить("ИспользоватьОплатуПлатежнойКартой",          ИспользоватьОплатуПлатежнойКартой);
	Оплата.Вставить("ИспользоватьОплатуПодарочнымиСертификатами", ИспользоватьОплатуПодарочнымиСертификатами);
	
	Закрыть(Оплата);
	
КонецПроцедуры

&НаКлиенте
Процедура Отмена(Команда)
	
	Закрыть(Неопределено);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

