﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	Если Параметры.Свойство("ОтборПоСпискуЗаказов") Тогда
		ОтборПоСпискуЗаказов(Параметры.ОтборПоСпискуЗаказов);
	КонецЕсли; 
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура ОтборПоСпискуЗаказов(СписокЗаказов)

	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	РаспоряжениеМатериалы.Ссылка
	|ИЗ
	|	Документ.РаспоряжениеНаПередачуМатериаловВПроизводство.Материалы КАК РаспоряжениеМатериалы
	|ГДЕ
	|	РаспоряжениеМатериалы.Распоряжение В(&СписокЗаказов)";
	
	Запрос.УстановитьПараметр("СписокЗаказов", СписокЗаказов);
	
	СписокДокументов = Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Ссылка");
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Ссылка", СписокДокументов);

	Заголовок = НСтр("ru = 'Распоряжения на передачу материалов (установлен отбор по заказам)'");
	АвтоЗаголовок = Ложь;
	
КонецПроцедуры

#КонецОбласти
