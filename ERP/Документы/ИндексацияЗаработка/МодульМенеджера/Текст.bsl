﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	Если ПолучитьФункциональнуюОпцию("ИспользоватьИндексациюЗаработка") Тогда
		// Приказ об индексации заработка.
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.МенеджерПечати = "Обработка.ПечатьКадровыхПриказовРасширенная";
		КомандаПечати.Идентификатор = "ПФ_MXL_ИндексацияЗаработка";
		КомандаПечати.Представление = НСтр("ru = 'Приказ об индексации заработка'");
	КонецЕсли;	
КонецПроцедуры

#КонецОбласти

#Область ЗаполнениеДокумента

Функция ИндексированныеЗначенияПоказателейСотрудников(ТекущиеПоказателиСотрудников, 
													КоэффициентИндексации, 
													ПоказателиСотрудниковВДокументе = Неопределено, 
													СпособыОкругленияПоказателей = Неопределено) Экспорт
	
	ЗначенияПоказателейСотрудников = Новый ТаблицаЗначений;
	ЗначенияПоказателейСотрудников.Колонки.Добавить("Сотрудник", Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
	ЗначенияПоказателейСотрудников.Колонки.Добавить("Показатель", Новый ОписаниеТипов("СправочникСсылка.ПоказателиРасчетаЗарплаты"));
	ЗначенияПоказателейСотрудников.Колонки.Добавить("Значение", Новый ОписаниеТипов("Число"));
	
	КоэффициентыИндексацииСотрудников = Новый ТаблицаЗначений;
	КоэффициентыИндексацииСотрудников.Колонки.Добавить("Сотрудник", Новый ОписаниеТипов("СправочникСсылка.Сотрудники"));
	КоэффициентыИндексацииСотрудников.Колонки.Добавить("КоэффициентИндексации", Новый ОписаниеТипов("Число"));
	КоэффициентыИндексацииСотрудников.Колонки.Добавить("ФиксСтрока", Новый ОписаниеТипов("Булево"));
	
	Если СпособыОкругленияПоказателей = Неопределено Тогда
		СпособыОкругленияПоказателей = Новый ТаблицаЗначений;
		СпособыОкругленияПоказателей.Колонки.Добавить("Показатель");
		СпособыОкругленияПоказателей.Колонки.Добавить("СпособОкругления");
	КонецЕсли;
	
	ОписаниеОкругленияПоказателей = ИндексацияЗаработка.ОписаниеОкругленияПоказателей(СпособыОкругленияПоказателей);	
	
	Для каждого ПоказательСотрудника Из ТекущиеПоказателиСотрудников Цикл 
		
		ОписаниеОкругленияПоказателя = ОписаниеОкругленияПоказателей.Получить(ПоказательСотрудника.Показатель);
		ТочностьОкругления 	= ОписаниеОкругленияПоказателя.ТочностьОкругления;
		ПравилоОкругления 	= ОписаниеОкругленияПоказателя.ПравилоОкругления;
		
		НоваяСтрока = ЗначенияПоказателейСотрудников.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, ПоказательСотрудника);
		
		ИндексированноеЗначение = ИндексацияЗаработка.ИндексированноеЗначениеПоказателя(НоваяСтрока.Значение, КоэффициентИндексации, ОписаниеОкругленияПоказателя);
		
		Если НоваяСтрока.Значение <> 0 Тогда
			КоэффициентИндексацииСотрудника  = ИндексированноеЗначение / НоваяСтрока.Значение;
			НоваяСтрока.Значение = ИндексированноеЗначение;
		Иначе
			КоэффициентИндексацииСотрудника  = КоэффициентИндексации;
		КонецЕсли;
		
		Если ПоказательСотрудника.ПоказательТарифнойСтавки = ПоказательСотрудника.Показатель Тогда
			НоваяСтрока = КоэффициентыИндексацииСотрудников.Добавить();
			НоваяСтрока.Сотрудник = ПоказательСотрудника.Сотрудник;
			НоваяСтрока.КоэффициентИндексации = КоэффициентИндексацииСотрудника;
		КонецЕсли;
	КонецЦикла; 
	
	// Сохраним данные которые пользователь мог отредактировать в документе.
	Если НЕ ПоказателиСотрудниковВДокументе = Неопределено Тогда
		ОтборПоказателя = Новый Структура("Сотрудник,Показатель");
		Для каждого ПоказательСотрудникаВДокументе Из ПоказателиСотрудниковВДокументе Цикл
				
			ОтборПоказателя.Вставить("Сотрудник", 	ПоказательСотрудникаВДокументе.Сотрудник);  
			ОтборПоказателя.Вставить("Показатель", 	ПоказательСотрудникаВДокументе.Показатель);  
			
			ЗначенияПоказателейСотрудника = ЗначенияПоказателейСотрудников.НайтиСтроки(ОтборПоказателя);
			
			Если ЗначенияПоказателейСотрудника.Количество() <> 0 Тогда
				ЗначенияПоказателейСотрудника[0].Значение = ПоказательСотрудникаВДокументе.Значение;
			КонецЕсли;
		КонецЦикла;		
	КонецЕсли;
	
	Возврат Новый Структура("ЗначенияПоказателейСотрудников,КоэффициентыИндексацииСотрудников", ЗначенияПоказателейСотрудников, КоэффициентыИндексацииСотрудников);	
	
КонецФункции

Процедура РассчитатьФОТ(Знач Ссылка, Знач Организация, Знач ДатаИндексации, НачисленияСотрудников, Знач ЗначенияПоказателей) Экспорт
	
	// Подготовка к расчету ФОТ
	РассчитываемыеОбъекты = Новый Соответствие;
	
	Сотрудники = Новый Соответствие;
	
	Для каждого СтрокаЗначенияПоказателей Из ЗначенияПоказателей Цикл
		
		ОписаниеСотрудника = Сотрудники.Получить(СтрокаЗначенияПоказателей.Сотрудник);
		Если ОписаниеСотрудника = Неопределено Тогда
			ОписаниеСотрудника = Новый Структура;
			ОписаниеСотрудника.Вставить("Организация", Организация);
			ОписаниеСотрудника.Вставить("ДатаРасчета", ДатаИндексации);
			ОписаниеСотрудника.Вставить("Начисления", Новый Соответствие);
			ОписаниеСотрудника.Вставить("Показатели", Новый Соответствие);
		КонецЕсли;
		
		СтрокиНачислений = НачисленияСотрудников.НайтиСтроки(Новый Структура("Сотрудник", СтрокаЗначенияПоказателей.Сотрудник));
		Для каждого СтрокаНачисления Из СтрокиНачислений Цикл
			ОписаниеСотрудника.Начисления.Вставить(СтрокаНачисления.Начисление, 0);
		КонецЦикла;
		
		ОписаниеСотрудника.Показатели.Вставить(СтрокаЗначенияПоказателей.Показатель, СтрокаЗначенияПоказателей.Значение);
		
		Сотрудники.Вставить(СтрокаЗначенияПоказателей.Сотрудник, ОписаниеСотрудника);
		
	КонецЦикла;
	
	РассчитываемыеОбъекты.Вставить(Ссылка, Сотрудники);
	
	// Расчет ФОТ
	РасчетЗарплатыРасширенный.РассчитатьФОТСотрудников(РассчитываемыеОбъекты, Организация, ДатаИндексации);
	
	// Заполнение документа результатами расчета.
	ОписаниеОбъекта = РассчитываемыеОбъекты.Получить(Ссылка);
	
	Для каждого НачислениеСотрудника Из НачисленияСотрудников Цикл
		
		ОписаниеСотрудника = ОписаниеОбъекта.Получить(НачислениеСотрудника.Сотрудник);
		Если ОписаниеСотрудника <> Неопределено Тогда
			
			РассчитанноеНачисление = ОписаниеСотрудника.Начисления.Получить(НачислениеСотрудника.Начисление);
			Если РассчитанноеНачисление <> Неопределено Тогда
				НачислениеСотрудника.Размер = РассчитанноеНачисление;
			КонецЕсли; 
			
		КонецЕсли; 

	КонецЦикла;
	
КонецПроцедуры

Функция СотрудникиДляИндексации(Организация, Подразделение, ДатаИндексации) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Параметры = КадровыйУчет.ПараметрыПолученияСотрудниковОрганизацийПоСпискуФизическихЛиц();
	Параметры.Организация 		= Организация;
	Если ЗначениеЗаполнено(Подразделение) Тогда
		Параметры.Подразделение 	= Подразделение;
	КонецЕсли;
	Параметры.НачалоПериода 	= ДатаИндексации;
	Параметры.ОкончаниеПериода 	= ДатаИндексации;
	
	Возврат КадровыйУчет.СотрудникиОрганизации(Истина, Параметры).ВыгрузитьКолонку("Сотрудник");
	                                                              
КонецФункции

Функция НачисленияСотрудников(Ссылка, ДатаИндексации, СписокСотрудников) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Период", ДатаИндексации);
	Запрос.УстановитьПараметр("СписокСотрудников", СписокСотрудников);
	
	Запрос.Текст =
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	|	Сотрудники.Ссылка КАК Сотрудник,
	|	&Период
	|ПОМЕСТИТЬ ВТИзмеренияДаты
	|ИЗ
	|	Справочник.Сотрудники КАК Сотрудники
	|ГДЕ
	|	Сотрудники.Ссылка В(&СписокСотрудников)";
	
	Запрос.Выполнить();
	
	ПараметрыПостроения = ЗарплатаКадрыОбщиеНаборыДанных.ПараметрыПостроенияДляСоздатьВТИмяРегистраСрез();
	ПараметрыПостроения.ФормироватьСПериодичностьДень = Ложь;
	
	ЗарплатаКадрыОбщиеНаборыДанных.ДобавитьВКоллекциюОтбор(ПараметрыПостроения.Отборы, "Регистратор", "<>", Ссылка);
	
	ЗарплатаКадрыОбщиеНаборыДанных.СоздатьВТИмяРегистраСрезПоследних("ПлановыеНачисления",
																	Запрос.МенеджерВременныхТаблиц,
																	Истина,
																	ЗарплатаКадрыОбщиеНаборыДанных.ОписаниеФильтраДляСоздатьВТИмяРегистра("ВТИзмеренияДаты", "Сотрудник"),
																	ПараметрыПостроения);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ПлановыеНачисления.Сотрудник,
	|	ПлановыеНачисления.Начисление,
	|	ПлановыеНачисления.Размер,
	|	ВЫРАЗИТЬ(ПлановыеНачисления.Начисление КАК ПланВидовРасчета.Начисления).РеквизитДопУпорядочивания КАК Порядок
	|ИЗ
	|	ВТПлановыеНачисленияСрезПоследних КАК ПлановыеНачисления
	|ГДЕ
	|	НЕ ВЫРАЗИТЬ(ПлановыеНачисления.Начисление КАК ПланВидовРасчета.Начисления).КатегорияНачисленияИлиНеоплаченногоВремени В (ЗНАЧЕНИЕ(Перечисление.КатегорииНачисленийИНеоплаченногоВремени.ПособиеПоУходуЗаРебенкомДоПолутораЛет), ЗНАЧЕНИЕ(Перечисление.КатегорииНачисленийИНеоплаченногоВремени.ПособиеПоУходуЗаРебенкомДоТрехЛет))
	|	И ПлановыеНачисления.Используется
	|
	|УПОРЯДОЧИТЬ ПО
	|	Порядок";
	
	НачисленияСотрудников = Запрос.Выполнить().Выгрузить();
	
	Возврат НачисленияСотрудников;
	
КонецФункции 

Функция ТекущиеПоказателиСотрудников(Ссылка, МассивПоказателей, ДатаИндексации, МассивСотрудников) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	КадровыйУчет.СоздатьНаДатуВТКадровыеДанныеСотрудников(Запрос.МенеджерВременныхТаблиц, Истина, МассивСотрудников, "ПоказательТарифнойСтавки", ДатаИндексации);
	                                                              
	ТекстЗапроса = 
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	Сотрудники.Период КАК Период,
	|	Сотрудники.Сотрудник КАК Сотрудник
	|ИЗ
	|	ВТКадровыеДанныеСотрудников КАК Сотрудники";
	Запрос.Текст = ТекстЗапроса;
	СотрудникиДаты = Запрос.Выполнить().Выгрузить();
	
	// Получаем текущие начисления и показатели по сотрудникам.
	ПараметрыПостроения = ЗарплатаКадрыОбщиеНаборыДанных.ПараметрыПостроенияДляСоздатьВТИмяРегистраСрез();
	ПараметрыПостроения.ФормироватьСПериодичностьДень = Ложь;
	
	ЗарплатаКадрыОбщиеНаборыДанных.ДобавитьВКоллекциюОтбор(ПараметрыПостроения.Отборы, "Регистратор", "<>", Ссылка);

	ЗарплатаКадрыОбщиеНаборыДанных.СоздатьВТИмяРегистраСрезПоследних(
		"ПлановыеНачисления",
		Запрос.МенеджерВременныхТаблиц,
		Истина,
		ЗарплатаКадрыОбщиеНаборыДанных.ОписаниеФильтраДляСоздатьВТИмяРегистра(СотрудникиДаты),
		ПараметрыПостроения);
	
	ЗарплатаКадрыОбщиеНаборыДанных.ДобавитьВКоллекциюОтбор(ПараметрыПостроения.Отборы, "Показатель", "В", МассивПоказателей);
	
	ЗарплатаКадрыОбщиеНаборыДанных.СоздатьВТИмяРегистраСрезПоследних(
		"ЗначенияПериодическихПоказателейРасчетаЗарплатыСотрудников",
		Запрос.МенеджерВременныхТаблиц,
		Истина,
		ЗарплатаКадрыОбщиеНаборыДанных.ОписаниеФильтраДляСоздатьВТИмяРегистра(СотрудникиДаты),
		ПараметрыПостроения);
	
	ЗарплатаКадрыОбщиеНаборыДанных.СоздатьВТИмяРегистраСрезПоследних(
		"ПрименениеДополнительныхПериодическихПоказателейРасчетаЗарплатыСотрудников",
		Запрос.МенеджерВременныхТаблиц,
		Истина,
		ЗарплатаКадрыОбщиеНаборыДанных.ОписаниеФильтраДляСоздатьВТИмяРегистра(СотрудникиДаты),
		ПараметрыПостроения);
	
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ДействующиеНачисления.Сотрудник КАК Сотрудник,
	|	НачисленияПоказатели.Показатель КАК Показатель,
	|	ЗначенияПериодическихПоказателей.Значение КАК Значение
	|ПОМЕСТИТЬ ДанныеЗаполнения
	|ИЗ
	|	ВТПлановыеНачисленияСрезПоследних КАК ДействующиеНачисления
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ПланВидовРасчета.Начисления.Показатели КАК НачисленияПоказатели
	|		ПО ДействующиеНачисления.Начисление = НачисленияПоказатели.Ссылка
	|			И (ДействующиеНачисления.Используется)
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТЗначенияПериодическихПоказателейРасчетаЗарплатыСотрудниковСрезПоследних КАК ЗначенияПериодическихПоказателей
	|		ПО (ЗначенияПериодическихПоказателей.Показатель = НачисленияПоказатели.Показатель)
	|			И (ЗначенияПериодическихПоказателей.Сотрудник = ДействующиеНачисления.Сотрудник)
	|ГДЕ
	|	ДействующиеНачисления.Используется
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ПрименениеДополнительныхПоказателей.Сотрудник,
	|	ПрименениеДополнительныхПоказателей.Показатель,
	|	ЗначенияПериодическихПоказателей.Значение
	|ИЗ
	|	ВТПрименениеДополнительныхПериодическихПоказателейРасчетаЗарплатыСотрудниковСрезПоследних КАК ПрименениеДополнительныхПоказателей
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ВТЗначенияПериодическихПоказателейРасчетаЗарплатыСотрудниковСрезПоследних КАК ЗначенияПериодическихПоказателей
	|		ПО (ЗначенияПериодическихПоказателей.Показатель = ПрименениеДополнительныхПоказателей.Показатель)
	|			И (ЗначенияПериодическихПоказателей.Сотрудник = ПрименениеДополнительныхПоказателей.Сотрудник)
	|ГДЕ
	|	ПрименениеДополнительныхПоказателей.Применение
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СотрудникиОрганизации.Сотрудник КАК Сотрудник,
	|	СотрудникиОрганизации.ПоказательТарифнойСтавки КАК ПоказательТарифнойСтавки,
	|	ЕСТЬNULL(ДанныеЗаполнения.Показатель, ЗНАЧЕНИЕ(Справочник.ПоказателиРасчетаЗарплаты.ПустаяСсылка)) КАК Показатель,
	|	ЕСТЬNULL(ДанныеЗаполнения.Значение, 0) КАК Значение
	|ИЗ
	|	ВТКадровыеДанныеСотрудников КАК СотрудникиОрганизации
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ДанныеЗаполнения КАК ДанныеЗаполнения
	|		ПО СотрудникиОрганизации.Сотрудник = ДанныеЗаполнения.Сотрудник";
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции 

Функция ТекущиеЗначенияСовокупныхТарифныхСтавокСотрудников(Ссылка, ДатаИндексации, МассивСотрудников) Экспорт 
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Период", ДатаИндексации);
	Запрос.УстановитьПараметр("МассивСотрудников", МассивСотрудников);
	
	Запрос.Текст = "ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
	               |	Сотрудники.Ссылка КАК Сотрудник,
	               |	&Период
	               |ПОМЕСТИТЬ ВТИзмеренияДаты
	               |ИЗ
	               |	Справочник.Сотрудники КАК Сотрудники
	               |ГДЕ
	               |	Сотрудники.Ссылка В(&МассивСотрудников)";
		
	Запрос.Выполнить();
	
	ПараметрыПостроения = ЗарплатаКадрыОбщиеНаборыДанных.ПараметрыПостроенияДляСоздатьВТИмяРегистраСрез();
	ПараметрыПостроения.ФормироватьСПериодичностьДень = Ложь;
	
	ЗарплатаКадрыОбщиеНаборыДанных.ДобавитьВКоллекциюОтбор(ПараметрыПостроения.Отборы, "Регистратор", "<>", Ссылка);
	
	ЗарплатаКадрыОбщиеНаборыДанных.СоздатьВТИмяРегистраСрезПоследних(
		"ЗначенияСовокупныхТарифныхСтавокСотрудников",
		Запрос.МенеджерВременныхТаблиц,
		Истина,
		ЗарплатаКадрыОбщиеНаборыДанных.ОписаниеФильтраДляСоздатьВТИмяРегистра(
			"ВТИзмеренияДаты",
			"Сотрудник"),
		ПараметрыПостроения);
	
	Запрос.Текст = "ВЫБРАТЬ
	               |	ЗначенияСовокупныхТарифныхСтавок.Сотрудник,
	               |	ЗначенияСовокупныхТарифныхСтавок.Значение КАК СовокупнаяТарифнаяСтавка,
	               |	ЗначенияСовокупныхТарифныхСтавок.ВидТарифнойСтавки
	               |ИЗ
	               |	ВТЗначенияСовокупныхТарифныхСтавокСотрудниковСрезПоследних КАК ЗначенияСовокупныхТарифныхСтавок
	               |ГДЕ
	               |	ЗначенияСовокупныхТарифныхСтавок.Значение <> 0";
				   
	ЗначенияСовокупныхТарифныхСтавок = Запрос.Выполнить().Выгрузить();			   
	
	Возврат ЗначенияСовокупныхТарифныхСтавок;
	
КонецФункции

Функция ЗначенияСовокупныхТарифныхСтавокСотрудников(НачисленияСотрудников, ДатаИндексации) Экспорт 
	
	ИсходныеДанные = ЗарплатаКадрыРасширенный.ИсходныеДанныеРасчетаСовокупныхТарифныхСтавок();
	
	НомерСтроки = 1;
	
	Для Каждого СтрокаНачисления Из НачисленияСотрудников Цикл
		
		НоваяСтрока = ИсходныеДанные.Добавить();
		НоваяСтрока.ДатаСобытия = ДатаИндексации;
		НоваяСтрока.Сотрудник = СтрокаНачисления.Сотрудник;
		НоваяСтрока.Начисление = СтрокаНачисления.Начисление;
		НоваяСтрока.РазмерФОТ = СтрокаНачисления.Размер;
		НоваяСтрока.НомерСтроки = НомерСтроки;
		
		НомерСтроки = НомерСтроки + 1;
		
	КонецЦикла;
	
	ЗначенияСовокупныхТарифныхСтавок = ЗарплатаКадрыРасширенный.ЗначенияСовокупныхТарифныхСтавок(ИсходныеДанные, ДатаИндексации);
	
	Возврат ЗначенияСовокупныхТарифныхСтавок.Выгрузить();
	
КонецФункции

#КонецОбласти

Процедура РассчитатьФОТПоДокументу(ДокументОбъект) Экспорт
	
	ВремяРегистрации = ЗарплатаКадрыРасширенный.ВремяРегистрацииДокумента(ДокументОбъект.Ссылка, ДокументОбъект.МесяцИндексации);
	
	РассчитатьФОТ(ДокументОбъект.Ссылка, ДокументОбъект.Организация, ВремяРегистрации, ДокументОбъект.НачисленияСотрудников, ДокументОбъект.ЗначенияПоказателей);
	
	РасчетЗарплатыРасширенный.ЗаполнитьФОТВДвиженияхЗагружаемогоДокумента(ДокументОбъект.Движения.ПлановыеНачисления, ДокументОбъект.НачисленияСотрудников, "Сотрудник");
	
КонецПроцедуры

#КонецЕсли