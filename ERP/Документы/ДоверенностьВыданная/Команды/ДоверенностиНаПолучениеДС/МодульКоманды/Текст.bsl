﻿&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПараметрыФормы = Новый Структура("ДоступностьРаспоряженийТовары", Ложь);
	ПараметрыФормы.Вставить("ЗаголовокФормы", НСтр("ru = 'Доверенности на получение наличных денежных средств'"));
	ОткрытьФорму("Документ.ДоверенностьВыданная.Форма.ФормаСпискаДокументов", ПараметрыФормы,
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры
