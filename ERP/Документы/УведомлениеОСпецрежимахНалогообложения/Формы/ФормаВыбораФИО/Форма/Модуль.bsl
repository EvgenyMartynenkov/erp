﻿&НаКлиенте
Перем Оповещение Экспорт;

&НаКлиенте
Процедура ОК(Команда)
	Закрыть(КодВозвратаДиалога.Да);
	ВыполнитьОбработкуОповещения(Оповещение, Новый Структура("Фамилия,Имя,Отчество", Фамилия, Имя, Отчество));
КонецПроцедуры

&НаКлиенте
Процедура Отмена(Команда)
	Закрыть(КодВозвратаДиалога.Нет);
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Параметры.Свойство("Фамилия") Тогда
		Фамилия = Параметры.Фамилия;
	КонецЕсли;
	Если Параметры.Свойство("Имя") Тогда
		Имя = Параметры.Имя;
	КонецЕсли;
	Если Параметры.Свойство("Отчество") Тогда
		Отчество = Параметры.Отчество;
	КонецЕсли;
КонецПроцедуры
