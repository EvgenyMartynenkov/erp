﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Интерфейс для работы с подсистемой Загрузка из файла.

// Устанавливает параметры загрузки.
//
Процедура УстановитьПараметрыЗагрузкиИзФайлаВТЧ(Параметры) Экспорт
	
КонецПроцедуры

// Производит сопоставление данных, загружаемых в табличную часть ПолноеИмяТабличнойЧасти,
// с данными в ИБ, и заполняет параметры АдресТаблицыСопоставления и СписокНеоднозначностей.
//
// Параметры:
//   ПолноеИмяТабличнойЧасти   - Строка - полное имя табличной части, в которую загружаются данные.
//   АдресЗагружаемыхДанных    - Строка - адрес временного хранилища с таблицей значений, в которой
//                                        находятся загруженные данные из файла. Состав колонок:
//     * Идентификатор - Число - порядковый номер строки;
//     * остальные колонки сооответствуют колонкам макета ЗагрузкаИзФайла.
//   АдресТаблицыСопоставления - Строка - адрес временного хранилища с пустой таблицей значений,
//                                        являющейся копией табличной части документа, 
//                                        которую необходимо заполнить из таблицы АдресЗагружаемыхДанных.
//   СписокНеоднозначностей - ТаблицаЗначений - список неоднозначных значений, для которых в ИБ имеется несколько подходящих вариантов.
//     * Колонка       - Строка - имя колонки, в которой была обнаружена неоднозначность;
//     * Идентификатор - Число  - идентификатор строки, в которой была обнаружена неоднозначность.
//
Процедура СопоставитьЗагружаемыеДанные(АдресЗагружаемыхДанных, АдресТаблицыСопоставления, СписокНеоднозначностей, ПолноеИмяТабличнойЧасти, ДополнительныеПараметры) Экспорт
	
	ТоварныеКатегории =  ПолучитьИзВременногоХранилища(АдресТаблицыСопоставления);
	ЗагружаемыеДанные = ПолучитьИзВременногоХранилища(АдресЗагружаемыхДанных);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ДанныеДляСопоставления.ТоварнаяКатегория,
	|	ДанныеДляСопоставления.Марка,
	|	ДанныеДляСопоставления.Идентификатор
	|ПОМЕСТИТЬ ДанныеДляСопоставления
	|ИЗ
	|	&ДанныеДляСопоставления КАК ДанныеДляСопоставления
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	МАКСИМУМ(ТоварныеКатегории.Ссылка) КАК ТоварнаяКатегорияСсылка,
	|	ДанныеДляСопоставления.Идентификатор,
	|	КОЛИЧЕСТВО(ДанныеДляСопоставления.Идентификатор) КАК Количество
	|ИЗ
	|	ДанныеДляСопоставления КАК ДанныеДляСопоставления
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ТоварныеКатегории КАК ТоварныеКатегории
	|		ПО (ТоварныеКатегории.Наименование ПОДОБНО ДанныеДляСопоставления.ТоварнаяКатегория)
	|			И НЕ ТоварныеКатегории.ЭтоГруппа
	|ГДЕ
	|	НЕ ТоварныеКатегории.Ссылка ЕСТЬ NULL 
	|
	|СГРУППИРОВАТЬ ПО
	|	ДанныеДляСопоставления.Идентификатор
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	МАКСИМУМ(Марки.Ссылка) КАК МаркаСсылка,
	|	ДанныеДляСопоставления.Идентификатор,
	|	КОЛИЧЕСТВО(ДанныеДляСопоставления.Идентификатор) КАК Количество
	|ИЗ
	|	ДанныеДляСопоставления КАК ДанныеДляСопоставления
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Марки КАК Марки
	|		ПО (Марки.Наименование ПОДОБНО ДанныеДляСопоставления.Марка)
	|			И НЕ Марки.ЭтоГруппа
	|ГДЕ
	|	НЕ Марки.Ссылка ЕСТЬ NULL 
	|
	|СГРУППИРОВАТЬ ПО
	|	ДанныеДляСопоставления.Идентификатор";
	
	Запрос.УстановитьПараметр("ДанныеДляСопоставления", ЗагружаемыеДанные);
	
	РезультатыЗапросов = Запрос.ВыполнитьПакет();
	
	ТаблицаТоварныеКатегории = РезультатыЗапросов[1].Выгрузить();
	ТаблицаМарки = РезультатыЗапросов[2].Выгрузить();
	
	Для каждого СтрокаТаблицы Из ЗагружаемыеДанные Цикл 
		
		НоваяСтрока = ТоварныеКатегории.Добавить();
		НоваяСтрока.Идентификатор = СтрокаТаблицы.Идентификатор;
		НоваяСтрока.Квота = СтрокаТаблицы.Квота;
		НоваяСтрока.ПроцентОтклонения = СтрокаТаблицы.ПроцентОтклонения;
		
		СтрокаТоварнаяКатегория = ТаблицаТоварныеКатегории.Найти(СтрокаТаблицы.Идентификатор, "Идентификатор");
		Если СтрокаТоварнаяКатегория <> Неопределено Тогда 
			Если СтрокаТоварнаяКатегория.Количество = 1 Тогда 
				НоваяСтрока.ТоварнаяКатегория  = СтрокаТоварнаяКатегория.ТоварнаяКатегорияСсылка;
			ИначеЕсли СтрокаТоварнаяКатегория.Количество > 1 Тогда 
				ЗаписьОНеоднозначности = СписокНеоднозначностей.Добавить();
				ЗаписьОНеоднозначности.Идентификатор = СтрокаТаблицы.Идентификатор; 
				ЗаписьОНеоднозначности.Колонка = "ТоварнаяКатегория";
			КонецЕсли;
		КонецЕсли;
		
		СтрокаМарка = ТаблицаМарки.Найти(СтрокаТаблицы.Идентификатор, "Идентификатор");
		Если СтрокаМарка <> Неопределено Тогда 
			Если СтрокаМарка.Количество = 1 Тогда 
				НоваяСтрока.Марка  = СтрокаМарка.МаркаСсылка;
			ИначеЕсли СтрокаМарка.Количество > 1 Тогда 
				ЗаписьОНеоднозначности = СписокНеоднозначностей.Добавить();
				ЗаписьОНеоднозначности.Идентификатор = СтрокаТаблицы.Идентификатор; 
				ЗаписьОНеоднозначности.Колонка = "Марка";
			КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;
	
	ПоместитьВоВременноеХранилище(ТоварныеКатегории, АдресТаблицыСопоставления);
	
КонецПроцедуры

// Возврашает список подходящих объектов ИБ для неоднозначного значения ячейки.
// 
// Параметры:
//   ПолноеИмяТабличнойЧасти  - Строка - полное имя табличной части, в которую загружаются данные.
//  ИмяКолонки                - Строка - имя колонки, в который возника неоднозначность 
//  СписокНеоднозначностей    - ТаблицаЗначений - Список для заполения с неоднозначными данными
//     * Идентификатор        - Число  - Уникальный идентификатор строки
//     * Колонка              - Строка -  Имя колонки с возникшей неоднозначностью 
//  ЗагружаемыеЗначенияСтрока - Строка - Загружаемые данные на основании которых возникла неоднозначность.
//
Процедура ЗаполнитьСписокНеоднозначностей(ПолноеИмяТабличнойЧасти, СписокНеоднозначностей, ИмяКолонки, ЗагружаемыеЗначенияСтрока) Экспорт 
	
	Если ИмяКолонки = "ТоварнаяКатегория" Тогда

		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	ТоварныеКатегории.Ссылка
		|ИЗ
		|	Справочник.ТоварныеКатегории КАК ТоварныеКатегории
		|ГДЕ
		|	ТоварныеКатегории.Наименование = &Наименование
		|	И НЕ ТоварныеКатегории.ЭтоГруппа";
		Запрос.УстановитьПараметр("Наименование", ЗагружаемыеЗначенияСтрока.ТоварнаяКатегория);
		РезультатЗапроса = Запрос.Выполнить();
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			СписокНеоднозначностей.Добавить(ВыборкаДетальныеЗаписи.Ссылка);  
		КонецЦикла;
	КонецЕсли;
	
	Если ИмяКолонки = "Марка" Тогда

		Запрос = Новый Запрос;
		Запрос.Текст = 
		"ВЫБРАТЬ
		|	Марки.Ссылка
		|ИЗ
		|	Справочник.Марки КАК Марки
		|ГДЕ
		|	Марки.Наименование = &Наименование
		|	И НЕ Марки.ЭтоГруппа";
		Запрос.УстановитьПараметр("Наименование", ЗагружаемыеЗначенияСтрока.Марка);
		РезультатЗапроса = Запрос.Выполнить();
		ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
		Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
			СписокНеоднозначностей.Добавить(ВыборкаДетальныеЗаписи.Ссылка);  
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Проведение

// Инициализирует таблицы значений, содержащие данные табличных частей документа.
// Таблицы значений сохраняет в свойствах структуры "ДополнительныеСвойства".
//
Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства) Экспорт

	Запрос = Новый Запрос(
	// 0 ТаблицаКвотыАссортимента
	"ВЫБРАТЬ
	|	ТоварныеКатегории.НомерСтроки КАК НСтр,
	|	ТоварныеКатегории.Ссылка.ДатаНачалаДействия КАК Период,
	|	ТоварныеКатегории.ТоварнаяКатегория КАК ТоварнаяКатегория,
	|	ТоварныеКатегории.Ссылка.ОбъектПланирования КАК ОбъектПланирования,
	|	ТоварныеКатегории.Марка КАК Марка,
	|	ТоварныеКатегории.Ссылка.КоллекцияНоменклатуры КАК КоллекцияНоменклатуры,
	|	ТоварныеКатегории.Квота КАК Квота,
	|	ТоварныеКатегории.ПроцентОтклонения КАК ПроцентОтклонения
	|ИЗ
	|	Документ.УстановкаКвотАссортимента.ТоварныеКатегории КАК ТоварныеКатегории
	|ГДЕ
	|	ТоварныеКатегории.Ссылка = &Ссылка
	|
	|УПОРЯДОЧИТЬ ПО
	|	НСтр");

	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Результат = Запрос.ВыполнитьПакет();

	ДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаКвотыАссортимента", Результат[0].Выгрузить());
	
КонецПроцедуры

#КонецОбласти

#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
КонецПроцедуры

#КонецОбласти

#Область Прочие

// 
Функция СуществуетПлан(ПроверяемыйОбъектПланирования, НаДату, ВызывающийДокумент) Экспорт
	ЕстьПлан = Ложь;
	Запрос = Новый Запрос("ВЫБРАТЬ ПЕРВЫЕ 1
	|	Регистратор КАК Регистратор
	|ИЗ
	|	РегистрСведений.КвотыАссортимента КАК КвотыАссортимента
	|ГДЕ
	|	КвотыАссортимента.Период = &Период
	|	И КвотыАссортимента.ОбъектПланирования = &ОбъектПланирования");
	Запрос.УстановитьПараметр("Период", НачалоМесяца(НаДату));
	Запрос.УстановитьПараметр("ОбъектПланирования", ПроверяемыйОбъектПланирования);
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		Если Выборка.Регистратор <> ВызывающийДокумент Тогда
			ЕстьПлан = Истина;
		КонецЕсли;
	КонецЦикла;
	Возврат ЕстьПлан;
КонецФункции

#КонецОбласти

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УТ 11.1.6.5
// Заполняется новый реквизит "КвотаВсего".
// Заполнение выполняется в отложенном режиме.
// 
// Параметры:
//	Параметры - структура.
//
Процедура ЗаполнитьРеквизитКвотаВсегоОтложенно(Параметры) Экспорт

	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	УстановкаКвотАссортиментаТоварныеКатегории.Ссылка КАК Ссылка
	|ИЗ
	|	Документ.УстановкаКвотАссортимента.ТоварныеКатегории КАК УстановкаКвотАссортиментаТоварныеКатегории
	|ГДЕ
	|	УстановкаКвотАссортиментаТоварныеКатегории.Ссылка.Проведен
	|	И УстановкаКвотАссортиментаТоварныеКатегории.Ссылка.КвотаВсего = 0
	|	И УстановкаКвотАссортиментаТоварныеКатегории.Квота <> 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	УстановкаКвотАссортиментаТоварныеКатегории.Ссылка.Дата УБЫВ";

	Выборка = Запрос.Выполнить().Выбрать();

	Пока Выборка.Следующий() Цикл
		
		НачатьТранзакцию();
		Попытка
			
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("Документ.УстановкаКвотАссортимента");
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			Блокировка.Заблокировать();
			
			ДокументОбъект = Выборка.Ссылка.ПолучитьОбъект();
			
			// Если объект ранее был удален или обработан другими сеансами, пропускаем его.
			Если ДокументОбъект = Неопределено Тогда
				ОтменитьТранзакцию();
				Возврат;
			КонецЕсли;
			Если ДокументОбъект.КвотаВсего <> 0 Тогда
				ОтменитьТранзакцию();
				Возврат;
			КонецЕсли;
			
			ДокументОбъект.КвотаВсего = ДокументОбъект.ТоварныеКатегории.Итог("Квота");
			
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(ДокументОбъект);
			
			ЗафиксироватьТранзакцию();
		Исключение
			ОтменитьТранзакцию();
			ВызватьИсключение;
		КонецПопытки;
		
	КонецЦикла;

	Параметры.ОбработкаЗавершена = (Выборка.Количество() = 0);

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли