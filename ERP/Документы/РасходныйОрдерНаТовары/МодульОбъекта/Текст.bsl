﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);

	НоменклатураСервер.ОчиститьНеиспользуемыеСерии(ЭтотОбъект,
														НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.РасходныйОрдерНаТовары));
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСтатусыРасходныхОрдеров", Новый Структура("Склад", Склад)) Тогда
		Статус = Перечисления.СтатусыРасходныхОрдеров.Отгружен;
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		ПроверитьОрдерныйСклад(Отказ);
	КонецЕсли;
	
	ВсегоМест = СкладыСервер.КоличествоМестВТЧ(Товары);
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	Если ЗначениеЗаполнено(ДанныеЗаполнения) Тогда
		Если ТипЗнч(ДанныеЗаполнения) <> Тип("Строка") Тогда
			
			Распоряжение = ДанныеЗаполнения;
			МенеджерДокумента = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(Распоряжение.Метаданные().ПолноеИмя());
			ДанныеПоРаспоряжению = МенеджерДокумента.ВыполнитьПроверкиРаспоряжения(Распоряжение, Склад);
			
			Операция	= ДанныеПоРаспоряжению.Операция;
			ТекстОшибки = ДанныеПоРаспоряжению.ТекстОшибки;
			
			Если Операция = "Отказ" Тогда
				ВызватьИсключение ТекстОшибки;
			ИначеЕсли Операция <> "Возврат" Тогда 
				
				ЗаполнитьПоРаспоряжению(ДанныеПоРаспоряжению);
				
				Если Не СкладыСервер.ИспользоватьСкладскиеПомещения(Склад, ДатаОтгрузки)
					И Не СкладыСервер.ИспользоватьАдресноеХранение(Склад, Помещение, ДатаОтгрузки)
					И ПолучитьФункциональнуюОпцию("ИспользоватьУпаковкиНоменклатуры") Тогда
					РазбитьПоУпаковкамСправочно();
				КонецЕсли;
				
			КонецЕсли;
		КонецЕсли;
		
	Иначе
		ТекстСообщения = НСтр("ru='Расходный ордер на товары можно вводить только на основании распоряжения на отгрузку товаров.'");
		
		ВызватьИсключение ТекстСообщения;
	КонецЕсли;
	
	ИнициализироватьДокумент(ДанныеЗаполнения);
	ЗаполнитьРеквизитыДоставкиПоРаспоряжению();
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);

	Документы.РасходныйОрдерНаТовары.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ЗапасыСервер.ОтразитьТоварыНаСкладах(ДополнительныеСвойства, Движения, Отказ);
	ЗаказыСервер.ОтразитьТоварыКОтгрузке(ДополнительныеСвойства, Движения, Отказ);
	СкладыСервер.ОтразитьТоварыКОтбору(ДополнительныеСвойства, Движения, Отказ);
 	СкладыСервер.ОтразитьДвиженияСерийТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьРезервыСерийТоваров(ДополнительныеСвойства, Движения, Отказ);
 	СкладыСервер.ОтразитьТоварыВСкладскихЯчейках(ДополнительныеСвойства, Движения, Отказ);

	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	
	СкладыСервер.ОтразитьСостоянияОтгрузки(Распоряжение, Отказ);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	
	СкладыСервер.ОтразитьСостоянияОтгрузки(Распоряжение, Отказ, Ссылка);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)

	Серии.Очистить();
	ИнициализироватьДокумент();

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	Если Не ДополнительныеСвойства.Свойство("ОтложенноеПроведение") Тогда
		
		ПараметрыФО = Новый Структура;
		ПараметрыФО.Вставить("Склад", Склад);
		
		Если Не Проведен
			И Статус <> Перечисления.СтатусыРасходныхОрдеров.КОтбору
			И Статус <> Перечисления.СтатусыРасходныхОрдеров.КУказаниюСерий
			И (ПолучитьФункциональнуюОпцию("ИспользоватьСерииНоменклатурыСклад",ПараметрыФО)
			Или СкладыСервер.ИспользоватьСкладскиеПомещения(Склад, ДатаОтгрузки)
			Или СкладыСервер.ИспользоватьАдресноеХранение(Склад, Помещение, ДатаОтгрузки)) Тогда
			
			ТекстСообщения = НСтр("ru = 'Документ должен быть проведен сначала в статусе ""К отбору"" или ""К указанию серий""'");
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,ЭтотОбъект,"Статус","Объект",Отказ);
		КонецЕсли;
	КонецЕсли;

	МассивНепроверяемыхРеквизитов = Новый Массив;

	МассивНепроверяемыхРеквизитов.Добавить("ЗаданиеНаПеревозку");
	
	Если ОтгрузкаПоЗаданиюНаПеревозку
		И Не ЗначениеЗаполнено(ЗаданиеНаПеревозку) Тогда
		ТекстСообщения = НСтр("ru = 'Распоряжение ""%Распоряжение%"" еще не добавлено в задание на перевозку (или задание еще не до конца сформировано). Расходный ордер по этому распоряжению нужно вводить позже.'");
        ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Распоряжение%", Распоряжение);
		
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,ЭтотОбъект,"ЗаданиеНаПеревозку","Объект",Отказ);
	КонецЕсли;
	
	Если Не СкладыСервер.ИспользоватьСкладскиеПомещения(Склад,ДатаОтгрузки) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("Помещение");
	КонецЕсли;
	
	ИспользоватьУпаковочныеЛисты = ПолучитьФункциональнуюОпцию("ИспользоватьУпаковочныеЛисты");
	
	Если Не СкладыСервер.ИспользоватьАдресноеХранение(Склад,Помещение,ДатаОтгрузки) Тогда
		МассивНепроверяемыхРеквизитов.Добавить("Товары.Упаковка");
		МассивНепроверяемыхРеквизитов.Добавить("ЗонаОтгрузки");
	Иначе
		Если Статус = Перечисления.СтатусыРасходныхОрдеров.КОтбору
			Или Статус = Перечисления.СтатусыРасходныхОрдеров.КУказаниюСерий Тогда
			МассивНепроверяемыхРеквизитов.Добавить("Товары.Упаковка");
		Иначе
			
			ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияУпаковок();
			ПараметрыПроверки.ВыводитьНомераСтрок = Не ИспользоватьУпаковочныеЛисты;
			ПараметрыПроверки.ОтборПроверяемыхСтрок.Вставить("ЭтоУпаковочныйЛист", Ложь);
			НоменклатураСервер.ПроверитьЗаполнениеУпаковок(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ,ПараметрыПроверки);
			
		КонецЕсли;
		
		Если Не ПолучитьФункциональнуюОпцию("ИспользоватьУпаковкиНоменклатуры") Тогда
			МассивНепроверяемыхРеквизитов.Добавить("Товары.Упаковка");
			ТекстСообщения = НСтр("ru='В настройках программы не включено использование упаковок номенклатуры, 
			|поэтому нельзя оформить документ по складу с адресным хранением остатков. Обратитесь к администратору'");
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,,,Отказ);
		КонецЕсли;
	КонецЕсли;	
	
	Если Статус = Перечисления.СтатусыРасходныхОрдеров.Отгружен Тогда
		
		Если ДатаОтгрузки > ТекущаяДата() Тогда
			
			ТекстСообщения = НСтр("ru='При проведении расходного ордера в статусе ""%Отгружен%"" дата отгрузки должна быть не больше текущей даты.'");
			
			ТекстСообщения = СтрЗаменить(ТекстСообщения,"%Отгружен%",Перечисления.СтатусыРасходныхОрдеров.Отгружен);
			
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,ЭтотОбъект,"ДатаОтгрузки","Объект",Отказ);
			
		КонецЕсли;
	КонецЕсли;

	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.ВыводитьНомераСтрок = Не ИспользоватьУпаковочныеЛисты;
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ,ПараметрыПроверки);
	
	НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект,
												НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект,Документы.РасходныйОрдерНаТовары),
												Отказ);
	
	СкладыСервер.ПроверитьЗаполнениеТЧСУпаковочнымиЛистами(ЭтотОбъект, ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов, Отказ);
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
	ОбщегоНазначенияУТ.ПроверитьЗаполнениеКоличества(ЭтотОбъект, ПроверяемыеРеквизиты, Отказ);
	
	СкладыСервер.ПроверитьОрдерностьСклада(Склад, ДатаОтгрузки, "ПриОтгрузке", Отказ);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ЗаполнитьОстаткамиТоваровКОтгрузке() Экспорт

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("Склад",  Склад);
	Запрос.УстановитьПараметр("Распоряжение", Распоряжение);
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	ТоварыКОтгрузке.Номенклатура КАК Номенклатура,
	|	ТоварыКОтгрузке.Характеристика КАК Характеристика,
	|	ТоварыКОтгрузке.Назначение КАК Назначение,
	|	ТоварыКОтгрузке.Серия КАК Серия,
	|	СУММА(ТоварыКОтгрузке.Количество) КАК Количество,
	|	СУММА(ТоварыКОтгрузке.Количество) КАК КоличествоУпаковок
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТоварыКОтгрузке.Номенклатура КАК Номенклатура,
	|		ТоварыКОтгрузке.Характеристика КАК Характеристика,
	|		ТоварыКОтгрузке.Назначение КАК Назначение,
	|		ТоварыКОтгрузке.Серия КАК Серия,
	|		ТоварыКОтгрузке.КОтгрузкеОстаток - ТоварыКОтгрузке.СобираетсяОстаток - ТоварыКОтгрузке.СобраноОстаток КАК Количество
	|	ИЗ
	|		РегистрНакопления.ТоварыКОтгрузке.Остатки(
	|				,
	|				ДокументОтгрузки = &Распоряжение
	|					И Склад = &Склад) КАК ТоварыКОтгрузке
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТоварыКОтгрузке.Номенклатура,
	|		ТоварыКОтгрузке.Характеристика,
	|		ТоварыКОтгрузке.Назначение КАК Назначение,
	|		ТоварыКОтгрузке.Серия,
	|		ВЫБОР
	|			КОГДА ТоварыКОтгрузке.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -ТоварыКОтгрузке.КОтгрузке
	|			ИНАЧЕ ТоварыКОтгрузке.КОтгрузке
	|		КОНЕЦ - ВЫБОР
	|			КОГДА ТоварыКОтгрузке.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -ТоварыКОтгрузке.Собирается
	|			ИНАЧЕ ТоварыКОтгрузке.Собирается
	|		КОНЕЦ - ВЫБОР
	|			КОГДА ТоварыКОтгрузке.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|				ТОГДА -ТоварыКОтгрузке.Собрано
	|			ИНАЧЕ ТоварыКОтгрузке.Собрано
	|		КОНЕЦ
	|	ИЗ
	|		РегистрНакопления.ТоварыКОтгрузке КАК ТоварыКОтгрузке
	|	ГДЕ
	|		ТоварыКОтгрузке.Регистратор = &Ссылка
	|		И ТоварыКОтгрузке.Склад = &Склад
	|		И ТоварыКОтгрузке.Активность
	|		И ТоварыКОтгрузке.ДокументОтгрузки = &Распоряжение) КАК ТоварыКОтгрузке
	|
	|СГРУППИРОВАТЬ ПО
	|	ТоварыКОтгрузке.Номенклатура,
	|	ТоварыКОтгрузке.Характеристика,
	|	ТоварыКОтгрузке.Назначение,
	|	ТоварыКОтгрузке.Серия
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТоварыКОтгрузке.Количество) > 0
	|
	|УПОРЯДОЧИТЬ ПО
	|	ТоварыКОтгрузке.Номенклатура.Наименование,
	|	ТоварыКОтгрузке.Характеристика.Наименование
	|ИТОГИ
	|	СУММА(Количество),
	|	СУММА(КоличествоУпаковок)
	|ПО
	|	Номенклатура,
	|	Характеристика,
	|	Назначение";
	Результат = Запрос.Выполнить();
	Если Не Результат.Пустой() Тогда

		ВыборкаПоНоменклатуре = Результат.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		
		Пока ВыборкаПоНоменклатуре.Следующий() Цикл
			ВыборкаПоХарактеристикам = ВыборкаПоНоменклатуре.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
			
			Пока ВыборкаПоХарактеристикам.Следующий() Цикл
				ВыборкаПоНазначению = ВыборкаПоХарактеристикам.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
				
				Пока ВыборкаПоНазначению.Следующий() Цикл
					НоваяСтрокаТоваров = Товары.Добавить();
					ЗаполнитьЗначенияСвойств(НоваяСтрокаТоваров, ВыборкаПоНазначению);
					
					ВыборкаПоСериям = ВыборкаПоНазначению.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
					
					Пока ВыборкаПоСериям.Следующий() Цикл
						
						Если ЗначениеЗаполнено(ВыборкаПоСериям.Серия) Тогда
							НоваяСтрокаСерий = Серии.Добавить();
							ЗаполнитьЗначенияСвойств(НоваяСтрокаСерий, ВыборкаПоСериям);
						КонецЕсли;
						
					КонецЦикла;
					
				КонецЦикла;
				
			КонецЦикла;
			
		КонецЦикла;
		
		ВсегоМест = СкладыСервер.КоличествоМестВТЧ(Товары);
		
	Иначе

		ТекстСообщения = НСтр("ru = 'Нет данных для заполнения по распоряжению ""%Распоряжение%"" .'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Распоряжение%", Распоряжение);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект, "Распоряжение");

	КонецЕсли;

КонецПроцедуры

Процедура ЗаполнитьПоРаспоряжению(СтруктураПараметров) Экспорт
	
	Если Не ЗначениеЗаполнено(Распоряжение) Тогда
		Возврат;
	КонецЕсли;
	
	ГруппаСкладовВРаспоряжении = СтруктураПараметров.ГруппаСкладовВРаспоряжении;
	ИспользоватьОрдернуюСхему  = СтруктураПараметров.ИспользоватьОрдернуюСхему;
	
	Если Не ГруппаСкладовВРаспоряжении Тогда
		
		Если ИспользоватьОрдернуюСхему Тогда
			
			ЭтотОбъект.ДатаОтгрузки = Макс(ТекущаяДатаСеанса(), СтруктураПараметров.ДатаОтгрузки);
			ЗаполнитьОстаткамиТоваровКОтгрузке();
			ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.РасходныйОрдерНаТовары);
			
			Если ЗначениеЗаполнено(Склад) 
				И (Не СкладыСервер.ИспользоватьСкладскиеПомещения(Склад, ДатаОтгрузки)
				Или ЗначениеЗаполнено(Помещение)) Тогда
				НоменклатураСервер.ЗаполнитьСерииПоFEFO(ЭтотОбъект, ПараметрыУказанияСерий, Ложь);	
			КонецЕсли;
			
			НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(ЭтотОбъект, ПараметрыУказанияСерий);
			Отказ = Ложь;
			НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект, ПараметрыУказанияСерий, Отказ,, Ложь);
			
			Если Не Отказ Тогда
				Статус = Перечисления.СтатусыРасходныхОрдеров.КОтбору;	
			Иначе
				Статус = Перечисления.СтатусыРасходныхОрдеров.КУказаниюСерий;
			КонецЕсли;
						
		КонецЕсли;
	Иначе
		ЗаполнитьОстаткамиТоваровКОтгрузке();
		ПараметрыУказанияСерий = НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.РасходныйОрдерНаТовары);
		
		Если ЗначениеЗаполнено(Склад) 
			И (Не СкладыСервер.ИспользоватьСкладскиеПомещения(Склад, ДатаОтгрузки)
				Или ЗначениеЗаполнено(Помещение)) Тогда
			НоменклатураСервер.ЗаполнитьСерииПоFEFO(ЭтотОбъект, ПараметрыУказанияСерий, Ложь);	
		КонецЕсли;
		
		НоменклатураСервер.ЗаполнитьСтатусыУказанияСерий(ЭтотОбъект, ПараметрыУказанияСерий);
		Отказ = Ложь;
		НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект, ПараметрыУказанияСерий, Отказ,, Ложь);
		
		Если Не Отказ Тогда
			Статус = Перечисления.СтатусыРасходныхОрдеров.КОтбору;	
		Иначе
			Статус = Перечисления.СтатусыРасходныхОрдеров.КУказаниюСерий;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

Процедура ЗаполнитьРеквизитыДоставкиПоРаспоряжению()
	
	СтруктураРеквизитов = Новый Структура;
	СтруктураРеквизитов.Вставить("Склад", Склад);
	СтруктураРеквизитов.Вставить("Распоряжение", Распоряжение);
	СтруктураРеквизитов.Вставить("ДатаОтгрузки", ДатаОтгрузки);
	СтруктураРеквизитов.Вставить("Приоритет", Приоритет);
	
	РеквизитыДоставки = Документы.РасходныйОрдерНаТовары.РеквизитыДоставкиПоРаспоряжению(СтруктураРеквизитов);
	
	ЗаполнитьЗначенияСвойств(ЭтотОбъект, РеквизитыДоставки);
	
КонецПроцедуры

Процедура РазбитьПоУпаковкамСправочно() Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Таблица.Номенклатура КАК Номенклатура,
	|	Таблица.Характеристика КАК Характеристика,
	|	Таблица.Назначение КАК Назначение,
	|	Таблица.УпаковочныйЛист КАК УпаковочныйЛист,
	|	Таблица.УпаковочныйЛистРодитель КАК УпаковочныйЛистРодитель,
	|	Таблица.ЭтоУпаковочныйЛист КАК ЭтоУпаковочныйЛист,
	|	Таблица.НомерСтроки КАК НомерСтроки,
	|	Таблица.Количество КАК Количество,
	|	Таблица.КоличествоУпаковок КАК КоличествоУпаковок,
	|	Таблица.Упаковка КАК Упаковка,
	|	Таблица.НеОтгружать КАК НеОтгружать
	|ПОМЕСТИТЬ ТаблицаНоменклатурыДляЗапроса
	|ИЗ
	|	&Таблица КАК Таблица
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Таблица.Номенклатура КАК Номенклатура,
	|	МИНИМУМ(Таблица.НомерСтроки) КАК НомерСтроки,
	|	ВЫРАЗИТЬ(Таблица.Номенклатура КАК Справочник.Номенклатура).НаборУпаковок КАК НаборУпаковок,
	|	Таблица.Характеристика КАК Характеристика,
	|	Таблица.Назначение КАК Назначение,
	|	СУММА(Таблица.Количество) КАК Количество,
	|	Таблица.УпаковочныйЛистРодитель КАК УпаковочныйЛистРодитель
	|ПОМЕСТИТЬ ТаблицаНоменклатуры
	|ИЗ
	|	ТаблицаНоменклатурыДляЗапроса КАК Таблица
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СпрНоменклатура
	|		ПО Таблица.Номенклатура = СпрНоменклатура.Ссылка
	|ГДЕ
	|	Таблица.УпаковочныйЛистРодитель = ЗНАЧЕНИЕ(Документ.УпаковочныйЛист.ПустаяСсылка)
	|	И Таблица.НеОтгружать = 0
	|
	|СГРУППИРОВАТЬ ПО
	|	Таблица.УпаковочныйЛистРодитель,
	|	Таблица.Номенклатура,
	|	СпрНоменклатура.НаборУпаковок,
	|	Таблица.Характеристика,
	|	Таблица.Назначение
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаНоменклатуры.Номенклатура КАК Номенклатура,
	|	ТаблицаНоменклатуры.Количество,
	|	ТаблицаНоменклатуры.НомерСтроки КАК НомерСтроки,
	|	ТаблицаНоменклатуры.Характеристика КАК Характеристика,
	|	ТаблицаНоменклатуры.Назначение КАК Назначение,
	|	ЕСТЬNULL(УпаковкиНоменклатуры.Коэффициент, 1) КАК КоличествоВУпаковке,
	|	ЕСТЬNULL(УпаковкиНоменклатуры.Ссылка, ЗНАЧЕНИЕ(Справочник.УпаковкиНоменклатуры.ПустаяСсылка)) КАК Упаковка,
	|	ТаблицаНоменклатуры.УпаковочныйЛистРодитель КАК УпаковочныйЛистРодитель,
	|	ЗНАЧЕНИЕ(Документ.УпаковочныйЛист.ПустаяСсылка) КАК УпаковочныйЛист,
	|	ЛОЖЬ КАК ЭтоУпаковочныйЛист,
	|	0 КАК КоличествоУпаковок,
	|	0 КАК НеОтгружать
	|ИЗ
	|	ТаблицаНоменклатуры КАК ТаблицаНоменклатуры
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|		ПО (ТаблицаНоменклатуры.Номенклатура = УпаковкиНоменклатуры.Владелец
	|				ИЛИ ТаблицаНоменклатуры.НаборУпаковок = УпаковкиНоменклатуры.Владелец)
	|			И (НЕ УпаковкиНоменклатуры.ПометкаУдаления)
	|			И ТаблицаНоменклатуры.Количество >= УпаковкиНоменклатуры.Коэффициент
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТаблицаБезВерхнегоУровня.Номенклатура,
	|	ТаблицаБезВерхнегоУровня.Количество,
	|	ТаблицаБезВерхнегоУровня.НомерСтроки,
	|	ТаблицаБезВерхнегоУровня.Характеристика,
	|	ЗНАЧЕНИЕ(Справочник.Назначения.ПустаяСсылка),
	|	0,
	|	ТаблицаБезВерхнегоУровня.Упаковка,
	|	ТаблицаБезВерхнегоУровня.УпаковочныйЛистРодитель,
	|	ТаблицаБезВерхнегоУровня.УпаковочныйЛист,
	|	ТаблицаБезВерхнегоУровня.ЭтоУпаковочныйЛист,
	|	ТаблицаБезВерхнегоУровня.КоличествоУпаковок,
	|	ТаблицаБезВерхнегоУровня.НеОтгружать
	|ИЗ
	|	ТаблицаНоменклатурыДляЗапроса КАК ТаблицаБезВерхнегоУровня
	|ГДЕ
	|	(ТаблицаБезВерхнегоУровня.ЭтоУпаковочныйЛист
	|			ИЛИ ТаблицаБезВерхнегоУровня.УпаковочныйЛистРодитель <> ЗНАЧЕНИЕ(Документ.УпаковочныйЛист.ПустаяСсылка)
	|			ИЛИ ТаблицаБезВерхнегоУровня.НеОтгружать = 1)
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки,
	|	КоличествоВУпаковке УБЫВ";
	
	ТаблицаНоменклатуры = Товары.Выгрузить();
	Запрос.УстановитьПараметр("Таблица",ТаблицаНоменклатуры);
	
	Товары.Очистить();
	
	Выборка = Запрос.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	
	ТекущиеЗначения = Новый Структура;
	ТекущиеЗначения.Вставить("НомерСтроки", Неопределено);
	ТекущиеЗначения.Вставить("Номенклатура", Неопределено);
	ТекущиеЗначения.Вставить("Характеристика", Неопределено);
	ТекущиеЗначения.Вставить("Назначение", Неопределено);
	
	Количество = 0;
	
	Пока Выборка.Следующий() Цикл
		Если Выборка.ЭтоУпаковочныйЛист
			Или ЗначениеЗаполнено(Выборка.УпаковочныйЛистРодитель)
			Или Выборка.НеОтгружать Тогда
			НоваяСтрока = Товары.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);
			Продолжить;
		КонецЕсли;
		
		Если Выборка.НомерСтроки <> ТекущиеЗначения.НомерСтроки Тогда
			
			Если Количество <> 0 Тогда
				НоваяСтрока = Товары.Добавить();
				НоваяСтрока.Количество = Количество;
				НоваяСтрока.КоличествоУпаковок = Количество;
				
				НоваяСтрока.Номенклатура = ТекущиеЗначения.Номенклатура;
				НоваяСтрока.Характеристика = ТекущиеЗначения.Характеристика;
				НоваяСтрока.Назначение = ТекущиеЗначения.Назначение;
			КонецЕсли;
			ЗаполнитьЗначенияСвойств(ТекущиеЗначения, Выборка);
			
			Количество = Выборка.Количество;
			
		КонецЕсли;
		
		Если НЕ ЗначениеЗаполнено(Выборка.Упаковка) Тогда
			Продолжить;
		КонецЕсли;
		
		КоличествоВДокумент = Цел(Количество / Выборка.КоличествоВУпаковке);
		
		Если КоличествоВДокумент > 0 Тогда
		
			НоваяСтрока = Товары.Добавить();
			НоваяСтрока.Количество = КоличествоВДокумент * Выборка.КоличествоВУпаковке;
			НоваяСтрока.КоличествоУпаковок = КоличествоВДокумент;
			НоваяСтрока.Упаковка = Выборка.Упаковка;
			
			НоваяСтрока.Номенклатура = ТекущиеЗначения.Номенклатура;
			НоваяСтрока.Характеристика = ТекущиеЗначения.Характеристика;
			НоваяСтрока.Назначение = ТекущиеЗначения.Назначение;
			
			Количество = Количество - КоличествоВДокумент * Выборка.КоличествоВУпаковке;
		КонецЕсли;
	КонецЦикла;
	
	Если Количество <> 0 Тогда
		НоваяСтрока = Товары.Добавить();
		НоваяСтрока.Количество = Количество;
		НоваяСтрока.КоличествоУпаковок = Количество;
		
		НоваяСтрока.Номенклатура = ТекущиеЗначения.Номенклатура;
		НоваяСтрока.Характеристика = ТекущиеЗначения.Характеристика;
		НоваяСтрока.Назначение = ТекущиеЗначения.Назначение;	
	КонецЕсли;
	ВсегоМест = СкладыСервер.КоличествоМестВТЧ(Товары);
	
КонецПроцедуры

Процедура ИнициализироватьДокумент(ДанныеЗаполнения = Неопределено)

	Ответственный = Пользователи.ТекущийПользователь();
	Приоритет     = Метаданные().Реквизиты.Приоритет.ЗначениеЗаполнения;
	Склад         = ЗначениеНастроекПовтИсп.ПолучитьСкладПоУмолчанию(Склад);
	Приоритет     = Справочники.Приоритеты.ПолучитьПриоритетПоУмолчанию(Приоритет);
	
	Если Не ЗначениеЗаполнено(ДатаОтгрузки) Тогда
		ДатаОтгрузки = ТекущаяДата();
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Склад) Тогда
		Если СкладыСервер.ИспользоватьАдресноеХранение(Склад,Помещение,ДатаОтгрузки) Тогда
			ЗонаОтгрузки = Справочники.СкладскиеЯчейки.ЗонаОтгрузкиПоУмолчанию(Склад,Помещение,ЗонаОтгрузки);
		КонецЕсли;
	КонецЕсли;
	
	СкладскаяОперация = Документы.РасходныйОрдерНаТовары.СкладскаяОперацияПоРаспоряжению(Распоряжение);
	
	Если ПолучитьФункциональнуюОпцию("ИспользоватьСтатусыРасходныхОрдеров", Новый Структура("Склад", Склад)) Тогда
		Статус = Метаданные().Реквизиты.Статус.ЗначениеЗаполнения;
	Иначе
		Статус = Перечисления.СтатусыРасходныхОрдеров.Отгружен;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

Процедура СформироватьСписокРегистровДляКонтроля()

	Массив = Новый Массив;
	Если ДополнительныеСвойства.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		Массив.Добавить(Движения.ТоварыКОтгрузке);
		Если Статус = Перечисления.СтатусыРасходныхОрдеров.КОтбору
			Или Статус = Перечисления.СтатусыРасходныхОрдеров.КУказаниюСерий Тогда
			Массив.Добавить(Движения.ТоварыНаСкладах);
		КонецЕсли;
	КонецЕсли;
	
	Если СкладыСервер.ИспользоватьАдресноеХранение(Склад, Помещение, ДатаОтгрузки) Тогда

		Массив.Добавить(Движения.ТоварыКОтбору);

	КонецЕсли;
	
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);

КонецПроцедуры

Процедура ПроверитьОрдерныйСклад(Отказ)
	
	Если НЕ СкладыСервер.ИспользоватьОрдернуюСхемуПриОтгрузке(Склад, Дата) Тогда
		
		Отказ = Истина;
		ТекстСообщения = НСтр("ru='На складе ""%Склад%"" на %Дата% не используется ордерная схема при отгрузке.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения,"%Склад%",Склад);
		ТекстСообщения = СтрЗаменить(ТекстСообщения,"%Дата%",Формат(Дата, "ДФ=dd.MM.yyyy"));
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			Ссылка);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли