﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда
		
		Возврат;
		
	КонецЕсли;
	
	Подразделение = Параметры.Подразделение;
	ДатаПотребности = Параметры.ДатаПотребности;
	НачатьНеРанее = Параметры.НачатьНеРанее;
	Назначение = Параметры.Назначение;
	
	ПодборТоваровКлиентСервер.СформироватьЗаголовокФормыПодбора(Заголовок, Параметры.Заказ);
	ЗаполнитьТаблицуПолуфабрикатов(Параметры);

	СобытияФорм.ПриСозданииНаСервере(ЭтаФорма, Отказ, СтандартнаяОбработка);

КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	Если Модифицированность Тогда
		
		Отказ = Истина;
		
		ПоказатьВопрос(Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект), НСтр("ru = 'Данные были изменены. Перенести изменения в документ?'"), РежимДиалогаВопрос.ДаНетОтмена);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	
	Ответ = РезультатВопроса;
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		
		ПеренестиСтрокиВДокумент();
		
	ИначеЕсли Ответ = КодВозвратаДиалога.Нет Тогда
		
		Модифицированность = Ложь;
		ЭтаФорма.Закрыть();
	
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура ВыбратьСтроки(Команда)

	ОтметитьСтроки(Истина);

КонецПроцедуры

&НаКлиенте
Процедура ИсключитьСтроки(Команда)

	ОтметитьСтроки(Ложь);

КонецПроцедуры

&НаКлиенте
Процедура ПеренестиВДокумент(Команда)

	ПеренестиСтрокиВДокумент();

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
	СобытияФормКлиент.ВыполнитьПереопределяемуюКоманду(ЭтаФорма, Команда);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ТаблицаПолуфабрикаты.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("ТаблицаПолуфабрикаты.ПрисутствуетВДокументе");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЦветТекстаНеактуальногоСписка);

КонецПроцедуры

#Область Прочее

&НаСервере
Процедура ЗаполнитьТаблицуПолуфабрикатов(ПараметрыПродукции)
	
	ПолуфабрикатыКОбеспечению = ПолучитьИзВременногоХранилища(ПараметрыПродукции.АдресПолуфабрикатыКОбеспечению);
	ОбеспеченныеПолуфабрикаты = ПолучитьИзВременногоХранилища(ПараметрыПродукции.АдресОбеспеченныеПолуфабрикаты);
	
	Для каждого ПолуфабрикатКОбеспечению из ПолуфабрикатыКОбеспечению Цикл
		
		НоваяСтрока = ТаблицаПолуфабрикаты.Добавить();
		НоваяСтрока.Номенклатура = ПолуфабрикатКОбеспечению.Номенклатура;
		НоваяСтрока.Характеристика = ПолуфабрикатКОбеспечению.Характеристика;
		НоваяСтрока.Упаковка = ПолуфабрикатКОбеспечению.Упаковка;
		НоваяСтрока.КоличествоУпаковок = ПолуфабрикатКОбеспечению.КоличествоУпаковок;
		НоваяСтрока.Количество = ПолуфабрикатКОбеспечению.Количество;
		НоваяСтрока.Склад = ПолуфабрикатКОбеспечению.Склад;
		НоваяСтрока.КлючСвязиМатериалыИУслуги = ПолуфабрикатКОбеспечению.КлючСвязи;
		НоваяСтрока.КлючСвязиПродукция = ПолуфабрикатКОбеспечению.КлючСвязиПродукция;
		НоваяСтрока.СтрокаВыбрана = Истина;
		НоваяСтрока.РазмещениеВыпуска = ПараметрыПродукции.РазмещениеВыпуска;
		НоваяСтрока.ДатаПотребности = ДатаПотребности;
		НоваяСтрока.НачатьНеРанее = НачатьНеРанее;
		НоваяСтрока.ВНаличии = 0;
		НоваяСтрока.ИндексСтроки = ТаблицаПолуфабрикаты.Индекс(НоваяСтрока);
		НоваяСтрока.Назначение = Назначение;
		
		ОбеспеченныйПолуфабрикат = ОбеспеченныеПолуфабрикаты.Найти(ПолуфабрикатКОбеспечению.КлючСвязи, "КлючСвязиМатериалыИУслуги");
		
		Если ОбеспеченныйПолуфабрикат <> Неопределено Тогда
			
			НоваяСтрока.Спецификация = ОбеспеченныйПолуфабрикат.Спецификация;
			НоваяСтрока.ОсновнаяСпецификация = ОбеспеченныйПолуфабрикат.ОсновнаяСпецификация;
			НоваяСтрока.ВыбранаОсновнаяСпецификация = ОбеспеченныйПолуфабрикат.ВыбранаОсновнаяСпецификация;
			НоваяСтрока.ДатаПотребности = ОбеспеченныйПолуфабрикат.ДатаПотребности;
			НоваяСтрока.НачатьНеРанее = ОбеспеченныйПолуфабрикат.НачатьНеРанее;
			НоваяСтрока.РазмещениеВыпуска = ОбеспеченныйПолуфабрикат.РазмещениеВыпуска;
			НоваяСтрока.Склад = ОбеспеченныйПолуфабрикат.Склад;
			НоваяСтрока.Назначение = ОбеспеченныйПолуфабрикат.Назначение;
			НоваяСтрока.ИзмененияЗапрещены = ОбеспеченныйПолуфабрикат.ИзмененияЗапрещены;
			НоваяСтрока.КодСтроки = ОбеспеченныйПолуфабрикат.КодСтроки;
			НоваяСтрока.СтрокаВыбрана = Ложь;
			НоваяСтрока.ПрисутствуетВДокументе = Истина;
			НоваяСтрока.Запланировано = ОбеспеченныйПолуфабрикат.КоличествоУпаковок;
			
		КонецЕсли;
		
		НоваяСтрока.Запланировать = НоваяСтрока.Количество - НоваяСтрока.ВНаличии - НоваяСтрока.Запланировано;
		
		Если НоваяСтрока.Запланировать > 0 Тогда
			
			НоваяСтрока.СтрокаВыбрана = Истина;
			
		КонецЕсли;
		
		СпецификацияПолуфабриката = УправлениеДаннымиОбИзделияхВызовСервера.СпецификацияИзделия(
												Подразделение,
												НоваяСтрока.Номенклатура,
												НоваяСтрока.Характеристика,
												НоваяСтрока.НачатьНеРанее,
												НоваяСтрока.Спецификация);
												
		Если СпецификацияПолуфабриката <> Неопределено Тогда
			
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СпецификацияПолуфабриката);
			
		КонецЕсли; 
		
	КонецЦикла;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ТаблицаПолуфабрикаты.ИндексСтроки   КАК ИндексСтроки,
	|	ТаблицаПолуфабрикаты.Номенклатура   КАК Номенклатура,
	|	ТаблицаПолуфабрикаты.Характеристика КАК Характеристика,
	|	ТаблицаПолуфабрикаты.Склад          КАК Склад
	|
	|ПОМЕСТИТЬ ВТПолуфабрикаты
	|
	|ИЗ
	|	&ТаблицаПолуфабрикаты КАК ТаблицаПолуфабрикаты
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗРЕШЕННЫЕ
	|	СвободныеОстатки.Номенклатура    КАК Номенклатура,
	|	СвободныеОстатки.Характеристика  КАК Характеристика,
	|	СвободныеОстатки.Склад           КАК Склад,
	|	СвободныеОстатки.ВНаличииОстаток КАК ВНаличии
	|
	|ПОМЕСТИТЬ ВТОстатки
	|
	|ИЗ
	|	РегистрНакопления.СвободныеОстатки.Остатки(
	|			,
	|			(Номенклатура, Характеристика, Склад) В
	|				(ВЫБРАТЬ
	|					ТПолуфабрикаты.Номенклатура   КАК Номенклатура,
	|					ТПолуфабрикаты.Характеристика КАК Характеристика,
	|					ТПолуфабрикаты.Склад          КАК Склад
	|				ИЗ
	|					ВТПолуфабрикаты КАК ТПолуфабрикаты)) КАК СвободныеОстатки
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТПолуфабрикаты.ИндексСтроки    КАК ИндексСтроки,
	|	ЕСТЬNULL(ТОстатки.ВНаличии, 0) КАК ВНаличии
	|ИЗ
	|	ВТПолуфабрикаты КАК ТПолуфабрикаты
	|
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВТОстатки КАК ТОстатки
	|		ПО ТПолуфабрикаты.Номенклатура = ТОстатки.Номенклатура
	|			И ТПолуфабрикаты.Характеристика = ТОстатки.Характеристика
	|			И ТПолуфабрикаты.Склад = ТОстатки.Склад
	|
	|ГДЕ
	|	ЕСТЬNULL(ТОстатки.ВНаличии, 0) > 0");
	
	Запрос.УстановитьПараметр("ТаблицаПолуфабрикаты", ТаблицаПолуфабрикаты.Выгрузить());
	Результат = Запрос.Выполнить();
	
	Если НЕ Результат.Пустой() Тогда
		
		Выборка = Результат.Выбрать();
		
		Пока Выборка.Следующий() Цикл
			
			ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].ВНаличии = Выборка.ВНаличии;
			ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].Запланировать = ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].Количество
																		- ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].ВНаличии
																		- ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].Запланировано;
																		
			Если ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].Запланировать <= 0 Тогда
				
				ТаблицаПолуфабрикаты[Выборка.ИндексСтроки].СтрокаВыбрана = Ложь;
				
			КонецЕсли;
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Функция ПоместитьПолуфабрикатыВХранилище()

	АдресВХранилище = ПоместитьВоВременноеХранилище(Новый Структура("Полуфабрикаты",
			ТаблицаПолуфабрикаты.Выгрузить(Новый Структура("СтрокаВыбрана", Истина))));

	Возврат АдресВХранилище;

КонецФункции

&НаКлиенте
Процедура ПеренестиСтрокиВДокумент()

	// Снятие модифицированности, т.к. перед закрытием признак проверяется.
	Модифицированность = Ложь;

	АдресВХранилище = ПоместитьПолуфабрикатыВХранилище();
	
	ОповеститьОВыборе(Новый Структура("ВыполняемаяОперация, АдресВХранилище",
						"ПланированиеПолуфабрикатов", АдресВХранилище));

КонецПроцедуры

&НаСервере
Процедура ОтметитьСтроки(Значение)

	Для каждого СтрокаТаблицаПолуфабрикаты Из ТаблицаПолуфабрикаты.НайтиСтроки(Новый Структура("СтрокаВыбрана", Не Значение)) Цикл

		СтрокаТаблицаПолуфабрикаты.СтрокаВыбрана = Значение;

	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#КонецОбласти
