﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс
	
// Функция формирует временные таблицы с данными документа.
// Используется для заполнения видов запасов.
//
// Возвращаемое значение:
//	МенеджерВременныхТаблиц - менеджер временных таблиц
//
Функция ВременныеТаблицыДанныхДокумента() Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	&Организация КАК Организация,
	|	&Дата КАК Дата,
	|	&Склад КАК Склад,
	|	Неопределено КАК Партнер,
	|	Неопределено КАК Контрагент,
	|	ЗНАЧЕНИЕ(Справочник.СоглашенияСПоставщиками.ПустаяСсылка) КАК Соглашение,
	|	ЗНАЧЕНИЕ(Справочник.ДоговорыКонтрагентов.ПустаяСсылка) КАК Договор,
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК НалогообложениеНДС,
	|
	|	ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка) КАК Подразделение,
	|	ЗНАЧЕНИЕ(Справочник.Пользователи.ПустаяСсылка) КАК Менеджер,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПорчаТоваров) КАК ХозяйственнаяОперация,
	|	Ложь КАК ЕстьСделкиВТабличнойЧасти
	|	
	|ПОМЕСТИТЬ ТаблицаДанныхДокумента
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки КАК НомерСтроки,
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура,
	|	ТаблицаТоваров.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Характеристика КАК Характеристика,
	|	ТаблицаТоваров.Серия КАК Серия,
	|	ТаблицаТоваров.НоменклатураОприходование КАК НоменклатураОприходование,
	|	ТаблицаТоваров.ХарактеристикаОприходование КАК ХарактеристикаОприходование,
	|	ТаблицаТоваров.ДокументРеализации КАК ДокументРеализации,
	|	ТаблицаТоваров.Количество КАК Количество,
	|	ТаблицаТоваров.Цена КАК Цена,
	|	ТаблицаТоваров.Назначение КАК Назначение
	|	
	|ПОМЕСТИТЬ ВтТаблицаТоваров
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки,
	|	ТаблицаТоваров.Номенклатура,
	|	(ВЫБОР КОГДА ТаблицаТоваров.АналитикаУчетаНоменклатуры <> ЗНАЧЕНИЕ(Справочник.КлючиАналитикиУчетаНоменклатуры.ПустаяСсылка) ТОГДА
	|		ТаблицаТоваров.АналитикаУчетаНоменклатуры
	|	ИНАЧЕ
	|		ЕСТЬNULL(Аналитика.КлючАналитики, ЗНАЧЕНИЕ(Справочник.КлючиАналитикиУчетаНоменклатуры.ПустаяСсылка))
	|	КОНЕЦ) КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Характеристика,
	|	ТаблицаТоваров.Серия,
	|	ТаблицаТоваров.НоменклатураОприходование,
	|	ТаблицаТоваров.ХарактеристикаОприходование,
	|	ТаблицаТоваров.ДокументРеализации,
	|	ТаблицаТоваров.Количество,
	|	ТаблицаТоваров.Цена,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ТаблицаТоваров.Назначение КАК Назначение,
	|	ЗНАЧЕНИЕ(Перечисление.СтавкиНДС.ПустаяСсылка) КАК СтавкаНДС,
	|	0 КАК СуммаСНДС,
	|	0 КАК СуммаНДС,
	|	0 КАК СуммаВознаграждения,
	|	0 КАК СуммаНДСВознаграждения,
	|	ИСТИНА КАК ПодбиратьВидыЗапасов
	|	
	|ПОМЕСТИТЬ ТаблицаТоваров
	|ИЗ
	|	ВтТаблицаТоваров КАК ТаблицаТоваров
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаНоменклатуры КАК Аналитика
	|		ПО Аналитика.Номенклатура = ТаблицаТоваров.Номенклатура
	|		И Аналитика.Характеристика = ТаблицаТоваров.Характеристика
	|		И Аналитика.Серия = ТаблицаТоваров.Серия
	|		И Аналитика.Склад = &Склад
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	ТаблицаВидыЗапасов.НоменклатураОприходование КАК НоменклатураОприходование,
	|	ТаблицаВидыЗапасов.ХарактеристикаОприходование КАК ХарактеристикаОприходование,
	|	ТаблицаВидыЗапасов.ДокументРеализации КАК ДокументРеализации,
	|	ТаблицаВидыЗапасов.ВидЗапасов КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.НомерГТД КАК НомерГТД,
	|	ТаблицаВидыЗапасов.Количество КАК Количество,
	|	ТаблицаВидыЗапасов.Сумма КАК Сумма,
	|	ЗНАЧЕНИЕ(Справочник.Склады.ПустаяСсылка) КАК СкладОтгрузки,
	|	&Склад КАК Склад,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	&ВидыЗапасовУказаныВручную КАК ВидыЗапасовУказаныВручную
	|	
	|ПОМЕСТИТЬ ВтВидыЗапасов
	|ИЗ
	|	&ТаблицаВидыЗапасов КАК ТаблицаВидыЗапасов
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаВидыЗапасов.НомерСтроки КАК НомерСтроки,
	|	ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры КАК АналитикаУчетаНоменклатуры,
	|	Аналитика.Номенклатура КАК Номенклатура,
	|	Аналитика.Характеристика КАК Характеристика,
	|	Аналитика.Серия КАК Серия,
	|	ТаблицаВидыЗапасов.НоменклатураОприходование КАК НоменклатураОприходование,
	|	ТаблицаВидыЗапасов.ХарактеристикаОприходование КАК ХарактеристикаОприходование,
	|	ТаблицаВидыЗапасов.ДокументРеализации КАК ДокументРеализации,
	|	ТаблицаВидыЗапасов.ВидЗапасов КАК ВидЗапасов,
	|	ТаблицаВидыЗапасов.НомерГТД КАК НомерГТД,
	|	ТаблицаВидыЗапасов.Количество КАК Количество,
	|	ТаблицаВидыЗапасов.Сумма КАК Сумма,
	|	ТаблицаВидыЗапасов.СкладОтгрузки КАК СкладОтгрузки,
	|	ТаблицаВидыЗапасов.Склад КАК Склад,
	|	ТаблицаВидыЗапасов.Сделка КАК Сделка,
	|	ТаблицаВидыЗапасов.ВидыЗапасовУказаныВручную КАК ВидыЗапасовУказаныВручную
	|	
	|ПОМЕСТИТЬ ТаблицаВидыЗапасов
	|ИЗ
	|	ВтВидыЗапасов КАК ТаблицаВидыЗапасов
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		РегистрСведений.АналитикаУчетаНоменклатуры КАК Аналитика
	|	ПО ТаблицаВидыЗапасов.АналитикаУчетаНоменклатуры = Аналитика.КлючАналитики
	|
	|ИНДЕКСИРОВАТЬ ПО
	|	АналитикаУчетаНоменклатуры
	|");
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("Дата", Дата);
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("Склад", Склад);
	Запрос.УстановитьПараметр("ВидыЗапасовУказаныВручную", ВидыЗапасовУказаныВручную);
	Запрос.УстановитьПараметр("ТаблицаТоваров", ЗапасыСервер.ТаблицаДополненнаяОбязательнымиКолонками(Товары.Выгрузить()));
	Запрос.УстановитьПараметр("ТаблицаВидыЗапасов", ЗапасыСервер.ТаблицаДополненнаяОбязательнымиКолонками(ВидыЗапасов.Выгрузить()));
	
	Запрос.Выполнить();
	
	Если ВидыЗапасовУказаныВручную Тогда
		ДополнительныеСвойства.Вставить("ИгнорироватьОперативныеОстатки", Истина);
	КонецЕсли;
	
	Возврат МенеджерВременныхТаблиц;
	
КонецФункции

// Процедура формирует временную таблицу товаров с аналитикой обособленного учета.
//
// Параметры:
//	МенеджерВременныхТаблиц - МенеджерВременныхТаблиц - менеджер временных таблиц,
//	который будет содержать созданную таблицу
//
Процедура СформироватьВременнуюТаблицуТоваровИАналитики(МенеджерВременныхТаблиц) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ТаблицаТоваров.АналитикаУчетаНоменклатуры,
	|	ТаблицаТоваров.Номенклатура,
	|	ТаблицаТоваров.Характеристика,
	|	ТаблицаТоваров.Серия,
	|	ТаблицаТоваров.Склад,
	|
	|	ЗНАЧЕНИЕ(Справочник.СтруктураПредприятия.ПустаяСсылка) КАК Подразделение,
	|	ЗНАЧЕНИЕ(Справочник.Пользователи.ПустаяСсылка) КАК Менеджер,
	|	ЗНАЧЕНИЕ(Справочник.СделкиСКлиентами.ПустаяСсылка) КАК Сделка,
	|	ТаблицаТоваров.Назначение КАК Назначение,
	|
	|	ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка) КАК Партнер,
	|	ЗНАЧЕНИЕ(Справочник.СоглашенияСПоставщиками.ПустаяСсылка) КАК Соглашение,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыНалогообложенияНДС.ПустаяСсылка) КАК НалогообложениеНДС,
	|
	|	ТаблицаТоваров.Количество КАК Количество
	|	
	|ПОМЕСТИТЬ ТаблицаТоваровИАналитики
	|ИЗ
	|	ТаблицаТоваров КАК ТаблицаТоваров
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		ТаблицаДанныхДокумента КАК ТаблицаДанныхДокумента
	|	ПО
	|		Истина
	|;
	|");
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.Выполнить();
	
КонецПроцедуры 

// Процедура формирует временную таблицу доступных видов запасов
//
// Параметры:
//	МенеджерВременныхТаблиц - МенеджерВременныхТаблиц - менеджер временных таблиц,
//								который будет содержать созданную таблицу
//
Процедура СформироватьДоступныеВидыЗапасов(МенеджерВременныхТаблиц) Экспорт
	
	ЗапасыСервер.ВидыЗапасовНеОбособленныеИОбособленные(
		Организация,
		Справочники.СделкиСКлиентами.ПустаяСсылка(),
		Ответственный,
		Подразделение,
		МенеджерВременныхТаблиц);
	
КонецПроцедуры 

// Процедура заполняет табличную часть документа по остаткам к оформлению излишков и недостач.
//
Процедура ЗаполнитьТабличнуюЧастьТовары(ДокументОснование = Неопределено) Экспорт

	Запрос = Новый Запрос;
	
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.УстановитьПараметр("Склад", Склад);
	Запрос.УстановитьПараметр("ДокументОснование", ДокументОснование);
	НаОснованииПересчета = ТипЗнч(ДокументОснование) = Тип("ДокументСсылка.ПересчетТоваров");
	Запрос.УстановитьПараметр("НаОснованииПересчета", НаОснованииПересчета);
	НаОснованииОрдера = ТипЗнч(ДокументОснование) = Тип("ДокументСсылка.ОрдерНаОтражениеПорчиТоваров");
	Запрос.УстановитьПараметр("НаОснованииОрдера", НаОснованииОрдера);
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|	ТоварыКОформлению.Характеристика КАК Характеристика,
	|	ТоварыКОформлению.Серия КАК Серия,
	|	ТоварыКОформлению.Назначение КАК Назначение,
	|	СУММА(ТоварыКОформлению.КОформлениюАктовОстаток) КАК Количество
	|ПОМЕСТИТЬ ТаблицаБрака
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|		ТоварыКОформлению.Характеристика КАК Характеристика,
	|		ТоварыКОформлению.Серия КАК Серия,
	|		ТоварыКОформлению.Назначение КАК Назначение,
	|		ТоварыКОформлению.КОформлениюАктовОстаток КАК КОформлениюАктовОстаток
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач.Остатки(, Склад = &Склад) КАК ТоварыКОформлению
	|	ГДЕ
	|		ТоварыКОформлению.КОформлениюАктовОстаток > 0
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура,
	|		ТоварыКОформлению.Характеристика,
	|		ТоварыКОформлению.Серия,
	|		ТоварыКОформлению.Назначение,
	|		ТоварыКОформлению.КОформлениюАктов
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач КАК ТоварыКОформлению
	|	ГДЕ
	|		ТоварыКОформлению.Регистратор = &Ссылка
	|		И ТоварыКОформлению.Активность = ИСТИНА
	|		И ТоварыКОформлению.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)) КАК ТоварыКОформлению
	|
	|СГРУППИРОВАТЬ ПО
	|	ТоварыКОформлению.Номенклатура,
	|	ТоварыКОформлению.Характеристика,
	|	ТоварыКОформлению.Назначение,
	|	ТоварыКОформлению.Серия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаБрака.Номенклатура,
	|	ТаблицаБрака.Характеристика,
	|	ТаблицаБрака.Серия,
	|	ТаблицаБрака.Назначение,
	|	ТаблицаБрака.Количество
	|ПОМЕСТИТЬ ТаблицаБракаСФильтром
	|ИЗ
	|	ТаблицаБрака КАК ТаблицаБрака
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|		ПО (&НаОснованииПересчета)
	|			И ТаблицаБрака.Номенклатура = ПересчетТоваровТовары.Номенклатура
	|			И ТаблицаБрака.Характеристика = ПересчетТоваровТовары.Характеристика
	|			И ТаблицаБрака.Назначение = ПересчетТоваровТовары.Назначение
	|			И (ПересчетТоваровТовары.СтатусУказанияСерий <> 14
	|				ИЛИ ТаблицаБрака.Серия = ПересчетТоваровТовары.Серия)
	|			И (ПересчетТоваровТовары.Количество <> ПересчетТоваровТовары.КоличествоФакт)
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОрдерНаОтражениеПорчиТоваров.Товары КАК ОрдерНаОтражениеПорчиТоваровТовары
	|		ПО (&НаОснованииОрдера)
	|			И ТаблицаБрака.Номенклатура = ОрдерНаОтражениеПорчиТоваровТовары.НоменклатураОприходование
	|			И ТаблицаБрака.Характеристика = ОрдерНаОтражениеПорчиТоваровТовары.ХарактеристикаОприходование
	|			И ТаблицаБрака.Назначение = ОрдерНаОтражениеПорчиТоваровТовары.Назначение
	|			И (ОрдерНаОтражениеПорчиТоваровТовары.СтатусУказанияСерий <> 14
	|				ИЛИ ТаблицаБрака.Серия = ОрдерНаОтражениеПорчиТоваровТовары.Серия)
	|ГДЕ
	|	(НЕ &НаОснованииПересчета
	|			ИЛИ &НаОснованииПересчета
	|				И НЕ ПересчетТоваровТовары.Номенклатура ЕСТЬ NULL 
	|				И ПересчетТоваровТовары.Ссылка = &ДокументОснование)
	|	И (НЕ &НаОснованииОрдера
	|			ИЛИ &НаОснованииОрдера
	|				И НЕ ОрдерНаОтражениеПорчиТоваровТовары.НоменклатураОприходование ЕСТЬ NULL 
	|				И ОрдерНаОтражениеПорчиТоваровТовары.Ссылка = &ДокументОснование)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|	ТоварыКОформлению.Характеристика КАК Характеристика,
	|	ТоварыКОформлению.Серия КАК Серия,
	|	ТоварыКОформлению.Назначение КАК Назначение,
	|	СУММА(ТоварыКОформлению.КОформлениюАктовОстаток) КАК Количество
	|ПОМЕСТИТЬ ТаблицаИсходногоТовара
	|ИЗ
	|	(ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура КАК Номенклатура,
	|		ТоварыКОформлению.Характеристика КАК Характеристика,
	|		ТоварыКОформлению.Назначение КАК Назначение,
	|		-ТоварыКОформлению.КОформлениюАктовОстаток КАК КОформлениюАктовОстаток,
	|		ТоварыКОформлению.Серия КАК Серия
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач.Остатки(, Склад = &Склад) КАК ТоварыКОформлению
	|	ГДЕ
	|		ТоварыКОформлению.КОформлениюАктовОстаток < 0
	|	
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТоварыКОформлению.Номенклатура,
	|		ТоварыКОформлению.Характеристика,
	|		ТоварыКОформлению.Назначение,
	|		ТоварыКОформлению.КОформлениюАктов,
	|		ТоварыКОформлению.Серия
	|	ИЗ
	|		РегистрНакопления.ТоварыКОформлениюИзлишковНедостач КАК ТоварыКОформлению
	|	ГДЕ
	|		ТоварыКОформлению.Регистратор = &Ссылка
	|		И ТоварыКОформлению.Активность = ИСТИНА
	|		И ТоварыКОформлению.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)) КАК ТоварыКОформлению
	|
	|СГРУППИРОВАТЬ ПО
	|	ТоварыКОформлению.Номенклатура,
	|	ТоварыКОформлению.Характеристика,
	|	ТоварыКОформлению.Назначение,
	|	ТоварыКОформлению.Серия
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ТаблицаИсходногоТовара.Номенклатура,
	|	ТаблицаИсходногоТовара.Характеристика,
	|	ТаблицаИсходногоТовара.Серия,
	|	ТаблицаИсходногоТовара.Назначение,
	|	ТаблицаИсходногоТовара.Количество
	|ПОМЕСТИТЬ ТаблицаИсходногоТовараСФильтром
	|ИЗ
	|	ТаблицаИсходногоТовара КАК ТаблицаИсходногоТовара
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ПересчетТоваров.Товары КАК ПересчетТоваровТовары
	|		ПО (&НаОснованииПересчета)
	|			И ТаблицаИсходногоТовара.Номенклатура = ПересчетТоваровТовары.Номенклатура
	|			И ТаблицаИсходногоТовара.Характеристика = ПересчетТоваровТовары.Характеристика
	|			И ТаблицаИсходногоТовара.Назначение = ПересчетТоваровТовары.Назначение
	|			И (ПересчетТоваровТовары.СтатусУказанияСерий <> 14
	|				ИЛИ ТаблицаИсходногоТовара.Серия = ПересчетТоваровТовары.Серия)
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ОрдерНаОтражениеПорчиТоваров.Товары КАК ОрдерНаОтражениеПорчиТоваровТовары
	|		ПО (&НаОснованииОрдера)
	|			И ТаблицаИсходногоТовара.Номенклатура = ОрдерНаОтражениеПорчиТоваровТовары.Номенклатура
	|			И ТаблицаИсходногоТовара.Характеристика = ОрдерНаОтражениеПорчиТоваровТовары.Характеристика
	|			И ТаблицаИсходногоТовара.Назначение = ОрдерНаОтражениеПорчиТоваровТовары.Назначение
	|			И (ОрдерНаОтражениеПорчиТоваровТовары.СтатусУказанияСерий <> 14
	|				ИЛИ ТаблицаИсходногоТовара.Серия = ОрдерНаОтражениеПорчиТоваровТовары.Серия)
	|ГДЕ
	|	(НЕ &НаОснованииПересчета
	|			ИЛИ &НаОснованииПересчета
	|				И НЕ ПересчетТоваровТовары.Номенклатура ЕСТЬ NULL 
	|				И ПересчетТоваровТовары.Ссылка = &ДокументОснование)
	|	И (НЕ &НаОснованииОрдера
	|			ИЛИ &НаОснованииОрдера
	|				И НЕ ОрдерНаОтражениеПорчиТоваровТовары.Номенклатура ЕСТЬ NULL 
	|				И ОрдерНаОтражениеПорчиТоваровТовары.Ссылка = &ДокументОснование)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаБракаСФильтром.Номенклатура КАК НоменклатураОприходование,
	|	ТаблицаБракаСФильтром.Характеристика КАК ХарактеристикаОприходование,
	|	ТаблицаИсходногоТовараСФильтром.Номенклатура КАК Номенклатура,
	|	ТаблицаИсходногоТовараСФильтром.Характеристика КАК Характеристика,
	|	ТаблицаИсходногоТовараСФильтром.Назначение КАК Назначение,
	|	ВЫБОР
	|		КОГДА ТаблицаБракаСФильтром.Количество < ТаблицаИсходногоТовараСФильтром.Количество
	|			ТОГДА ТаблицаБракаСФильтром.Количество
	|		ИНАЧЕ ТаблицаИсходногоТовараСФильтром.Количество
	|	КОНЕЦ КАК Количество,
	|	ТаблицаИсходногоТовараСФильтром.Серия КАК Серия
	|ИЗ
	|	РегистрСведений.ТоварыДругогоКачества КАК ТоварыДругогоКачества
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаБракаСФильтром КАК ТаблицаБракаСФильтром
	|		ПО ТоварыДругогоКачества.НоменклатураБрак = ТаблицаБракаСФильтром.Номенклатура
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ ТаблицаИсходногоТовараСФильтром КАК ТаблицаИсходногоТовараСФильтром
	|		ПО ТоварыДругогоКачества.Номенклатура = ТаблицаИсходногоТовараСФильтром.Номенклатура
	|ГДЕ
	|	ЕСТЬNULL(ВЫРАЗИТЬ(ТаблицаИсходногоТовараСФильтром.Серия КАК Справочник.СерииНоменклатуры).Наименование, """") = ЕСТЬNULL(ВЫРАЗИТЬ(ТаблицаБракаСФильтром.Серия КАК Справочник.СерииНоменклатуры).Наименование, """")
	|	И ЕСТЬNULL(ВЫРАЗИТЬ(ТаблицаИсходногоТовараСФильтром.Характеристика КАК Справочник.ХарактеристикиНоменклатуры).Наименование, """") = ЕСТЬNULL(ВЫРАЗИТЬ(ТаблицаБракаСФильтром.Характеристика КАК Справочник.ХарактеристикиНоменклатуры).Наименование, """")
	|	И (ВЫРАЗИТЬ(ТаблицаБракаСФильтром.Назначение КАК Справочник.Назначения)) = ЗНАЧЕНИЕ(Справочник.Назначения.ПустаяСсылка)";
	
	УстановитьПривилегированныйРежим(Истина);
	Результат = Запрос.Выполнить();
	УстановитьПривилегированныйРежим(Ложь);
	Если Не Результат.Пустой() Тогда
		
		Товары.Загрузить(Результат.Выгрузить());
		
		Если ЗначениеЗаполнено(ВидЦены) И Товары.Количество() > 0 И Не ПриходоватьТоварыПоСебестоимостиСписания Тогда
			
			ПродажиСервер.ЗаполнитьЦены(
				Товары, // Табличная часть
				 , // Выделенные строки (заполнять во всех строках)
				Новый Структура( // Параметры заполнения
					"Дата, Валюта, ВидЦены, КолонкиПоЗначению, ДругиеИменаКолонок",
					Дата,
					Константы.ВалютаУправленческогоУчета.Получить(),
					ВидЦены,
					Новый Структура("Упаковка", Справочники.УпаковкиНоменклатуры.ПустаяСсылка()),
					Новый Структура("НоменклатураОприходование, ХарактеристикаОприходование", "Номенклатура", "Характеристика")));
		КонецЕсли;
		
	ИначеЕсли ЗначениеЗаполнено(ДокументОснование) Тогда  

		ТекстСообщения = НСтр("ru = 'В документе ""%ДокументОснование%"" отсутствуют товары, по которым необходимо оформить порчу.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДокументОснование%", ДокументОснование);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЭтотОбъект);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ДокументОснование = Неопределено;
	ЗаполнитьТабличнуюЧастьТовары = Истина;
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект,ДанныеЗаполнения);
		ЗаполнитьТабличнуюЧастьТовары = НЕ ДанныеЗаполнения.Свойство("НеЗаполнятьТаблинуюЧастьТовары");
	ИначеЕсли Документы.ТипВсеСсылки().СодержитТип(ТипЗнч(ДанныеЗаполнения))
		И ДанныеЗаполнения <> Неопределено Тогда 	
	
		Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ПересчетТоваров") Тогда 
			СтруктураРезультат = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДанныеЗаполнения, "Статус, Склад");
			Если СтруктураРезультат.Статус <> Перечисления.СтатусыПересчетовТоваров.Выполнено Тогда 
				ТекстСообщения = НСтр("ru='Документ ""%ДокументПересчет%"" находится в статусе ""%СтатусПересчета%"". Ввод документа ""%ДокументАкт%"" на основании разрешен только в статусе ""%СтатусВыполнено%"".'");
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДокументПересчет%", ДанныеЗаполнения);
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%ДокументАкт%", Метаданные.Документы.ПорчаТоваров.Синоним);
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%СтатусВыполнено%", Перечисления.СтатусыПересчетовТоваров.Выполнено);
				ТекстСообщения = СтрЗаменить(ТекстСообщения, "%СтатусПересчета%", СтруктураРезультат.Статус);
				ВызватьИсключение ТекстСообщения;
			КонецЕсли;
			Склад = СтруктураРезультат.Склад;
			Если ЗначениеЗаполнено(Склад) И НЕ СкладыСервер.ИспользоватьОрдернуюСхемуПриОтраженииИзлишковНедостач(Склад, Дата) Тогда 
				ПоРезультатамИнвентаризации = Истина;
			КонецЕсли;

		КонецЕсли;
				
		Если Не ЗначениеЗаполнено(Склад) Тогда
			Склад = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ДанныеЗаполнения, "Склад");	
		КонецЕсли;
		
		ДокументОснование = ДанныеЗаполнения;
		
	КонецЕсли;
	
	ИнициализироватьДокумент(ДанныеЗаполнения);

	ОтветственныеЛицаСервер.ОтветственныеЛицаДокументаОбработкаЗаполнения(Ссылка, ДанныеЗаполнения, СтандартнаяОбработка);
	
	Если ЗаполнитьТабличнуюЧастьТовары Тогда 
		ЗаполнитьТабличнуюЧастьТовары(ДокументОснование);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	ПроведениеСервер.УстановитьРежимПроведения(ЭтотОбъект, РежимЗаписи, РежимПроведения);

	ДополнительныеСвойства.Вставить("ЭтоНовый",    ЭтоНовый());
	ДополнительныеСвойства.Вставить("РежимЗаписи", РежимЗаписи);
	
	НоменклатураСервер.ОчиститьНеиспользуемыеСерии(ЭтотОбъект,
														НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ПорчаТоваров));
														
	Если ПриходоватьТоварыПоСебестоимостиСписания Тогда
		Для Каждого СтрокаТаблицы Из Товары Цикл
			Если СтрокаТаблицы.Цена <> 0 Тогда
				СтрокаТаблицы.Цена = 0;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		МестаУчета = Новый Структура("Произвольный", Склад);
		РегистрыСведений.АналитикаУчетаНоменклатуры.ЗаполнитьВКоллекции(Товары, МестаУчета);
		ЗаполнитьВидыЗапасов(Отказ);
		ЗаполнитьКлючиАналитикиУчетаПартийДокумента();
		ВзаиморасчетыСервер.ЗаполнитьИдентификаторыСтрокВТабличнойЧасти(ВидыЗапасов);
	ИначеЕсли РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
		Если Не ВидыЗапасовУказаныВручную Тогда
			ВидыЗапасов.Очистить();
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если ПриходоватьТоварыПоСебестоимостиСписания Тогда
		МассивНепроверяемыхРеквизитов.Добавить("СтатьяРасходов");
		МассивНепроверяемыхРеквизитов.Добавить("Товары.Цена");
	КонецЕсли;

	ПараметрыПроверки = НоменклатураСервер.ПараметрыПроверкиЗаполненияХарактеристик();
	ПараметрыПроверки.СуффиксДопРеквизита = "Оприходование";
	НоменклатураСервер.ПроверитьЗаполнениеХарактеристик(ЭтотОбъект,МассивНепроверяемыхРеквизитов,Отказ,ПараметрыПроверки);
	
	НоменклатураСервер.ПроверитьЗаполнениеСерий(ЭтотОбъект,
												НоменклатураСервер.ПараметрыУказанияСерий(ЭтотОбъект, Документы.ПорчаТоваров),
												Отказ,
												МассивНепроверяемыхРеквизитов);
	
	Для Каждого СтрТабл Из Товары Цикл
		АдресОшибки = НСтр("ru=' в строке %НомерСтроки% списка ""Товары""'");
		АдресОшибки = СтрЗаменить(АдресОшибки, "%НомерСтроки%", СтрТабл.НомерСтроки);
		Если ЗначениеЗаполнено(СтрТабл.Номенклатура)
			И СтрТабл.Номенклатура = СтрТабл.НоменклатураОприходование Тогда
			ТекстОшибки = НСтр("ru='Нельзя выбирать товар исходного качества. Выберите товар другого качества'");
			Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", СтрТабл.НомерСтроки, "НоменклатураОприходование");
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстОшибки + АдресОшибки,ЭтотОбъект,Поле,,Отказ);
		КонецЕсли;
	КонецЦикла;
	
	НоменклатураСервер.ПроверитьВидНоменклатурыОприходования(ЭтотОбъект,Отказ);
	ПланыВидовХарактеристик.СтатьиРасходов.ПроверитьЗаполнениеАналитик(
		ЭтотОбъект,, МассивНепроверяемыхРеквизитов, Отказ);
		
	Если ПолучитьФункциональнуюОпцию("ФормироватьВидыЗапасовПоПодразделениямМенеджерам") Тогда
		ПроверяемыеРеквизиты.Добавить("Подразделение");
	КонецЕсли;
		
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
	
	ПроверитьСтавкиНДС(Отказ);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства, РежимПроведения);
	Документы.ПорчаТоваров.ИнициализироватьДанныеДокумента(Ссылка, ДополнительныеСвойства);
	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	ЗапасыСервер.ОтразитьТоварыНаСкладах(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыОрганизацийКПередаче(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьСвободныеОстатки(ДополнительныеСвойства, Движения, Отказ);
	СкладыСервер.ОтразитьДвиженияСерийТоваров(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыКОформлениюИзлишковНедостач(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьТоварыКОформлениюОтчетовКомитента(ДополнительныеСвойства, Движения, Отказ);
	ЗапасыСервер.ОтразитьОбеспечениеЗаказов(ДополнительныеСвойства, Движения, Отказ);	
		
	ДоходыИРасходыСервер.ОтразитьСебестоимостьТоваров(ДополнительныеСвойства, Движения, Отказ);
	
	ВзаиморасчетыСервер.ОтразитьСуммыДокументаВВалютеРегл(ДополнительныеСвойства, Движения, Отказ);
	
	ПартионныйУчетСервер.ОтразитьПартииТоваровОрганизаций(ДополнительныеСвойства, Движения, Отказ);
	
	// Движения по оборотным регистрам управленческого учета
	УправленческийУчетПроведениеСервер.ОтразитьДвиженияНоменклатураНоменклатура(ДополнительныеСвойства, Движения, Отказ);
	
	СформироватьСписокРегистровДляКонтроля();
	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);
	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);
	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)

	ПроведениеСервер.ИнициализироватьДополнительныеСвойстваДляПроведения(Ссылка, ДополнительныеСвойства);

	ПроведениеСервер.ПодготовитьНаборыЗаписейКРегистрацииДвижений(ЭтотОбъект);

	СформироватьСписокРегистровДляКонтроля();

	ПроведениеСервер.ЗаписатьНаборыЗаписей(ЭтотОбъект);

	ПроведениеСервер.ВыполнитьКонтрольРезультатовПроведения(ЭтотОбъект, Отказ);

	ПроведениеСервер.ОчиститьДополнительныеСвойстваДляПроведения(ДополнительныеСвойства);

КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	ИнициализироватьДокумент();
	
	ОтветственныеЛицаСервер.ОтветственныеЛицаДокументаПриКопировании(Ссылка, ОбъектКопирования);

КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ИнициализацияИЗаполнение

Процедура ИнициализироватьДокумент(ДанныеЗаполнения = Неопределено)

	Ответственный = Пользователи.ТекущийПользователь();

	Организация   = ЗначениеНастроекПовтИсп.ПолучитьОрганизациюПоУмолчанию(Организация);
	Склад         = ЗначениеНастроекПовтИсп.ПолучитьСкладПоУмолчанию(Склад);
	Подразделение = ЗначениеНастроекПовтИсп.ПодразделениеПользователя(Ответственный, Подразделение);
	
	Если Не ЗначениеЗаполнено(ИсточникИнформацииОЦенахДляПечати) Тогда 
		ИсточникИнформацииОЦенахДляПечати = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад, "ИсточникИнформацииОЦенахДляПечати");
		Если НЕ ЗначениеЗаполнено(ИсточникИнформацииОЦенахДляПечати) И НЕ ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоВидовЦен") Тогда
			ИсточникИнформацииОЦенахДляПечати = Перечисления.ИсточникиИнформацииОЦенахДляПечати.ПоСебестоимости;
		КонецЕсли;
		Если ИсточникИнформацииОЦенахДляПечати = Перечисления.ИсточникиИнформацииОЦенахДляПечати.ПоСебестоимости Тогда
			ВидЦены = Неопределено;
		Иначе
			ВидЦены = Справочники.Склады.УчетныйВидЦены(Склад);
		КонецЕсли;
	КонецЕсли;
	
	ЗаполнитьРеквизитыУчетаДоходовРасходов();
	
КонецПроцедуры

Процедура ЗаполнитьРеквизитыУчетаДоходовРасходов()
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПорчаТоваров.ПриходоватьТоварыПоСебестоимостиСписания,
	|	ПорчаТоваров.СтатьяРасходов,
	|	ПорчаТоваров.АналитикаРасходов
	|ИЗ
	|	Документ.ПорчаТоваров КАК ПорчаТоваров
	|ГДЕ
	|	ПорчаТоваров.Ответственный = &Ответственный
	|	И ПорчаТоваров.Проведен
	|
	|УПОРЯДОЧИТЬ ПО
	|	ПорчаТоваров.Дата УБЫВ";
	Запрос.УстановитьПараметр("Ответственный", Ответственный);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Если Не Выборка.ПриходоватьТоварыПоСебестоимостиСписания Тогда
			ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
		Иначе
			ПриходоватьТоварыПоСебестоимостиСписания = Ложь;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ВидыЗапасов

Функция ИзмененыРеквизитыДокумента(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ВЫБОР КОГДА ДанныеДокумента.Организация <> СохраненныеДанные.Организация ТОГДА
	|		Истина
	|	КОГДА ДанныеДокумента.Дата <> СохраненныеДанные.Дата ТОГДА
	|		Истина
	|	КОГДА ДанныеДокумента.Склад <> СохраненныеДанные.Склад ТОГДА
	|		Истина
	|	ИНАЧЕ
	|		Ложь
	|	КОНЕЦ КАК РеквизитыИзменены
	|ИЗ
	|	ТаблицаДанныхДокумента КАК ДанныеДокумента
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Документ.ПорчаТоваров КАК СохраненныеДанные
	|	ПО
	|		СохраненныеДанные.Ссылка = &Ссылка
	|");
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		РеквизитыИзменены = Выборка.РеквизитыИзменены;
	Иначе
		РеквизитыИзменены = Ложь;
	КонецЕсли;
	
	Возврат РеквизитыИзменены;
	
КонецФункции

Функция ИзмененаТЧТовары(МенеджерВременныхТаблиц)
	
	Запрос = Новый Запрос("
	|////////////////////////////////////////////////////////////////////////////////
	|// Проверим соответствие табличных частей Товары и ВидыЗапасов
	|ВЫБРАТЬ
	|	ТаблицаТоваров.Номенклатура КАК Номенклатура,
	|	ТаблицаТоваров.Характеристика КАК Характеристика,
	|	ТаблицаТоваров.НоменклатураОприходование,
	|	ТаблицаТоваров.ХарактеристикаОприходование
	|ИЗ (
	|	ВЫБРАТЬ
	|		ТаблицаТоваров.Номенклатура КАК Номенклатура,
	|		ТаблицаТоваров.Характеристика КАК Характеристика,
	|		ТаблицаТоваров.НоменклатураОприходование КАК НоменклатураОприходование,
	|		ТаблицаТоваров.ХарактеристикаОприходование КАК ХарактеристикаОприходование,
	|		ТаблицаТоваров.Количество КАК Количество,
	|		ВЫРАЗИТЬ(ТаблицаТоваров.Количество * ТаблицаТоваров.Цена КАК ЧИСЛО(15,2)) КАК Сумма
	|	ИЗ
	|		ТаблицаТоваров КАК ТаблицаТоваров
	|
	|	ОБЪЕДИНИТЬ ВСЕ
	|	
	|	ВЫБРАТЬ
	|		ТаблицаВидыЗапасов.Номенклатура КАК Номенклатура,
	|		ТаблицаВидыЗапасов.Характеристика КАК Характеристика,
	|		ТаблицаВидыЗапасов.НоменклатураОприходование КАК НоменклатураОприходование,
	|		ТаблицаВидыЗапасов.ХарактеристикаОприходование КАК ХарактеристикаОприходование,
	|		-ТаблицаВидыЗапасов.Количество КАК Количество,
	|		-ТаблицаВидыЗапасов.Сумма КАК Сумма
	|	ИЗ
	|		ТаблицаВидыЗапасов КАК ТаблицаВидыЗапасов
	|
	|	) КАК ТаблицаТоваров
	|
	|СГРУППИРОВАТЬ ПО
	|	ТаблицаТоваров.Номенклатура,
	|	ТаблицаТоваров.Характеристика,
	|	ТаблицаТоваров.НоменклатураОприходование,
	|	ТаблицаТоваров.ХарактеристикаОприходование
	|
	|ИМЕЮЩИЕ
	|	СУММА(ТаблицаТоваров.Количество) <> 0
	|	ИЛИ СУММА(ТаблицаТоваров.Сумма) <> 0
	|;
	|////////////////////////////////////////////////////////////////////////////////
	|");
	Запрос.УстановитьПараметр("ПриходПоСебестоимости", ПриходоватьТоварыПоСебестоимостиСписания);
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;

	РезультатЗапрос = Запрос.Выполнить();
	
	Возврат (Не РезультатЗапрос.Пустой());
	
КонецФункции 

Процедура СообщитьОбОшибкахЗаполненияВидовЗапасов(ТаблицаОшибок)
	
	Для Каждого СтрокаТаблицы Из ТаблицаОшибок Цикл
		
		ТекстСообщения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			НСтр("ru = 'Номенклатура: %1 
			|Списание превышает остаток товара организации %2 на складе %3 на %4 %5'"),
			НоменклатураКлиентСервер.ПредставлениеНоменклатуры(СтрокаТаблицы.Номенклатура, СтрокаТаблицы.Характеристика, СтрокаТаблицы.Серия),
			Организация,
			Склад,
			СтрокаТаблицы.Количество,
			СтрокаТаблицы.ЕдиницаИзмерения);
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения,
			Ссылка);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьНоменклатуруОприходованиеВидовЗапасов() Экспорт
	
	СтруктураПоиска = Новый Структура("АналитикаУчетаНоменклатуры");
	Для Каждого СтрокаТоваров Из Товары Цикл

		КоличествоТоваров = СтрокаТоваров.Количество;
		
		ЗаполнитьЗначенияСвойств(СтруктураПоиска, СтрокаТоваров);
		Для Каждого СтрокаЗапасов Из ВидыЗапасов.НайтиСтроки(СтруктураПоиска) Цикл

			Если СтрокаЗапасов.Количество = 0 Тогда
				Продолжить;
			КонецЕсли;
			
			Количество = Мин(КоличествоТоваров, СтрокаЗапасов.Количество);

			НоваяСтрока = ВидыЗапасов.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрока, СтрокаЗапасов);
			
			НоваяСтрока.НоменклатураОприходование	= СтрокаТоваров.НоменклатураОприходование;
			НоваяСтрока.ХарактеристикаОприходование	= СтрокаТоваров.ХарактеристикаОприходование;
			НоваяСтрока.Серия						= СтрокаТоваров.Серия;
			НоваяСтрока.Сумма						= ?(Не ПриходоватьТоварыПоСебестоимостиСписания, Количество * СтрокаТоваров.Цена, 0);
			НоваяСтрока.Количество					= Количество;

			СтрокаЗапасов.Количество = СтрокаЗапасов.Количество - НоваяСтрока.Количество;
			
			КоличествоТоваров = КоличествоТоваров - НоваяСтрока.Количество;

			Если КоличествоТоваров = 0 Тогда
				Прервать;
			КонецЕсли;

		КонецЦикла;
		
	КонецЦикла;
	
	МассивУдаляемыхСтрок = ВидыЗапасов.НайтиСтроки(Новый Структура("Количество", 0));
	Для Каждого СтрокаТаблицы Из МассивУдаляемыхСтрок Цикл
		ВидыЗапасов.Удалить(СтрокаТаблицы);
	КонецЦикла;
	
КонецПроцедуры 

Процедура ЗаполнитьВидыЗапасов(Отказ)
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерВременныхТаблиц = ВременныеТаблицыДанныхДокумента();
	ПерезаполнитьВидыЗапасов = ДополнительныеСвойства.Свойство("ПерезаполнитьВидыЗапасов");
	Если Не Проведен
	 ИЛИ ПерезаполнитьВидыЗапасов
	 ИЛИ ИзмененыРеквизитыДокумента(МенеджерВременныхТаблиц)
	 ИЛИ ИзмененаТЧТовары(МенеджерВременныхТаблиц) Тогда
	 
		СформироватьДоступныеВидыЗапасов(МенеджерВременныхТаблиц);
		ЗапасыСервер.УстановитьБлокировкуОстатковТоваровОрганизаций(МенеджерВременныхТаблиц);
		ЗапасыСервер.ТаблицаОстатковТоваровОрганизаций(Ссылка, Организация, Дата, ДополнительныеСвойства, МенеджерВременныхТаблиц);
		ТаблицаОшибок = ЗапасыСервер.ТаблицаОшибокЗаполненияВидовЗапасов();
		
		ЗапасыСервер.ЗаполнитьВидыЗапасовДокумента(
			МенеджерВременныхТаблиц,
			ДополнительныеСвойства,
			ВидыЗапасов,
			ТаблицаОшибок,
			Отказ);
		ВидыЗапасов.Свернуть("АналитикаУчетаНоменклатуры, ВидЗапасов, НомерГТД", "Количество");
		ЗаполнитьНоменклатуруОприходованиеВидовЗапасов();
		СообщитьОбОшибкахЗаполненияВидовЗапасов(ТаблицаОшибок);
		
	КонецЕсли;
	
КонецПроцедуры 

#КонецОбласти

#Область Прочее

Процедура ПроверитьСтавкиНДС(Отказ)
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ТаблицаТовары.Номенклатура КАК НоменклатураСписание,
	|	ТаблицаТовары.НоменклатураОприходование,
	|	ТаблицаТовары.НомерСтроки
	|ПОМЕСТИТЬ ТаблицаТовары
	|ИЗ
	|	&ТаблицаТовары КАК ТаблицаТовары
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТовары.НомерСтроки КАК НомерСтроки
	|ИЗ
	|	ТаблицаТовары КАК ТаблицаТовары
	|ГДЕ
	|	ВЫРАЗИТЬ(ТаблицаТовары.НоменклатураСписание КАК Справочник.Номенклатура).СтавкаНДС <> ВЫРАЗИТЬ(ТаблицаТовары.НоменклатураОприходование КАК Справочник.Номенклатура).СтавкаНДС
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки";
	
	Запрос.УстановитьПараметр("ТаблицаТовары", Товары.Выгрузить());
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		ТекстСообщения = НСтр("ru='Невозможно отразить порчу, т.к. у некачественной и исходной номенклатуры не равны ставки НДС'");

		Поле = ОбщегоНазначенияКлиентСервер.ПутьКТабличнойЧасти("Товары", Выборка.НомерСтроки, "Номенклатура");
			
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,Поле,"Объект",Отказ);
		
	КонецЦикла;
	
КонецПроцедуры

Процедура СформироватьСписокРегистровДляКонтроля()

	Массив = Новый Массив;
	// Приходы в регистр (сторно расхода из регистра) контролируем при перепроведении и отмене проведения
	Если Не ДополнительныеСвойства.ЭтоНовый Тогда
		Массив.Добавить(Движения.ТоварыОрганизаций);
	КонецЕсли;
	ДополнительныеСвойства.ДляПроведения.Вставить("РегистрыДляКонтроля", Массив);

КонецПроцедуры

Процедура ЗаполнитьКлючиАналитикиУчетаПартийДокумента() Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки 					КАК НомерСтроки,
	|	ТаблицаТоваров.НоменклатураОприходование 	КАК Номенклатура,
	|	ТаблицаТоваров.АналитикаУчетаПартий	 		КАК АналитикаУчетаПартий
	|ПОМЕСТИТЬ ТаблицаТоваровДокумента
	|ИЗ
	|	&ТаблицаТоваров КАК ТаблицаТоваров
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ТаблицаТоваров.НомерСтроки 		КАК НомерСтроки,
	|	ТаблицаТоваров.Номенклатура 	КАК Номенклатура,
	| 	НЕОПРЕДЕЛЕНО 					КАК СтавкаНДС,
	|	НЕОПРЕДЕЛЕНО					КАК Поставщик,
	|	НЕОПРЕДЕЛЕНО					КАК Контрагент,
	|	&НалогообложениеНДС 			КАК НалогообложениеНДС,
	|	ТаблицаТоваров.АналитикаУчетаПартий
	|ПОМЕСТИТЬ ИсходнаяТаблицаТоваров
	|ИЗ
	|	ТаблицаТоваровДокумента КАК ТаблицаТоваров
	|ГДЕ
	|	ТаблицаТоваров.АналитикаУчетаПартий = ЗНАЧЕНИЕ(Справочник.КлючиАналитикиУчетаПартий.ПустаяСсылка)
	|");
	                	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("ТаблицаТоваров"		, ВидыЗапасов.Выгрузить(, 
														"НомерСтроки, НоменклатураОприходование, АналитикаУчетаПартий"));
	Запрос.УстановитьПараметр("НалогообложениеНДС"	, Справочники.Организации.НалогообложениеНДС(Организация,Склад, Дата));
				
	Запрос.Выполнить();
	
	ПартионныйУчетСервер.ЗаполнитьАналитикуУчетаПартийВТабличнойЧастиТовары(МенеджерВременныхТаблиц, ВидыЗапасов);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
