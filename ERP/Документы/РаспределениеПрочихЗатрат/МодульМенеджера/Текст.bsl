﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

// Функция возвращает текст запроса для отражения документа в регламентированном учете.
//
// Возвращаемое значение:
//	Строка - Текст запроса
//                                                                               
Функция ТекстОтраженияВРеглУчете() Экспорт
	
	ТекстРаспределениеРасходов = "
	|ВЫБРАТЬ //// Распределение расходов (Дт 20 :: Кт 25, 26)
	|
	|	Операция.Дата КАК Период,
	|	Операция.Организация КАК Организация,
	|	НЕОПРЕДЕЛЕНО КАК ИдентификаторСтроки,
	|
	|	Строки.Сумма КАК Сумма,
	|
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.Расходы) КАК ВидСчетаДт,
	|	ЗНАЧЕНИЕ(ПланВидовХарактеристик.СтатьиРасходов.ПустаяСсылка) КАК АналитикаУчетаДт,
	|	Строки.Подразделение КАК МестоУчетаДт,
	|
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК ВалютаДт,
	|	Строки.Подразделение КАК ПодразделениеДт,
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка) КАК СчетДт,
	|
	|	Строки.СтатьяРасходов КАК СубконтоДт1,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыЗатратРегл.Прочее) КАК СубконтоДт2,
	|	Строки.ГруппаПродукции КАК СубконтоДт3,
	|
	|	0 КАК ВалютнаяСуммаДт,
	|	0 КАК КоличествоДт,
	|	Строки.СуммаНУ КАК СуммаНУДт,
	|	Строки.СуммаПР КАК СуммаПРДт,
	|	Строки.СуммаВР КАК СуммаВРДт,
	|
	|	ЗНАЧЕНИЕ(Перечисление.ВидыСчетовРеглУчета.Расходы) КАК ВидСчетаКт,
	|	Операция.СтатьяРасходов КАК АналитикаУчетаКт,
	|	Операция.Подразделение КАК МестоУчетаКт,
	|
	|	ЗНАЧЕНИЕ(Справочник.Валюты.ПустаяСсылка) КАК ВалютаКт,
	|	Операция.Подразделение КАК ПодразделениеКт,
	|	ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ПустаяСсылка) КАК СчетКт,
	|
	|	Операция.СтатьяРасходов КАК СубконтоКт1,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыЗатратРегл.Прочее) КАК СубконтоКт2,
	|	НЕОПРЕДЕЛЕНО КАК СубконтоКт3,
	|
	|	0 КАК ВалютнаяСуммаКт,
	|	0 КАК КоличествоКт,
	|	Строки.СуммаНУ КАК СуммаНУКт,
	|	Строки.СуммаПР КАК СуммаПРКт,
	|	Строки.СуммаВР КАК СуммаВРКт
	|ИЗ
	|	Документ.РаспределениеПрочихЗатрат КАК Операция
	|
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ (
	|		ВЫБРАТЬ
	|			Движения.Подразделение КАК Подразделение,
	|			Движения.СтатьяРасходов КАК СтатьяРасходов,
	|			Движения.ГруппаПродукции КАК ГруппаПродукции,
	|			СУММА(Движения.СтоимостьРегл) КАК Сумма,
	|			СУММА(Движения.СтоимостьРегл - Движения.ПостояннаяРазница - Движения.ВременнаяРазница) КАК СуммаНУ,
	|			СУММА(Движения.ПостояннаяРазница) КАК СуммаПР,
	|			СУММА(Движения.ВременнаяРазница) КАК СуммаВР
	|		ИЗ
	|			РегистрНакопления.ПрочиеРасходыНезавершенногоПроизводства КАК Движения
	|		ГДЕ
	|			Движения.ДокументДвижения = &Ссылка
	|
	|		СГРУППИРОВАТЬ ПО
	|			Движения.Подразделение,
	|			Движения.СтатьяРасходов,
	|			Движения.ГруппаПродукции
	|		) КАК Строки
	|	ПО
	|		ИСТИНА
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПорядокОтраженияРасходов КАК Счета
	|		ПО Счета.Организация = Операция.Организация
	|		И Счета.Подразделение = Операция.Подразделение
	|		И Счета.СтатьяРасходов = Операция.СтатьяРасходов
	|	ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ПорядокОтраженияРасходов КАК КорСчета
	|		ПО КорСчета.Организация = Операция.Организация
	|		И КорСчета.Подразделение = Строки.Подразделение
	|		И КорСчета.СтатьяРасходов = ЗНАЧЕНИЕ(ПланВидовХарактеристик.СтатьиРасходов.ПустаяСсылка)	
	|ГДЕ
	|	Операция.Ссылка = &Ссылка
	|	И (Строки.Сумма <> 0 ИЛИ Строки.СуммаНУ <> 0 ИЛИ Строки.СуммаВР <> 0 ИЛИ Строки.СуммаПР <> 0)
	|	И (ЕСТЬNULL(Счета.СчетУчета, ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОбщепроизводственныеРасходы))
	|		<> ЕСТЬNULL(КорСчета.СчетУчета, ЗНАЧЕНИЕ(ПланСчетов.Хозрасчетный.ОсновноеПроизводство))
	|	   ИЛИ
	|	   Операция.Подразделение <> Строки.Подразделение)
	|";
	Возврат ТекстРаспределениеРасходов;
	
КонецФункции

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УП 2.0.10.46
// Заполняет СпособРаспределенияПоГруппамПродукции в реквизитах документа
//
Процедура ЗаполнитьСпособРаспределенияПоГруппамПродукции(Параметры) Экспорт
	
	ТекстСобытия = НСтр("ru = 'Заполнение способа распределения по группам продукции'", ОбщегоНазначенияКлиентСервер.КодОсновногоЯзыка());
	ЗаписьЖурналаРегистрации(ТекстСобытия, УровеньЖурналаРегистрации.Информация, , ,
				НСтр("ru = 'Запуск задания заполнения способа распределения по группам продукции в документах: Распределение расходов на себестоимость продукции'"));
				
	Запрос = Новый Запрос;
	ТекстЗапроса = "
	|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 1000
	|	Таб.Ссылка КАК Ссылка,
	|	Таб.Дата КАК Дата
	|ИЗ
	|	Документ.РаспределениеПрочихЗатрат КАК Таб
	|ГДЕ
	|	Таб.СпособРаспределенияПоГруппамПродукции = ЗНАЧЕНИЕ(Перечисление.СпособыРаспределенияСтатейРасходов.ПустаяСсылка)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Таб.Дата УБЫВ
	|";
	
	Запрос.Текст = ТекстЗапроса;
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		НачатьТранзакцию();
		Попытка
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("Документ.РаспределениеПрочихЗатрат");
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			Блокировка.Заблокировать();

			ДокОбъект = Выборка.Ссылка.ПолучитьОбъект();
			ДокОбъект.СпособРаспределенияПоГруппамПродукции = Перечисления.СпособыРаспределенияСтатейРасходов.НаВсеВидыВыпускаемойПродукции;
			
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(ДокОбъект); 
			
			ЗафиксироватьТранзакцию();
		Исключение
			ОтменитьТранзакцию();
			ТекстСообщения = НСтр("ru = 'Не удалось обработать: %РаспределениеПрочихЗатрат% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%РаспределениеПрочихЗатрат%", Выборка.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ЗаписьЖурналаРегистрации(ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(), УровеньЖурналаРегистрации.Предупреждение,
									Метаданные.Документы.РаспределениеПрочихЗатрат, Выборка.Ссылка, ТекстСообщения);
		КонецПопытки;
	КонецЦикла;
	
	ЗаписьЖурналаРегистрации(ТекстСобытия, УровеньЖурналаРегистрации.Информация, , ,
				НСтр("ru = 'Задание способа распределения по группам продукции в документах: Распределение расходов на себестоимость продукции завершено'"));
				
	Параметры.ОбработкаЗавершена = (Выборка.Количество() = 0);
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли


