﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда


Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;
	МассивНепроверяемыхРеквизитов.Добавить("СкладскаяГруппаУпаковок");
	
	Для Каждого СтрТабл Из ЭтотОбъект Цикл
		Если Не ЗначениеЗаполнено(СтрТабл.СкладскаяГруппаУпаковок) Тогда
			ТипНоменклатуры = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(СтрТабл.СкладскаяГруппаНоменклатуры, "ТипНоменклатуры");
			
			Если ТипНоменклатуры = Перечисления.ТипыНоменклатуры.Товар Тогда
				ТекстСообщения = НСтр("ru = 'Поле ""Складская группа упаковок"" не заполнено'");
				
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения,,"СкладскаяГруппаУпаковок","Запись",Отказ);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;

	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты,МассивНепроверяемыхРеквизитов);
	
КонецПроцедуры

#КонецЕсли