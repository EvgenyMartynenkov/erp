﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область ОбновлениеИнформационнойБазы

// Обработчик обновления УТ 11.1.9.22
// Производит заполнение раздела учета в движениях регистра
Процедура ЗаполнитьРазделыУчета(Параметры) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	ПараметрыОбработчиков = Константы.ПараметрыОбработчиковОбновления.Получить().Получить();
	Если ТипЗнч(ПараметрыОбработчиков) <> Тип("Соответствие") Тогда
		ПараметрыОбработчиков = Новый Соответствие;
	КонецЕсли;
	Если ПараметрыОбработчиков.Получить("ГраницаИсправленияДвиженийСтоимости") = Неопределено Тогда
		ПараметрыОбработчиков.Вставить("ГраницаИсправленияДвиженийСтоимости", Новый МоментВремени('39991231'));
	КонецЕсли;
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ РАЗЛИЧНЫЕ ПЕРВЫЕ 200
	|	Стоимости.Регистратор КАК Ссылка,
	|	Расчет.МоментВремени КАК МоментВремени
	|ИЗ
	|	РегистрСведений.СтоимостьТоваров КАК Стоимости
	|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ Документ.РасчетСебестоимостиТоваров КАК Расчет
	|	ПО Стоимости.Регистратор = Расчет.Ссылка
	|ГДЕ
	|	Стоимости.РазделУчета = ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ПустаяСсылка)
	|	И Расчет.МоментВремени < &МоментВремени
	|УПОРЯДОЧИТЬ ПО
	|	Расчет.МоментВремени УБЫВ
	|");
		
	Запрос.УстановитьПараметр("МоментВремени", ПараметрыОбработчиков["ГраницаИсправленияДвиженийСтоимости"]);
	
	МоментВремени = Неопределено;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		Попытка
			НачатьТранзакцию();
			Блокировка = Новый БлокировкаДанных;
			ЭлементБлокировки = Блокировка.Добавить("Документ.РасчетСебестоимостиТоваров");
			ЭлементБлокировки.УстановитьЗначение("Ссылка", Выборка.Ссылка);
			Блокировка.Заблокировать();
			
			МоментВремени = Выборка.МоментВремени;
			
			Запрос.Текст = "
			|ВЫБРАТЬ
			|	Стоимости.Период,
			|	Стоимости.Регистратор,
			|	Стоимости.НомерСтроки,
			|	Стоимости.Активность,
			|	Стоимости.АналитикаУчетаНоменклатуры,
			|	Стоимости.ВидЗапасов,
			|	Стоимости.Организация,
			|	ВЫБОР
			|		КОГДА ТИПЗНАЧЕНИЯ(Аналитика.Склад) = ТИП(Справочник.Склады)
			|			ТОГДА ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыНаСкладах)
			|		КОГДА ТИПЗНАЧЕНИЯ(Аналитика.Склад) = ТИП(Справочник.СтруктураПредприятия)
			|			ТОГДА ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ПроизводственныеЗатраты)
			|		КОГДА ТИПЗНАЧЕНИЯ(Аналитика.Склад) = ТИП(Справочник.Партнеры)
			|			ТОГДА ЗНАЧЕНИЕ(Перечисление.РазделыУчетаСебестоимостиТоваров.ТоварыПереданныеНаКомиссию)
			|	КОНЕЦ КАК РазделУчета,
			|	Стоимости.Стоимость,
			|	Стоимости.СтоимостьДопРасходы,
			|	Стоимости.СтоимостьБезНДС,
			|	Стоимости.СтоимостьДопРасходыБезНДС,
			|	Стоимости.СтоимостьРегл,
			|	Стоимости.ПостояннаяРазница,
			|	Стоимости.ВременнаяРазница
			|ИЗ
			|	РегистрСведений.СтоимостьТоваров КАК Стоимости
			|	ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаНоменклатуры КАК Аналитика
			|	ПО Стоимости.АналитикаУчетаНоменклатуры = Аналитика.КлючАналитики
			|ГДЕ
			|	Стоимости.Регистратор = &Ссылка
			|";
			
			Запрос.УстановитьПараметр("Ссылка", Выборка.Ссылка);
			
			Набор = РегистрыСведений.СтоимостьТоваров.СоздатьНаборЗаписей();
			Набор.Отбор.Регистратор.Установить(Выборка.Ссылка);
			Набор.Загрузить(Запрос.Выполнить().Выгрузить());
			
			ОбновлениеИнформационнойБазы.ЗаписатьДанные(Набор);
			
			ЗаписьЖурналаРегистрации(
				НСтр("ru = 'Изменение движений по регистрам'"),
				УровеньЖурналаРегистрации.Информация,
				Выборка.Ссылка.Метаданные(),
				Выборка.Ссылка,
				НСтр("ru = 'Заполнен раздел учета в регистре ""Стоимость товаров""'"),
				РежимТранзакцииЗаписиЖурналаРегистрации.Транзакционная);
				
				ЗафиксироватьТранзакцию();
				
		Исключение
			ОтменитьТранзакцию();
			ТекстСообщения = НСтр("ru = 'Не удалось обработать: %Документ% по причине: %Причина%'");
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Документ%", Выборка.Ссылка);
			ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Причина%", ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
			ЗаписьЖурналаРегистрации(ОбновлениеИнформационнойБазы.СобытиеЖурналаРегистрации(), 
									УровеньЖурналаРегистрации.Предупреждение,
									Выборка.Ссылка.Метаданные(),
									Выборка.Ссылка, ТекстСообщения);
		КонецПопытки;
		
	КонецЦикла;
	
	ОтложенноеОбновлениеИБ.ЗаписатьИнформациюОбОтложенномОбновлении(ПараметрыОбработчиков, МоментВремени, "ГраницаИсправленияДвиженийСтоимости");
	Параметры.ОбработкаЗавершена = (Выборка.Количество() = 0);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли