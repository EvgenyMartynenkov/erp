﻿#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура КомандаЗаполнитьЗначениямиПоУмолчанию(Команда)
	
	ЗаполнитьНаСервере();
	Элементы.Список.Обновить();
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервереБезКонтекста
Процедура ЗаполнитьНаСервере()
	
	Параметры = Новый Структура("ОбработкаЗавершена", Ложь);
	РегистрыСведений.ГраницыКонтролируемостиСделок.ЗаполнитьГраницыКонтролируемости(Параметры);
	
КонецПроцедуры

#КонецОбласти
