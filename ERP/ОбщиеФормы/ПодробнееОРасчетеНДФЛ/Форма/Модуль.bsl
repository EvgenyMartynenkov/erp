﻿&НаКлиенте
Перем СтарыеЗначенияКонтролируемыхПолей;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	РаботаВБюджетномУчреждении = ПолучитьФункциональнуюОпцию("РаботаВБюджетномУчреждении");
	
	Элементы.ГруппаКнопокПросмотр.Видимость			= ТолькоПросмотр;
	Элементы.ГруппаКнопокРедактирование.Видимость	= Не ТолькоПросмотр;
	Если ТолькоПросмотр Тогда
		Элементы.ФормаЗакрыть.КнопкаПоУмолчанию	= Истина;
	Иначе
		Элементы.ФормаОК.КнопкаПоУмолчанию		= Истина;
	КонецЕсли;
	
	ИмяДокумента = Параметры.ИмяДокумента;
	ОписаниеДокумента = Новый ФиксированнаяСтруктура(Параметры.ОписаниеДокумента);
	
	СписокСотрудников = Параметры.УчитываемыеСотрудники;
	Если ТипЗнч(СписокСотрудников) = Тип("Строка")
		И Не ПустаяСтрока(СписокСотрудников) Тогда
		
		СписокСотрудников = ПолучитьИзВременногоХранилища(СписокСотрудников);
		
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(СписокСотрудников) Тогда
		
		Если ТипЗнч(СписокСотрудников) <> Тип("Массив") Тогда
			СписокСотрудников = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(СписокСотрудников);
		КонецЕсли;
		
		Запрос = Новый Запрос;
		Запрос.УстановитьПараметр("СписокСотрудников", СписокСотрудников);
		Запрос.Текст =
			"ВЫБРАТЬ РАЗРЕШЕННЫЕ РАЗЛИЧНЫЕ
			|	Сотрудники.ФизическоеЛицо
			|ИЗ
			|	Справочник.Сотрудники КАК Сотрудники
			|ГДЕ
			|	Сотрудники.Ссылка В(&СписокСотрудников)
			|
			|ОБЪЕДИНИТЬ ВСЕ
			|
			|ВЫБРАТЬ РАЗЛИЧНЫЕ
			|	ФизическиеЛица.Ссылка
			|ИЗ
			|	Справочник.ФизическиеЛица КАК ФизическиеЛица
			|ГДЕ
			|	ФизическиеЛица.Ссылка В(&СписокСотрудников)";
			
		УчитываемыеСотрудники = Новый ФиксированныйМассив(Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("ФизическоеЛицо"));
		
	КонецЕсли; 
	
	ЗаполнитьФормуНаСервере(Параметры.СведенияОбНДФЛ, Истина, Параметры.Объект);
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	УчетНДФЛКлиент.НДФЛПриАктивизацииСтроки(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ИзмененоРаспределениеИсточниковФинансирования" Тогда
		ЗарплатаКадрыРасширенныйКлиент.ОбработкаОповещенияИзмененияРаспределенияИсточниковФинансирования(ЭтаФорма, ИмяСобытия, Параметр, Источник);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура Подключаемый_ВычетыЛичныеПриИзменении(Элемент)
	
	ПриИзмененииДанныхСтрокиЕДФЛ();
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовТаблицыФормыНДФЛ

&НаКлиенте
Процедура НДФЛПриАктивизацииСтроки(Элемент)
	
	УчетНДФЛКлиент.НДФЛПриАктивизацииСтроки(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Найти(Поле.Имя, "КомандаРедактированияРаспределения") = 0 Тогда
		Возврат;
	КонецЕсли;
	
	ЗарплатаКадрыРасширенныйКлиент.ОткрытьФормуРедактированияРезультатовРаспределенияПоИсточникамФинансирования(ЭтаФорма, ОписаниеТаблицыНДФЛ(), ВыбраннаяСтрока, ЭтаФорма.Объект[ОписаниеДокумента.МесяцНачисленияИмя]);
	СтандартнаяОбработка = Ложь;
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПередНачаломИзменения(Элемент, Отказ)
	
	УчетНДФЛКлиентРасширенный.НДФЛПередНачаломИзменения(ЭтаФорма, Элементы.НДФЛ.ТекущиеДанные, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	Если НоваяСтрока И НЕ Копирование Тогда
		ЗаполнитьНовуюСтроку(Элементы.НДФЛ.ТекущиеДанные);
	КонецЕсли;
	
	РасчетЗарплатыРасширенныйКлиент.СтрокаРасчетаПриНачалеРедактирования(ЭтаФорма, "НДФЛ", Элементы.НДФЛ.ТекущиеДанные, НоваяСтрока, Копирование);
	УчетНДФЛКлиент.НДФЛПриНачалеРедактирования(ЭтаФорма, Элементы.НДФЛ.ТекущиеДанные, НоваяСтрока, Копирование);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если ОтменаРедактирования Тогда
		Возврат;
	КонецЕсли;
	
	РасчетЗарплатыРасширенныйКлиент.СтрокаРасчетаПриОкончанииРедактирования(ЭтаФорма, ОписаниеТаблицыНДФЛ());
	// Механизм закладки примененные вычеты.
	УчетНДФЛКлиентРасширенный.НДФЛУстановитьДоступностьЭлементовЛичныхВычетов(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПередУдалением(Элемент, Отказ)
	// Механизм закладки примененные вычеты.
	УчетНДФЛКлиент.НДФЛПередУдалением(ЭтаФорма, Элементы.НДФЛ.ТекущиеДанные, Отказ);
КонецПроцедуры

&НаКлиенте
Процедура НДФЛПослеУдаления(Элемент)
	// Механизм закладки примененные вычеты.
	УчетНДФЛКлиентРасширенный.УдалитьПримененныеВычеты(ЭтаФорма);
КонецПроцедуры

&НаКлиенте
Процедура НДФЛМесяцНалоговогоПериодаПриИзменении(Элемент)
	
	ЗарплатаКадрыКлиент.ВводМесяцаПриИзменении(Элементы.НДФЛ.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛМесяцНалоговогоПериодаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаНачалоВыбора(ЭтаФорма, Элементы.НДФЛ.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой");
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛМесяцНалоговогоПериодаРегулирование(Элемент, Направление, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаРегулирование(Элементы.НДФЛ.ТекущиеДанные, "МесяцНалоговогоПериода", "МесяцНалоговогоПериодаСтрокой", Направление, Модифицированность);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛМесяцНалоговогоПериодаАвтоПодбор(Элемент, Текст, ДанныеВыбора, Параметры, Ожидание, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаАвтоПодборТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура НДФЛМесяцНалоговогоПериодаОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	ЗарплатаКадрыКлиент.ВводМесяцаОкончаниеВводаТекста(Текст, ДанныеВыбора, СтандартнаяОбработка);
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовТаблицыФормыПримененныеВычетыКДоходам

&НаКлиенте
Процедура Подключаемый_ВычетыКДоходамПередНачаломИзменения(Элемент, Отказ, Копирование, Родитель, Группа)
	
	УчетНДФЛКлиентРасширенный.ПримененныеВычетыКДоходам(ЭтаФорма, Элемент, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВычетыКДоходамПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если ОтменаРедактирования Тогда
		Возврат;
	КонецЕсли;
	
	ВычетыПриОкончанииРедактирования(Элемент.ТекущиеДанные, ОтменаРедактирования, "ФизическоеЛицо,Подразделение");
	ОбновитьПредставлениеВычетовКДоходамСтрокиНДФЛ();
	Модифицированность = Истина;
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиСобытийЭлементовТаблицыФормыПримененныеВычетыИИмущественныеНаДетейИИмущественные

&НаКлиенте
Процедура Подключаемый_ВычетыНаДетейИИмущественныеПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	УчетНДФЛКлиентРасширенный.МожноИзменятьПримененныеВычеты(ЭтаФорма, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВычетыНаДетейИИмущественныеПередНачаломИзменения(Элемент, Отказ)
	
	УчетНДФЛКлиентРасширенный.ПримененныеВычетыНаДетейИИмущественныеПередНачаломИзменения(ЭтаФорма, Элемент, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВычетыНаДетейИИмущественныеПередУдалением(Элемент, Отказ)
	
	УчетНДФЛКлиентРасширенный.МожноИзменятьПримененныеВычеты(ЭтаФорма, Отказ);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВычетыНаДетейИИмущественныеПриНачалеРедактирования(Элемент, НоваяСтрока, Копирование)
	
	УчетНДФЛКлиент.ВычетыПриНачалеРедактирования(Элемент.ТекущиеДанные, НоваяСтрока, Элементы.НДФЛ.ТекущиеДанные);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ВычетыНаДетейИИмущественныеПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	Если ОтменаРедактирования Тогда
		Возврат;
	КонецЕсли;
	
	ВычетыПриОкончанииРедактирования(Элемент.ТекущиеДанные, ОтменаРедактирования, "ИдентификаторСтрокиНДФЛ");
	ОбновитьПредставлениеВычетовНаДетейИИмущественныхСтрокиНДФЛ();
	Модифицированность = Истина;
	
КонецПроцедуры

#КонецОбласти


#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОК(Команда)
	
	Если Модифицированность Тогда
		
		Оповестить(
			"ИзмененыРезультатыРасчетаНДФЛ",
			ПоместитьИзмененныеДанныеВоВременноеХранилище(),
			ЭтаФорма);
			
	КонецЕсли;
	
	Закрыть();
	
КонецПроцедуры

&НаКлиенте
Процедура РегистрНалоговогоУчетаПоНДФЛ(Команда)
	
	Если Элементы.НДФЛ.ТекущиеДанные <> Неопределено
		И ЗначениеЗаполнено(Элементы.НДФЛ.ТекущиеДанные.ФизическоеЛицо) Тогда
		
		ФизическоеЛицо = Элементы.НДФЛ.ТекущиеДанные.ФизическоеЛицо; 
	
	Иначе
		ФизическоеЛицо = УчитываемыеСотрудники[0];
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ФизическоеЛицо) Тогда
		
		РегистрНалоговогоУчетаПоНДФЛ = НДФЛПодробнееНаСервере(ФизическоеЛицо);
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати("Отчет.РегистрНалоговогоУчетаПоНДФЛ", "РегистрНалоговогоУчетаПоНДФЛ",
			РегистрНалоговогоУчетаПоНДФЛ, ЭтаФорма);
			
	КонецЕсли; 
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_НДФЛОтменитьИсправление(Команда)
	РасчетЗарплатыРасширенныйКлиент.ОтменитьИсправление(ЭтаФорма, ОписаниеТаблицыНДФЛ());
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_НДФЛОтменитьВсеИсправления(Команда)
	РасчетЗарплатыРасширенныйКлиент.ОтменитьВсеИсправления(ЭтаФорма, ОписаниеТаблицыНДФЛ());
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаКлиентеНаСервереБезКонтекста
Функция ОписаниеТаблицыНДФЛ()
	
	Описание = РасчетЗарплатыРасширенныйКлиентСервер.ОписаниеТаблицыРасчета();
	Описание.ИмяТаблицы = "НДФЛ";
	Описание.ПутьКДанным = "НДФЛ";
	Описание.ИмяПоляДляВставкиПоказателей = "Налог";
	Описание.ИмяПоляРезультат = "Налог";
	Описание.ИмяРеквизитаПериод = "МесяцНалоговогоПериода";
	Описание.НомерТаблицы = 3;
	Описание.СодержитПолеВидРасчета = Ложь;
	Описание.СодержитПолеСотрудник = Истина;
	Описание.ИмяРеквизитаСотрудник = "ФизическоеЛицо";
	Описание.ПроверяемыеРеквизиты = "ФизическоеЛицо,МесяцНалоговогоПериода";
	Описание.ПутьКДаннымРаспределениеРезультатов = "Объект.РаспределениеРезультатовУдержаний";
	Описание.ИмяРеквизитаИдентификаторСтроки = "ИдентификаторСтрокиНДФЛ";
	Описание.ИмяПоляДляВставкиРаспределенияРезультатов = "НДФЛМесяцНалоговогоПериода";
	
	Описание.ОтменятьВсеИсправления	= Истина;
	
	Возврат Описание;
	
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Функция ОписаниеПанелиВычеты()
	
	ОписаниеПанелиВычеты = УчетНДФЛКлиентСервер.ОписаниеПанелиВычеты();
	
	ОписаниеТабличнойЧастиНДФЛ = УчетНДФЛКлиентСервер.ОписаниеТабличнойЧастиНДФЛ();
	ОписаниеТабличнойЧастиНДФЛ.ИспользуетсяФиксРасчет = Истина;
	ОписаниеТабличнойЧастиНДФЛ.ПутьКДаннымНДФЛ = "НДФЛ";
	
	ОписаниеПанелиВычеты.ТабличнаяЧастьНДФЛ = ОписаниеТабличнойЧастиНДФЛ;
	
	НастраиваемыеПанели = Новый Соответствие;
	НастраиваемыеПанели.Вставить("ВычетыЛичные", 		Истина);
	НастраиваемыеПанели.Вставить("ВычетыНаДетейИИмущественные", 		"Объект.ПримененныеВычетыНаДетейИИмущественные");
	
	ОписаниеПанелиВычеты.НастраиваемыеПанели = НастраиваемыеПанели;
	
	Возврат ОписаниеПанелиВычеты;
	
КонецФункции

&НаКлиенте
Функция ОписаниеПанелиВычетыНаКлиенте() Экспорт
	
	Возврат ОписаниеПанелиВычеты();
	
КонецФункции

&НаСервере
Функция ОписаниеПанелиВычетыНаСервере() Экспорт
	
	Возврат ОписаниеПанелиВычеты();
	
КонецФункции

&НаКлиенте
Процедура ВычетыПриОкончанииРедактирования(ТекущиеДанные, ОтменаРедактирования, КлючиПоиска)
	
	Если Элементы.НДФЛ.ТекущиеДанные <> Неопределено И НЕ Элементы.НДФЛ.ТекущиеДанные.ФиксРасчет Тогда
		Если УчетНДФЛКлиентРасширенный.ВычетыИзменены(ЭтаФорма, ТекущиеДанные, ОтменаРедактирования) Тогда
			
			СтруктураПоиска = Новый Структура(КлючиПоиска);
			ЗаполнитьЗначенияСвойств(СтруктураПоиска, ТекущиеДанные);
			
			УстановитьФиксРасчетСтрокНДФЛ(СтруктураПоиска);
			
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьПредставлениеВычетовНаДетейИИмущественныхСтрокиНДФЛ()
	
	УчетНДФЛФормы.ОбновитьПредставлениеВычетовНаДетейИИмущественныхСтрокиНДФЛ(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьПредставлениеВычетовКДоходамСтрокиНДФЛ()
	
	УчетНДФЛФормы.ОбновитьПредставлениеВычетовКДоходамСтрокиНДФЛ(ЭтаФорма);
	
КонецПроцедуры

&НаСервере
Процедура УстановитьФиксРасчетСтрокНДФЛ(СтруктураПоиска)
	
	УчетНДФЛФормыРасширенный.УстановитьФиксРасчетСтрокНДФЛ(ЭтаФорма, СтруктураПоиска);
	
КонецПроцедуры

&НаСервере
Функция НДФЛПодробнееНаСервере(ФизическоеЛицо)
	
	ЭтаФорма["Объект"].НДФЛ.Загрузить(НДФЛ.Выгрузить());
	ДокументОбъект = РеквизитФормыВЗначение("Объект");
	
	НДФЛПодробнее = Новый Массив;
	НДФЛПодробнее.Добавить(ДокументОбъект.Ссылка);
	НДФЛПодробнее.Добавить(УчетНДФЛФормы.РегистрНалоговогоУчетаПоНДФЛ(ДокументОбъект, Модифицированность, ФизическоеЛицо, КонецМесяца(ЭтаФорма.Объект[ОписаниеДокумента.МесяцНачисленияИмя])));
	
	Возврат НДФЛПодробнее;
	
КонецФункции

&НаСервере
Функция ПоместитьИзмененныеДанныеВоВременноеХранилище()
	
	Возврат ПоместитьВоВременноеХранилище(Новый Структура("НДФЛ, ПримененныеВычетыНаДетейИИмущественные", НДФЛ, ЭтаФорма["Объект"].ПримененныеВычетыНаДетейИИмущественные), Новый УникальныйИдентификатор);
	
КонецФункции

&НаСервере
Функция ПолучитьКонтролируемыеПоля() Экспорт
	
	Возврат Новый Структура("НДФЛ", УчетНДФЛРасширенный.КонтролируемыеПоляДляФиксацииРезультатов());
	
КонецФункции

&НаКлиенте
Функция ПолучитьСтарыеЗначенияКонтролируемыхПолей() Экспорт
	Возврат СтарыеЗначенияКонтролируемыхПолей;
КонецФункции

&НаСервере
Функция ОписанияТаблицДляРаспределенияРезультата()

	ОписанияТаблиц = Новый Массив;
	ОписанияТаблиц.Добавить(ОписаниеТаблицыНДФЛ());
	
	Возврат ОписанияТаблиц;

КонецФункции

&НаКлиенте
Процедура ПриИзмененииДанныхСтрокиЕДФЛ()

	УчетНДФЛКлиент.НДФЛПриОкончанииРедактирования(ЭтаФорма);
	
	РасчетЗарплатыРасширенныйКлиент.СтрокаРасчетаПриОкончанииРедактирования(ЭтаФорма, ОписаниеТаблицыНДФЛ(), , Ложь);
	УчетНДФЛКлиентРасширенный.НДФЛУстановитьДоступностьЭлементовЛичныхВычетов(ЭтаФорма);
	
КонецПроцедуры

&НаКлиенте
Процедура ПерезаполнитьНачисленияСотрудника(Сотрудники, СохранятьИсправления = Истина) Экспорт
	
	ВладелецФормы.ПерезаполнитьНачисленияСотрудника(Сотрудники, СохранятьИсправления);
	ЗарплатаКадрыКлиент.ПодключитьОбработчикОжиданияОбработкиСобытия(ЭтаФорма, "ПерезаполнитьФормуПоОбъекту");
	
КонецПроцедуры

&НаКлиенте
Процедура ПерезаполнитьФормуПоОбъекту()
	
	ЗаполнитьФормуНаСервере(ВладелецФормы.СведенияОбНДФЛ(), Ложь);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьФормуНаСервере(Знач СведенияОбНДФЛ, Знач ПервичноеЗаполнение, Знач ОткрываемыйОбъект = Неопределено)
	
	ТабличныеЧастиНДФЛ = ПолучитьИзВременногоХранилища(СведенияОбНДФЛ);
	
	Если ПервичноеЗаполнение Тогда
		
		РеквизитОбъект = Новый РеквизитФормы("Объект", Новый ОписаниеТипов("ДокументОбъект." + ИмяДокумента));
		РеквизитОбъект.СохраняемыеДанные = Истина;
		
		МассивРеквизитов = Новый Массив;
		МассивРеквизитов.Добавить(РеквизитОбъект);
		МассивРеквизитов.Добавить(Новый РеквизитФормы("МесяцНалоговогоПериодаСтрокой", Новый ОписаниеТипов("Строка"), "Объект.НДФЛ"));
		ИзменитьРеквизиты(МассивРеквизитов);
		
	КонецЕсли; 
	
	ОбъектФормы = ЭтаФорма["Объект"];
	
	Если ПервичноеЗаполнение Тогда
		ДокОбъект = ДанныеФормыВЗначение(ОткрываемыйОбъект, Тип("ДокументОбъект." + ИмяДокумента));
		ЗначениеВДанныеФормы(ДокОбъект, ОбъектФормы);
	Иначе
		ДокОбъект = РеквизитФормыВЗначение("Объект");
	КонецЕсли;
	
	// Заполнение табличных частей.
	Для каждого ОписаниеТабличнойЧасти Из ТабличныеЧастиНДФЛ Цикл
		ОбъектФормы[ОписаниеТабличнойЧасти.Ключ].Загрузить(ОписаниеТабличнойЧасти.Значение);
	КонецЦикла;
	
	Если ТолькоПросмотр И ОбъектФормы.НДФЛ.Количество() = 0 ИЛИ
		УчитываемыеСотрудники = Неопределено ИЛИ УчитываемыеСотрудники.Количество() = 0 Тогда
		ВызватьИсключение  НСтр("ru='Нет сведений о расчете НДФЛ'");
	КонецЕсли; 
	
	ЕдинственныйСотрудник = УчитываемыеСотрудники.Количество() < 2;
	
	Если ПервичноеЗаполнение Тогда
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"НДФЛФизическоеЛицо",
			"ПараметрыВыбора",
			Новый ФиксированныйМассив(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Новый ПараметрВыбора("Отбор.Ссылка", УчитываемыеСотрудники)))
		);
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"НДФЛФизическоеЛицо",
			"Видимость",
			Не ЕдинственныйСотрудник
		);
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"НДФЛПодразделение",
			"ПараметрыВыбора",
			Новый ФиксированныйМассив(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Новый ПараметрВыбора("Отбор.Владелец", ОбъектФормы.Организация)))
		);
		
		Элементы.НДФЛ.ТекущаяСтрока			= 0;
		
		Если ТолькоПросмотр Тогда
			Элементы.НДФЛ.ПоложениеКоманднойПанели	= ПоложениеКоманднойПанелиЭлементаФормы.Нет;
			Элементы.НДФЛ.ИзменятьСоставСтрок		= Ложь;
		КонецЕсли;
			
	КонецЕсли; 
	
	НДФЛ.Загрузить(ОбъектФормы.НДФЛ.Выгрузить());
	
	УчетНДФЛФормы.ДополнитьФормуПанельюВычетов(ЭтаФорма);
	УчетНДФЛФормы.ЗаполнитьВторичныеДанныеТабличныхЧастей(ЭтаФорма);
	
	ОписаниеТаблицы = ОписаниеТаблицыНДФЛ();
	ОписаниеТаблицы.ОтменятьВсеИсправления = Не ЕдинственныйСотрудник;
	
	РасчетЗарплатыРасширенныйФормы.ДокументыВыполненияНачисленийДополнитьФорму(ЭтаФорма, ОписаниеТаблицы, "");
	РасчетЗарплатыРасширенныйФормы.ДокументыНачисленийДополнитьФормуРезультатыРаспределения(ЭтаФорма, ОписанияТаблицДляРаспределенияРезультата());
	Если ПолучитьФункциональнуюОпцию("ИспользоватьСтатьиФинансированияЗарплата") Тогда
		Для каждого СтрокаНДФЛ Из НДФЛ Цикл
			СтрокаНДФЛ.РезультатРаспределения = ОткрываемыйОбъект.НДФЛ[СтрокаНДФЛ.НомерСтроки-1].РезультатРаспределения;
			СтрокаНДФЛ.КомандаРедактированияРаспределения = ОткрываемыйОбъект.НДФЛ[СтрокаНДФЛ.НомерСтроки-1].КомандаРедактированияРаспределения;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ЗаполнитьНовуюСтроку(НоваяСтрока)
	
	НоваяСтрока.ФиксСтрока = Истина;
	
	Если УчитываемыеСотрудники.Количество() = 1 И ЕдинственныйСотрудник Тогда
		НоваяСтрока.ФизическоеЛицо = УчитываемыеСотрудники[0];
	КонецЕсли; 
	
	Если ЗначениеЗаполнено(НоваяСтрока.ФизическоеЛицо) Тогда
		
		ОписаниеПанелейВычетов = ОписаниеПанелиВычетыНаКлиенте();
		ДанныеНДФЛ = ОбщегоНазначенияКлиентСервер.ПолучитьРеквизитФормыПоПути(ЭтаФорма, ОписаниеПанелейВычетов.ТабличнаяЧастьНДФЛ.ПутьКДаннымНДФЛ);
		СтрокиНДФЛ = ДанныеНДФЛ.НайтиСтроки(Новый Структура("ФизическоеЛицо", НоваяСтрока.ФизическоеЛицо));
		Если СтрокиНДФЛ.Количество() > 0 Тогда
			Для каждого СтрокаНДФЛ Из СтрокиНДФЛ Цикл
				Если ЗначениеЗаполнено(СтрокаНДФЛ.Подразделение) Тогда
					НоваяСтрока.Подразделение = СтрокаНДФЛ.Подразделение;
					Прервать;
				КонецЕсли; 
			КонецЦикла;
		КонецЕсли; 
		
		Если НЕ ЗначениеЗаполнено(НоваяСтрока.Подразделение) Тогда
			НоваяСтрока.Подразделение = ПодходящееПодразделение(НоваяСтрока.ФизическоеЛицо,
				ЭтаФорма["Объект"].Организация,
				ЭтаФорма["Объект"][ЭтаФорма.ОписаниеДокумента.МесяцНачисленияИмя]);
		КонецЕсли; 
		
	КонецЕсли; 
	
	НоваяСтрока.МесяцНалоговогоПериода = ЭтаФорма["Объект"][ЭтаФорма.ОписаниеДокумента.МесяцНачисленияИмя];
	Если ЗначениеЗаполнено(НоваяСтрока.МесяцНалоговогоПериода) Тогда
		ЗарплатаКадрыКлиентСервер.ЗаполнитьМесяцПоДате(
			НоваяСтрока,
			"МесяцНалоговогоПериода",
			"МесяцНалоговогоПериодаСтрокой");
	КонецЕсли; 

	ОписаниеПанелиВычеты = ОписаниеПанелиВычетыНаКлиенте();
	
	ЭтаФорма[ОписаниеПанелиВычеты.ИмяГруппыФормыПанелиВычеты + "МаксимальныйИдентификаторСтрокиНДФЛ"] =
		ЭтаФорма[ОписаниеПанелиВычеты.ИмяГруппыФормыПанелиВычеты + "МаксимальныйИдентификаторСтрокиНДФЛ"] + 1;
		
	ИдентификаторСтроки = ЭтаФорма[ОписаниеПанелиВычеты.ИмяГруппыФормыПанелиВычеты + "МаксимальныйИдентификаторСтрокиНДФЛ"];
	
	НоваяСтрока.ИдентификаторСтрокиНДФЛ = ИдентификаторСтроки;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПодходящееПодразделение(ФизическоеЛицо, Организация, МесяцНачисления)
	
	Подразделение = Неопределено;
	Если ЗначениеЗаполнено(ФизическоеЛицо) Тогда
		
		ПараметрыПолученияСотрудников = КадровыйУчет.ПараметрыПолученияСотрудниковОрганизацийПоСпискуФизическихЛиц();
		ПараметрыПолученияСотрудников.Организация = Организация;
		ПараметрыПолученияСотрудников.НачалоПериода = МесяцНачисления;
		ПараметрыПолученияСотрудников.ОкончаниеПериода = КонецМесяца(МесяцНачисления);
		ПараметрыПолученияСотрудников.СписокФизическихЛиц = ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(ФизическоеЛицо);
		ПараметрыПолученияСотрудников.КадровыеДанные = "Подразделение";
		
		СотрудникиОрганизации = КадровыйУчет.СотрудникиОрганизации(Истина, ПараметрыПолученияСотрудников);
		Если СотрудникиОрганизации.Количество() > 0 Тогда
			Подразделение = СотрудникиОрганизации[0].Подразделение;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Подразделение;
	
КонецФункции

&НаКлиенте
Процедура РассчитатьСотрудника(Сотрудник, ОписаниеТаблицы) Экспорт
	
	ВладелецФормы.РассчитатьСотрудника(Сотрудник, ОписаниеТаблицы);
	
КонецПроцедуры

#КонецОбласти

СтарыеЗначенияКонтролируемыхПолей = Новый Соответствие;
