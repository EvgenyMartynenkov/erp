﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	Одобрено  = Параметры.Одобрено;
	Заявление = Параметры.Заявление;
	Организация = Параметры.Организация;
	Комментарий = Параметры.Комментарий;
	
	Если Одобрено Тогда
		Если Параметры.ЭтоВторичноеЗаявление Тогда
			ТекстОповещения = НСтр("ru = 'Ваше заявление на изменение реквизитов подключения организации ""%1"" к 1С-Отчетности одобрено.'");
			
			Комментарий = НСтр("ru = 'Хотите завершить изменение реквизитов сейчас?'");
			
			Элементы.КнопкаНачатьИспользование.Заголовок = НСтр("ru = 'Завершить изменение реквизитов сейчас'");
			Элементы.КнопкаНапомнитьПозже.Заголовок = НСтр("ru = 'Напомнить позже'");
		Иначе
			ТекстОповещения = НСтр("ru = 'Ваше заявление на подключение организации ""%1"" к 1С-Отчетности одобрено.'");
			
			Комментарий = НСтр("ru = 'Хотите завершить подключение сейчас?'");
			
			Элементы.КнопкаНачатьИспользование.Заголовок = НСтр("ru = 'Завершить подключение сейчас'");
			Элементы.КнопкаНапомнитьПозже.Заголовок = НСтр("ru = 'Напомнить позже'");
		КонецЕсли;
	Иначе
		Если Параметры.ЭтоВторичноеЗаявление Тогда
			ТекстОповещения = НСтр("ru = 'Ваше заявление на изменение реквизитов подключения организации ""%1"" к 1С-Отчетности отклонено.
                                    |'");
			
			Элементы.КнопкаНачатьИспользование.Заголовок = НСтр("ru = 'Подготовить новое заявление'");
			Элементы.КнопкаНапомнитьПозже.Заголовок = НСтр("ru = 'Закрыть'");
		Иначе
			ТекстОповещения = НСтр("ru = 'Ваше заявление на подключение организации ""%1"" к 1С-Отчетности отклонено.
                                    |'");
			
			Элементы.КнопкаНачатьИспользование.Заголовок = НСтр("ru = 'Подготовить новое заявление'");
			Элементы.КнопкаНапомнитьПозже.Заголовок = НСтр("ru = 'Закрыть'");
		КонецЕсли;
	КонецЕсли;
	
	ТекстОповещения = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(ТекстОповещения, Организация, Комментарий);
	
	Элементы.ТекстОповещения.Заголовок = ТекстОповещения;
	Элементы.ТекстПричины.Заголовок = Комментарий;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	КомандаНапомнитьПозже(Неопределено);
	
КонецПроцедуры


////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ШАПКИ ФОРМЫ


////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура КомандаНачатьИспользование(Команда)

	ОписаниеОповещения = Новый ОписаниеОповещения("КомандаНачатьИспользованиеЗавершение", ЭтотОбъект);
	ДокументооборотСКОКлиент.ПолучитьКонтекстЭДО(ОписаниеОповещения);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаНачатьИспользованиеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	
	КонтекстЭДОКлиент = Результат.КонтекстЭДО;
	
	ИдентификаторАбонента = "";
		
	Если Одобрено Тогда
		РеквизитыДокумента = КонтекстЭДОКлиент.ПолучитьСтруктуруРеквизитовДокумента(Заявление);
		РеквизитыДляЗаписи = Неопределено;		
		КонтекстЭДОКлиент.РегистрацияПолучитьОтветНаЗаявлениеСервер(Заявление, ИдентификаторАбонента, РеквизитыДляЗаписи, Истина);		
		ИдентификаторАбонента = ВРег(ИдентификаторАбонента);
	КонецЕсли;
	
	СтруктураПараметров = Новый Структура();
	СтруктураПараметров.Вставить("Ключ", Заявление);
	СтруктураПараметров.Вставить("ЭтоЭлектроннаяПодписьВМоделиСервиса", Истина);
	СтруктураПараметров.Вставить("ИдентификаторАбонента", ИдентификаторАбонента);
	
	Если Одобрено Тогда
		СтруктураПараметров.Вставить("НовыйСтатусДокумента", ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Одобрено"));
	Иначе
		СтруктураПараметров.Вставить("НовыйСтатусДокумента", ПредопределенноеЗначение("Перечисление.СтатусыЗаявленияАбонентаСпецоператораСвязи.Отклонено"));
	КонецЕсли;
		
	Если ЗначениеЗаполнено(ИдентификаторАбонента) Тогда
		Результат = ОткрытьФорму(
			КонтекстЭДОКлиент.ПутьКОбъекту + ".Форма.МастерФормированияЗаявкиНаПодключениеНастройкаПрограммы", СтруктураПараметров);
			
		ПодключитьОбработчикОжидания("Подключаемый_ЗакрытьФорму", 0.2, Истина);			
	КонецЕсли;
	
	Если Не Одобрено Тогда
		// Если статус заявления - "Отклонено", тогда создаем новое заявление
		ПараметрыФормы = Новый Структура("ЗначениеКопирования", Заявление);
		ОткрытьФорму("Документ.ЗаявлениеАбонентаСпецоператораСвязи.ФормаОбъекта", ПараметрыФормы,, Истина);

		ПометитьНаУдалениеЗаявление(Заявление);
		
		ПодключитьОбработчикОжидания("Подключаемый_ЗакрытьФорму", 0.2, Истина);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаНапомнитьПозже(Команда)
	
	Если Одобрено Тогда
		ЭлектроннаяПодписьВМоделиСервисаКлиент.ОтключитьОбработчикПроверкиЗаявлений();
	Иначе
		ПометитьНаУдалениеЗаявление(Заявление);
	КонецЕсли;
	
	Если Открыта() Тогда
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

&НаСервереБезКонтекста
Процедура ПометитьНаУдалениеЗаявление(Заявление)
	
	Попытка
		ЗаявлениеОбъект = Заявление.ПолучитьОбъект();
		ЗаявлениеОбъект.УстановитьПометкуУдаления(Истина);
		ЗаявлениеОбъект.Записать();
	Исключение
		ЗаписьЖурналаРегистрации(
			НСтр("ru = 'Электронная подпись в модели сервиса.Обработка заявлений'"),
			УровеньЖурналаРегистрации.Ошибка, 
			Метаданные.Документы.ЗаявлениеАбонентаСпецоператораСвязи,
			Заявление, 
			ПодробноеПредставлениеОшибки(ИнформацияОбОшибке()));
	КонецПопытки;
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ЗакрытьФорму()
	
	Если Открыта() Тогда
		Закрыть();
	КонецЕсли;
	
КонецПроцедуры